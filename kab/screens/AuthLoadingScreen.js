import React from 'react';
import {
    ActivityIndicator,
    AsyncStorage,
    StatusBar,
    StyleSheet,
    View,
    Image,
} from 'react-native';

class AuthLoadingScreen extends React.Component {
    constructor(props) {
        super(props);
        this._bootstrapAsync();
    }

    // Fetch the token from storage then navigate to our appropriate place
    _bootstrapAsync = async () => {

        const userToken = await AsyncStorage.getItem('IsLogin');

        // This will switch to the App screen or Auth screen and this loading
        // screen will be unmounted and thrown away.
        
        this.props.navigation.navigate(userToken === 'true' ? 'App' : 'Auth');
    };

    render() {

        return (
            <View style={styles.container}>
                <Image style={{width: 100, height: 100, }} 
                    source={require('../assets/images/logo_qaccess.png')}/>
                <StatusBar barStyle="default" />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#fff',
    },
  });

export default AuthLoadingScreen;