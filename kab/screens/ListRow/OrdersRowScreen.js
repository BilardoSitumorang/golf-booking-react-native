import React from 'react';
import { 
    StyleSheet, 
    Image,
    View,
    Text,
} from 'react-native';
import Colors from '../../constants/Colors';
import Strings from '../../constants/Strings';
import Enums, { TransactionTypeIntEnum, StatusIntEnum, PaymentIntEnum, StatusStringEnum, } from '../../constants/Enums';
import { getDoubleToStringDate,  } from '../../API/SettingService';
import moment from 'moment';
import { Divider } from 'react-native-elements';
import Utils from '../Utils';

export default class OrdersRowScreen extends React.Component {
    
    static navigationOptions = {
        title: '',
        header: null,
    };

    constructor(props){
        super(props);

        this.state = {
            row: this.props.row,
            bookingcode: this.props.code,
            bookingdate: '',
            teetime: this.props.teetime,
            status: this.props.status,
            payment: this.props.payment,
            statusstr: '',
            statuscolor: '#414141',
            course: this.props.course,
            pulsa: this.props.pulsa,

            locationvisible: true,
            iconvisible: this.props.type == TransactionTypeIntEnum.Reservation ? true : false,
            teetimevisible: this.props.type == TransactionTypeIntEnum.Reservation ? true : false ,
        }
    }

    componentDidMount() {
        getDoubleToStringDate(this.state.row.Tanggal).then((jsonDate) =>
        {
            if(this.props.type == TransactionTypeIntEnum.MobilePulsaPhonePrepaid){
                var arrMobile = this.props.row.MobilePulsaProduct != "" ? this.props.row.MobilePulsaProduct.split(',') : '';
                this.setState({ 
                    course: this.props.row.MobilePulsaProduct != "" ? arrMobile[1].split(':')[1] + " " + arrMobile[2].split(':')[1] : this.props.course,
                });
            }
            else{
                this.setState({ 
                    course: this.props.course,
                });
            }

            this.setState({ 
                bookingdate : moment(Date.parse(jsonDate.result)).format('DD MMM YYYY'),
            });

            var date1 = moment(Date.parse(jsonDate.result)).format('YYYY MM DD');
            getDoubleToStringDate(this.state.row.TanggalBuat).then(async (jsonDate2) =>
            {
                getDoubleToStringDate(this.state.row.EventDate).then(async (jsonDate3) =>
                {
                    var date2 = moment(Date.parse(jsonDate2.result)).format('YYYY MM DD');
                    var totalminuteremain = Utils.getTotalMinuteRemaining(Utils.getCountDays(Date.parse(jsonDate.result),
                        Date.parse(jsonDate2.result)), this.state.row.Type, this.state.row.Status);
                    var timeremain= Utils.getTotalMinutes(Date.now(), Utils.getLocalTimezone(Date.parse(jsonDate2.result)));

                    if (this.state.row.Type == TransactionTypeIntEnum.Event){
                        var timeEvent = this.state.row.TimeStart.split(' ');
                        var hour = Number.parseInt(timeEvent[0].split(':')[0]);
                        var minute = Number.parseInt(timeEvent[0].split(':')[1]);
                        if(timeEvent[1] == "PM"){
                            hour = Number.parseInt(hour) + 12;
                        }
                        
                        var date3 = moment(Date.parse(jsonDate3.result)).format('YYYY MM DD');
                        var currentOffset = (new Date()).getTimezoneOffset() / 60;
                        var dateevent = date3.split(' ');
                        var newdate = moment(new Date(dateevent[0], dateevent[1] - 1, dateevent[2], hour, minute)).valueOf();
                        totalminuteremain = Utils.getTotalMinuteRemaining(Utils.getCountDays(newdate, 
                            Date.parse(jsonDate2.result)), this.state.row.Type, this.state.row.Status);
                        
                    }
                    
                    if (this.state.status == StatusIntEnum.Book)
                    {
                        if (timeremain > totalminuteremain)
                        {
                            this.setState({
                                statusstr: 'Time Out',
                                statuscolor: '#b84242',
                            });
                        }
                        else if (this.state.payment == PaymentIntEnum.NotSpecified)
                        {
                            this.setState({
                                statusstr: 'Waiting for payment method',
                                statuscolor: '#808080',
                            });
                        }
                        else if (this.state.payment == PaymentIntEnum.BankTransfer)
                        {
                            this.setState({
                                statusstr: 'Waiting for payment method',
                                statuscolor: '#808080',
                            });
                        }
                        else if (this.state.payment == PaymentIntEnum.CreditCard)
                        {
                            this.setState({
                                statusstr: 'Waiting for payment method',
                                statuscolor: '#808080',
                            });
                        }
                    }
                    else if (this.state.status == StatusIntEnum.Pending && timeremain > totalminuteremain)
                    {
                        this.setState({
                            statusstr: 'Time Out',
                            statuscolor: '#b84242',
                        });
                    }
                    else if (this.state.row.Status == StatusIntEnum.Paid)
                    {
                        this.setState({
                            statusstr: StatusStringEnum.Paid,
                            statuscolor: '#93b843',
                        });
                    }
                    else if (this.state.status == StatusIntEnum.Pending)
                    {
                        this.setState({
                            statusstr: StatusStringEnum.Pending,
                            statuscolor: '#808080',
                        });
                    }
                    else {
                        this.setState({
                            statusstr: Enums.StatusEnum2(this.state.row.Status),
                            statuscolor: '#b84242',
                        });
                    }
                })
            })
        })
        .catch((error) => {
            console.error(error);
        });
    }

    render() {
        return (
            <View style={styles.container} >
                <View style={{flex: 1, flexDirection: 'column', marginLeft: 20, marginRight: 20, backgroundColor: '#f2f2f2'}}>
                    <View style={{flex: 1, flexDirection: 'row', marginTop: 5, marginBottom: 5,}}>
                        <Text style={styles.textLeft}>{"#" + this.state.bookingcode}</Text>
                        <Text style={styles.textRight}>{this.state.bookingdate}</Text>
                    </View>
                    <View style={{flexDirection: 'row', flex: 1, marginBottom: 5, }}>
                        {
                            Utils.renderIf(this.state.iconvisible,
                                <Image style={{height: 20, width: 20, tintColor: '#93b843'}} 
                                    source={require('../../assets/images/location.png')}/>
                            )
                        }
                        <Text style={{fontSize: 17, color: '#93b843'}}>{this.state.course}</Text>
                    </View>
                    {
                        Utils.renderIf(this.state.teetimevisible,
                            <View style={{flex: 1, flexDirection: 'row', marginBottom: 5,}}>
                                <Text style={styles.textLeft2}>{'Tee Time'}</Text>
                                <Text style={styles.textRight}>{this.state.teetime + "(GMT +7)"}</Text>
                            </View>
                        )
                    }
                    <Divider style={{ backgroundColor: '#999', height: 1, width: '100%', marginBottom: 5, }} />
                    <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginBottom: 5, }}>
                        <Text style={{fontSize: 17, color: this.state.statuscolor, fontWeight: 'bold', }}>{this.state.statusstr}</Text>
                    </View>
                </View>
            </View>       
        );
    }
}

const styles = StyleSheet.create({
    
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#f2f2f2',
    },

    textType: {
        fontSize: 14,
        marginTop: 5,
        color: '#edb636',
        width: 10,
    },

    textLeft: {
        fontSize: 15,
        marginTop: 5,
        color: Colors.text,
        flex: 2,
    },

    textLeft2: {
        fontSize: 15,
        marginTop: 5,
        color: '#939393',
        flex: .5,
    },

    textRight: {
        flex: 2,
        fontSize: 15,
        marginTop: 5,
        color: Colors.text,
    },
});