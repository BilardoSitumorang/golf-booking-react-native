import React from 'react';
import {
    ListView,
} from 'react-native';
import Moment from 'moment';
import clsSetting from '../API/clsSetting';
import LoginModel from '../Model/LoginModel';
import Utils from '../screens/Utils';

exports.postLogin = (_username, _password) => {
    return fetch(clsSetting.APIUrl + "account/login", {  
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json; charset=utf-8',
        },
        body: JSON.stringify({
            username: _username,
            password: _password,
            apisecretkey: clsSetting.ApiSecretKey,
        })
    })
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        // if (json.status !== 400 && json.status === 200 ) {
        //     console.log('json Login ', json.result);
            
        // } else {
        //     let error = new Error(json.message);
        //     Utils.errAlert("Error Message", json.message);
        //     throw error;
        // }
        console.log('json Login ', json);
        return json; 
    })
    .catch(e => {
        console.log('Error Login ', e);
    });  
}

exports.postRegister = (_username, _password, _confirmpass, _referral) => {
    return fetch(clsSetting.APIUrl + "account/register", {  
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json; charset=utf-8',
        },
        body: JSON.stringify({
            username: _username,
            password: _password,
            confirmpassword: _confirmpass,
            referral: _referral,
            ApiSecretKey: clsSetting.ApiSecretKey,
        })
    })
    .then(response => response.json())
    .then(function(json) {
        return json;
    })
    .catch(e => {
        console.log('Error Register ', e);
    });
    
}

exports.postForgotPassword = (_email) => {
    return fetch(clsSetting.APIUrl + "account/forgotPassword", {  
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json; charset=utf-8',
        },
        body: JSON.stringify({
            email: _email,
            apisecretkey: clsSetting.ApiSecretKey,
        })
    })
    .then(response => response.json())
    .then(function(json) {
        console.log('json Forgot Password ', json);
        if (json.status === 200) {
            return true; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => {
        console.log('Error Forgot Password ', e);
    });
    
}

exports.postRegisterPhone = (_idd, _phone, _email) => {
    return fetch(clsSetting.APIUrl + "account/phone", {  
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json; charset=utf-8',
        },
        body: JSON.stringify({
            idd: _idd,
            phonenumber: _phone,
            email: _email,
            apisecretkey: clsSetting.ApiSecretKey,
        })
    })
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        console.log('data Register Phone ', json);
        return json; 
    })
    .catch(e => {
        console.log('Error Register Phone ', e);
    });
}

exports.postRegisterPhoneVerification = (_otp, _email) => {
    return fetch(clsSetting.APIUrl + "account/phoneverification", {  
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json; charset=utf-8',
        },
        body: JSON.stringify({
            otp: _otp,
            email: _email,
            apisecretkey: clsSetting.ApiSecretKey,
        })
    })
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        console.log('data Register Phone Verification ', json);
        return json;
    })
    .catch(e => {
        console.log('Error Register Phone Verification ', e);
    });
}

exports.postRegisterLinkQ = (_member, _birthday, _email) => {
    return fetch(clsSetting.APIUrl + "account/linkmember", {  
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json; charset=utf-8',
        },
        body: JSON.stringify({
            member: _member,
            birthdate:  (new Date(_birthday)).getTime(),
            email: _email,
            apisecretkey: clsSetting.ApiSecretKey,
        })
    })
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        console.log('data Register LinkQ ', json.result);
        if (json.status === 200) {
            return true; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => {
        console.log('Error Register LinkQ ', e);
    });
}

exports.getProfile = (_email) => {
    return fetch(clsSetting.APIUrl + "player/GetProfile?email=" + _email + "&ApiSecretKey=" + clsSetting.ApiSecretKey)
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        if (json.status !== 400 && json.status === 200) {
            console.log('data Get Profile ', json.result);
            return json.result; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error Get Profile ', e));
}

exports.getBalance = (_email) => {
    return fetch(clsSetting.APIUrl + "player/GetBalance?email=" + _email + "&ApiSecretKey=" + clsSetting.ApiSecretKey)
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        if (json.status !== 400 && json.status === 200) {
            console.log('data Balance ', json);
            return json.result; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error Balance ', e));
}

exports.checkPlayerType = (_accno) => {
    return fetch(clsSetting.APIUrl + "player/gettypeplayerevent?accountnumber=" + _accno + "&ApiSecretKey=" + clsSetting.ApiSecretKey)
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        if (json.status !== 400 && json.status === 200) {
            console.log('data Check Player ', json);
            return json.result; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error Check Player ', e));
}

exports.checkPlayerTypeReservation = (_keyword, _courseid, _date, _session) => {
    return fetch(clsSetting.APIUrl + "player/gettypeplayer?courseid=" + _courseid + "&date=" + _date + "&session=" + _session 
        + "&keyword=" + _keyword + "&playernumbers=&ApiSecretKey=" + clsSetting.ApiSecretKey)
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        return json;
    })
    .catch(e => console.log('Error Check Reservation ', e));
}

exports.getVirtualAccount = (_email) => {
    return fetch(clsSetting.APIUrl + "player/GetVirtualAccount?email=" + _email + "&ApiSecretKey=" + clsSetting.ApiSecretKey)
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        if (json.status === 200) {
            return json.result; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error Virtual Card ', e));
}

exports.getReferralCode = (_email) => {
    return fetch(clsSetting.APIUrl + "player/GetLinkRefferal?email=" + _email + "&ApiSecretKey=" + clsSetting.ApiSecretKey)
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        console.log('data Referral Code ', json);
        if (json.status !== 400 && json.status === 200) {
            console.log('data Referral Code ', json.result);
            return json.result; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => {
        console.log('Error Referral Code ', e);
    });
}

exports.getPromoBanner = () => {
    return fetch(clsSetting.APIUrl + "promobanner/GetAvailablePromotionalBanners?ApiSecretKey=" + clsSetting.ApiSecretKey)
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        if (json.status !== 400 && json.status === 200) {
            console.log('data Promo Banner ', json);
            return json.result; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error Promo Banner ', e));
}

exports.findPromoBanner = (_activty) => {
    return fetch(clsSetting.APIUrl + "promobanner/FindAvailablePromotionalBanner?activity=" + _activty + "&ApiSecretKey=" + clsSetting.ApiSecretKey)
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        if (json.status !== 400 && json.status === 200) {
            console.log('data Find Promo Banner ', json);
            return json.result; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error Find Promo Banner ', e));
}

exports.postLastAccess = (_username, _device, _os, _version) => {
    return fetch(clsSetting.APIUrl + "AccessHistory/CreateAccessHistory", {  
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json; charset=utf-8',
        },
        body: JSON.stringify({
            username: _username,
            device:  _device,
            os: _os,
            version: _version,
            apisecretkey: clsSetting.ApiSecretKey,
        })
    })
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        if (json.status !== 400 && json.status === 200) {
            console.log('data Last Access ', json);
            return json.result; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error Last Access ', e));
}

exports.getVersionAndroid = () => {
    return fetch(clsSetting.APIUrl + "MobileVersion/GetMobileVersion?tipeMobile=1&apiSecretKey=" + clsSetting.ApiSecretKey)
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        return json; 
    })
    .catch(e => console.log('Error Version Android ', e));
}

exports.getVersion = (_os) => {
    return fetch(clsSetting.APIUrl + "MobileVersion/GetMobileVersion?tipeMobile=" + _os + "&apiSecretKey=" + clsSetting.ApiSecretKey)
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        return json; 
    })
    .catch(e => console.log('Error Version ', e));
}

exports.findMemberCard = (_id) => {
    return fetch(clsSetting.APIUrl + "pengaturan/FindMemberCard?id=" + _id + "&apiSecretKey=" + clsSetting.ApiSecretKey)
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        if (json.status !== 400 && json.status === 200) {
            console.log('data Find Member Card ', json);
            return json.result; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error Find Member Card ', e));
}

exports.getMemberCards = () => {
    return fetch(clsSetting.APIUrl + "pengaturan/GetMemberCards?ApiSecretKey=" + clsSetting.ApiSecretKey)
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        if (json.status !== 400 && json.status === 200) {
            console.log('data Member Card ', json);
            return json.result; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error Member Card ', e));
}

exports.getIdd = () => {
    return fetch(clsSetting.APIUrl + "pengaturan/getidds?ApiSecretKey=" + clsSetting.ApiSecretKey)
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        console.log('data Idd ', json);
        if (json.status !== 400 && json.status === 200) {
            
            return json; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error Idd ', e));
}

exports.getReferralData = (_email) => {
    return fetch(clsSetting.APIUrl + "player/GetReferralInformation?email=" + _email + "&ApiSecretKey=" + clsSetting.ApiSecretKey)
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        if (json.status !== 400 && json.status === 200) {
            console.log('data Referral Data ', json);
            return json.result; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error Referral Data ', e));
}

exports.getMainData = (_email) => {
    return fetch(clsSetting.APIUrl + "custommobile/getmaincontents?email=" + _email + "&ApiSecretKey=" + clsSetting.ApiSecretKey)
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        console.log('json Main Data ', json);
        if (json.status !== 400 && json.status === 200) {
            return json; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error Main Data ', e));
}

exports.postAddMember = (_member) => {
    return fetch(clsSetting.APIUrl + "account/RegisterQ", {  
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json; charset=utf-8',
        },
        body: JSON.stringify(_member)
    })
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        console.log('api', JSON.stringify(_member));
        if (json.status === 200) {
            console.log('json Add Member ', json);
            return json.result; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error Add Member ', e));  
}

exports.postUpdateProfile = (_profilemodel) => {
    return fetch(clsSetting.APIUrl + "player/UpdateProfile", {  
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json; charset=utf-8',
        },
        body: JSON.stringify(_profilemodel)
    })
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        console.log('api', JSON.stringify(_profilemodel));
        console.log(json);
        return json;
    })
    .catch(e => console.log('Error Update Profile ', e));  
}

//Upload File Belum