import React from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    AsyncStorage,
    Platform,
    FlatList,
    ListView,
    View,
    ScrollView,
    ActivityIndicator,
    SafeAreaView,
    RefreshControl,
} from 'react-native';
import OrdersRowScreen from './ListRow/OrdersRowScreen';
import { getBookingActive } from '../API/TransactionService';
import { getDoubleToStringDate,  } from '../API/SettingService';
import { getOptions,  } from '../API/OptionService';
import { TransactionTypeIntEnum, StatusIntEnum, PaymentIntEnum, TipePemainIntEnum, OptionIntEnum, } from '../constants/Enums';
import Utils from './Utils';
import moment from 'moment';
import Colors from '../constants/Colors';
import { Button } from 'react-native-elements';

export default class OrdersActiveScreen extends React.Component {
    static navigationOptions = {
        title: '',
        header: null
    };

    constructor(props){
        super(props);

        this.state = {
            isLoading: true,
            refreshing: false,
            username: '',
        }
    }

    async componentDidMount() {
        this.setState({ username : await AsyncStorage.getItem('Username')});
        var Enumerable = require('./linq');
        getOptions().then((jsonOption) => 
        {
            this.setState({
                maintenanceOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionMaintenancePeriod).firstOrDefault(),
                reservationOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionReservation).firstOrDefault(),
                registrationOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionRegistration).firstOrDefault(),
                eventOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionEvent).firstOrDefault(),
                pulsaOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionMobilePulsa).firstOrDefault(),
            });

            getBookingActive(this.state.username, this.props.type).then((json) =>
            {
                let ds= new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
                this.setState({
                    isLoading: false,
                    dataSource: ds.cloneWithRows(json),
                });

            })
        })
        .catch((error) => {
            console.error(error);
            this.setState({isLoading: false,});
        });
    }

    async GetItem (row) {
        //Selected Row List
        
        if (row.Status == StatusIntEnum.Paid && row.Type == TransactionTypeIntEnum.MobilePulsaPhonePrepaid)
        { //Pulsa
            await AsyncStorage.setItem("BookingCode", row.Kode);
            this.props.nav.navigate('TicketPulsa');
        }
        else if (row.Status == StatusIntEnum.Paid && row.Type == TransactionTypeIntEnum.Event)
        {
            await AsyncStorage.setItem("BookingCode", row.Kode);
            this.props.nav.navigate('TicketEvent');
        }
        else if (row.Status == StatusIntEnum.Paid && row.Type == TransactionTypeIntEnum.RegistrationQ)
        {
            await AsyncStorage.setItem("BookingCode", row.Kode);
            this.props.nav.navigate('TicketRegistration');
        }
        else if (row.Status == StatusIntEnum.Paid || row.Status == StatusIntEnum.Verifying)
        {
            await AsyncStorage.setItem("BookingCode", row.Kode);
            this.props.nav.navigate('TicketReservation');
        }
        else if (row.Type == TransactionTypeIntEnum.MobilePulsaPhonePrepaid && row.Status == StatusIntEnum.Pending)
        {
            await AsyncStorage.setItem("BookingCode", row.Kode);
            await AsyncStorage.setItem("TransType", this.props.type + "");
            this.props.nav.navigate('PendingBank');
        }
        else if (row.Type == TransactionTypeIntEnum.RegistrationQ && row.Status == StatusIntEnum.Pending)
        {
            await AsyncStorage.setItem("BookingCode", row.Kode);
            await AsyncStorage.setItem("TransType", this.props.type + "");
            this.props.nav.navigate('PendingBank');
        }
        else if (row.Type == TransactionTypeIntEnum.Event && row.Status == StatusIntEnum.Pending)
        {
            await AsyncStorage.setItem("BookingCode", row.Kode);
            await AsyncStorage.setItem("TransType", this.props.type + "");
            this.props.nav.navigate('PendingBank');
        }
        else if (row.Status == StatusIntEnum.Book || row.Status == StatusIntEnum.TimeOut || row.Payment == PaymentIntEnum.BankTransfer || row.Status == StatusIntEnum.Pending)
        {
            var Enumerable = require('./linq');
            //var total = Utils.getTotalBooking(row.PemainViewModel);
            var containMandiri = false;
            var countClub = Enumerable.from(row.PemainViewModel).count(x => x.TipeMember == TipePemainIntEnum.Club);
            if (countClub > 0)
            {
                containMandiri = true;
            }

            getDoubleToStringDate(row.Tanggal).then((jsonDate) =>
            {
                var date1 = moment(Date.parse(jsonDate.result)).format('YYYY MM DD');
                getDoubleToStringDate(row.TanggalBuat).then(async (jsonDate2) =>
                {
                    getDoubleToStringDate(row.EventDate).then(async (jsonDate3) =>
                    {
                        var totalminuteremain = Utils.getTotalMinuteRemaining(Utils.getCountDays(Date.parse(jsonDate.result),
                            Date.parse(jsonDate2.result)), row.Type, row.Status);
                        var timeremain= Utils.getTotalMinutes(Date.now(), Utils.getLocalTimezone(Date.parse(jsonDate2.result)));
                        if (row.Type == TransactionTypeIntEnum.Event){
                            var timeEvent = row.TimeStart.split(' ');
                            var hour = Number.parseInt(timeEvent[0].split(':')[0]);
                            var minute = Number.parseInt(timeEvent[0].split(':')[1]);
                            if(timeEvent[1] == "PM"){
                                hour = Number.parseInt(hour) + 12;
                            }
                            
                            var date3 = moment(Date.parse(jsonDate3.result)).format('YYYY MM DD');
                            var currentOffset = (new Date()).getTimezoneOffset() / 60;
                            var dateevent = date3.split(' ');
                            var newdate = moment(new Date(dateevent[0], dateevent[1] - 1, dateevent[2], hour, minute)).valueOf();
                            totalminuteremain = Utils.getTotalMinuteRemaining(Utils.getCountDays(newdate, 
                                Date.parse(jsonDate2.result)), row.Type, row.Status);
                            
                        }

                        if (row.Status == StatusIntEnum.TimeOut || (timeremain > totalminuteremain && row.Status == StatusIntEnum.Book) || (timeremain > totalminuteremain && row.Status == StatusIntEnum.Pending))
                        {
                            this.props.nav.navigate('CustomerCall');
                        }
                        else if (row.Type == TransactionTypeIntEnum.RegistrationQ && row.Status == StatusIntEnum.Book)
                        {
                            var platformRegister = Platform.OS === 'ios' ? this.state.registrationOption.iOS : this.state.registrationOption.Android;
                            if(Utils.getMaintenanceOption(this.state.maintenanceOption)){
                                Utils.alertBackToMain(this.state.maintenanceOption.Note);
                            }
                            else if(!platformRegister) {
                                Utils.errAlert("QAccess Alert", this.state.registrationOption.Note);
                            }
                            else
                            {
                                await AsyncStorage.setItem("BookingCode", row.Kode);
                                this.props.nav.navigate('Payment', {TransType: this.props.type});
                            }
                        }
                        else if (row.Type == TransactionTypeIntEnum.MobilePulsaPhonePrepaid && row.Status == StatusIntEnum.Book)
                        {
                            var platformPulsa = Platform.OS === 'ios' ? this.state.pulsaOption.iOS : this.state.pulsaOption.Android;
                            if(Utils.getMaintenanceOption(this.state.maintenanceOption)){
                                Utils.alertBackToMain(this.state.maintenanceOption.Note);
                            }
                            else if(!platformPulsa) {
                                Utils.errAlert("QAccess Alert", this.state.pulsaOption.Note);
                            }
                            else
                            {
                                await AsyncStorage.setItem("BookingCode", row.Kode);
                                this.props.nav.navigate('Payment', {TransType: this.props.type});
                            }
                        }
                        else if (row.Type == TransactionTypeIntEnum.Event && row.Status == StatusIntEnum.Book)
                        {
                            var platformEvent = Platform.OS === 'ios' ? this.state.eventOption.iOS : this.state.eventOption.Android;
                            if(Utils.getMaintenanceOption(this.state.maintenanceOption)){
                                Utils.alertBackToMain(this.state.maintenanceOption.Note);
                            }
                            else if(!platformEvent) {
                                Utils.errAlert("QAccess Alert", this.state.eventOption.Note);
                            }
                            else
                            {
                                await AsyncStorage.setItem("BookingCode", row.Kode);
                                this.props.nav.navigate('Payment', {TransType: this.props.type});
                            }
                        }
                        else if (row.Status == StatusIntEnum.Pending)
                        {
                            await AsyncStorage.setItem("BookingCode", row.Kode);
                            await AsyncStorage.setItem("TransType", this.props.type + "");
                            this.props.nav.navigate('PendingBank');
                        }
                        else
                        {
                            var platformReservation = Platform.OS === 'ios' ? this.state.reservationOption.iOS : this.state.reservationOption.Android;
                            if(Utils.getMaintenanceOption(this.state.maintenanceOption)){
                                Utils.alertBackToMain(this.state.maintenanceOption.Note);
                            }
                            else if(!platformReservation) {
                                Utils.errAlert("QAccess Alert", this.state.reservationOption.Note);
                            }
                            else
                            {
                                await AsyncStorage.setItem("BookingCode", row.Kode);
                                this.props.nav.navigate('Payment', {TransType: this.props.type, ContainMandiri: containMandiri,});
                            }
                        }
                    })
                })
            })
        }
    }

    ListViewItemSeparator = () => {
        return (
            <View style={{ height: 1, width: "100%", backgroundColor: "#414141", }} />
        );
    }

    _refreshControl(){
        return (
            <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={() => this._refreshListView()} />
        )
    }

    _refreshListView(){

        this.setState({ refreshing: true })

        getBookingActive(this.state.username, this.props.type).then((json) =>
        {
            console.log(json);
            let ds= new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
            this.setState({ dataSource : ds.cloneWithRows([ ])})
            this.setState({ dataSource: ds.cloneWithRows(json), });
        })

        

        this.setState({refreshing:false})
    }

    render() {
        if (this.state.isLoading) {
            return (
                <View style={{flex: 1, paddingTop: 20}}>
                    <ActivityIndicator />
                </View>
            );
        }

        const gotoHistory = () =>  {
            this.props.nav.navigate('OrdersHistory', {type: this.props.type});
        }

        return (
            <SafeAreaView style={styles.safeAreaView}>
                <ScrollView 
                    refreshControl={this._refreshControl()}
                    style={styles.containerScroll}>
                    <View style={styles.container}>
                        <ListView
                            dataSource={this.state.dataSource}
                            renderSeparator= {this.ListViewItemSeparator}
                            renderRow={(rowData) =>
                                <TouchableOpacity 
                                    onPress={ this.GetItem.bind(this, rowData) } >
                                    <OrdersRowScreen 
                                        row={rowData}
                                        code={rowData.Kode}
                                        teetime={rowData.TeeTime}
                                        status={rowData.Status}
                                        payment={rowData.Payment}
                                        course={this.props.type === TransactionTypeIntEnum.Reservation ? rowData.NamaLapangan : 
                                            this.props.type === TransactionTypeIntEnum.RegistrationQ ? rowData.RegisteredQName === "" ? "-" : 
                                            rowData.RegisteredQName : rowData.EventName}
                                        pulsa={rowData.MobilePulsaProduct}
                                        type={this.props.type}/>
                                </TouchableOpacity>
                            }
                        />
                        <Button 
                            onPress={ gotoHistory }
                            buttonStyle={{backgroundColor: Colors.button, borderRadius: 5, marginTop: 20, marginBottom: 10, height: 50, }}
                            title={'History'}/>
                    </View>
                </ScrollView>
            </SafeAreaView>
        );
    }   
}

const styles = StyleSheet.create({
    safeAreaView: {
        flex: 1
    },

    containerScroll: {
        flex: 1,
        backgroundColor: '#f2f2f2',
    },

    container: {
        flex: 1,
        flexDirection: 'column',
    },
});
