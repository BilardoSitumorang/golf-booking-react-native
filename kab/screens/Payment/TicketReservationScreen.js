import React from 'react';
import {
    Image,
    ScrollView,
    StyleSheet,
    AsyncStorage,
    SafeAreaView,
    TouchableOpacity,
    ActivityIndicator,
    ListView,
    Text,
    Alert,
    View,
    BackHandler,
} from 'react-native';
import Colors from '../../constants/Colors';
import Strings from '../../constants/Strings';
import { Divider, Button, Icon, } from 'react-native-elements';
import { Card } from 'react-native-material-cards';
import Enums, { SessionIntEnum, TipePemainStringEnum, RefundStatusIntEnum, TipePemainIntEnum } from '../../constants/Enums';
import { findTrans, findRefundRequestByTrans, postRefund } from '../../API/TransactionService';
import { getDoubleToStringDate,  } from '../../API/SettingService';
import moment from 'moment';
import Utils from '../Utils';
import clsSetting from '../../API/clsSetting';
import TicketRowScreen from '../ListRow/TicketRowScreen';
import BookModel from '../../Model/BookModel';

export default class TicketReservationScreen extends React.Component {
    static navigationOptions = ({navigation}) => ({
        title: 'E-Ticket',
        headerTintColor: '#ffffff', 
        headerStyle: {
            backgroundColor: '#93b843'
        },
        headerLeft: (
            <Icon name="chevron-left"
                onPress={() => {
                    navigation.popToTop();
                }}
                size={35} color="white"/>
        ),
    });

    constructor(props){
        super(props);

        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);

        this.state = {
            isLoading: true,
            username: '',
            bookingcode: '',
            bookingdate: '',
            point: 0,
            balance: 0,
            total: 0,
            txtPoint: '',
            txtReedemPoint: 0,

            transdate: '',
            coursename: '',
            session: '',
            teetime: '',
            status: '',
            paymentmethod: '',
            bookmodel: [],

            publishrates: "0",
            visitorrates: '0',
            clubrates: "0",
            qrates: "0",

            publishvisible: true,
            visitorvisible: true,
            clubvisible: true,
            qvisible: true,

            statusrefund: '-',
            
            containmandiri: false,
            refundvisible: false,
            notevisible: false,

            json: null,
            bookingid: '',
        };
    }

    handleBackButtonClick() {
        this.props.navigation.popToTop();
        return true;
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount()
    {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    GetItem (row) {
        //Selected Row List
    }

    ListViewItemSeparator = () => {
        return (
            <View style={{ height: .5, width: "100%", backgroundColor: "#f2f2f2", }} />
        );
    }

    async componentDidMount() {
        this.setState({ bookingcode : await AsyncStorage.getItem('BookingCode')});
        this.setState({ username : await AsyncStorage.getItem('Username')});
        
        this.fn_reloadData(); 
    }

    fn_reloadData() {
        var Enumerable = require('../linq');

        findTrans(this.state.bookingcode).then((jsonTrans) =>
        {
            this.setState({
                coursename: jsonTrans.NamaLapangan,
                session: Enums.SessionEnum(jsonTrans.Session),
                teetime: jsonTrans.TeeTime + ' (GMT +7)',
                paymentmethod: Enums.PaymentEnum(jsonTrans.Payment),
                status: Enums.StatusEnum(jsonTrans.Status),
                json: jsonTrans,
                bookingid: jsonTrans.Id,
                total: 0,
            });
            var publish = Enumerable.from(jsonTrans.PemainViewModel).count(x => x.TipeMember == TipePemainIntEnum.Publish);
            var visitor = Enumerable.from(jsonTrans.PemainViewModel).count(x => x.TipeMember == TipePemainIntEnum.Visitor);
            var club = Enumerable.from(jsonTrans.PemainViewModel).count(x => x.TipeMember == TipePemainIntEnum.Club);
            var qrate = Enumerable.from(jsonTrans.PemainViewModel).count(x => x.TipeMember == TipePemainIntEnum.QMember);
            
            this.setState({ total: 0, });
            if(publish > 0)
            {
                var publishdata= Enumerable.from(jsonTrans.PemainViewModel).where(x => x.TipeMember == TipePemainIntEnum.Publish).firstOrDefault();
                var publishrate= publishdata.Harga;
                this.setState({
                    publishrates: publish + " x " + Utils.currencyfunc(Math.round(publishrate*100)/100),
                    total: this.state.total + (publish * publishrate),
                    publishvisible: true,
                });
            }
            else { this.setState({ publishvisible: false }); }

            if(visitor > 0)
            {
                var visitordata= Enumerable.from(jsonTrans.PemainViewModel).where(x => x.TipeMember == TipePemainIntEnum.Visitor).firstOrDefault();
                var visitorrate= visitordata.Harga;
                this.setState({
                    visitorrates: visitor + " x " + Utils.currencyfunc(Math.round(visitorrate*100)/100),
                    total: this.state.total + (visitor * visitorrate),
                    visitorvisible: true, 
                });
            }
            else { this.setState({ visitorvisible: false }); }

            if(club > 0)
            {
                var clubdata= Enumerable.from(jsonTrans.PemainViewModel).where(x => x.TipeMember == TipePemainIntEnum.Club).firstOrDefault();
                var clubrate= clubdata.Harga;
                this.setState({
                    clubrates: club + " x " + Utils.currencyfunc(Math.round(clubrate*100)/100),
                    total: this.state.total + (club * clubrate),
                    clubvisible: true, 
                    containmandiri: true,
                });
            }
            else { this.setState({ clubvisible: false }); }

            if(qrate > 0)
            {
                var qmemberdata= Enumerable.from(jsonTrans.PemainViewModel).where(x => x.TipeMember == TipePemainIntEnum.QMember).firstOrDefault();
                var qmemberrate= qmemberdata.Harga;
                this.setState({
                    qrates: qrate + " x " + Utils.currencyfunc(Math.round(qmemberrate*100)/100),
                    total: this.state.total + (qrate * qmemberrate),
                    qvisible: true, 
                });
            }
            else { this.setState({ qvisible: false }); }

            var i;
            this.setState({
                bookmodel: [],
            });
            for(i = 0; i < jsonTrans.PemainViewModel.length; i++) {
                this.state.bookmodel.push(new BookModel(jsonTrans.PemainViewModel[i].NoMember, '', jsonTrans.PemainViewModel[i].TipeMember, 
                    jsonTrans.PemainViewModel[i].JenisMember[i], jsonTrans.PemainViewModel[i].NamaPemain));
            }
            
            let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
            this.setState({
                dataSource: ds.cloneWithRows(this.state.bookmodel),
            });

            getDoubleToStringDate(jsonTrans.Tanggal).then((jsonDate) =>
            {
                this.setState({ transdate: moment(Date.parse(jsonDate.result)).format('DD/MM/YYYY'), });

                getDoubleToStringDate(jsonTrans.TanggalBuat).then((jsonDate2) =>
                {
                    this.setState({ bookingdate: moment(Date.parse(jsonDate2.result)).format('DD MMM YYYY'), });
                    findRefundRequestByTrans(jsonTrans.Id).then((jsonRefund) =>
                    {
                        var spanDeadline = new Date(jsonDate.result);
                        if(jsonTrans.Session == SessionIntEnum.Morning){
                            var dateDeadline = spanDeadline.getDate() - 1;
                            spanDeadline.setDate(dateDeadline);
                            var hoursDeadline = spanDeadline.getHours() + 12;
                            spanDeadline.setHours(hoursDeadline);
                        }
                        else if(jsonTrans.Session == SessionIntEnum.Afternoon){
                            var dateDeadline = spanDeadline.getDate() - 1;
                            spanDeadline.setDate(dateDeadline);
                            var hoursDeadline = spanDeadline.getHours() + 24;
                            spanDeadline.setHours(hoursDeadline);
                        }
                        if(this.state.containmandiri) 
                        {
                            this.setState({ 
                                statusrefund: 'Non-Refundable',
                                notevisible: false,
                                refundvisible: false,
                            });
                        }
                        else{
                            if(jsonRefund.result != null){
                                if(jsonRefund.result.Status == RefundStatusIntEnum.Process){
                                    this.setState({ 
                                        statusrefund: 'Waiting For Approval',
                                        notevisible: true,
                                        refundvisible: false,
                                    });
                                }
                                else if(jsonRefund.result.Status == RefundStatusIntEnum.Rejected){
                                    this.setState({ 
                                        statusrefund: 'Rejected',
                                        notevisible: true,
                                        refundvisible: false,
                                    });
                                }
                                else if(jsonRefund.result.Status == RefundStatusIntEnum.Approved){
                                    this.setState({ 
                                        statusrefund: 'Approved',
                                        notevisible: true,
                                        refundvisible: false,
                                    });
                                }
                            }
                            else if(spanDeadline > Date.now())
                            {
                                this.setState({ 
                                    statusrefund: 'Refundable',
                                    notevisible: true,
                                    refundvisible: true,
                                });
                            }
                            else{
                                this.setState({ 
                                    statusrefund: 'Non-Refundable',
                                    notevisible: false,
                                    refundvisible: false,
                                });
                            }
                        }
                        this.setState({ isLoading: false });
                    })
                })
            })
        })
        .catch((error) => {
            console.error(error);
            this.setState({isLoading: false, });
        });
        
    } 

    render() {
        if (this.state.isLoading) {
            return (
                <View style={{flex: 1, paddingTop: 20}}>
                    <ActivityIndicator />
                </View>
            );
        }

        const gotoRefund = () => {
            Alert.alert("Refund Booking", "Are you sure?",
                [
                    {text: 'Yes', onPress: () => {
                        this.setState({ isLoading: true });
                        postRefund(this.state.bookingid, RefundStatusIntEnum.Process).then((jsonRefundBook) => {
                            if(jsonRefundBook.result){
                                this.fn_reloadData();
                            }
                            else{
                                this.setState({ isLoading: false });
                                Utils.errAlert("Error Message", jsonRefundBook.message);
                            }
                        })
                    }},
                    {text: 'No', onPress: () => console.log('No Pressed') },
                ],
                { cancelable: false }
            )
        }

        return (
            <SafeAreaView style={styles.safeAreaView}>
                <ScrollView style={styles.containerScroll}>
                    <View style={styles.container}>
                        <View style={{flex: 1, flexDirection: 'row', backgroundColor: '#efefef', paddingLeft: 15, 
                            paddingRight: 15, paddingTop: 10, paddingRight: 10, alignItems: 'center'}}>
                            <View style={{flex: 1, flexDirection: 'row', marginTop: 5, marginBottom: 5,}}>
                                <Text style={{fontSize: 15, color: Colors.text, fontWeight: 'bold',}} >
                                    {Strings.TicketReservation.text14}</Text>
                                <Text style={{fontSize: 15, color: Colors.text, fontWeight: 'bold', }} >
                                    {this.state.bookingcode}</Text>
                            </View>
                            <Text style={{fontSize: 12, color: Colors.text, flex: 1, textAlign: 'right',}} >
                                {this.state.bookingdate}</Text>
                        </View>
                        <Text style={styles.textKet} >{Strings.TicketReservation.text1}</Text>
                        <Card style={styles.cardVertical}>
                            <View style={{flex: 1, flexDirection: 'row',}}>
                                <Image source={require('../../assets/images/location.png')} 
                                    style={{height: 20, width: 20, resizeMode: 'contain', }}/>
                                <Text style={styles.textLocation} >{this.state.coursename}</Text>
                            </View>
                            <View style={{flex: 1, flexDirection: 'row', marginTop: 5, }}>
                                <Image source={require('../../assets/images/calendar.png')} 
                                    style={{height: 20, width: 20, resizeMode: 'contain', }}/>
                                <Text style={[styles.text, {marginLeft: 5, }]} >{this.state.transdate}</Text>
                                <Text style={[styles.textSession, {marginLeft: 5, }]} >{this.state.session}</Text>
                            </View>
                            <View style={{flex: 1, flexDirection: 'row', marginTop: 5, }}>
                                <Text style={{color: '#939393', fontSize: 15, }} >{Strings.TicketReservation.text2}</Text>
                                <Text style={{color: '#414141', fontSize: 15, textAlign: 'right', marginLeft: 5, }} >{this.state.teetime}</Text>
                            </View>
                            <View style={{flex: 1, flexDirection: 'row', marginTop: 5, }}>
                                <Text style={{color: '#939393', fontSize: 15, }} >{Strings.TicketReservation.text13}</Text>
                                <Text style={{color: '#414141', fontSize: 15, textAlign: 'right', marginLeft: 5, }} >{this.state.paymentmethod}</Text>
                            </View>
                            <Divider style={{ backgroundColor: '#414141', height: 1, width: '100%', marginTop: 5, }} />
                            <View style={{flex: 1, flexDirection: 'row', marginTop: 5, }}>
                                <Text style={styles.textStatus} >{this.state.status}</Text>
                            </View>
                        </Card>
                        <Text style={styles.textKet} >{Strings.TicketReservation.text3}</Text>
                        <View style={{backgroundColor: '#ffffff', margin: 10, padding: 10, }}>
                            {/* List Player */}
                            <ListView
                                dataSource={this.state.dataSource}
                                renderSeparator= {this.ListViewItemSeparator}
                                renderRow={(rowData) =>
                                    <TouchableOpacity 
                                        onPress={ this.GetItem.bind(this, rowData) } >
                                        <TicketRowScreen 
                                            name={rowData.Nama}
                                            playername={rowData.PlayerName}
                                            type={rowData.Tipe}/>
                                    </TouchableOpacity>
                                }
                            />
                        </View>
                        <Text style={styles.textKet} >{Strings.TicketReservation.text4}</Text>
                        <View style={styles.cardVertical}>
                            {
                                Utils.renderIf(this.state.publishvisible,
                                    <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', }}>
                                        <Text style={styles.textLeft2}>{Strings.TicketReservation.text5}</Text>
                                        <Text style={styles.textRight}>{this.state.publishrates}</Text>
                                    </View>
                                )
                            }
                            {
                                Utils.renderIf(this.state.visitorvisible,
                                    <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', marginTop: 5, }}>
                                        <Text style={styles.textLeft2}>{Strings.TicketReservation.text6}</Text>
                                        <Text style={styles.textRight}>{this.state.visitorrates}</Text>
                                    </View>
                                )
                            }
                            {
                                Utils.renderIf(this.state.clubvisible,
                                    <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', marginTop: 5, }}>
                                        <Text style={styles.textLeft2}>{Strings.TicketReservation.text7}</Text>
                                        <Text style={styles.textRight}>{this.state.clubrates}</Text>
                                    </View>
                                )
                            }
                            {
                                Utils.renderIf(this.state.qvisible,
                                    <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', marginTop: 5, }}>
                                        <Text style={styles.textLeft2}>{Strings.TicketReservation.text8}</Text>
                                        <Text style={styles.textRight}>{this.state.qrates}</Text>
                                    </View>
                                )
                            }
                            <Divider style={{ backgroundColor: '#414141', height: 1, width: '100%', marginTop: 5, }} />
                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', marginTop: 5, }}>
                                <Text style={styles.textRight}>{'Rp. ' + Utils.currencyfunc(Math.round(this.state.total*100)/100)}</Text>
                            </View>
                        </View>
                        <Text style={styles.textKet} >{Strings.TicketReservation.text10}</Text>
                        <View style={{backgroundColor: '#ffffff', paddingLeft: 20, paddingRight: 20, paddingTop: 10, paddingBottom: 10,}}>
                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', }}>
                                <Text style={styles.textLeft2}>{'• '}</Text>
                                <Text style={styles.textRight2}>{Strings.TicketReservation.text11}</Text>
                            </View>
                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', }}>
                                <Text style={styles.textLeft2}>{'• '}</Text>
                                <Text style={styles.textRight2}>{Strings.TicketReservation.text12}</Text>
                            </View>
                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', }}>
                                <Text style={styles.textLeft2}>{'• '}</Text>
                                <Text style={styles.textRight2}>{Strings.TicketReservation.text15}</Text>
                            </View>
                        </View>
                        <Text style={styles.textKet} >{Strings.TicketReservation.text16}</Text>
                        <View style={{backgroundColor: '#ffffff', paddingLeft: 20, paddingRight: 20, paddingTop: 10, paddingBottom: 10,}}>
                            {
                                Utils.renderIf(this.state.notevisible,
                                    <Text style={styles.textNote}>{Strings.TicketReservation.text17}</Text>
                                )
                            }
                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', }}>
                                <Text style={styles.textLeft2}>{Strings.TicketReservation.text18}</Text>
                                <Text style={styles.textRight}>{this.state.statusrefund}</Text>
                            </View>
                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', }}>
                                <Text style={styles.textLeft2}>{Strings.TicketReservation.text19}</Text>
                                <Text style={styles.textRight}>{this.state.statusrefund == 'Non-Refundable' || this.state.statusrefund == 'Refundable' ? '-' : this.state.bookingdate}</Text>
                            </View>
                        </View>
                        {
                            Utils.renderIf(this.state.refundvisible,
                                <Button 
                                    onPress= { gotoRefund }
                                    buttonStyle={{backgroundColor: '#b84242', borderRadius: 5, marginTop: 10, marginLeft: 5, marginRight: 5, marginBottom: 20, height: 50, }}
                                    title={Strings.TicketReservation.refundbutton}/>
                            )
                        }
                    </View>
                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({

    safeAreaView: {
        flex: 1,
    },

    containerScroll:
    {
        flex: 1,
        backgroundColor: '#efefef',
    },

    container: {
        flex: 1,
        flexDirection: 'column',
        marginBottom: 10,
    },

    image: {
        height: 20,
        width: 20,
        resizeMode: 'contain',
    },

    textKet: {
        fontSize: 18,
        marginTop: 10,
        marginLeft: 15,
        marginBottom: 5,
        color: Colors.text,
        fontWeight: 'bold',
    },

    text: {
        fontSize: 15,
        color: Colors.text,
    },

    textLeft: {
        flex: 1,
        fontSize: 15,
        marginRight: 10,
        textAlign: 'left',
        color: Colors.text,
    },

    textRight: {
        flex: 1,
        fontSize: 15,
        textAlign: 'right',
        color: Colors.text,
    },

    textRight2: {
        flex: 1,
        fontSize: 15,
    },

    textTotalRight: {
        flex: 1,
        fontSize: 15,
        textAlign: 'right',
        color: '#93b843',
    },

    textStatus: {
        flex: 1,
        fontSize: 15,
        textAlign: 'center',
        color: '#93b843',
    },

    textNote: {
        flex: 1,
        fontSize: 15,
        textAlign: 'center',
        color: '#b84242',
    },

    textSession: {
        fontSize: 15,
        marginLeft: 3,
        color: '#edb636',
    },

    textLocation: {
        fontSize: 15,
        marginLeft: 5,
        color: '#93b843',
    },

    cardVertical: {
        backgroundColor: Colors.backgroundColor,
        flex: 1,
        flexDirection: 'column',
        marginTop: 5,
        marginBottom: 5,
        marginLeft: 10,
        marginRight: 10,
        padding: 10,
        elevation: 2,
        borderRadius: 5,
    },

    cardHorizontal: {
        backgroundColor: Colors.backgroundColor,
        flex: 1,
        flexDirection: 'row',
        marginTop: 5,
        marginBottom: 5,
        marginLeft: 10,
        marginRight: 10,
        padding: 10,
        elevation: 2,
        borderRadius: 5,
    },
});


//textDecorationLine: 'underline'