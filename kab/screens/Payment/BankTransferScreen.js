import React from 'react';
import {
    Image,
    ScrollView,
    Platform,
    StyleSheet,
    SafeAreaView,
    TouchableOpacity,
    ActivityIndicator,
    Text,
    View,
} from 'react-native';
import Colors from '../../constants/Colors';
import Strings from '../../constants/Strings';
import { Divider, } from 'react-native-elements';
import { Card } from 'react-native-material-cards';
import { TransactionTypeIntEnum, OptionIntEnum, PaymentIntEnum } from '../../constants/Enums';
import clsSetting from '../../API/clsSetting';
import { getOptions,  } from '../../API/OptionService';
import Utils from '../Utils';
import moment from 'moment';

export default class BankTransferScreen extends React.Component {
    static navigationOptions = {
        title: 'Bank Transfer',
        headerTintColor: '#ffffff', 
        headerStyle: {
            backgroundColor: '#93b843'
        },
    };

    constructor(props){
        super(props);

        this.params = this.props.navigation.state.params;

        this.state = {
            isLoading: true,
            transtype: this.params.TransType,
            point: this.params.Point,
            paymenttype: this.params.PaymentType,
            chargepayment: this.params.ChargePayment,
            bookingcode: this.params.BookingCode,
            bookingdate: this.params.BookingDate,
            total: this.params.Total,
            transdate: this.params.TransDate,
            eventdate: this.params.EventDate,
            timeevent: this.params.TimeEvent,
            status: this.params.Status,
            bookingdateraw: this.params.BookingDateRaw,

            txtTotalTransfer: '',
            txtPoint: '',
            txtTotalPayment: '',
            txtConvenienceFee: 0,
            conveniencevisible: false,
            pointvisible: true,

            platform: '',
            noteerr: '',
            timeremaining: 30,
            timeremain: 0,
            totalminuteremain: 0,
        };
    }

    componentDidUpdate(){
        if(Number.parseInt(this.state.timeremaining) <= 0){ 
            clearInterval(this.interval);

            this.props.navigation.navigate('App');
        }
    }

    onSelect = data => {
        this.setState(data);
    };

    componentWillUnmount()
    {
        clearInterval(this.interval);
    }

    componentDidMount() {
        var Enumerable = require('../linq');

        getOptions().then((jsonOption) => 
        {
            if(this.state.transtype == TransactionTypeIntEnum.Reservation)
            {
                this.setState({
                    maintenanceOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionMaintenancePeriod).firstOrDefault(),
                    reservationOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionReservation).firstOrDefault(),
                    mandiriOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionBankTransferMandiriReservation).firstOrDefault(),
                    bcaOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionBankTransferBCAReservation).firstOrDefault(),
                    permataOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionBankTransferPermataReservation).firstOrDefault(),
                });

                this.setState({
                    platform: Platform.OS === 'ios' ? this.state.reservationOption.iOS : this.state.reservationOption.Android,
                    noteerr: this.state.reservationOption.Note,
                });
            }
            else if(this.state.transtype == TransactionTypeIntEnum.RegistrationQ)
            {
                this.setState({
                    maintenanceOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionMaintenancePeriod).firstOrDefault(),
                    registrationOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionRegistration).firstOrDefault(),
                    mandiriOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionBankTransferMandiriRegistration).firstOrDefault(),
                    bcaOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionBankTransferBCARegistration).firstOrDefault(),
                    permataOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionBankTransferPermataRegistration).firstOrDefault(),
                });

                this.setState({
                    platform: Platform.OS === 'ios' ? this.state.registrationOption.iOS : this.state.registrationOption.Android,
                    noteerr: this.state.registrationOption.Note,
                });
            }
            else if(this.state.transtype == TransactionTypeIntEnum.Event)
            {
                this.setState({
                    maintenanceOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionMaintenancePeriod).firstOrDefault(),
                    eventOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionEvent).firstOrDefault(),
                    mandiriOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionBankTransferMandiriEvent).firstOrDefault(),
                    bcaOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionBankTransferBCAEvent).firstOrDefault(),
                    permataOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionBankTransferPermataEvent).firstOrDefault(),
                });
                this.setState({ conveniencevisible: true, });
                this.setState({
                    platform: Platform.OS === 'ios' ? this.state.eventOption.iOS : this.state.eventOption.Android,
                    noteerr: this.state.eventOption.Note,
                });
            }
            else if(this.state.transtype == TransactionTypeIntEnum.MobilePulsaPhonePrepaid)
            {
                this.setState({
                    maintenanceOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionMaintenancePeriod).firstOrDefault(),
                    pulsaOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionMobilePulsa).firstOrDefault(),
                    mandiriOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionBankTransferMandiriMobilePulsa).firstOrDefault(),
                    bcaOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionBankTransferBCAMobilePulsa).firstOrDefault(),
                    permataOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionBankTransferPermataMobilePulsa).firstOrDefault(),
                });
                this.setState({ conveniencevisible: true, });
                this.setState({
                    platform: Platform.OS === 'ios' ? this.state.pulsaOption.iOS : this.state.pulsaOption.Android,
                    noteerr: this.state.pulsaOption.Note,
                });
            }

            if(this.state.chargepayment){
                this.setState({
                    txtConvenienceFee: 4000,
                    conveniencevisible: this.state.chargepayment,
                });
            }
            else{
                this.setState({
                    conveniencevisible: this.state.chargepayment,
                });
            }

            if (this.state.point > 0)
            {
                this.setState({ 
                    txtPoint: this.state.point,
                    txtTotalPayment: this.state.total,
                    txtTotalTransfer: this.state.total - this.state.point + this.state.txtConvenienceFee,
                    pointvisible: true,
                });
            }
            else
            {
                this.setState({ 
                    txtPoint: 0,
                    pointvisible: false,
                    txtTotalPayment: this.state.total,
                    txtTotalTransfer: this.state.total + this.state.txtConvenienceFee,
                }); 
            }

            this.setState({ 
                timeremain: Utils.getTotalMinutes(Date.now(), Utils.getLocalTimezone(Date.parse(this.state.bookingdateraw))),
                totalminuteremain: Utils.getTotalMinuteRemaining(Utils.getCountDays(Date.parse(this.state.transdate), Date.parse(this.state.bookingdateraw)),
                    this.state.transtype, this.state.status),
            });

            if(this.state.transtype == TransactionTypeIntEnum.Event)
            {
                var date2 = moment(Date.parse(this.state.transdate)).format('YYYY MM DD');
                var timeEvent = this.state.timeevent.split(' ');
                var hour = Number.parseInt(timeEvent[0].split(':')[0]);
                var minute = Number.parseInt(timeEvent[0].split(':')[1]);
                if(timeEvent[1] == "PM"){
                    hour = Number.parseInt(hour) + 12;
                }

                var date3 = moment(Date.parse(this.state.eventdate)).format('YYYY MM DD');
                var currentOffset = (new Date()).getTimezoneOffset() / 60;
                var dateevent = date3.split(' ');
                var newdate = moment(new Date(dateevent[0], dateevent[1] - 1, dateevent[2], hour, minute)).valueOf();

                this.setState({ 
                    totalminuteremain: Utils.getTotalMinuteRemaining(Utils.getCountDays(newdate, 
                        Date.parse(date2)), this.state.transtype, this.state.status),
                });
            }
            
            if(this.state.timeremain <= this.state.totalminuteremain){
                this.setState({
                    timeremaining: Number.parseInt(this.state.totalminuteremain) - Number.parseInt(this.state.timeremain),
                });
            }
            
            this.interval = setInterval(
                () => this.setState({ timeremaining: Number.parseInt(this.state.timeremaining) - 1 }), 60000
            );
            this.setState({isLoading: false, });
        })
        .catch((error) => {
            console.error(error);
            this.setState({isLoading: false, });
        });
    }

    render() {
        if (this.state.isLoading) {
            return (
                <View style={{flex: 1, paddingTop: 20}}>
                    <ActivityIndicator />
                </View>
            );
        }

        const goto3DSMandiri = () => {
            var mandiriPlatform = Platform.OS === 'ios' ? this.state.mandiriOption.iOS : this.state.mandiriOption.Android;

            if(Utils.getMaintenanceOption(this.state.maintenanceOption)){
                Utils.alertBackToMain(this.state.maintenanceOption.Note, this.props);
            }
            else if(!this.state.platform) {
                Utils.errAlert("QAccess Alert", this.state.noteerr);
            }
            else if(!mandiriPlatform) {
                Utils.errAlert("QAccess Alert", mandiriOption.Note);
            }
            else
            {
                this.props.navigation.navigate('BankTransfer3DS', {Point: this.state.point, BookingCode: this.state.bookingcode,
                    BookingDate: this.state.bookingdate, ChargePayment: this.state.chargepayment, 
                    TransType: this.state.transtype, PaymentType: this.state.paymenttype, Total: this.state.total,
                    Bank: 'echannel', onSelect: this.onSelect});
            }
        }

        const goto3DSBCA = () => {
            var bcaPlatform = Platform.OS === 'ios' ? this.state.bcaOption.iOS : this.state.bcaOption.Android;

            if(Utils.getMaintenanceOption(this.state.maintenanceOption)){
                Utils.alertBackToMain(this.state.maintenanceOption.Note, this.props);
            }
            else if(!this.state.platform) {
                Utils.errAlert("QAccess Alert", this.state.noteerr);
            }
            else if(!bcaPlatform) {
                Utils.errAlert("QAccess Alert", bcaOption.Note);
            }
            else
            {
                this.props.navigation.navigate('BankTransfer3DS', {Point: this.state.point, BookingCode: this.state.bookingcode,
                    BookingDate: this.state.bookingdate, ChargePayment: this.state.chargepayment, 
                    TransType: this.state.transtype, PaymentType: PaymentIntEnum.BankTransfer, Total: this.state.total,
                    Bank: 'bca', onSelect: this.onSelect});
            }
        }

        const goto3DSPermata = () => {
            var permataPlatform = Platform.OS === 'ios' ? this.state.permataOption.iOS : this.state.permataOption.Android;

            if(Utils.getMaintenanceOption(this.state.maintenanceOption)){
                Utils.alertBackToMain(this.state.maintenanceOption.Note, this.props);
            }
            else if(!this.state.platform) {
                Utils.errAlert("QAccess Alert", this.state.noteerr);
            }
            else if(!permataPlatform) {
                Utils.errAlert("QAccess Alert", permataOption.Note);
            }
            else
            {
                this.props.navigation.navigate('BankTransfer3DS', {Point: this.state.point, BookingCode: this.state.bookingcode,
                    BookingDate: this.state.bookingdate, ChargePayment: this.state.chargepayment, 
                    TransType: this.state.transtype, PaymentType: PaymentIntEnum.BankTransfer, Total: this.state.total,
                    Bank: 'permata', onSelect: this.onSelect});
            }
        }

        return (
            <SafeAreaView style={styles.safeAreaView}>
                <ScrollView style={styles.containerScroll}>
                    <View style={styles.container}>
                        <View style={{flex: 1, flexDirection: 'row', backgroundColor: '#efefef', paddingLeft: 15, 
                            paddingRight: 15, paddingTop: 10, paddingRight: 10, alignItems: 'center'}}>
                            <View style={{flex: 1, flexDirection: 'row', marginTop: 5, marginBottom: 5,}}>
                                <Text style={{fontSize: 15, color: Colors.text, fontWeight: 'bold',}} >
                                    {Strings.BankTransfer.text1}</Text>
                                <Text style={{fontSize: 15, color: Colors.text, fontWeight: 'bold', }} >
                                    {this.state.bookingcode}</Text>
                            </View>
                            <Text style={{fontSize: 12, color: Colors.text, flex: 1, textAlign: 'right',}} >
                                {this.state.bookingdate}</Text>
                        </View>
                        <Card style={styles.cardNote}>
                            <Image source={require('../../assets/images/information.png')} 
                                style={{height: 20, width: 20, resizeMode: 'contain', }}/>
                            <Text style={styles.textNote} >{Strings.BankTransfer.text2}</Text>
                            <Text style={styles.textNote} >{Utils.convertMinuteIntoString(this.state.timeremaining)}</Text>
                        </Card>
                        <View style={styles.cardVertical}>
                            {
                                Utils.renderIf(this.state.pointvisible || this.state.conveniencevisible,
                                    <View style={{flex: 1, flexDirection: 'row', }}>
                                        <Text style={styles.textLeft} >{Strings.BankTransfer.text3}</Text>
                                        <Text style={styles.textRight} >{'Rp. ' + Utils.currencyfunc(Math.round(this.state.txtTotalPayment*100)/100)}</Text>
                                    </View>
                                )
                            }
                            {
                                Utils.renderIf(this.state.pointvisible,
                                    <View style={{flex: 1, flexDirection: 'row', marginTop: 5, }}>
                                        <Text style={styles.textLeft} >{Strings.BankTransfer.text4}</Text>
                                        <Text style={styles.textRight} >{'Rp. ' + Utils.currencyfunc(Math.round(this.state.txtPoint*100)/100)}</Text>
                                    </View>
                                )
                            }
                            {
                                Utils.renderIf(this.state.conveniencevisible, 
                                    <View style={{flex: 1, flexDirection: 'row', marginTop: 5, }}>
                                        <Text style={styles.textLeft} >{Strings.BankTransfer.text5}</Text>
                                        <Text style={styles.textRight} >{'Rp. ' + Utils.currencyfunc(Math.round(this.state.txtConvenienceFee*100)/100)}</Text>
                                    </View>
                                )
                            }
                            {
                                Utils.renderIf(this.state.pointvisible || this.state.conveniencevisible,
                                    <Divider style={{ backgroundColor: '#414141', height: 1, width: '100%', marginTop: 5, }} />
                                )
                            }
                            <View style={{flex: 1, flexDirection: 'row', marginTop: 5, }}>
                                <Text style={styles.textLeft} >{Strings.BankTransfer.text6}</Text>
                                <Text style={styles.textTotalRight} >{'Rp. ' + Utils.currencyfunc(Math.round(this.state.txtTotalTransfer*100)/100)}</Text>
                            </View>
                        </View>
                        <Card style={styles.cardHorizontal}>
                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', }}>
                                <TouchableOpacity style={{flex: 1, flexDirection: 'row', alignItems: 'center', }}
                                    onPress={ goto3DSMandiri }>
                                    <View style={{flex: 2, flexDirection: 'row', justifyContent: 'center',}}>
                                        <Image source={require('../../assets/images/mandiri.png')} 
                                            style={{height: 60, width: 100, resizeMode: 'contain', }}/>
                                    </View>
                                    <Image source={require('../../assets/images/next2.png')} 
                                        style={{height: 15, width: 15, resizeMode: 'contain', alignItems: 'flex-end',}}/>
                                </TouchableOpacity>
                            </View>
                        </Card>
                        <Card style={styles.cardHorizontal}>
                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', }}>
                                <TouchableOpacity style={{flex: 1, flexDirection: 'row', alignItems: 'center', }}
                                    onPress={ goto3DSPermata }>
                                    <View style={{flex: 2, flexDirection: 'row', justifyContent: 'center',}}>
                                        <Image source={require('../../assets/images/permata.png')} 
                                            style={{height: 60, width: 100, resizeMode: 'contain', }}/>
                                    </View>
                                    <Image source={require('../../assets/images/next2.png')} 
                                        style={{height: 15, width: 15, resizeMode: 'contain', alignItems: 'flex-end',}}/>
                                </TouchableOpacity>
                            </View>
                        </Card>
                        <Card style={styles.cardHorizontal}>
                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', }}>
                                <TouchableOpacity style={{flex: 1, flexDirection: 'row', alignItems: 'center', }}
                                    onPress={ goto3DSBCA }>
                                    <View style={{flex: 2, flexDirection: 'row', justifyContent: 'center',}}>
                                        <Image source={require('../../assets/images/bca.png')} 
                                            style={{height: 60, width: 100, resizeMode: 'contain', }}/>
                                    </View>
                                    <Image source={require('../../assets/images/next2.png')} 
                                        style={{height: 15, width: 15, resizeMode: 'contain', alignItems: 'flex-end',}}/>
                                </TouchableOpacity>
                            </View>
                        </Card>
                    </View>
                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({

    safeAreaView: {
        flex: 1,
    },

    containerScroll:
    {
        flex: 1,
        backgroundColor: '#efefef',
    },

    container: {
        flex: 1,
        flexDirection: 'column',
    },

    image: {
        height: 20,
        width: 20,
        resizeMode: 'contain',
    },

    cardNote: {
        backgroundColor: Colors.backgroundColor,
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 5,
        marginBottom: 5,
        marginLeft: 10,
        marginRight: 10,
        padding: 10,
        elevation: 2,
        borderRadius: 5,
    },

    textNote: {
        fontSize: 12,
        marginLeft: 3,
        color: Colors.text,
    },

    text: {
        fontSize: 15,
        marginLeft: 10,
        color: Colors.text,
    },

    textLeft: {
        flex: 1,
        fontSize: 15,
        marginRight: 10,
        textAlign: 'left',
        color: Colors.text,
    },

    textRight: {
        flex: 1,
        fontSize: 15,
        textAlign: 'right',
        color: Colors.text,
    },

    textTotalRight: {
        flex: 1,
        fontSize: 15,
        textAlign: 'right',
        color: '#93b843',
    },

    textSession: {
        fontSize: 15,
        marginLeft: 3,
        color: '#edb636',
    },

    cardVertical: {
        backgroundColor: Colors.backgroundColor,
        flex: 1,
        flexDirection: 'column',
        marginTop: 5,
        marginBottom: 5,
        marginLeft: 10,
        marginRight: 10,
        padding: 10,
        elevation: 2,
        borderRadius: 5,
    },

    cardHorizontal: {
        backgroundColor: Colors.backgroundColor,
        flex: 1,
        flexDirection: 'row',
        marginTop: 5,
        marginBottom: 5,
        marginLeft: 10,
        marginRight: 10,
        padding: 10,
        elevation: 2,
        borderRadius: 5,
    },
});


//textDecorationLine: 'underline'