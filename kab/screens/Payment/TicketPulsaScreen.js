import React from 'react';
import {
    Image,
    ScrollView,
    StyleSheet,
    AsyncStorage,
    SafeAreaView,
    ActivityIndicator,
    Text,
    View,
    BackHandler,
} from 'react-native';
import Colors from '../../constants/Colors';
import Strings from '../../constants/Strings';
import { Divider, Icon, } from 'react-native-elements';
import { Card } from 'react-native-material-cards';
import { findPulsaTrans, } from '../../API/PulsaService';
import Utils from '../Utils';
import Enums, { PaymentIntEnum, PaymentEnum } from '../../constants/Enums';
import { getDoubleToStringDate,  } from '../../API/SettingService';
import moment from 'moment';
import clsSetting from '../../API/clsSetting';

export default class TicketPulsaScreen extends React.Component {
    static navigationOptions = ({navigation}) => ({
        title: 'E-Ticket',
        headerTintColor: '#ffffff', 
        headerStyle: {
            backgroundColor: '#93b843'
        },
        headerLeft: (
            <Icon name="chevron-left"
                onPress={() => {
                    navigation.popToTop();
                }}
                size={35} color="white"/>
        ),
    });

    constructor(props){
        super(props);

        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);

        this.state = {
            isLoading: true,
            username: '',
            bookingcode: '',
            bookingdate: '',
            point: 0,
            conveniencefee: 0,
            total: 0,
            totaltransfer: 0,

            imageprovider: '',
            title: '',
            destnumber: '',
            productcode: '',
            activeperiod: '',
            paymentmethod: '',
            status: '',
        };
    }

    handleBackButtonClick() {
        this.props.navigation.popToTop();
        return true;
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount()
    {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    async componentDidMount() {
        this.setState({ bookingcode : await AsyncStorage.getItem('BookingCode')});
        this.setState({ username : await AsyncStorage.getItem('Username')});
            
        findPulsaTrans(this.state.bookingcode).then((jsonTicket) =>
        {
            this.setState({
                title: jsonTicket.ProductDescription,
                destnumber: jsonTicket.CustomerNumber,
                productcode: jsonTicket.ProductCode,
                activeperiod: jsonTicket.ProductActivePeriod + " days",
                paymentmethod: Enums.PaymentEnum(jsonTicket.Payment),
                point: jsonTicket.Point,
                totaltransfer: jsonTicket.TotalDetail,
                total: jsonTicket.Total,
                imageprovider: Utils.getImageProvider2(jsonTicket.CustomerNumber),
                status: 'Paid',
            });

            if(jsonTicket.Payment == PaymentIntEnum.CreditCard){
                this.setState({
                    conveniencefee: Math.round(2.2 / 100 * jsonTicket.TotalDetail - jsonTicket.Point) + 2500,
                });
            }
            else if(jsonTicket.Payment == PaymentIntEnum.BankTransfer){
                this.setState({
                    conveniencefee: 4000,
                });
            }

            if (!jsonTicket.ProductAbsorbPayment)
            {
                this.setState({
                    conveniencefee: 0,
                });
            }
            
            getDoubleToStringDate(jsonTicket.CreatedDate).then((jsonDate) =>
            {
                this.setState({ bookingdate: moment(Date.parse(jsonDate.result)).format('DD MMM YYYY'), });
                this.setState({isLoading: false, });
            })
        })
        .catch((error) => {
            console.error(error);
            this.setState({isLoading: false, });
        });
    }

    render() {
        if (this.state.isLoading) {
            return (
                <View style={{flex: 1, paddingTop: 20}}>
                    <ActivityIndicator />
                </View>
            );
        }

        return (
            <SafeAreaView style={styles.safeAreaView}>
                <ScrollView style={styles.containerScroll}>
                    <View style={styles.container}>
                        <View style={{flex: 1, flexDirection: 'row', backgroundColor: '#efefef', paddingLeft: 15, 
                            paddingRight: 15, paddingTop: 10, paddingRight: 10, alignItems: 'center'}}>
                            <View style={{flex: 1, flexDirection: 'row', marginTop: 5, marginBottom: 5,}}>
                                <Text style={{fontSize: 15, color: Colors.text, fontWeight: 'bold',}} >
                                    {Strings.TicketPulsa.text1}</Text>
                                <Text style={{fontSize: 15, color: Colors.text, fontWeight: 'bold', }} >
                                    {this.state.bookingcode}</Text>
                            </View>
                            <Text style={{fontSize: 12, color: Colors.text, flex: 1, textAlign: 'right',}} >
                                {this.state.bookingdate}</Text>
                        </View>
                        <Card style={styles.cardVertical}>
                            <View style={{flex: 1, flexDirection: 'row', marginTop: 5, justifyContent: 'center', }}>
                                <Image style={{height: 50, width: 50}} 
                                    source={this.state.imageprovider}/>
                                <Text style={[styles.text, {color: Colors.text}]} >{this.state.title}</Text>
                            </View>
                            <View style={{flex: 1, flexDirection: 'row', marginTop: 5, }}>
                                <Text style={[styles.text, {color: '#939393'}]} >{Strings.TicketPulsa.text2}</Text>
                                <Text style={[styles.text, {color: Colors.text}]} >{this.state.destnumber}</Text>
                            </View>
                            <View style={{flex: 1, flexDirection: 'row', marginTop: 5, }}>
                                <Text style={[styles.text, {color: '#939393'}]} >{Strings.TicketPulsa.text3}</Text>
                                <Text style={[styles.text, {color: Colors.text}]} >{this.state.productcode}</Text>
                            </View>
                            <View style={{flex: 1, flexDirection: 'row', marginTop: 5, }}>
                                <Text style={[styles.text, {color: '#939393'}]} >{Strings.TicketPulsa.text4}</Text>
                                <Text style={[styles.text, {color: Colors.text}]} >{this.state.activeperiod}</Text>
                            </View>
                            <View style={{flex: 1, flexDirection: 'row', marginTop: 5, }}>
                                <Text style={[styles.text, {color: '#939393'}]} >{Strings.TicketPulsa.text5}</Text>
                                <Text style={[styles.text, {color: Colors.text}]} >{this.state.paymentmethod}</Text>
                            </View>
                            <View style={{flex: 1, flexDirection: 'row', marginTop: 5, }}>
                                <Text style={[styles.text, {color: '#939393'}]} >{Strings.TicketPulsa.text6}</Text>
                                <Text style={[styles.text, {color: Colors.text}]} >{this.state.status}</Text>
                            </View>
                        </Card>
                        <Text style={styles.textKet} >{Strings.TicketPulsa.text7}</Text>
                        <View style={styles.cardVertical}>
                            {
                                Utils.renderIf(this.state.point != 0 || this.state.conveniencefee != 0,
                                    <View style={{flex: 1, flexDirection: 'row', }}>
                                        <Text style={styles.textLeft2} >{Strings.TicketPulsa.text8}</Text>
                                        <Text style={styles.textRight} >{'Rp. ' + Utils.currencyfunc(Math.round(this.state.totaltransfer*100)/100)}</Text>
                                    </View>
                                )
                            }
                            {
                                Utils.renderIf(this.state.point != 0,
                                    <View style={{flex: 1, flexDirection: 'row', marginTop: 5, }}>
                                        <Text style={styles.textLeft2} >{Strings.TicketPulsa.text9}</Text>
                                        <Text style={styles.textRight} >{'Rp. ' + Utils.currencyfunc(Math.round(this.state.point*100)/100)}</Text>
                                    </View>
                                )
                            }
                            {
                                Utils.renderIf(this.state.conveniencefee != 0,
                                    <View style={{flex: 1, flexDirection: 'row', marginTop: 5, }}>
                                        <Text style={styles.textLeft2} >{Strings.TicketPulsa.text10}</Text>
                                        <Text style={styles.textRight} >{'Rp. ' + Utils.currencyfunc(Math.round(this.state.conveniencefee*100)/100)}</Text>
                                    </View>
                                )
                            }
                            {
                                Utils.renderIf(this.state.point != 0 || this.state.conveniencefee != 0,
                                    <Divider style={{ backgroundColor: '#414141', height: 1, width: '100%', marginTop: 5, }} />
                                )
                            }
                            <View style={{flex: 1, flexDirection: 'row', marginTop: 5, }}>
                                <Text style={styles.textLeft2} >{Strings.TicketPulsa.text11}</Text>
                                <Text style={styles.textTotalRight} >{'Rp. ' + Utils.currencyfunc(Math.round(this.state.total*100)/100)}</Text>
                            </View>
                        </View>
                        <View style={styles.cardVertical}>
                            <View style={{flex: 1, flexDirection: 'row', padding: 10, justifyContent: 'center', }}>
                                <Image style={{height: 30, width: 30}} 
                                    source={require('../../assets/images/phone.png')}/>
                                <Text style={[styles.text, {color: Colors.text, }]} >{Strings.TicketPulsa.text12}</Text>
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({

    safeAreaView: {
        flex: 1,
    },

    containerScroll:
    {
        flex: 1,
        backgroundColor: '#efefef',
    },

    container: {
        flex: 1,
        flexDirection: 'column',
    },

    textKet: {
        fontSize: 18,
        marginTop: 10,
        marginLeft: 15,
        marginBottom: 5,
        color: Colors.text,
        fontWeight: 'bold',
    },

    text: {
        fontSize: 15,
        marginLeft: 10,
    },

    textLeft: {
        flex: 1,
        fontSize: 15,
        marginRight: 10,
        textAlign: 'left',
        color: Colors.text,
    },

    textRight: {
        flex: 1,
        fontSize: 15,
        textAlign: 'right',
        color: Colors.text,
    },

    textTotalRight: {
        flex: 1,
        fontSize: 15,
        textAlign: 'right',
        color: '#93b843',
    },

    cardVertical: {
        backgroundColor: Colors.backgroundColor,
        flex: 1,
        flexDirection: 'column',
        marginTop: 5,
        marginBottom: 5,
        marginLeft: 10,
        marginRight: 10,
        padding: 10,
        elevation: 2,
        borderRadius: 5,
    },

    cardHorizontal: {
        backgroundColor: Colors.backgroundColor,
        flex: 1,
        flexDirection: 'row',
        marginTop: 5,
        marginBottom: 5,
        marginLeft: 10,
        marginRight: 10,
        padding: 10,
        elevation: 2,
        borderRadius: 5,
    },
});


//textDecorationLine: 'underline'