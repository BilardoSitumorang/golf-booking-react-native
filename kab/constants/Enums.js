import React from 'react';
import { 
    TipePemainIntEnum, 
    TipePemainStringEnum, 
    KartuStringEnum, 
    StatusPemainIntEnum, 
    StatusPemainStringEnum,
    KartuIntEnum, 
    OptionIntEnum, 
    OptionStringEnum,
    StatusIntEnum, 
    StatusStringEnum,
    SessionIntEnum,
    SessionStringEnum,
    PaymentIntEnum,
    PaymentStringEnum,
    EventSessionIntEnum,
    EventSessionStringEnum,
} from './Enums';

exports.OptionStringEnum = {
    OptionMaintenancePeriod: 'OptionMaintenancePeriod',
    OptionGlobalMessage: 'OptionGlobalMessage',
    OptionPublishQuota: 'OptionPublishQuota',

    OptionReservation: 'OptionReservation',
    OptionRegistration: 'OptionRegistration',
    OptionMobilePulsa: 'OptionMobilePulsa',
    OptionPLN: 'OptionPLN',
    OptionBPJS: 'OptionBPJS',
    OptionEvent: 'OptionEvent',

    OptionBankTransferBCAReservation: 'OptionBankTransferBCAReservation',
    OptionBankTransferBCARegistration: 'OptionBankTransferBCARegistration',
    OptionBankTransferBCARenewal: 'OptionBankTransferBCARenewal',
    OptionBankTransferBCAMobilePulsa: 'OptionBankTransferBCAMobilePulsa',
    OptionBankTransferBCAPLN: 'OptionBankTransferBCAPLN',
    OptionBankTransferBCABPJS: 'OptionBankTransferBCABPJS',
    OptionBankTransferBCAEvent: 'OptionBankTransferBCAEvent',

    OptionBankTransferPermataReservation: 'OptionBankTransferPermataReservation',
    OptionBankTransferPermataRegistration: 'OptionBankTransferPermataRegistration',
    OptionBankTransferPermataRenewal: 'OptionBankTransferPermataRenewal',
    OptionBankTransferPermataMobilePulsa: 'OptionBankTransferPermataMobilePulsa',
    OptionBankTransferPermataPLN: 'OptionBankTransferPermataPLN',
    OptionBankTransferPermataBPJS: 'OptionBankTransferPermataBPJS',
    OptionBankTransferPermataEvent: 'OptionBankTransferPermataEvent',

    OptionBankTransferMandiriReservation: 'OptionBankTransferMandiriReservation',
    OptionBankTransferMandiriRegistration: 'OptionBankTransferMandiriRegistration',
    OptionBankTransferMandiriRenewal: 'OptionBankTransferMandiriRenewal',
    OptionBankTransferMandiriMobilePulsa: 'OptionBankTransferMandiriMobilePulsa',
    OptionBankTransferMandiriPLN: 'OptionBankTransferMandiriPLN',
    OptionBankTransferMandiriBPJS: 'OptionBankTransferMandiriBPJS',
    OptionBankTransferMandiriEvent: 'OptionBankTransferMandiriEvent',
    OptionCreditCardReservation: 'OptionCreditCardReservation',
    OptionCreditCardRegistration: 'OptionCreditCardRegistration',
    OptionCreditCardRenewal: 'OptionCreditCardRenewal',
    OptionCreditCardMobilePulsa: 'OptionCreditCardMobilePulsa',
    OptionCreditCardPLN: 'OptionCreditCardPLN',
    OptionCreditCardBPJS: 'OptionCreditCardBPJS',
    OptionCreditCardEvent: 'OptionCreditCardEvent',
    OptionPointReservation: 'OptionPointReservation',
    OptionPointRegistration: 'OptionPointRegistration',
    OptionPointRenewal: 'OptionPointRenewal',
    OptionPointMobilePulsa: 'OptionPointMobilePulsa',
    OptionPointPLN: 'OptionPointPLN',
    OptionPointBPJS: 'OptionPointBPJS',
    OptionPointEvent: 'OptionPointEvent',
    OptionSharePaymentLinkReservation: 'OptionSharePaymentLinkReservation',
    OptionSharePaymentLinkRegistration: 'OptionSharePaymentLinkRegistration',
    OptionSharePaymentLinkRenewal: 'OptionSharePaymentLinkRenewal',
    OptionSharePaymentLinkMobilePulsa: 'OptionSharePaymentLinkMobilePulsa',
    OptionSharePaymentLinkPLN: 'OptionSharePaymentLinkPLN',
    OptionSharePaymentLinkBPJS: 'OptionSharePaymentLinkBPJS',
    OptionSharePaymentLinkEvent: 'OptionSharePaymentLinkEvent',
    OptionPaymentInstallmentWebRegistration: 'OptionPaymentInstallmentWebRegistration',
    OptionPaymentInstallmentMobileRegistration: 'OptionPaymentInstallmentMobileRegistration',
}

exports.OptionIntEnum = {
    OptionMaintenancePeriod: 1,
    OptionGlobalMessage: 2,
    OptionPublishQuota: 3,

    OptionReservation: 4,
    OptionRegistration: 5,
    OptionMobilePulsa: 6,
    OptionPLN: 7,
    OptionBPJS: 8,
    OptionEvent: 9,

    OptionBankTransferBCAReservation: 10,
    OptionBankTransferBCARegistration: 11,
    OptionBankTransferBCARenewal: 12,
    OptionBankTransferBCAMobilePulsa: 13,
    OptionBankTransferBCAPLN: 14,
    OptionBankTransferBCABPJS: 15,
    OptionBankTransferBCAEvent: 16,

    OptionBankTransferPermataReservation: 17,
    OptionBankTransferPermataRegistration: 18,
    OptionBankTransferPermataRenewal: 19,
    OptionBankTransferPermataMobilePulsa: 20,
    OptionBankTransferPermataPLN: 21,
    OptionBankTransferPermataBPJS: 22,
    OptionBankTransferPermataEvent: 23,

    OptionBankTransferMandiriReservation: 24,
    OptionBankTransferMandiriRegistration: 25,
    OptionBankTransferMandiriRenewal: 26,
    OptionBankTransferMandiriMobilePulsa: 27,
    OptionBankTransferMandiriPLN: 28,
    OptionBankTransferMandiriBPJS: 29,
    OptionBankTransferMandiriEvent: 30,
    OptionCreditCardReservation: 31,
    OptionCreditCardRegistration: 32,
    OptionCreditCardRenewal: 33,
    OptionCreditCardMobilePulsa: 34,
    OptionCreditCardPLN: 35,
    OptionCreditCardBPJS: 36,
    OptionCreditCardEvent: 37,
    OptionPointReservation: 38,
    OptionPointRegistration: 39,
    OptionPointRenewal: 40,
    OptionPointMobilePulsa: 41,
    OptionPointPLN: 42,
    OptionPointBPJS: 43,
    OptionPointEvent: 44,
    OptionSharePaymentLinkReservation: 45,
    OptionSharePaymentLinkRegistration: 46,
    OptionSharePaymentLinkRenewal: 47,
    OptionSharePaymentLinkMobilePulsa: 48,
    OptionSharePaymentLinkPLN: 49,
    OptionSharePaymentLinkBPJS: 50,
    OptionSharePaymentLinkEvent: 51,
    OptionPaymentInstallmentRegistration: 52,
    OptionPaymentInstallmentRenewal: 53,
    OptionRenewal: 54,
}

exports.AccountBalanceStringEnum = {
    Registration: 'Registration',
    Referral: 'Referral',
    Booking: 'Booking',
    BookingReferral: 'Booking Referral',
    TopUp: 'Top Up',
    Redeem: 'Redeem',
    CancelRedeem: 'Cancel Redeem',
    Expired: 'Expired',
    Refund: 'Refund',
    RefundReferral: 'Refund Referral',
    Cashback: 'Cashback',
    AgentRegCommission: 'Agent Reg Commission',
    AgentEventCommission: 'Agent Event Comm',
}

exports.AccountBalanceIntEnum = {
    Registration: 1,
    Referral: 2,
    Booking: 3,
    BookingReferral: 4,
    TopUp: 5,
    Redeem: 6,
    CancelRedeem: 7,
    Expired: 8,
    Refund: 9,
    RefundReferral: 10,
    Cashback: 11,
    AgentRegCommission: 12,
    AgentEventCommission: 13,
}

exports.EventSessionStringEnum = {
    Morning: 'Morning',
    Afternoon: 'Afternoon',
    FullDay: 'Full Day',
}

exports.EventSessionIntEnum = {
    Morning: 1,
    Afternoon: 2,
    FullDay: 3,
}

exports.KartuStringEnum = {
    Blank: 'Blank',
    Easy: 'Easy',
    Premium: 'Premium',
    Corporate: 'Corporate',
    Elder: 'Elder',
    Mandiri: 'Mandiri',
}

exports.KartuIntEnum = {
    Blank: 0,
    Easy: 1,
    Premium: 2,
    Corporate: 3,
    Elder: 4,
    Mandiri: 11,
}

exports.MemberCardDaySesionStringEnum = {
    MONM: 'Monday Morning',
    MONA: 'Monday Afternoon',
    TUEM: 'Tuesday Morning',
    TUEA: 'Tuesday Afternoon',
    WEDM: 'Wednesday Morning',
    WEDA: 'Wednesday Afternoon',
    THUM: 'Thursday Morning',
    THUA: 'Thursday Afternoon',
    FRIM: 'Friday Morning',
    FRIA: 'Friday Afternoon',
    SATM: 'Saturday Morning',
    SATA: 'Saturday Afternoon',
    SUNM: 'Sunday Morning',
    SUNA: 'Sunday Afternoon',
}

exports.MemberCardDaySesionIntEnum = {
    MONM: 1,
    MONA: 2,
    TUEM: 3,
    TUEA: 4,
    WEDM: 5,
    WEDA: 6,
    THUM: 7,
    THUA: 8,
    FRIM: 9,
    FRIA: 10,
    SATM: 11,
    SATA: 12,
    SUNM: 13,
    SUNA: 14,
}

exports.NotificationStringEnum = {
    Other: 'Other',
    Partnership: 'Partnership',
    Event: 'Event',
}

exports.NotificationIntEnum = {
    Other: 0,
    Partnership: 1,
    Event: 2,
}

exports.NotificationUserStatusStringEnum = {
    Unread: 'Unread',
    Read: 'Read',
}

exports.NotificationUserStatusIntEnum = {
    Unread: 0,
    Read: 1,
}

exports.PaymentStringEnum = {
    NotSpecified: "Not Specified",
    BankTransfer: "Bank Transfer",
    CreditCard: "Credit Card",
    Other: "Other",
    FullPoint: "Full Point",
}

exports.PaymentIntEnum = {
    NotSpecified: 1,
    BankTransfer: 2,
    CreditCard: 3,
    Other: 4,
    FullPoint: 7,
}

exports.PlatformStringEnum = {
    Android: 'Android',
    IOs: 'iOS',
    Online: 'Online',
}

exports.PlatformIntEnum = {
    Android: 1,
    IOs: 2,
    Online: 3,
}

exports.RefundStatusStringEnum = {
    Process: "In-Process",
    Rejected: 'Rejected',
    Approved: 'Approved',
}

exports.RefundStatusIntEnum = {
    Process: 0,
    Rejected: 1,
    Approved: 2,
}

exports.SessionStringEnum = {
    Morning: "Morning Session",
    Afternoon: "Afternoon Session",
}

exports.SessionIntEnum = {
    Morning: 1,
    Afternoon: 2,
}

exports.StatusStringEnum = {
    Book: 'Book',
    Paid: 'Paid',
    Cancelled: 'Cancelled',
    TimeOut: 'Time Out',
    Verifying: 'Verifying',
    Other: 'Other',
    Pending: 'Pending',
}

exports.StatusIntEnum = {
    Book: 1,
    Paid: 2,
    Cancelled: 3,
    TimeOut: 4,
    Verifying: 5,
    Other: 6,
    Pending: 7,
}

exports.StatusPemainStringEnum = {
    Inactive: 'Inactive',
    Active: 'Active',
    Banned: 'Banned',
    Expired: 'Expired',
}

exports.StatusPemainIntEnum = {
    Inactive: 1,
    Active: 2,
    Banned: 3,
    Expired: 4,
}

exports.StatusTransaksiStringEnum = {
    Book: 'Book',
    Paid: 'Paid',
    Cancelled: 'Cancelled',
    TimeOut: "Time Out",
    Verifying: 'Verifying',
    Other: 'Other',
    Pending: 'Pending',
    BookOperator: "Book - Operator Offline",
    BookNetwork: "Book - Network Offline",
}

exports.StatusTransaksiIntEnum = {
    Book: 1,
    Paid: 2,
    Cancelled: 3,
    TimeOut: 4,
    Verifying: 5,
    Other: 6,
    Pending: 7,
    BookOperator: 8,
    BookNetwork: 9,
}

exports.TipeActivityStringEnum = {
    Credit: 'Credit',
    Debit: 'Debit',
}

exports.TipeActivityIntEnum = {
    Credit: 1,
    Debit: 2,
}

exports.TipePemainStringEnum = {
    Publish: 'Publish',
    Visitor: 'Visitor',
    Club: 'Mandiri',
    QMember: 'QMember',
    Prospect: 'Prospect',
    Agent: 'Agent',
}

exports.TipePemainIntEnum = {
    Publish: 1,
    Visitor: 2,
    Club: 3,
    QMember: 4,
    Prospect: 5,
    Agent: 6,
}

exports.TransactionTypeStringEnum = {
    Reservation: 'Reservation',
    RegistrationQ: 'RegistrationQ',
    Event: 'Event',
    MobilePulsaPLNPostpaid: "Mobile Pulsa - PLN Postpaid",
    MobilePulsaBPJS: "Mobile Pulsa - BPJS",
    MobilePulsaPLNPrepaid: "Mobile Pulsa - PLN Prepaid",
    MobilePulsaPhonePrepaid: "Mobile Pulsa - Phone Credit"
}

exports.TransactionTypeIntEnum = {
    Reservation: 0,
    RegistrationQ: 1,
    Event: 2,
    MobilePulsaPLNPostpaid: 10,
    MobilePulsaBPJS: 11,
    MobilePulsaPLNPrepaid: 15,
    MobilePulsaPhonePrepaid: 16,
}

export default class Enums extends React.Component
{
    static TipePemainEnum = (enumvar) => {
        if (enumvar == TipePemainIntEnum.Publish) {
            return TipePemainStringEnum.Publish;
        }
        else if(enumvar == TipePemainIntEnum.Visitor)
        {
            return TipePemainStringEnum.Visitor;
        }
        else if(enumvar == TipePemainIntEnum.Club)
        {
            return TipePemainStringEnum.Club;
        }
        else if(enumvar == TipePemainIntEnum.QMember)
        {
            return TipePemainStringEnum.QMember;
        }
        else if(enumvar == TipePemainIntEnum.Prospect)
        {
            return TipePemainStringEnum.Prospect;
        }
        else if(enumvar == TipePemainIntEnum.Agent)
        {
            return TipePemainStringEnum.Agent;
        }
        else
        {
            return TipePemainStringEnum.Publish;
        }
    }

    static TransactionTypeEnum = (enumvar) => {
        switch (enumvar) {
            case TransactionTypeIntEnum.Reservation:
                return TransactionTypeStringEnum.Reservation;
            case TransactionTypeIntEnum.RegistrationQ:
                return TransactionTypeStringEnum.RegistrationQ;
    
            case TransactionTypeIntEnum.Event:
                return TransactionTypeStringEnum.Event;
            case TransactionTypeIntEnum.MobilePulsaPLNPostpaid:
                return TransactionTypeStringEnum.MobilePulsaPLNPostpaid;
    
            case TransactionTypeIntEnum.MobilePulsaBPJS:
                return TransactionTypeStringEnum.MobilePulsaBPJS;
            case TransactionTypeIntEnum.MobilePulsaPLNPrepaid:
                return TransactionTypeStringEnum.MobilePulsaPLNPrepaid;
    
            case TransactionTypeIntEnum.MobilePulsaPhonePrepaid:
                return TransactionTypeStringEnum.MobilePulsaPhonePrepaid;
        }
    }

    static TipeActivityEnum = (enumvar) => {
        if (enumvar == TipeActivityIntEnum.Credit){
            return TipeActivityStringEnum.Credit;
        } else if(enumvar == TipeActivityIntEnum.Debit) {
            return TipeActivityStringEnum.Debit;
        }
    }

    static OptionEnum = (enumvar) => {
        switch (enumvar) {
            case OptionIntEnum.OptionMaintenancePeriod:
                return OptionStringEnum.OptionMaintenancePeriod;
            case OptionIntEnum.OptionGlobalMessage:
                return OptionStringEnum.OptionGlobalMessage;
            case OptionIntEnum.OptionPublishQuota:
                return OptionStringEnum.OptionPublishQuota;
            case OptionIntEnum.OptionReservation:
                return OptionStringEnum.OptionReservation;
            case OptionIntEnum.OptionMobilePulsa:
                return OptionStringEnum.OptionMobilePulsa;
            case OptionIntEnum.OptionPLN:
                return OptionStringEnum.OptionPLN;
            case OptionIntEnum.OptionBPJS:
                return OptionStringEnum.OptionBPJS;
            case OptionIntEnum.OptionEvent:
                return OptionStringEnum.OptionEvent;
            case OptionIntEnum.OptionBankTransferBCAReservation:
                return OptionStringEnum.OptionBankTransferBCAReservation;
            case OptionIntEnum.OptionBankTransferBCARegistration:
                return OptionStringEnum.OptionBankTransferBCARegistration;
            case OptionIntEnum.OptionBankTransferBCARenewal:
                return OptionStringEnum.OptionBankTransferBCARenewal;
            case OptionIntEnum.OptionBankTransferBCAMobilePulsa:
                return OptionStringEnum.OptionBankTransferBCAMobilePulsa;
            case OptionIntEnum.OptionBankTransferBCAPLN:
                return OptionStringEnum.OptionBankTransferBCAPLN;
            case OptionIntEnum.OptionBankTransferBCABPJS:
                return OptionStringEnum.OptionBankTransferBCABPJS;
            case OptionIntEnum.OptionBankTransferBCAEvent:
                return OptionStringEnum.OptionBankTransferBCAEvent;
            case OptionIntEnum.OptionBankTransferPermataReservation:
                return OptionStringEnum.OptionBankTransferPermataReservation;
            case OptionIntEnum.OptionBankTransferPermataRegistration:
                return OptionStringEnum.OptionBankTransferPermataRegistration;
            case OptionIntEnum.OptionBankTransferPermataRenewal:
                return OptionStringEnum.OptionBankTransferPermataRenewal;
            case OptionIntEnum.OptionBankTransferPermataMobilePulsa:
                return OptionStringEnum.OptionBankTransferPermataMobilePulsa;
            case OptionIntEnum.OptionBankTransferPermataPLN:
                return OptionStringEnum.OptionBankTransferPermataPLN;
            case OptionIntEnum.OptionBankTransferPermataBPJS:
                return OptionStringEnum.OptionBankTransferPermataBPJS;
            case OptionIntEnum.OptionBankTransferPermataEvent:
                return OptionStringEnum.OptionBankTransferPermataEvent;
            case OptionIntEnum.OptionBankTransferMandiriReservation:
                return OptionStringEnum.OptionBankTransferMandiriReservation;
            case OptionIntEnum.OptionBankTransferMandiriRegistration:
                return OptionStringEnum.OptionBankTransferMandiriRegistration;
            case OptionIntEnum.OptionBankTransferMandiriRenewal:
                return OptionStringEnum.OptionBankTransferMandiriRenewal;
            case OptionIntEnum.OptionBankTransferMandiriMobilePulsa:
                return OptionStringEnum.OptionBankTransferMandiriMobilePulsa;
            case OptionIntEnum.OptionBankTransferMandiriPLN:
                return OptionStringEnum.OptionBankTransferMandiriPLN;
            case OptionIntEnum.OptionBankTransferMandiriBPJS:
                return OptionStringEnum.OptionBankTransferMandiriBPJS;
            case OptionIntEnum.OptionBankTransferMandiriEvent:
                return OptionStringEnum.OptionBankTransferMandiriEvent;
            case OptionIntEnum.OptionCreditCardReservation:
                return OptionStringEnum.OptionCreditCardReservation;
            case OptionIntEnum.OptionCreditCardRegistration:
                return OptionStringEnum.OptionCreditCardRegistration;
            case OptionIntEnum.OptionCreditCardRenewal:
                return OptionStringEnum.OptionCreditCardRenewal;
            case OptionIntEnum.OptionCreditCardMobilePulsa:
                return OptionStringEnum.OptionCreditCardMobilePulsa;
            case OptionIntEnum.OptionCreditCardPLN:
                return OptionStringEnum.OptionCreditCardPLN;
            case OptionIntEnum.OptionCreditCardBPJS:
                return OptionStringEnum.OptionCreditCardBPJS;
            case OptionIntEnum.OptionCreditCardEvent:
                return OptionStringEnum.OptionCreditCardEvent;
            case OptionIntEnum.OptionPointReservation:
                return OptionStringEnum.OptionPointReservation;
            case OptionIntEnum.OptionPointRegistration:
                return OptionStringEnum.OptionPointRegistration;
            case OptionIntEnum.OptionPointRenewal:
                return OptionStringEnum.OptionPointRenewal;
            case OptionIntEnum.OptionPointMobilePulsa:
                return OptionStringEnum.OptionPointMobilePulsa;
            case OptionIntEnum.OptionPointPLN:
                return OptionStringEnum.OptionPointPLN;
            case OptionIntEnum.OptionPointBPJS:
                return OptionStringEnum.OptionPointBPJS;
            case OptionIntEnum.OptionPointEvent:
                return OptionStringEnum.OptionPointEvent;
            case OptionIntEnum.OptionSharePaymentLinkReservation:
                return OptionStringEnum.OptionSharePaymentLinkReservation;
            case OptionIntEnum.OptionSharePaymentLinkRegistration:
                return OptionStringEnum.OptionSharePaymentLinkRegistration;
            case OptionIntEnum.OptionSharePaymentLinkRenewal:
                return OptionStringEnum.OptionSharePaymentLinkRenewal;
            case OptionIntEnum.OptionSharePaymentLinkMobilePulsa:
                return OptionStringEnum.OptionSharePaymentLinkMobilePulsa;
            case OptionIntEnum.OptionSharePaymentLinkPLN:
                return OptionStringEnum.OptionSharePaymentLinkPLN;
            case OptionIntEnum.OptionSharePaymentLinkBPJS:
                return OptionStringEnum.OptionSharePaymentLinkBPJS;
            case OptionIntEnum.OptionSharePaymentLinkEvent:
                return OptionStringEnum.OptionSharePaymentLinkEvent;
            case OptionIntEnum.OptionPaymentInstallmentRegistration:
                return OptionStringEnum.OptionPaymentInstallmentRegistration;
            case OptionIntEnum.OptionPaymentInstallmentRenewal:
                return OptionStringEnum.OptionPaymentInstallmentRenewal;
            case OptionIntEnum.OptionRenewal:
                return OptionStringEnum.OptionRenewal;
        }
    }

    static AccountBalanceEnum = (enumvar) => {
        switch (enumvar) {
            case AccountBalanceIntEnum.Registration:
                return AccountBalanceStringEnum.Registration;
            case AccountBalanceIntEnum.Referral:
                return AccountBalanceStringEnum.Referral;
            case AccountBalanceIntEnum.Booking:
                return AccountBalanceStringEnum.Booking;
            case AccountBalanceIntEnum.BookingReferral:
                return AccountBalanceStringEnum.BookingReferral;
            case AccountBalanceIntEnum.TopUp:
                return AccountBalanceStringEnum.TopUp;
            case AccountBalanceIntEnum.Redeem:
                return AccountBalanceStringEnum.Redeem;
            case AccountBalanceIntEnum.CancelRedeem:
                return AccountBalanceStringEnum.CancelRedeem;
            case AccountBalanceIntEnum.Expired:
                return AccountBalanceStringEnum.Expired;
            case AccountBalanceIntEnum.Refund:
                return AccountBalanceStringEnum.Refund;
            case AccountBalanceIntEnum.RefundReferral:
                return AccountBalanceStringEnum.RefundReferral;
            case AccountBalanceIntEnum.Cashback:
                return AccountBalanceStringEnum.Cashback;
            case AccountBalanceIntEnum.AgentRegCommission:
                return AccountBalanceStringEnum.AgentRegCommission;
            case AccountBalanceIntEnum.AgentEventCommission:
                return AccountBalanceStringEnum.AgentEventCommission;
        }
    }
    
    static EventSessionEnum = (enumvar) => {
        switch (enumvar) {
            case EventSessionIntEnum.Morning:
                return EventSessionStringEnum.Morning;
            case EventSessionIntEnum.Afternoon:
                return EventSessionStringEnum.Afternoon;
            case EventSessionIntEnum.FullDay:
                return EventSessionStringEnum.FullDay;
        }
    }

    static StatusTransaksiEnum = (enumvar) => {
        switch (enumvar) {
            case StatusTransaksiIntEnum.Book:
                return StatusTransaksiStringEnum.Book;
            case StatusTransaksiIntEnum.Paid:
                return StatusTransaksiStringEnum.Paid;
    
            case StatusTransaksiIntEnum.Cancelled:
                return StatusTransaksiStringEnum.Cancelled;
            case StatusTransaksiIntEnum.TimeOut:
                return StatusTransaksiStringEnum.TimeOut;
    
            case StatusTransaksiIntEnum.Verifying:
                return StatusTransaksiStringEnum.Verifying;
            case StatusTransaksiIntEnum.Other:
                return StatusTransaksiStringEnum.Other;
    
            case StatusTransaksiIntEnum.Pending:
                return StatusTransaksiStringEnum.Pending;
            case StatusTransaksiIntEnum.BookOperator:
                return StatusTransaksiStringEnum.BookOperator;
    
            case StatusTransaksiIntEnum.BookNetwork:
                return StatusTransaksiStringEnum.BookNetwork;
        }
    }

    static MemberCardDaySessionEnum = (enumvar) => {
        switch (enumvar) {
            case MemberCardDaySesionIntEnum.MONM:
                return MemberCardDaySesionStringEnum.MONM;
            case MemberCardDaySesionIntEnum.MONA:
                return MemberCardDaySesionStringEnum.MONA;
            case MemberCardDaySesionIntEnum.TUEM:
                return MemberCardDaySesionStringEnum.TUEM;
            case MemberCardDaySesionIntEnum.TUEA:
                return MemberCardDaySesionStringEnum.TUEA;
            case MemberCardDaySesionIntEnum.WEDM:
                return MemberCardDaySesionStringEnum.WEDM;
            case MemberCardDaySesionIntEnum.WEDA:
                return MemberCardDaySesionStringEnum.WEDA;
            case MemberCardDaySesionIntEnum.THUM:
                return MemberCardDaySesionStringEnum.THUM;
            case MemberCardDaySesionIntEnum.THUA:
                return MemberCardDaySesionStringEnum.THUA;
            case MemberCardDaySesionIntEnum.FRIM:
                return MemberCardDaySesionStringEnum.FRIM;
            case MemberCardDaySesionIntEnum.FRIA:
                return MemberCardDaySesionStringEnum.FRIA;
            case MemberCardDaySesionIntEnum.SATM:
                return MemberCardDaySesionStringEnum.SATM;
            case MemberCardDaySesionIntEnum.SUNM:
                return MemberCardDaySesionStringEnum.SUNM;
        }
    }
    
    static NotificationEnum = (enumvar) => {
        switch (enumvar) {
            case NotificationIntEnum.Other:
                return NotificationStringEnum.Other;
            case NotificationIntEnum.Partnership:
                return NotificationStringEnum.Partnership;
            case NotificationIntEnum.Event:
                return NotificationStringEnum.Event;
        }
    }
    
    static NotificationUserStatusEnum = (enumvar) => {
        switch (enumvar) {
            case NotificationUserStatusIntEnum.Unread:
                return NotificationUserStatusStringEnum.Unread;
            case NotificationUserStatusIntEnum.Read:
                return NotificationUserStatusStringEnum.Read;
        }
    }
    
    static PaymentEnum = (enumvar) => {
        switch (enumvar) {
            case PaymentIntEnum.NotSpecified:
                return PaymentStringEnum.NotSpecified;
            case PaymentIntEnum.BankTransfer:
                return PaymentStringEnum.BankTransfer;
            case PaymentIntEnum.CreditCard:
                return PaymentStringEnum.CreditCard;
            case PaymentIntEnum.Other:
                return PaymentStringEnum.Other;
            case PaymentIntEnum.FullPoint:
                return PaymentStringEnum.FullPoint;  
        }
    }
    
    static PlatformEnum = (enumvar) => {
        switch (enumvar) {
            case PlatformIntEnum.Android:
                return PlatformStringEnum.Android;
            case PlatformIntEnum.IOs:
                return PlatformStringEnum.IOs;
            case PlatformIntEnum.Online:
                return PlatformStringEnum.Online;
        }
    }
    
    static RefundStatusEnum = (enumvar) => {
        switch (enumvar) {
            case RefundStatusIntEnum.Process:
                return RefundStatusStringEnum.Process;
            case RefundStatusIntEnum.Rejected:
                return RefundStatusStringEnum.Rejected;
            case RefundStatusIntEnum.Approved:
                return RefundStatusStringEnum.Approved;
        }
    }
    
    static SessionEnum = (enumvar) => {
        switch (enumvar) {
            case SessionIntEnum.Morning:
                return SessionStringEnum.Morning;
            case SessionIntEnum.Afternoon:
                return SessionStringEnum.Afternoon;
        }
    }
    
    static StatusEnum = (enumvar) => {
        if(enumvar == StatusIntEnum.Book){
            return StatusStringEnum.Book;
        }
        else if(enumvar == StatusIntEnum.Paid){
            return StatusStringEnum.Paid;
        }
        else if(enumvar == StatusIntEnum.Cancelled){
            return StatusStringEnum.Cancelled;
        }
        else if(enumvar == StatusIntEnum.TimeOut){
            return StatusStringEnum.TimeOut;
        }
        else if(enumvar == StatusIntEnum.Verifying){
            return StatusStringEnum.Verifying;
        }
        else if(enumvar == StatusIntEnum.Other){
            return StatusStringEnum.Other;
        }  
        else if(enumvar == StatusIntEnum.Pending){
            return StatusStringEnum.Pending;
        }
    }

    static StatusEnum2 = (enumvar) => {
        if(enumvar == StatusIntEnum.Book){
            return "Waiting for Payment Method";
        }
        else if(enumvar == StatusIntEnum.Paid){
            return StatusStringEnum.Paid;
        }
        else if(enumvar == StatusIntEnum.Cancelled){
            return StatusStringEnum.Cancelled;
        }
        else if(enumvar == StatusIntEnum.TimeOut){
            return StatusStringEnum.TimeOut;
        }
        else if(enumvar == StatusIntEnum.Verifying){
            return StatusStringEnum.Verifying;
        }
        else if(enumvar == StatusIntEnum.Other){
            return StatusStringEnum.Other;
        }  
        else if(enumvar == StatusIntEnum.Pending){
            return StatusStringEnum.Pending;
        }
    }
    
    static StatusPemainEnum = (enumvar) => {
        switch (enumvar) {
            case StatusPemainIntEnum.Inactive:
                return StatusPemainStringEnum.Inactive;
            case StatusPemainIntEnum.Active:
                return StatusPemainStringEnum.Active;
            case StatusPemainIntEnum.Banned:
                return StatusPemainStringEnum.Banned;
            case StatusPemainIntEnum.Expired:
                return StatusPemainStringEnum.Expired;
        }
    }
    
    static TipePemainEnumConvert = (enumvar) => {
        if (enumvar === TipePemainStringEnum.Publish) {
            return TipePemainIntEnum.Publish;
        }
        else if(enumvar === TipePemainStringEnum.Visitor)
        {
            return TipePemainIntEnum.Visitor;
        }
        else if(enumvar === TipePemainStringEnum.Club)
        {
            return TipePemainIntEnum.Club;
        }
        else if(enumvar === TipePemainStringEnum.QMember)
        {
            return TipePemainIntEnum.QMember;
        }
        else if(enumvar === TipePemainStringEnum.Prospect)
        {
            return TipePemainIntEnum.Prospect;
        }
        else if(enumvar === TipePemainStringEnum.Agent)
        {
            return TipePemainIntEnum.Agent;
        }
        else
        {
            return TipePemainIntEnum.Publish;
        }
    }

    static KartuEnum = (enumvar) => {
        switch (enumvar) {
            case KartuIntEnum.Blank:
                return KartuStringEnum.Blank;
            case KartuIntEnum.Easy:
                return KartuStringEnum.Easy;
            case KartuIntEnum.Premium:
                return KartuStringEnum.Premium;
            case KartuIntEnum.Corporate:
                return KartuStringEnum.Corporate;
            case KartuIntEnum.Elder:
                return KartuStringEnum.Elder;
            case KartuIntEnum.Mandiri:
                return KartuStringEnum.Mandiri;
        }
    }
}
