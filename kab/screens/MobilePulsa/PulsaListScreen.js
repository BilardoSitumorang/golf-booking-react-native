import React from 'react';
import {
    Image,
    Platform,
    ScrollView,
    StyleSheet,
    SafeAreaView,
    TouchableOpacity,
    ListView,
    Text,
    View,
    AsyncStorage,
    ActivityIndicator,
} from 'react-native';
import Colors from '../../constants/Colors';
import Strings from '../../constants/Strings';
import PulsaRowScreen from '../ListRow/PulsaRowScreen';
import { getProvidesPulsa, getProvidesData, postCreateTrans, } from '../../API/PulsaService';
import { TransactionTypeIntEnum, PlatformIntEnum } from '../../constants/Enums';
import PulsaTransactionModel from '../../Model/PulsaTransactionModel';
import { getOptions,  } from '../../API/OptionService';
import Utils from '../Utils';

export default class PulsaListScreen extends React.Component {
    static navigationOptions = {
        title: '',
        header: null
    };

    constructor(props){
        super(props);

        this.state = {
            username: this.props.username,
            phone: this.props.phone,
        }
    }

    GetItem (row) {
        //Selected Row List
        var platform = Platform.OS === 'ios' ? this.props.pulsaOption.iOS : this.props.pulsaOption.Android;
        if(Utils.getMaintenanceOption(this.props.maintenanceOption)){
            Utils.alertBackToMain(this.props.maintenanceOption.Note, this.props);
        }
        else if(!platform) {
            Utils.errAlert("QAccess Alert", this.props.pulsaOption.Note);
        }
        else if(this.props.phone.length < 8) {
            Utils.errAlert("QAccess Alert", 'Phone number is less than 8 character.');
        }
        else if (row.Nominal != 0)
        {
            var platformMobile = Platform.OS === 'ios' ? PlatformIntEnum.IOs : PlatformIntEnum.Android;

            var pulsatrans = new PulsaTransactionModel(this.state.username, row.Code, row.Description, row.Nominal, row.ActivePeriod,
                this.props.phone, null, row.Type, row.Price + row.KABFee, platformMobile, TransactionTypeIntEnum.MobilePulsaPhonePrepaid );
            postCreateTrans(pulsatrans).then(async (jsonAddMember) => 
            {
                this.setState({isLoading: false, });
                await AsyncStorage.setItem("BookingCode", jsonAddMember);
                this.props.property.navigation.navigate('Payment', {TransType: TransactionTypeIntEnum.MobilePulsaPhonePrepaid});
            })
            .catch((error) => {
                console.error(error);
                this.setState({isLoading: false, });
            });
        }
    }

    ListViewItemSeparator = () => {
        return (
            <View style={{ height: .5, width: "100%", backgroundColor: "#414141", }} />
        );
    }

    render() {

        const gotoPulsa = () => {
            this.props.navigation.navigate('Pulsa')
        }

        return (
            <SafeAreaView style={styles.safeAreaView}>
                <View style={styles.container}>
                    <ListView
                        dataSource={this.props.dataSource}
                        renderSeparator={this.ListViewItemSeparator}
                        renderRow={(rowData) =>
                            <TouchableOpacity 
                                onPress={ this.GetItem.bind(this, rowData) } >
                                <PulsaRowScreen
                                    Nominal={rowData.Nominal}
                                    Note={rowData.ActivePeriod + " day active period"}
                                    Total={"Rp. " + Utils.currencyfunc(Math.round((rowData.Price + rowData.KABFee)*100)/100)}/>
                            </TouchableOpacity>
                        }
                    />
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({

    safeAreaView: {
        flex: 1,
    },

    containerScroll:
    {
        flex: 1,
        backgroundColor: '#efefef',
    },

    container: {
        flex: 1,
        flexDirection: 'column',
    },

    card: {
        backgroundColor: Colors.backgroundColor,
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 10,
        marginRight: 10,
        marginTop: 10,
        marginBottom: 5,
        padding: 10,
        elevation: 2,
        borderRadius: 5,
    },

});