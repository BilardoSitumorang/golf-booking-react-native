import React from 'react';
import {
    Image,
    Dimensions, 
    StyleSheet,
    AsyncStorage,
    View,
    Text,
    ScrollView,
    ActivityIndicator,
    ListView,
    SafeAreaView,
    TouchableOpacity,
    Animated,
} from 'react-native';

import Colors from '../../constants/Colors';
import Strings from '../../constants/Strings';
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import AccountBalanceDetailListScreen from './AccountBalanceDetailListScreen';
import clsSetting from '../../API/clsSetting';
import { getActiveBalance, getPendingBalance, } from '../../API/TransactionService';
import { Constants } from 'expo';

export default class AccountBalanceDetailScreen extends React.Component {
    static navigationOptions = {
        title: 'Point Balance',
        headerTintColor: '#ffffff', 
        headerStyle: {
            backgroundColor: '#93b843'
        },
    };

    constructor(props){
        super(props);

        this.params = this.props.navigation.state.params;
    }

    state = {
        isLoading: true,
        index: 0,
        routes: [
            { key: 'active', title: 'ACTIVE' },
            { key: 'pending', title: 'PENDING' },
        ],
        username: '',
    };

    async componentDidMount() {
        this.setState({ username : await AsyncStorage.getItem('Username')});
        getActiveBalance(this.state.username).then((jsonActive) =>
        {
            let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
            this.setState({
                dataSourceActive: ds.cloneWithRows(jsonActive.PointActivity),
            });

            getPendingBalance(this.state.username).then((jsonPending) =>
            {
                this.setState({
                    isLoading: false,
                    dataSourcePending: ds.cloneWithRows(jsonPending.PointActivity),
                });
            })
        })
        .catch((error) => {
            console.error(error);
            this.setState({ isLoading: false });
        });
    }

    _renderTabBar = props => {
        const inputRange = props.navigationState.routes.map((x, i) => i);
    
        return (
            <View style={styles.tabBar}>
                {props.navigationState.routes.map((route, i) => {
                    const color = props.position.interpolate({
                        inputRange,
                        outputRange: inputRange.map(
                        inputIndex => (inputIndex === i ? '#93b843' : '#414141')
                    ),
                });
                return (
                    <TouchableOpacity
                        style={styles.tabItem}
                        onPress={() => this.setState({ index: i })}>
                            <Animated.Text style={[{color}, {fontSize: 15,}]}>{route.title}</Animated.Text>
                    </TouchableOpacity>
              );
            })}
          </View>
        );
    };

    render() {
        const Active = () => (
            <AccountBalanceDetailListScreen status={'active'} dataSource={this.state.dataSourceActive}/>
        );
        
        const Pending = () => (
            <AccountBalanceDetailListScreen type={'pending'} dataSource={this.state.dataSourcePending}/>
        );

        if (this.state.isLoading) {
            return (
                <View style={{flex: 1, paddingTop: 20}}>
                    <ActivityIndicator />
                </View>
            );
        }

        return (
            <SafeAreaView style={styles.safeAreaView}>
                <View style={styles.container}>
                    <View style={{flexDirection: 'row', backgroundColor: '#fff', alignItems: 'center', padding: 5}}>
                        <View style={{flex:1, flexDirection: 'row', marginTop: 5, marginBottom: 5,  marginLeft: 15, marginRight: 15,
                            justifyContent: 'center', alignContent: 'center', }}>
                            <Image style={styles.image} 
                                source={require('../../assets/images/yellowmedal.png')}/>
                            <Text style={{fontSize: 17, color: '#414141', marginLeft: 5, fontWeight: 'bold', }} >
                                {(this.params.Point / clsSetting.AccountBalance)}</Text>
                            <Text style={{fontSize: 17, color: '#414141', marginLeft: 3, }} >
                                {'QPoint'}</Text>
                        </View>
                    </View>
                    <TabView
                        navigationState={this.state}
                        renderScene={SceneMap({
                            active: Active,
                            pending: Pending,
                        })}
                        renderTabBar={this._renderTabBar}
                        onIndexChange={index => this.setState({ index })}
                        initialLayout={{ height: 0, width: Dimensions.get('window').width }}
                    />
                </View>
            </SafeAreaView>
        );
    }   
}

const styles = StyleSheet.create({
    safeAreaView: {
        flex: 1
    },

    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#efefef',
    },

    image: {
        width: 30,
        height: 30,
    },

    tabBar: {
        flexDirection: 'row',
        paddingTop: Constants.statusBarHeight,
        backgroundColor: '#fff',
    },

    tabItem: {
        flex: 1,
        alignItems: 'center',
        padding: 5,
    },
});
