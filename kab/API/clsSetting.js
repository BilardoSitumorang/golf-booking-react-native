
/** Live */
export default {
    APIUrl: 'https://www.qaccess.asia/api/',
    HomeUrl: 'https://www.qaccess.asia/',
    PartnershipUrl: 'https://www.qaccess.asia/partnership',
    EventUrl: 'https://www.qaccess.asia/event',
    PaymentUrl: 'https://www.qaccess.asia/payment',
    PaymentLinkUrl: 'https://www.qaccess.asia/booking/payment?code=',
    BankTransferPaymentUrl: 'https://www.qaccess.asia/banktransfer',
    CreditCardPaymentUrl: 'https://www.qaccess.asia/creditcard',
    PaymentSuccessUrl: PaymentUrl + "/PaymentSuccess",
    BankTransferPaymentSuccessUrl: BankTransferPaymentUrl + "/PaymentSuccess",
    CreditCardPaymentSuccessUrl: CreditCardPaymentUrl + "/PaymentSuccess",
    PaymentFailedUrl: PaymentUrl + " /PaymentFailed",
    BankTransferPaymentFailedUrl: BankTransferPaymentUrl + " /PaymentFailed",
    CreditCardPaymentFailedUrl: CreditCardPaymentUrl + " /PaymentFailed",
    ImageUrl: "https://www.qaccess.asia/images/",
    ApiSecretKey: "e2248ccb8bf34b73a776cebb120746e2htk",
    KeyPulsa: "3875b8e3834c0a54387",
    GoogleAPIKey: "AIzaSyC8j75orXrh4w2QOypDOf8iC3RikK2c3x8",
    AccountBalance: 1000,
    FaqUrl: "https://www.qaccess.asia/faq",
    AllowPublish: "AllowPublishQuota",
    AllowGlobalMessage: "AllowGlobalMessage",
    CustomSplashScreen: "CustomSplashScreen",
    VersionMobile: '452075',
};

/** SIT */
//export default {
    // APIUrl: "https://sit.qaccess.asia/api/",
    // HomeUrl: "https://sit.qaccess.asia/",
    // PartnershipUrl: "https://sit.qaccess.asia/partnership",
    // EventUrl: "https://sit.qaccess.asia/event",
    // PaymentUrl: "https://sit.qaccess.asia/payment",
    // PaymentLinkUrl: "https://sit.qaccess.asia/booking/payment?code=",
    // BankTransferPaymentUrl: "https://sit.qaccess.asia/banktransfer",
    // CreditCardPaymentUrl: "https://sit.qaccess.asia/creditcard",
    // PaymentSuccessUrl: "https://sit.qaccess.asia/payment/PaymentSuccess",
    // BankTransferPaymentSuccessUrl: "https://sit.qaccess.asia/banktransfer/PaymentSuccess",
    // CreditCardPaymentSuccessUrl: "https://sit.qaccess.asia/creditcard/PaymentSuccess",
    // PaymentFailedUrl: "https://sit.qaccess.asia/payment/PaymentFailed",
    // BankTransferPaymentFailedUrl: "https://sit.qaccess.asia/banktransfer/PaymentFailed",
    // CreditCardPaymentFailedUrl: "https://sit.qaccess.asia/creditcard/PaymentFailed",
    // ImageUrl: "https://www.qaccess.asia/images/",

    // ApiSecretKey: "e2248ccb8bf34b73a776cebb120746e2htk",
    // KeyPulsa: "7975b695ca9c3f4a",
    // GoogleAPIKey: "AIzaSyC8j75orXrh4w2QOypDOf8iC3RikK2c3x8",

    // AccountBalance: 1000,
    // FaqUrl: "https://www.qaccess.asia/faq",
    // AllowPublish: "AllowPublishQuota",
    // AllowGlobalMessage: "AllowGlobalMessage",
    // CustomSplashScreen: "CustomSplashScreen",
    // VersionMobile: '452075',
//};