import React from 'react';
import { createSwitchNavigator, createStackNavigator } from 'react-navigation';
import LoginScreen from '../screens/LoginScreen';
import RegisterScreen from '../screens/RegisterScreen';
import AuthLoadingScreen from '../screens/AuthLoadingScreen';
import MainTabNavigator from './MainTabNavigator';
import ForgotPasswordScreen from '../screens/ForgotPassowordScreen';
import RegisterLinktoQMemberScreen from '../screens/RegiterLinktoQMemberScreen';
import RegisterPhoneScreen from '../screens/RegisterPhoneScreen';
import RegisterOTPScreen from '../screens/RegisterOTPScreen';
import SearchScreen from '../screens/Reservation/SearchScreen';
import DetailCourseScreen from '../screens/Reservation/DetailCourseScreen';
import BookReservationScreen from '../screens/Reservation/BookReservationScreen';
import ReviewReservationScreen from '../screens/Reservation/ReviewReservationScreen';
import PlayerReservationScreen from '../screens/Reservation/PlayerReservationScreen';
import QCardListCardScreen from '../screens/RegistrationOrRenewal/QCardListCardScreen';
import QCardScreen from '../screens/RegistrationOrRenewal/QCardScreen';
import DetailCardScreen from '../screens/RegistrationOrRenewal/DetailCardScreen';
import RegisterForm1Screen from '../screens/RegistrationOrRenewal/RegisterForm1Screen';
import RegisterForm2Screen from '../screens/RegistrationOrRenewal/RegisterForm2Screen';
import RenewalScreen from '../screens/RegistrationOrRenewal/RenewalScreen';
import AccountBalanceDetailScreen from '../screens/Account/AccountBalanceDetailScreen';
import PointScreen from '../screens/Account/PointScreen';
import UpdateProfileScreen from '../screens/Account/UpdateProfileScreen';
import PaymentScreen from '../screens/Payment/PaymentScreen';
import BankTransferScreen from '../screens/Payment/BankTransferScreen';
import CreditCardScreen from '../screens/Payment/CreditCardScreen';
import TermsReservationScreen from '../screens/Payment/TermsReservationScreen';
import TermsEventScreen from '../screens/Payment/TermsEventScreen';
import TermsPulsaScreen from '../screens/Payment/TermsPulsaScreen';
import TermsRegisterQScreen from '../screens/Payment/TermsRegisterQScreen';
import PendingBankTransferScreen from '../screens/Payment/PendingBankTransferScreen';
import BillsandTopupScreen from '../screens/MobilePulsa/BillsandTopupScreen';
import PulsaScreen from '../screens/MobilePulsa/PulsaScreen';
import ListEventScreen from '../screens/Event/ListEventScreen';
import DetailEventScreen from '../screens/Event/DetailEventScreen';
import InviteFriendsScreen from '../screens/InviteFriendsScreen';
import OurPartnerScreen from '../screens/OurPartners/OurPartnerScreen';
import MerchantScreen from '../screens/OurPartners/MerchantScreen';
import DetailMerchantScreen from '../screens/OurPartners/DetailMerchantScreen';
import DetailPartnershipScreen from '../screens/OurPartners/DetailPartnershipScreen';
import BankTransfer3DSScreen from '../screens/Payment/BankTransfer3DSScreen';
import CreditCardDSScreen from '../screens/Payment/CreditCard3DSScreen';
import TicketRegistrationScreen from '../screens/Payment/TicketRegistrationScreen';
import TicketReservationScreen from '../screens/Payment/TicketReservationScreen';
import TicketPulsaScreen from '../screens/Payment/TicketPulsaScreen';
import TicketEventScreen from '../screens/Payment/TicketEventScreen';
import ListCourseScreen from '../screens/Reservation/ListCourseScreen';
import BookEventScreen from '../screens/Event/BookEventScreen';
import PlayerEventScreen from '../screens/Event/PlayerEventScreen';
import ListEventPriceScreen from '../screens/Event/ListEventPriceScreen';
import LinkQMemberScreen from '../screens/Account/LinkQMemberScreen';
import CustomerCallScreen from '../screens/CustomerCallScreen';
import OrdersHistoryScreen from '../screens/OrdersHistoryScreen';

// Implementation of HomeScreen, OtherScreen, SignInScreen, AuthLoadingScreen
// goes here.

const AppStack = createStackNavigator({ 
    Main: {
            screen: MainTabNavigator, 
            navigationOptions: {
                header: null
            }
    },
    CustomerCall: CustomerCallScreen,
    OrdersHistory: OrdersHistoryScreen,

    //Reservation
    Courses: ListCourseScreen,
    Search: SearchScreen,
    DetailCourse: DetailCourseScreen,
    BookReservation: BookReservationScreen, 
    PlayerReservation: PlayerReservationScreen,
    ReviewReservation: ReviewReservationScreen,

    //RegisterQ or Renewal
    QCard: QCardScreen,
    QCardList: QCardListCardScreen,
    DetailCard: DetailCardScreen,
    Form1: RegisterForm1Screen,
    Form2: RegisterForm2Screen,
    Renewal: RenewalScreen,

    //Payment
    Payment: PaymentScreen,
    BankTransfer: BankTransferScreen,
    CreditCard: CreditCardScreen,
    TermsReservation: TermsReservationScreen,
    TermsEvent: TermsEventScreen,
    TermsPulsa: TermsPulsaScreen,
    TermsRegisterQ: TermsRegisterQScreen,
    PendingBank: PendingBankTransferScreen,
    BankTransfer3DS: BankTransfer3DSScreen,
    CreditCard3DS : CreditCardDSScreen,
    TicketReservation: TicketReservationScreen,
    TicketRegistration: TicketRegistrationScreen,
    TicketPulsa: TicketPulsaScreen,
    TicketEvent: TicketEventScreen,

    //Bills And TopUp
    BillAndTopUp: BillsandTopupScreen,
    Pulsa: PulsaScreen,

    //Event
    ListEvent: ListEventScreen,
    DetailEvent: DetailEventScreen,
    BookEvent: BookEventScreen,
    PlayerEvent: PlayerEventScreen,
    ListEventPrice: ListEventPriceScreen,

    //Account
    AccountBalanceDetail: AccountBalanceDetailScreen,
    Point: PointScreen,
    UpdateProfile: UpdateProfileScreen,
    linkqmember: LinkQMemberScreen,

    //Invite Friends
    InviteFriends: InviteFriendsScreen,

    //Our Partners
    OurPartner: OurPartnerScreen,
    Merchant: MerchantScreen,
    DetailMerchant: DetailMerchantScreen,
    DetailPartnership: DetailPartnershipScreen,
    
});

const AuthStack = createStackNavigator({ 
    SignIn: LoginScreen,
    SignUp: RegisterScreen,
    Forgot: ForgotPasswordScreen,
    RegisterLinkQMember: RegisterLinktoQMemberScreen,
    RegisterPhone: RegisterPhoneScreen,
    RegisterOTP: RegisterOTPScreen,
});

export default createSwitchNavigator(
    {
        AuthLoading: AuthLoadingScreen,
        App: AppStack,
        Auth: AuthStack,
    },
    {
        initialRouteName: 'AuthLoading',
    }
);