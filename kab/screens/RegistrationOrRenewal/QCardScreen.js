import React from 'react';
import {
    Image,
    Platform,
    ScrollView,
    StyleSheet,
    SafeAreaView,
    TouchableOpacity,
    Dimensions,
    View,
} from 'react-native';
import Colors from '../../constants/Colors';
import Strings from '../../constants/Strings';
import {Button,} from 'react-native-elements';
import { Card, CardTitle, CardContent, CardAction, CardButton, CardImage } from 'react-native-material-cards';

export default class QCardScreen extends React.Component {
    static navigationOptions = {
        title: 'QCard',
        headerTintColor: '#ffffff', 
        headerStyle: {
            backgroundColor: '#93b843'
        },
    };

    render() {
        const gotoRegisterQ = () => {
            this.props.navigation.navigate('QCardList')
        }

        const gotoRenewal = () => {
            this.props.navigation.navigate('Renewal')
        }

        const dimensions = Dimensions.get('window');
        const imageHeight = Math.round(dimensions.width * 9 / 16);
        const imageWidth = dimensions.width - 20;

        return (
            <SafeAreaView style={styles.safeAreaView}>
                <ScrollView style={styles.containerScroll}>
                    <View style={styles.container}>
                        <Card style={styles.card}>
                            <TouchableOpacity 
                                onPress={ gotoRegisterQ } >
                                <Image 
                                    style={{height: 170, width: imageWidth, resizeMode: 'contain', }} 
                                    source={require('../../assets/images/RegisterQCard.png')}
                                />
                            </TouchableOpacity>
                        </Card>
                        <Card style={styles.card}>
                            <TouchableOpacity 
                                onPress={ gotoRenewal } >
                                <Image 
                                    style={{width: imageWidth, height: 170, resizeMode: 'contain', }} 
                                    source={require('../../assets/images/Renewal.png')}
                                />
                            </TouchableOpacity>
                        </Card>
                    </View>
                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({

    safeAreaView: {
        flex: 1,
    },

    containerScroll:
    {
        flex: 1,
        backgroundColor: '#ffffff',
    },

    container: {
        flex: 1,
    },

    card:
    {
        height: 170,
        margin: 10,
        elevation: 2,
        borderRadius: 3,
    },
});
