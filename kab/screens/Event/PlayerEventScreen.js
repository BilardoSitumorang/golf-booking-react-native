import React from 'react';
import {
    ScrollView,
    Platform,
    StyleSheet,
    Text,
    SafeAreaView,
    View,
    Image,
    TouchableOpacity,
    ActivityIndicator,
    AsyncStorage,
} from 'react-native';
import Colors from '../../constants/Colors';
import Strings from '../../constants/Strings';
import { Button } from 'react-native-elements';
import { Card } from 'react-native-material-cards';
import BpkTextInput from 'react-native-bpk-component-text-input';
import { getVirtualAccount, checkPlayerType } from '../../API/UserService';
import Enums, { TipePemainIntEnum, TipePemainStringEnum, KartuStringEnum, StatusPemainIntEnum, KartuIntEnum, OptionIntEnum } from '../../constants/Enums';
import Utils from '../Utils';

export default class PlayerEventScreen extends React.Component {
    static navigationOptions = {
        title: 'Player',
        headerTintColor: '#ffffff', 
        headerStyle: {
            backgroundColor: '#93b843'
        },
    };

    constructor(props){
        super(props);

        this.params = this.props.navigation.state.params;

        this.state = {
            isLoading: true,
            username: '',
            playernameinput: '',
            playernoinput: '',
            usertype: '',
            typeMember: '',

            idevent: this.params.IdEvent,
            location: this.params.Location,
            calendar: this.params.Calendar,
            containmandiri: false,

            publishrate: this.params.PublishRate,
            idpublish: this.params.IdPublish,
            clubrate: this.params.ClubRate,
            idclub: this.params.IdClub,
            qrate: this.params.QRate,
            idqrate: this.params.IdQRate,

            total: this.params.Total,

            arrname: JSON.parse(this.params.ArrName),
            arrplayername: JSON.parse(this.params.ArrPlayerName),
            arrtype: JSON.parse(this.params.ArrType),
            arrkartu: JSON.parse(this.params.ArrKartu),
            arridpricing: JSON.parse(this.params.ArrIdPricing),
            arrpricing: JSON.parse(this.params.ArrPricing),
            bookmodel: JSON.parse(this.params.BookModel),

            editedposition: this.params.EditedPosition,
            playernoinput: '',
            txtsebelumnya: '',
            playernameold: '',
            passselection: false,
        }
    }

    async componentDidMount() {
        this.setState({ usertype : await AsyncStorage.getItem('TipeUser')});
        this.setState({ username : await AsyncStorage.getItem('Username')});
        this.setState({
            playernoinput: this.state.arrname[this.state.editedposition] === '' ? '' : this.state.arrname[this.state.editedposition],
            txtsebelumnya: this.state.playernoinput,
            playernameinput: this.state.arrplayername[this.state.editedposition],
            isLoading: false,
        });
    }

    validasiNonPublishPlayer = (_json, _type, _mandiricard) => 
    {
        if(this.state.qQuota <= 0)
        {
            Utils.errAlert("Error Message", Strings.PlayerEvent.errmsg4);
        }
        else if(this.state.playernoinput === '')
        {
            Utils.errAlert("Error Message", Strings.PlayerEvent.errmsg5);
        }
        else if(_json.Status != 0 && _json.Status != StatusPemainIntEnum.Active)
        {
            Utils.errAlert("Error Message", Strings.PlayerEvent.errmsg6);
        }
        else if(_type != TipePemainIntEnum.QMember && _type != TipePemainIntEnum.Club)
        {
            Utils.errAlert("Error Message", Strings.PlayerEvent.errmsg7);
        }
        else if(this.state.playernameinput === '' && _type != TipePemainIntEnum.QMember)
        {
            Utils.errAlert("Error Message", Strings.PlayerEvent.errmsg2);
        }
        else
        {
            if(this.state.playernoinput === this.state.txtsebelumnya && this.state.typeMember != TipePemainIntEnum.Club)
            {
                if(this.state.playernameinput !== '' && this.state.typeMember != TipePemainStringEnum.QMember)
                {
                    this.setPlayerName();
                }
                this.callActivity();
            }
            else if(_mandiricard !== '' && this.state.playernoinput === _mandiricard && this.state.typeMember === KartuIntEnum.Mandiri)
            {
                Utils.errAlert("Error Message", Strings.PlayerEvent.errmsg8);
            }

            var typeName = Enums.TipePemainEnum(_type);

            if(_type == TipePemainIntEnum.Visitor || _type == TipePemainIntEnum.Visitor)
            {
                typeName = TipePemainStringEnum.Publish;
                this.state.arridpricing[this.state.editedposition] = this.state.idpublish;
                this.setState({
                    passselection: this.state.idpublish == 0 ? false : true,
                });
                this.state.arrpricing[this.state.editedposition] = this.state.publishrate;
            }

            var kartu = _json.JenisMember;
            this.state.arrtype[this.state.editedposition] = typeName;
            this.state.arrkartu[this.state.editedposition] = kartu;

            if(_type != TipePemainIntEnum.Publish && _type != TipePemainIntEnum.Club)
            {
                var existname = false;
                if(_type == TipePemainIntEnum.QMember)
                {
                    this.setState({
                        playernameold: this.state.playernameinput,
                        playernameinput: _json.CardName,
                    });
                }
                if(kartu != KartuIntEnum.Corporate)
                {
                    var i;
                    for(i = 0; i < this.state.arrname.length; i++)
                    {
                        if(this.state.arrname[i] === this.state.playernoinput){
                            existname = true;
                        }
                    }

                    if(existname)
                    {
                        Utils.errAlert("Error Message", Strings.PlayerEvent.errmsg9);
                    }
                    else
                    {
                        this.setState({ 
                            passselection: (this.state.idqrate == 0 && this.state.idpublish == 0) ? false : true,
                        });
                        this.state.arridpricing[this.state.editedposition] = (this.state.idqrate != 0) ? this.state.idqrate : this.state.idpublish;
                        this.state.arrpricing[this.state.editedposition] = (this.state.qrate != 0) ? this.state.qrate : this.state.publishrate;
                        this.setArrName();
                        this.setPlayerName();
                        this.callActivity();
                    }
                }
                else
                {
                    if(this.state.arrname.length < 4)
                    {
                        this.setState({ 
                            passselection: (this.state.idqrate == 0 && this.state.idpublish == 0) ? false : true,
                        });
                        this.state.arridpricing[this.state.editedposition] = (this.state.idqrate != 0) ? this.state.idqrate : this.state.idpublish;
                        this.state.arrpricing[this.state.editedposition] = (this.state.qrate != 0) ? this.state.qrate : this.state.publishrate;
                        this.setArrName();
                        this.setPlayerName();

                        var n = 0;
                        var i;
                        for(i = 0; i < this.state.arrname.length; i++){
                            if(this.state.arrname[i] === this.state.playernoinput){
                                n = n + 1;
                            }
                            if(n > 1) {
                                existname = true;
                            }
                        }

                        if(existname)
                        {
                            this.setPlayerName();
                        }
                        this.callActivity();
                    }
                    else
                    {
                        var n = 0;
                        var changename = false;
                        var i;
                        for(i = 0; i < this.state.arrname.length; i++){
                            if(this.state.arrname[i] === this.state.playernoinput){
                                n = n + 1;
                            }

                            if(n >= 3) {
                                existname = true;
                            }

                            if(n > 0)
                            {
                                changename = true;
                            }
                        }

                        if(existname)
                        {
                            if(kartu != KartuIntEnum.Corporate)
                            {
                                Utils.errAlert("Error Message", Strings.PlayerEvent.errmsg9);
                            }
                            else
                            {
                                Utils.errAlert("Error Message", Strings.PlayerEvent.errmsg10);
                            }
                        }
                        else
                        {
                            this.setState({ 
                                passselection: (this.state.idqrate == 0 && this.state.idpublish == 0) ? false : true,
                            });
                            this.state.arridpricing[this.state.editedposition] = (this.state.idqrate != 0) ? this.state.idqrate : this.state.idpublish;
                            this.state.arrpricing[this.state.editedposition] = (this.state.qrate != 0) ? this.state.qrate : this.state.publishrate;
                            this.setArrName();
                            this.setPlayerName();
                            if(changename)
                            {
                                this.state.arrplayername[this.state.editedposition] = this.state.playernameold.slice(0, 1).toUpperCase + "" +
                                    this.state.playernameold.slice(1, this.state.playernameold.length);
                            }
                            this.callActivity();
                        }
                    } 
                }
            }
            else
            {
                this.setArrName();
                this.setPlayerName();
                if(_type == TipePemainIntEnum.Club){
                    this.setState({ 
                        passselection: (this.state.idclub == 0 && this.state.idpublish == 0) ? false : true,
                    });
                    this.state.arridpricing[this.state.editedposition] = (this.state.idclub != 0) ? this.state.idclub : this.state.idpublish;
                    this.state.arrpricing[this.state.editedposition] = (this.state.clubrate != 0) ? this.state.clubrate : this.state.publishrate;
                }
                else{
                    this.setState({ 
                        passselection: (this.state.idqrate == 0 && this.state.idpublish == 0) ? false : true,
                    });
                    this.state.arridpricing[this.state.editedposition] = (this.state.idqrate != 0) ? this.state.idqrate : this.state.idpublish;
                    this.state.arrpricing[this.state.editedposition] = (this.state.qrate != 0) ? this.state.qrate : this.state.publishrate;
                }
                
                this.callActivity();
            }
        }
    }

    setPlayerName = () => 
    {
        this.state.arrplayername[this.state.editedposition] = this.state.playernameinput.slice(0, 1).toUpperCase() + "" +
            this.state.playernameinput.slice(1, this.state.playernameinput.length);
    }

    setArrName = () =>
    {
        this.state.arrname[this.state.editedposition] = this.state.playernoinput;
    }

    getMandiriInList = () =>
    {
        var mandiricard = "";
        var i;
        for(i = 0; i < this.state.arrtype.length; i++){
            if(this.state.arrtype[i] === KartuStringEnum.Mandiri){
                mandiricard = this.state.arrname[index];
            }
        }
        
        return mandiricard;
    }

    validasiPublishPlayer = () => {
        if(this.state.publishQuota <= 0)
        {
            Utils.errAlert("Error Message", Strings.PlayerReservation.errmsg4);
        }
        else if(this.state.playernameinput === "")
        {
            Utils.errAlert("Error Message", Strings.PlayerReservation.errmsg2);
        }
        else
        {
            this.setState({publishQuota: this.state.publishQuota - 1});
            this.state.arrtype[this.state.editedposition] = TipePemainStringEnum.Publish;
            this.state.arrname[this.state.editedposition] = ' ';
            this.state.arridpricing[this.state.editedposition] = this.state.idpublish;
            this.state.arrpricing[this.state.editedposition] = this.state.publishrate;
            
            this.setState({passSelection : (this.state.publishrate == 0) ? false : true,});
            this.setPlayerName();

            this.callActivity();
        }

    }

    callActivity = () => {
        if(this.state.passselection == false){
            Utils.errAlert("QAccess Alert", 'There is no Visitor Rate!');
        }
        else{
            this.state.bookmodel[this.state.editedposition].Nama =  this.state.arrname[this.state.editedposition];
            this.state.bookmodel[this.state.editedposition].PlayerName = this.state.arrplayername[this.state.editedposition];
            this.state.bookmodel[this.state.editedposition].Tipe = this.state.arrtype[this.state.editedposition];
            this.state.bookmodel[this.state.editedposition].Kartu = this.state.arrkartu[this.state.editedposition];
            if(this.state.arrtype[this.state.editedposition] == TipePemainStringEnum.Publish || this.state.arrtype[this.state.editedposition] == TipePemainStringEnum.Visitor){
                this.state.bookmodel[this.state.editedposition].IdPricing = this.state.idpublish;
                this.state.bookmodel[this.state.editedposition].Pricing = this.state.publishrate;
            }
            else if(this.state.arrtype[this.state.editedposition] == TipePemainStringEnum.Club){
                this.state.bookmodel[this.state.editedposition].IdPricing = this.state.idclub;
                this.state.bookmodel[this.state.editedposition].Pricing = this.state.clubrate;
            }
            else if(this.state.arrtype[this.state.editedposition] == TipePemainStringEnum.QMember){
                this.state.bookmodel[this.state.editedposition].IdPricing = this.state.idqrate;
                this.state.bookmodel[this.state.editedposition].Pricing = this.state.qrate;
            }
            var totalCount = 0;
            
            var i;
            for(i = 0; i < this.state.bookmodel.length; i++){
                totalCount = totalCount + this.state.bookmodel[i].Pricing;
            }
    
            const { navigation } = this.props;
            navigation.goBack();
            this.params.onSelect({arrname: this.state.arrname, arrtype: this.state.arrtype, 
                arrkartu: this.state.arrkartu, arrplayername: this.state.arrplayername,
                arridpricing: this.state.arridpricing, arrpricing: this.state.arrpricing,
                publishQuota: this.state.publishQuota, qQuota: this.state.qQuota,
                bookmodel: this.state.bookmodel, total: totalCount });
        }
        
    }

    validasiPlayer = () => 
    {
        if(this.state.playernameinput == ''){
            Utils.errAlert("Error Message", Strings.PlayerEvent.errmsg1);
        }
        else {
            checkPlayerType(this.state.playernoinput).then((json) =>
            {
                var type = json.TipeMember;
                if(type == TipePemainIntEnum.Visitor || type == TipePemainIntEnum.Publish)
                {
                    this.validasiPublishPlayer();
                }
                else 
                {
                    if(Date.parse(json.ExpirationDate) < Date.now()){
                        Utils.errAlert("Error Message", Strings.PlayerEvent.errmsg3);
                    }
                    else{
                        this.validasiNonPublishPlayer(json, type, this.getMandiriInList());
                    }

                    this.setState({isLoading: false});
                }
                
            })
            .catch((error) => {
                console.error(error);
                this.setState({isLoading: false});
            });
        }
    }

    render() {
        const gotoBook = () => {
            //this.props.navigation.navigate('BookReservation')
            this.validasiPlayer();
        }

        const fn_GetMyCard = () =>
        {
            if(this.state.usertype === TipePemainIntEnum.Visitor || this.state.usertype === TipePemainIntEnum.Publish)
            {
                Utils.errAlert("Error Message", Strings.PlayerReservation.errmsg1);
            }
            else 
            {
                getVirtualAccount(this.state.username).then((jsonCard) =>
                {
                    this.setState({ 
                        typeMember : Enums.TipePemainEnum(jsonCard.TypeMember),
                        playernoinput: jsonCard.NoMember,
                        playernameinput: jsonCard.CardName == null ? '' : jsonCard.CardName,
                    });
                    this.validasiPlayer();
                    this.setState({isLoading: false});
                })
                .catch((error) => {
                    console.error(error);
                    this.setState({isLoading: false});
                });
            }
        }

        if (this.state.isLoading) {
            return (
                <View style={styles.loading}>
                    <ActivityIndicator size='large' />
                </View>
            );
        }

        return (
            <SafeAreaView style={styles.safeAreaView}>
                <ScrollView style={styles.containerScroll}>
                    <View style={styles.container}>
                        <Card style={styles.card}>
                            <View style={{width: '100%', flexDirection: 'row', }}>
                                <BpkTextInput 
                                    label={Strings.PlayerEvent.text1}
                                    onChangeText={(text) => this.setState({playernoinput: text})}
                                    value={this.state.playernoinput}
                                    style={{flex: 1, marginBottom: 3, }}/>
                            </View>
                            <View style={{width: '100%', flexDirection: 'row', marginTop: 5,}}>
                                <BpkTextInput 
                                    label={Strings.PlayerEvent.text2}
                                    onChangeText={(text) => this.setState({playernameinput: text})}
                                    value={this.state.playernameinput}
                                    style={{flex: 1, marginBottom: 3, }}/>
                            </View>
                        </Card>
                        <View
                            style={{flex: 1, justifyContent: 'space-between', marginBottom: 20, }}>
                            <Button 
                                onPress= { fn_GetMyCard }
                                buttonStyle={{backgroundColor: '#d6a431', borderRadius: 5, marginTop: 20, marginBottom: 10, height: 50, }}
                                title={Strings.PlayerEvent.usemycardbutton}/>
                            <Button 
                                onPress= { gotoBook }
                                buttonStyle={{backgroundColor: Colors.button, borderRadius: 5, marginTop: 10, marginBottom: 20, height: 50, }}
                                title={Strings.PlayerEvent.donebutton}/>
                        </View>
                        
                    </View>
                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({

    safeAreaView: {
        flex: 1,
    },

    containerScroll:
    {
        flex: 1,
        backgroundColor: '#efefef',
    },

    container: {
        flex: 1,
        flexDirection: 'column',
    },

    text: {
        fontSize: 13,
        marginTop: 5,
        color: Colors.text,
    },

    textBold: {
        fontSize: 13,
        marginTop: 5,
        color: Colors.text,
        fontWeight: 'bold',
    },

    textLeft2: {
        fontSize: 14,
        marginTop: 5,
        color: Colors.text,
        width: 10,
    },

    textRight2: {
        flex: 1,
        fontSize: 12,
        marginTop: 5,
        color: Colors.text,
    },

    textLeftBold: {
        fontSize: 14,
        marginTop: 5,
        color: Colors.text,
        width: 10,
        fontWeight: 'bold',
    },

    textRightBold: {
        flex: 1,
        fontSize: 12,
        marginTop: 5,
        color: Colors.text,
        fontWeight: 'bold',
    },

    textKet: {
        fontSize: 18,
        marginTop: 10,
        marginLeft: 15,
        marginBottom: 10,
        marginRight: 15,
        color: Colors.text,
        fontWeight: 'bold',
    },

    card: {
        backgroundColor: Colors.backgroundColor,
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 10,
        marginRight: 10,
        marginTop: 10,
        marginBottom: 5,
        padding: 10,
        elevation: 2,
        borderRadius: 5,
    },

    loading: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(255, 255, 255, 0.2)',
    },

});
