import clsSetting from "../API/clsSetting";

export default class RefundModel
{
    constructor(_id, _status)
    {
        this.Id = _id;
        this.Status = _status;
        this.ApiSecretKey = clsSetting.ApiSecretKey;
    }
}