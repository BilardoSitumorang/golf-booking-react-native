const tintColor = '#2f95dc';

export default {
    tintColor ,
    tabIconDefault: '#999',
    tabIconSelected: '#93b843',
    tabBar: '#fefefe',
    errorBackground: 'red',
    errorText: '#fff',
    warningBackground: '#EAEB5E',
    warningText: '#666804',
    noticeBackground: tintColor,
    noticeText: '#fff',
    title: '#00B75D',
    text: '#414141',
    button: '#93b843',
    buttontext: '#ffffff',
    background: '#ffffff',
    backgroundColor: '#ffffff',
    graybackground: '#f2f2f2',
};
