import React from 'react';
import {
    StyleSheet, 
    AsyncStorage,
    Text, 
    Image,
    TouchableOpacity,
    View,
    ScrollView,
    SafeAreaView,
    KeyboardAvoidingView,
} from 'react-native';
import { Button, } from 'react-native-elements';
import Strings from '../constants/Strings';
import Colors from '../constants/Colors';
import BpkTextInput from 'react-native-bpk-component-text-input';
import { postLogin, getReferralCode } from '../API/UserService';
import Utils from './Utils';

export default class LoginScreen extends React.Component{
    static navigationOptions = {
        title: '',
        header: null,
    };

    constructor(props)
    {
        super(props);
        this.state = {
            username: '',
            password: '',
        };
    }

    render() {

        const gotoRegister = () => {
            this.props.navigation.navigate('SignUp');
        }

        const gotoContinue = async () => {
            
            if(this.state.username.trim() == "")
            {
                Utils.errAlert("Error Message", Strings.Login.errmsg1);
            }
            else if(this.state.password.trim() == "")
            {
                Utils.errAlert("Error Message", Strings.Login.errmsg2);
            }
            else
            {
                postLogin(this.state.username, this.state.password).then(async (json) =>
                {
                    if(json.status == 200){
                        if(json.result.IsApproved == true){
                        
                            getReferralCode(this.state.username).then(async (referraljson) =>{
                                await AsyncStorage.setItem("Referral", referraljson);
                                await AsyncStorage.setItem("IsLogin", "true");
                                await AsyncStorage.setItem("Username", this.state.username);
                                this.props.navigation.navigate('App');
                            });
                        }
                        else{
                            await AsyncStorage.setItem("Username", this.state.username);
                            this.props.navigation.navigate('RegisterPhone');
                        }
                    }
                    else{
                        Utils.errAlert("Error Message", json.message);
                    }
                    
                });
            }
        }

        const gotoForgot = async () => {
            this.props.navigation.navigate('Forgot');
        }
    
        return (
            <SafeAreaView style={styles.safeAreaView}>
                <ScrollView style={styles.container}>
                    <KeyboardAvoidingView style={styles.container}>
                        <View style={styles.containerView}>
                            <Image source={require('../assets/images/logo_qaccess.png')} style={styles.image}/>
                            <View style={{width: '100%', marginTop: 5,}}>
                                <BpkTextInput 
                                    label={Strings.Login.emailplaceholder}
                                    returnKeyLabel = {"next"}
                                    onChangeText={(text) => this.setState({username: text})}
                                    value={this.state.username}
                                    style={{marginBottom: 3, marginLeft: 20, marginRight: 20, }}/>
                            </View>
                            <View style={{width: '100%', marginTop: 5,}}>
                                <BpkTextInput 
                                    label={Strings.Login.passwordplaceholder}
                                    returnKeyLabel = {"next"}
                                    secureTextEntry
                                    onChangeText={(text) => this.setState({password: text})}
                                    value={this.state.password}
                                    style={{marginBottom: 3, marginLeft: 20, marginRight: 20, }}/>
                            </View>
                            <View style={styles.forgotcontainer}>
                                <TouchableOpacity onPress={ gotoForgot } >
                                    <Text style={{fontSize: 14, color: Colors.button, textAlign: 'right',}}>{Strings.Login.forgotbutton}</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{width: '95%', marginTop: 10,}}>
                                <Button 
                                    onPress={ gotoContinue }
                                    buttonStyle={{backgroundColor: Colors.button, borderRadius: 5, marginTop: 20, height: 50, }}
                                    title={Strings.Login.loginbutton}/>
                            </View>
                            <View style={styles.textcontainer}>
                                <Text style={styles.text}>{Strings.Login.text}</Text>
                                <TouchableOpacity onPress={ gotoRegister }>
                                    <Text style={styles.textsignup} >{Strings.Login.signupbutton}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </KeyboardAvoidingView>
                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },

    safeAreaView: {
        flex: 1
    },

    containerView: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
    },

    image: {
        height: 120,
        width: 120,
        marginTop: 120,
    },

    textcontainer: {
        flexDirection: 'row',
    },

    text: {
        fontSize: 13,
        textAlign: 'right',
        marginTop: 15,
        color: Colors.text,
    },

    textsignup: {
        fontSize: 13,
        textAlign: 'left',
        marginTop: 15,
        marginLeft: 5,
        color: Colors.button,
    },

    forgotcontainer: {
        width: '90%',
        marginTop: 10,
        marginRight: 20,
    },
});

