import React from 'react';
import Moment from 'moment';
import clsSetting from '../API/clsSetting';

exports.getCourses = (_date, _session, _player, _location) => {
    return fetch(clsSetting.APIUrl + "lapangan/get?date=" + _date + "&player=" + _player 
        + "&session=" + _session + "&location=" + _location + "&ApiSecretKey=" +
        clsSetting.ApiSecretKey)
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        if (json.status === 200) {
            console.log('json Courses ', json);
            return json.result;
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error Courses ', e));  
}

exports.findCourse = (_date, _session, _player, _id) => {
    return fetch(clsSetting.APIUrl + "lapangan/find?date=" + _date + "&player=" + _player 
        + "&session=" + _session + "&id=" + _id + "&ApiSecretKey=" +
        clsSetting.ApiSecretKey)
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        if (json.status === 200) {
            return json.result; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error Find Course ', e));  
}

exports.findCourseWithType = (_date, _session, _player, _id, _playertype) => {
    return fetch(clsSetting.APIUrl + "lapangan/findwithtype?date=" + _date + "&player=" + _player 
        + "&session=" + _session + "&id=" + _id + "&playertype=" + _playertype + "&ApiSecretKey=" +
        clsSetting.ApiSecretKey)
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        if (json.status === 200) {
            return json.result; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error Find Course With Type ', e));  
}

exports.findCourseDetail = ( _id) => {
    return fetch(clsSetting.APIUrl + "lapangan/find?id=" + _id + "&ApiSecretKey=" +
        clsSetting.ApiSecretKey)
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        if (json.status === 200) {
            return json.result; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error Find Course Detail ', e));  
}

exports.getCoursesByName = (_location) => {
    return fetch(clsSetting.APIUrl + "lapangan/get?location=" + _location + "&ApiSecretKey=" +
        clsSetting.ApiSecretKey)
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        if (json.status === 200) {
            return json.result; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error Course by Name ', e));  
}

exports.isMemberValid = (_memberno) => {
    return fetch(clsSetting.APIUrl + "player/GetAvailableQMember?noMember=" + _memberno + "&ApiSecretKey=" +
        clsSetting.ApiSecretKey)
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        if (json.status === 200) {
            return json.result; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error Member Valid ', e));  
}

exports.getCourseImage = (_courseid) => {
    return fetch(clsSetting.APIUrl + "lapangan/GetLapanganGambar?idLapangan=" + _courseid + "&ApiSecretKey=" +
        clsSetting.ApiSecretKey)
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        if (json.status === 200) {
            return json.result; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error Course Image ', e));  
}

exports.postLastSearch = (_email, _courseid, _date, _doubledate, _session, _player) => {
    return fetch(clsSetting.APIUrl + "player/appenduserlastsearch", {  
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json; charset=utf-8',
        },
        body: JSON.stringify({
            Email: _email,
            IdCourse: _courseid,
            Date: _date,
            DoubleDate: _doubledate,
            Session: _session,
            Players: _player,
            ApiSecretKey: clsSetting.ApiSecretKey,
        })
    })
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        return json;
    })
    .catch(e => console.log('json Last Search ', e));  
}

exports.getLastSearch = (_email) => {
    return fetch(clsSetting.APIUrl + "player/getuserlastsearch?email=" + _email + "&ApiSecretKey=" +
        clsSetting.ApiSecretKey)
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        if (json.status === 200) {
            return json.result; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('json Get Last Search ', e)); 
}