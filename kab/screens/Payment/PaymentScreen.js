import React from 'react';
import {
    Image,
    Platform,
    ScrollView,
    StyleSheet,
    AsyncStorage,
    SafeAreaView,
    TouchableOpacity,
    ActivityIndicator,
    Text,
    Alert,
    View,
    Share,
    BackHandler,
} from 'react-native';
import Colors from '../../constants/Colors';
import Strings from '../../constants/Strings';
import { Divider, Icon, } from 'react-native-elements';
import { Card } from 'react-native-material-cards';
import { TransactionTypeIntEnum, OptionIntEnum, SessionIntEnum, SessionStringEnum, PaymentIntEnum, TipePemainIntEnum } from '../../constants/Enums';
import { getBalance, getVirtualAccount, } from '../../API/UserService';
import { getOptions,  } from '../../API/OptionService';
import { findTrans, cancelTrans,  } from '../../API/TransactionService';
import { getDoubleToStringDate,  } from '../../API/SettingService';
import { findPulsaTrans,  } from '../../API/PulsaService';
import moment from 'moment';
import Utils from '../Utils';
import clsSetting from '../../API/clsSetting';

export default class PaymentScreen extends React.Component {
    static navigationOptions = ({navigation}) => ({
        title: 'Payment',
        headerTintColor: '#ffffff', 
        headerStyle: {
            backgroundColor: '#93b843'
        },
        headerLeft: (
            <Icon name="chevron-left"
                onPress={() => {
                    Alert.alert(
                        "Cancel Booking",
                        "All data will be lost, are you sure?",
                        [
                            { text: 'YES', onPress: () => {
                                console.log('booking code', navigation.getParam('bookingcode', ''));
                                cancelTrans(navigation.getParam('bookingcode', '')).then((json) => {
                                    if(json == true){
                                        navigation.popToTop();
                                    }
                                    else{
                                        navigation.popToTop();
                                    }
                                });
                                
                            }},
                            { text: 'NO', onPress: () => navigation.popToTop() }
                        ],
                        { cancelable: false }
                    )
                }}
                size={35} color="white"/>
        ),
    });

    constructor(props){
        super(props);

        this.params = this.props.navigation.state.params;

        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);

        this.state = {
            isLoading: true,
            username: '',
            bookingcode: '',
            bookingdate: '',
            bookingdateraw: '',
            point: 0,
            pointdb: 0,
            balance: 0,
            total: 0,
            txtPoint: '',
            txtReedemPoint: 0,
            timeremaining: 30,
            timeremain: 0,
            totalminuteremain: 0,
            status: 0,

            transdate: '',
            eventdate: '',
            timeeventdate: '',
            coursenameReservation: '',
            sessionReservation: '',
            
            titlepulsa: '',
            destpulsa: '',
            activeperiodpulsa: '',
            imgproviderpulsa: '',

            transtype: this.params.TransType,
            containmandiri: this.params.ContainMandiri,

            reservationvisible: false,
            registrationqvisible: false,
            eventvisible: false,
            pulsavisible: false,

            sharepaymentvisible: true,
            sharepaymentoptionvisible: true,
            banktransfervisible: true,
            creditcardvisible: true,
            pointvisible: true,
            processpointvisible: false,
            redeembuttonvisible: true,
            unredeembuttonvible: false,
            halfpoint: false,

            btnUsePointClickable: true,

            platform: '',
            noteerr: '',
            linkterms: '',
            chargepaymentcc: false,
            chargepaymentbank: false, 
            mandiricard: '',
        };
    }

    componentDidUpdate(){
        if(Number.parseInt(this.state.timeremaining) <= 0){ 
            clearInterval(this.interval);
            //Call Process
            this.props.navigation.popToTop();
        }
    }

    handleBackButtonClick() {
        Alert.alert(
            "Cancel Booking",
            "All data will be lost, are you sure?",
            [
                { text: 'YES', onPress: () => {
                    console.log('booking code', this.state.bookingcode);
                    cancelTrans(this.state.bookingcode).then((json) => {
                        if(json.status === 200){
                            this.props.navigation.popToTop();
                            
                        }
                        else {
                            this.props.navigation.popToTop();
                        }
                    });
                    
                }},
                { text: 'NO', onPress: () => this.props.navigation.popToTop()}
            ],
            { cancelable: false }
        )

        return true;
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount()
    {
        clearInterval(this.interval);
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    async componentDidMount() {
        //Time Remaining Interval
        
        var Enumerable = require('../linq');
        this.setState({ bookingcode : await AsyncStorage.getItem('BookingCode')});
        this.setState({ username : await AsyncStorage.getItem('Username')});

        this.props.navigation.setParams( {bookingcode: this.state.bookingcode});

        getOptions().then((jsonOption) => 
        {
            if(this.state.transtype == TransactionTypeIntEnum.Reservation)
            {
                this.setState({
                    maintenanceOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionMaintenancePeriod).firstOrDefault(),
                    reservationOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionReservation).firstOrDefault(),
                    pointOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionPointReservation).firstOrDefault(),
                    ccOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionCreditCardReservation).firstOrDefault(),
                    sharepaymentlinkOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionSharePaymentLinkReservation).firstOrDefault(),
                });

                this.setState({
                    reservationvisible: true,
                    registrationqvisible: false,
                    eventvisible: false,
                    pulsavisible: false,
                });

                console.log(this.state.sharepaymentlinkOption);

                this.setState({
                    platform: Platform.OS === 'ios' ? this.state.reservationOption.iOS : this.state.reservationOption.Android,
                    sharepaymentoptionvisible: Platform.OS === 'ios' ? this.state.sharepaymentlinkOption.iOS : this.state.sharepaymentlinkOption.Android,
                    noteerr: this.state.reservationOption.Note,
                    linkterms: 'TermsReservation',
                });

            }
            else if(this.state.transtype == TransactionTypeIntEnum.RegistrationQ)
            {
                this.setState({
                    maintenanceOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionMaintenancePeriod).firstOrDefault(),
                    registrationOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionRegistration).firstOrDefault(),
                    pointOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionPointRegistration).firstOrDefault(),
                    ccOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionCreditCardRegistration).firstOrDefault(),
                    sharepaymentlinkOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionSharePaymentLinkRegistration).firstOrDefault(),
                });

                this.setState({
                    reservationvisible: false,
                    registrationqvisible: true,
                    eventvisible: false,
                    pulsavisible: false,
                });

                this.setState({
                    platform: Platform.OS === 'ios' ? this.state.registrationOption.iOS : this.state.registrationOption.Android,
                    sharepaymentoptionvisible: Platform.OS === 'ios' ? this.state.sharepaymentlinkOption.iOS : this.state.sharepaymentlinkOption.Android,
                    noteerr: this.state.registrationOption.Note,
                    linkterms: 'TermsRegisterQ',
                });
            }
            else if(this.state.transtype == TransactionTypeIntEnum.Event)
            {
                this.setState({
                    maintenanceOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionMaintenancePeriod).firstOrDefault(),
                    eventOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionEvent).firstOrDefault(),
                    pointOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionPointEvent).firstOrDefault(),
                    ccOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionCreditCardEvent).firstOrDefault(),
                    sharepaymentlinkOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionSharePaymentLinkEvent).firstOrDefault(),
                });

                this.setState({
                    reservationvisible: false,
                    registrationqvisible: false,
                    eventvisible: true,
                    pulsavisible: false,
                });

                this.setState({
                    platform: Platform.OS === 'ios' ? this.state.eventOption.iOS : this.state.eventOption.Android,
                    sharepaymentoptionvisible: Platform.OS === 'ios' ? this.state.sharepaymentlinkOption.iOS : this.state.sharepaymentlinkOption.Android,
                    noteerr: this.state.eventOption.Note,
                    linkterms: 'TermsEvent',
                });
            }
            else if(this.state.transtype == TransactionTypeIntEnum.MobilePulsaPhonePrepaid)
            {
                this.setState({
                    maintenanceOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionMaintenancePeriod).firstOrDefault(),
                    pulsaOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionMobilePulsa).firstOrDefault(),
                    pointOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionPointMobilePulsa).firstOrDefault(),
                    ccOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionCreditCardMobilePulsa).firstOrDefault(),
                    sharepaymentlinkOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionSharePaymentLinkMobilePulsa).firstOrDefault(),
                });

                this.setState({
                    reservationvisible: false,
                    registrationqvisible: false,
                    eventvisible: false,
                    pulsavisible: true,
                });

                this.setState({
                    platform: Platform.OS === 'ios' ? this.state.pulsaOption.iOS : this.state.pulsaOption.Android,
                    sharepaymentoptionvisible: Platform.OS === 'ios' ? this.state.sharepaymentlinkOption.iOS : this.state.sharepaymentlinkOption.Android,
                    noteerr: this.state.pulsaOption.Note,
                    linkterms: 'TermsPulsa',
                });
            }

            getBalance(this.state.username).then((jsonBalance) =>
            {
                this.setState({ 
                    pointdb: jsonBalance.Balance,
                    balance: jsonBalance.Balance,
                    txtPoint: jsonBalance.Balance / clsSetting.AccountBalance,
                    txtReedemPoint: jsonBalance.Balance,
                });
                
                
                if(this.state.transtype == TransactionTypeIntEnum.Reservation)
                {
                    findTrans(this.state.bookingcode).then((jsonTrans) =>
                    {
                        this.setState({ 
                            coursenameReservation: jsonTrans.NamaLapangan,
                            sessionReservation: jsonTrans.Session == SessionIntEnum.Morning ? SessionStringEnum.Morning : SessionStringEnum.Afternoon,
                            total: Utils.getTotalBooking(jsonTrans.PemainViewModel),
                            chargepaymentbank: false,
                            chargepaymentcc: false,

                            status: jsonTrans.Status,
                        });

                        if(this.state.containmandiri)
                        {
                            this.setState({ 
                                banktransfervisible: false,
                                btnUsePointClickable: false,
                            });
                            var i;
                            for(i = 0; i < jsonTrans.PemainViewModel.length; i++){
                                if(jsonTrans.PemainViewModel[i].TipeMember == TipePemainIntEnum.Club)
                                {
                                    this.setState({
                                        mandiricard: jsonTrans.PemainViewModel[i].NoMember.replace('-', ''),
                                    });
                                }
                            }
                        }
                        
                        getDoubleToStringDate(jsonTrans.TanggalBuat).then((jsonDate) =>
                        {
                            this.setState({ 
                                bookingdate: moment(Date.parse(jsonDate.result)).format('DD MMM YYYY'),
                                bookingdateraw: jsonDate.result,
                            });

                            getDoubleToStringDate(jsonTrans.Tanggal).then((jsonDateReservation) =>
                            {
                                this.setState({ transdate: moment(Date.parse(jsonDateReservation.result)).format('DD MMM YYYY'), });
                                this.setState({ 
                                    timeremain: Utils.getTotalMinutes(Date.now(), Utils.getLocalTimezone(Date.parse(jsonDate.result))),
                                    totalminuteremain: Utils.getTotalMinuteRemaining(Utils.getCountDays(Date.parse(this.state.transdate), Date.parse(this.state.bookingdate)),
                                        this.state.transtype, jsonTrans.Status),
                                });

                                console.log('Time Remain', this.state.timeremain);
                                console.log('Total Minute Remain', this.state.totalminuteremain);
                                
                                if(this.state.timeremain < this.state.totalminuteremain){
                                    this.setState({
                                        timeremaining: Number.parseInt(this.state.totalminuteremain) - Number.parseInt(this.state.timeremain),
                                    });
                                }
                                else{
                                    this.setState({isLoading: false, });
                                    this.props.navigation.popToTop();
                                }

                                
                                console.log('Time Remain', this.state.timeremaining);

                                this.interval = setInterval(
                                    () => this.setState({ timeremaining: Number.parseInt(this.state.timeremaining) - 1 }), 60000
                                );
                                this.setState({isLoading: false, });
                            })
                        })
                    })
                }
                else if(this.state.transtype == TransactionTypeIntEnum.RegistrationQ)
                {
                    findTrans(this.state.bookingcode).then((jsonTrans) =>
                    {
                        this.setState({ 
                            total: jsonTrans.PemainViewModel[0].Total,
                            chargepaymentbank: false,
                            chargepaymentcc: jsonTrans.AbsorbCreditCardFee,
                        });

                        getDoubleToStringDate(jsonTrans.TanggalBuat).then((jsonDate) =>
                        {
                            this.setState({ 
                                bookingdate: moment(Date.parse(jsonDate.result)).format('DD MMM YYYY'),
                                bookingdateraw: jsonDate.result,
                            });

                            getDoubleToStringDate(jsonTrans.Tanggal).then((jsonDate2) =>
                            {
                                this.setState({ transdate: moment(Date.parse(jsonDate2.result)).format('DD MMM YYYY'), });
                                this.setState({ 
                                    timeremain: Utils.getTotalMinutes(Date.now(), Utils.getLocalTimezone(Date.parse(jsonDate.result))),
                                    totalminuteremain: Utils.getTotalMinuteRemaining(Utils.getCountDays(Date.parse(this.state.transdate), Date.parse(jsonDate.result)),
                                        this.state.transtype, jsonTrans.Status),
                                });
                                if(this.state.timeremain < this.state.totalminuteremain){
                                    this.setState({
                                        timeremaining: Number.parseInt(this.state.totalminuteremain)  - Number.parseInt(this.state.timeremain),
                                    });
                                }
                                else{
                                    this.setState({isLoading: false, });
                                    this.props.navigation.popToTop();
                                }

                                this.interval = setInterval(
                                    () => this.setState((prevState)=> ({ timeremaining: Number.parseInt(this.state.timeremaining) - 1 })), 60000
                                );
                                this.setState({isLoading: false, });
                            })
                        })
                    })
                }
                else if(this.state.transtype == TransactionTypeIntEnum.Event)
                {
                    findTrans(this.state.bookingcode).then((jsonTrans) =>
                    {
                        var totalEvent = 0;
                        for(i = 0; i < jsonTrans.PemainViewModel.length; i++)
                        {
                            totalEvent = totalEvent + jsonTrans.PemainViewModel[i].Total;
                        }
                        this.setState({ 
                            total: totalEvent,
                            chargepaymentbank: jsonTrans.ChargeBankTransferFee,
                            chargepaymentcc: jsonTrans.AbsorbCreditCardFee,
                            timeeventdate: jsonTrans.TimeStart,
                        });

                        getDoubleToStringDate(jsonTrans.TanggalBuat).then((jsonDate) =>
                        {
                            this.setState({ 
                                bookingdate: moment(Date.parse(jsonDate.result)).format('DD MMM YYYY'),
                                bookingdateraw: jsonDate.result,
                            });

                            getDoubleToStringDate(jsonTrans.Tanggal).then((jsonDate2) =>
                            {
                                getDoubleToStringDate(jsonTrans.EventDate).then(async (jsonDate3) =>
                                {
                                    this.setState({ transdate: moment(Date.parse(jsonDate2.result)).format('DD MMM YYYY'), });
                                    this.setState({ 
                                        timeremain: Utils.getTotalMinutes(Date.now(), Utils.getLocalTimezone(Date.parse(jsonDate.result))),
                                    });
                                    var date2 = Date.parse(jsonDate2.result);
                                    var timeEvent = jsonTrans.TimeStart.split(' ');
                                    var hour = Number.parseInt(timeEvent[0].split(':')[0]);
                                    var minute = Number.parseInt(timeEvent[0].split(':')[1]);
                                    if(timeEvent[1] == "PM"){
                                        hour = Number.parseInt(hour) + 12;
                                    }
                                
                                    this.setState({ eventdate: moment(Date.parse(jsonDate3.result)).format('YYYY MM DD'), });
                                    var date3 = moment(Date.parse(jsonDate3.result)).format('YYYY MM DD');
                                    var currentOffset = (new Date()).getTimezoneOffset() / 60;
                                    var dateevent = date3.split(' ');
                                    var newdate = moment(new Date(dateevent[0], dateevent[1] - 1, dateevent[2], hour, minute)).valueOf();

                                    this.setState({ 
                                        totalminuteremain: Utils.getTotalMinuteRemaining(Utils.getCountDays(newdate,
                                            Date.parse(jsonDate.result)), this.state.transtype, jsonTrans.Status),
                                    });
                                    console.log('total minute remain 1 ', this.state.totalminuteremain);
                                    if(this.state.timeremain < this.state.totalminuteremain){
                                        this.setState({
                                            timeremaining: Number.parseInt(this.state.totalminuteremain)  - Number.parseInt(this.state.timeremain),
                                        });
                                    }
                                    else{
                                        this.setState({isLoading: false, });
                                        this.props.navigation.popToTop();
                                    }

                                    console.log('Time Remain', this.state.timeremain);
                                    console.log('Total Minute Remain', this.state.totalminuteremain);

                                    this.interval = setInterval(
                                        () => this.setState((prevState)=> ({ timeremaining: Number.parseInt(this.state.timeremaining) - 1 })), 60000
                                    );
                                })

                                this.setState({isLoading: false, });
                            })
                        })
                    })
                }
                else if(this.state.transtype == TransactionTypeIntEnum.MobilePulsaPhonePrepaid)
                {
                    findTrans(this.state.bookingcode).then((jsonTrans) =>
                    {
                        findPulsaTrans(this.state.bookingcode).then((jsonPulsaTrans) =>
                        {
                            this.setState({ 
                                titlepulsa: jsonPulsaTrans.ProductDescription,
                                destpulsa: jsonPulsaTrans.CustomerNumber,
                                activeperiodpulsa: jsonPulsaTrans.ProductActivePeriod + " days",
                                imgproviderpulsa: Utils.getImageProvider(jsonPulsaTrans.CustomerNumber),
                                total: jsonPulsaTrans.Total,
                                chargepaymentbank: jsonPulsaTrans.ProductAbsorbPayment,
                                chargepaymentcc: jsonPulsaTrans.ProductAbsorbPayment,
                            });

                            getDoubleToStringDate(jsonTrans.TanggalBuat).then((jsonDate) =>
                            {
                                this.setState({ 
                                    bookingdate: moment(Date.parse(jsonDate.result)).format('DD MMM YYYY'),
                                    bookingdateraw: jsonDate.result,
                                });

                                getDoubleToStringDate(jsonTrans.Tanggal).then((jsonDate2) =>
                                {
                                    this.setState({ transdate: moment(Date.parse(jsonDate2.result)).format('DD MMM YYYY'), });
                                    this.setState({ 
                                        timeremain: Utils.getTotalMinutes(Date.now(), Utils.getLocalTimezone(Date.parse(jsonDate.result))),
                                        totalminuteremain: Utils.getTotalMinuteRemaining(Utils.getCountDays(Date.parse(this.state.transdate), Date.parse(jsonDate.result)),
                                            this.state.transtype, jsonTrans.Status),
                                    });
                                    if(this.state.timeremain < this.state.totalminuteremain){
                                        this.setState({
                                            timeremaining: Number.parseInt(this.state.totalminuteremain)  - Number.parseInt(this.state.timeremain),
                                        });
                                    }
                                    else{
                                        this.setState({isLoading: false, });
                                        this.props.navigation.popToTop();
                                    }
                                    this.interval = setInterval(
                                        () => this.setState((prevState)=> ({ timeremaining: Number.parseInt(this.state.timeremaining) - 1 })), 60000
                                    );
                                    this.setState({isLoading: false, });
                                })
                            })
                        })
                    })
                }
            })
        })
        .catch((error) => {
            console.error(error);
            this.setState({isLoading: false, });
        });
    }

    fn_usepoint = () =>
    {
        if (this.state.balance != 0)
        {
            if (this.state.total == this.state.balance || this.state.balance >= this.state.total)
            {
                this.setState({
                    processpointvisible: true,
                    banktransfervisible: false,
                    creditcardvisible: false,
                    sharepaymentvisible: false,
                    txtPoint: ((Number.parseInt(this.state.balance) - Number.parseInt(this.state.total)) / clsSetting.AccountBalance),
                    txtReedemPoint: Number.parseInt(this.state.balance) - Number.parseInt(this.state.total),
                    redeembuttonvisible: false,
                    unredeembuttonvible: true,
                    halfpoint: true,

                    point: (Number.parseInt(this.state.balance) - Number.parseInt(this.state.total)),
                    balance: (Number.parseInt(this.state.balance) - Number.parseInt(this.state.total)),
                });
                console.log('point', this.state.txtPoint);
            }
            else
            {
                this.setState({
                    processpointvisible: false,
                    banktransfervisible: true,
                    creditcardvisible: true,
                    sharepaymentvisible: true,
                    txtPoint: 0,
                    txtReedemPoint: 0,
                    redeembuttonvisible: false,
                    unredeembuttonvible: true,
                    halfpoint: false,

                    point: this.state.balance,
                    balance: 0,
                });
            }

        }
        else
        {
            Utils.errAlert("Error Message", Strings.Payment.errmsg3);
        }
    }

    render() {
        if (this.state.isLoading) {
            return (
                <View style={{flex: 1, paddingTop: 20}}>
                    <ActivityIndicator />
                </View>
            );
        }

        const gotoBankTransfer = () => {
            this.props.navigation.navigate(this.state.linkterms, {Point: this.state.point, BookingCode: this.state.bookingcode,
                BookingDate: this.state.bookingdate, ChargePayment: this.state.chargepaymentbank, 
                TransType: this.state.transtype, PaymentType: PaymentIntEnum.BankTransfer, Total: this.state.total,
                TransDate: this.state.transdate, EventDate: this.state.eventdate, TimeEvent: this.state.timeeventdate, 
                BookingDateRaw: this.state.bookingdateraw, Status: this.state.status });
        }

        const gotoCreditCard = () => {
            var ccplatform = Platform.OS === 'ios' ? this.state.ccOption.iOS : this.state.ccOption.Android;

            if(Utils.getMaintenanceOption(this.state.maintenanceOption)){
                Utils.alertBackToMain(this.state.maintenanceOption.Note, this.props);
            }
            else if(!this.state.platform) {
                Utils.errAlert("QAccess Alert", this.state.noteerr);
            }
            else if(!ccplatform) {
                Utils.errAlert("QAccess Alert", ccOption.Note);
            }
            else
            {
                this.props.navigation.navigate(this.state.linkterms, {Point: this.state.point, BookingCode: this.state.bookingcode,
                    BookingDate: this.state.bookingdate, ChargePayment: this.state.chargepaymentcc, 
                    TransType: this.state.transtype, PaymentType: PaymentIntEnum.CreditCard, Total: this.state.total,
                    ContainMandiri: this.state.containmandiri, MandiriCard: this.state.mandiricard,
                    TransDate: this.state.transdate, Status: this.state.status });
            }
        }

        const gotoRedeem = () => {
            if (this.state.containmandiri)
            {
                Utils.errAlert("Error Message", Strings.Payment.errmsg2);
            }
            else
            {
                getVirtualAccount(this.state.username).then((jsonVA) => 
                {
                    var type = jsonVA.TypeMember;
                    var tipeUser = false;
                    getDoubleToStringDate(jsonVA.ValidEnd).then((jsonDate) =>
                    {
                        if (type == TipePemainIntEnum.Publish || type == TipePemainIntEnum.Visitor)
                        {
                            //Bukan QGolf Member
                            Utils.errAlert("Error Message", Strings.Payment.errmsg4);
                            tipeUser = false;
                        }
                        else if (type == TipePemainIntEnum.Agent)
                        {
                            //Agent Member
                            tipeUser = true;
                        }
                        else
                        {
                            if (Date.now() > Date.parse(jsonDate))
                            {
                                //QGolf Member Expired
                                Utils.errAlert("Error Message", Strings.Payment.errmsg5);
                                tipeUser = false;
                            }
                            else
                            {
                                //QGolf Member
                                tipeUser = true;
                            }
                        }

                        if (tipeUser && this.state.redeembuttonvisible)
                        {
                            this.fn_usepoint();
                        }
                        else if (tipeUser && this.state.unredeembuttonvible)
                        {
                            this.setState({
                                point: 0,
                                balance: this.state.pointdb,
                                
                                processpointvisible: false,
                                banktransfervisible: true,
                                creditcardvisible: true,
                                sharepaymentvisible: true,
                                txtPoint: Number.parseInt(this.state.pointdb) / clsSetting.AccountBalance,
                                txtReedemPoint: this.state.pointdb,
                                redeembuttonvisible: true,
                                unredeembuttonvible: false,
                                halfpoint: false,
                            });
                        }
                    })
                })
            }
        }

        const gotoPointProcess = () => {
            var pointplatform = Platform.OS === 'ios' ? this.state.pointOption.iOS : this.state.pointOption.Android;

            if(!this.state.btnUsePointClickable){
                Utils.errAlert("Error Message", Strings.Payment.errmsg2);
            }
            else if(Utils.getMaintenanceOption(this.state.maintenanceOption)){
                Utils.alertBackToMain(this.state.maintenanceOption.Note, this.props);
            }
            else if(!this.state.platform) {
                Utils.errAlert("QAccess Alert", this.state.noteerr);
            }
            else if(!pointplatform) {
                Utils.errAlert("QAccess Alert", pointOption.Note);
            }
            else
            {
                this.props.navigation.navigate(this.state.linkterms, {Point: this.state.pointdb, BookingCode: this.state.bookingcode,
                    BookingDate: this.state.bookingdate, ChargePayment: this.state.chargepaymentcc, 
                    TransType: this.state.transtype, PaymentType: PaymentIntEnum.FullPoint, Total: this.state.total,
                    TransDate: this.state.transdate });
            }
        }

        const gotoSharePaymentLink = () => 
        {
            var sharepaymentplatform = Platform.OS === 'ios' ? this.state.sharepaymentlinkOption.iOS : this.state.sharepaymentlinkOption.Android;

            if(Utils.getMaintenanceOption(this.state.maintenanceOption)){
                Utils.alertBackToMain(this.state.maintenanceOption.Note, this.props);
            }
            else if(!this.state.platform) {
                Utils.errAlert("QAccess Alert", this.state.noteerr);
            }
            else if(!sharepaymentplatform) {
                Utils.errAlert("QAccess Alert", sharepaymentlinkOption.Note);
            }
            else
            {
                //Share by email or sms
                var url = clsSetting.PaymentLinkUrl + this.state.bookingcode;
                Share.share({
                    message: "Hi! You hereby receive an email to pay for your transaction on QAccess. Please click on the following link to start the payment process :" + url,
                    url: url,
                    title: "[QAccess] : Payment Request",
                }, {
                    // Android only:
                    dialogTitle: 'Share',
                    // iOS only:
                    excludedActivityTypes: [
                      'com.apple.UIKit.activity.PostToTwitter'
                    ]
                })
            }
        }

        return (
            <SafeAreaView style={styles.safeAreaView}>
                <ScrollView style={styles.containerScroll}>
                    <View style={styles.container}>
                        <View style={{flex: 1, flexDirection: 'row', backgroundColor: '#efefef', paddingLeft: 15, 
                            paddingRight: 15, paddingTop: 10, paddingRight: 10, alignItems: 'center'}}>
                            <View style={{flex: 1, flexDirection: 'row', marginTop: 5, marginBottom: 5,}}>
                                <Text style={{fontSize: 15, color: Colors.text, fontWeight: 'bold',}} >
                                    {Strings.Payment.text1}</Text>
                                <Text style={{fontSize: 15, color: Colors.text, fontWeight: 'bold', }} >
                                    {this.state.bookingcode}</Text>
                            </View>
                            <Text style={{fontSize: 12, color: Colors.text, flex: 1, textAlign: 'right',}} >
                                {this.state.bookingdate}</Text>
                        </View>
                        <Card style={styles.cardNote}>
                            <Image source={require('../../assets/images/information.png')} 
                                style={{height: 20, width: 20, resizeMode: 'contain', }}/>
                            <Text style={styles.textNote} >{Strings.Payment.text2}</Text>
                            <Text style={styles.textNote} >{Utils.convertMinuteIntoString(this.state.timeremaining)}</Text>
                        </Card>
                        {
                            Utils.renderIf(this.state.pulsavisible, 
                                <Card style={styles.cardVertical}>
                                    <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center', }}>
                                        <Image source={this.state.imgproviderpulsa} 
                                            style={{height: 40, width: 50, resizeMode: 'contain', }}/>
                                        <Text style={styles.text} >{this.state.titlepulsa}</Text>
                                    </View>
                                    <View style={{flex: 1, flexDirection: 'row', marginTop: 5, }}>
                                        <Text style={styles.textLeft} >{Strings.Payment.text8}</Text>
                                        <Text style={styles.textRight} >{this.state.destpulsa}</Text>
                                    </View>
                                    <View style={{flex: 1, flexDirection: 'row', marginTop: 5, }}>
                                        <Text style={styles.textLeft} >{Strings.Payment.text9}</Text>
                                        <Text style={styles.textRight} >{this.state.activeperiodpulsa}</Text>
                                    </View>
                                    <View style={{flex: 1, flexDirection: 'row', marginTop: 5, }}>
                                        <Text style={styles.textLeft} >{Strings.Payment.text10}</Text>
                                        <Text style={styles.textTotalRight} >{"Rp. " + Utils.currencyfunc(Math.round(this.state.total*100)/100)}</Text>
                                    </View>
                                </Card>
                            )
                        }
                        {
                            Utils.renderIf(this.state.reservationvisible, 
                                <Card style={styles.cardVertical}>
                                    <View style={{flex: 1, flexDirection: 'row',}}>
                                        <Image source={require('../../assets/images/location.png')} 
                                            style={{height: 20, width: 20, resizeMode: 'contain', }}/>
                                        <Text style={styles.textLocation} >{this.state.coursenameReservation}</Text>
                                    </View>
                                    <View style={{flex: 1, flexDirection: 'row', marginTop: 10, }}>
                                        <Image source={require('../../assets/images/calendar.png')} 
                                            style={{height: 20, width: 20, resizeMode: 'contain', }}/>
                                        <Text style={styles.text} >{this.state.transdate}</Text>
                                        <Text style={styles.textSession} >{this.state.sessionReservation}</Text>
                                    </View>
                                    <Divider style={{ backgroundColor: '#414141', height: 1, width: '100%', marginTop: 5, }} />
                                    <View style={{flex: 1, flexDirection: 'row', marginTop: 10, }}>
                                        <Text style={styles.textTotalRight} >{"Rp. " + Utils.currencyfunc(Math.round(this.state.total*100)/100)}</Text>
                                    </View>
                                </Card>
                            )
                        }
                        {
                            Utils.renderIf(this.state.registrationqvisible == true || this.state.eventvisible == true, 
                                <Card style={styles.cardHorizontal}>
                                    <Text style={styles.textLeft} >{Strings.Payment.text3}</Text>
                                    <Text style={styles.textTotalRight} >{"Rp. " + Utils.currencyfunc(Math.round(this.state.total*100)/100)}</Text>
                                </Card>
                            )
                        }
                        {
                            Utils.renderIf(this.state.sharepaymentvisible && this.state.sharepaymentoptionvisible, 
                                <Card style={styles.cardHorizontal}>
                                    <View style={{flex: 1, flexDirection: 'row', }}>
                                        <TouchableOpacity style={{flex: 1, flexDirection: 'row', justifyContent:'center', alignContent:'center'}}
                                            onPress={ gotoSharePaymentLink } >
                                            <Text style={{flex: 2, fontSize: 15, color: Colors.text, textDecorationLine: 'underline', }} >{Strings.Payment.text4}</Text>
                                            <Image source={require('../../assets/images/next2.png')} 
                                                style={{height: 15, width: 15, resizeMode: 'contain', alignItems: 'flex-end',}}/>
                                        </TouchableOpacity>
                                    </View>
                                </Card>
                            )
                        }
                        {
                            Utils.renderIf(this.state.redeembuttonvisible,
                                <Card style={styles.cardVertical}>
                                    <View style={{flex: 1, flexDirection: 'row', }}>
                                        <Text style={styles.textLeft} >{Strings.Payment.text5}</Text>
                                        <TouchableOpacity style={{flex: 1, }}
                                            onPress={ gotoRedeem } >
                                            <Text style={styles.textTotalRight} >{Strings.Payment.redeembutton}</Text>
                                        </TouchableOpacity>
                                    </View>
                                    <Divider style={{ backgroundColor: '#414141', height: 1, width: '100%', marginTop: 5, }} />
                                    <View style={{flex: 2, flexDirection: 'row', marginTop: 10, }}>
                                        <Image source={require('../../assets/images/medal.png')} 
                                            style={{height: 20, width: 20, resizeMode: 'contain', }}/>
                                        <Text style={{fontSize: 15, marginLeft: 5, color: '#93b843',}} >{this.state.txtPoint}</Text>
                                        <Text style={styles.textTotalRight} >{"Rp. " + Utils.currencyfunc(Math.round(this.state.txtReedemPoint*100)/100)}</Text>
                                    </View>
                                </Card>
                            )
                        }
                        {
                            Utils.renderIf(this.state.unredeembuttonvible,
                                <Card style={styles.cardVertical}>
                                    <View style={{flex: 1, flexDirection: 'row', }}>
                                        <Text style={styles.textLeft} >{Strings.Payment.text5}</Text>
                                        {
                                            Utils.renderIf(this.state.halfpoint,
                                                <TouchableOpacity style={{flex: 1, }}
                                                    onPress={ gotoPointProcess } >
                                                    <Text style={[styles.textRight2, {color: '#93b843'}]} >{Strings.Payment.processbutton}</Text>
                                                </TouchableOpacity>
                                            )
                                        }
                                        <TouchableOpacity
                                            onPress={ gotoRedeem } >
                                            <Text style={{fontSize: 14, textAlign: 'right', color: '#b84242', marginLeft: 5,}} >{Strings.Payment.unredeembutton}</Text>
                                        </TouchableOpacity>
                                    </View>
                                    <Divider style={{ backgroundColor: '#414141', height: 1, width: '100%', marginTop: 5, }} />
                                    <View style={{flex: 2, flexDirection: 'row', marginTop: 10, }}>
                                        <Image source={require('../../assets/images/medal.png')} 
                                            style={{height: 20, width: 20, resizeMode: 'contain', }}/>
                                        <Text style={{fontSize: 15, marginLeft: 5, color: '#93b843',}} >{this.state.txtPoint}</Text>
                                        <Text style={styles.textTotalRight} >{"Rp. " + Utils.currencyfunc(Math.round(this.state.txtReedemPoint*100)/100)}</Text>
                                    </View>
                                </Card>
                            )
                        }
                        {
                            Utils.renderIf(this.state.banktransfervisible, 
                                <View style={styles.cardVertical}>
                                    <TouchableOpacity style={{ flex: 3, justifyContent:'center', alignContent:'center'}}
                                        onPress={ gotoBankTransfer }>
                                        <View  style={{flex: 4, flexDirection:'column', }}>
                                            <View style={{flex: 4, flexDirection: 'row', }}>
                                                <Text style={{fontSize: 15, color: Colors.text, flex: 2, }} >{Strings.Payment.text6}</Text>
                                                <Image source={require('../../assets/images/next2.png')} 
                                                    style={{height: 15, width: 15, resizeMode: 'contain', alignItems: 'flex-end',}}/>
                                            </View>
                                            <Divider style={{ backgroundColor: '#414141', height: 1, flex: 1, marginTop: 5, }} />
                                            <View style={{flex: 4, flexDirection: 'row', marginTop: 10, }}>
                                                <Image source={require('../../assets/images/mandiri.png')} 
                                                    style={{height: 40, width: 60, resizeMode: 'contain', }}/>
                                                <Image source={require('../../assets/images/permata.png')} 
                                                    style={{height: 40, width: 80, resizeMode: 'contain', marginLeft: 5, }}/>
                                                <Image source={require('../../assets/images/bca.png')} 
                                                    style={{height: 20, width: 80, resizeMode: 'contain', marginLeft: 5, marginTop: 10, }}/>
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            )
                        }
                        {
                            Utils.renderIf(this.state.creditcardvisible, 
                                <View style={styles.cardVertical}>
                                    <TouchableOpacity style={{flex: 3, justifyContent:'center', alignContent:'center' }}
                                        onPress={ gotoCreditCard }>
                                        <View style={{flex: 4, flexDirection:'column', }}>
                                            <View style={{flex: 4, flexDirection: 'row', }}>
                                                <Text style={{flex: 2, fontSize: 15, color: Colors.text,}} >{Strings.Payment.text7}</Text>
                                                <Image source={require('../../assets/images/next2.png')} 
                                                    style={{height: 15, width: 15, resizeMode: 'contain', alignItems: 'flex-end',}}/>
                                            </View>
                                            <Divider style={{ backgroundColor: '#414141', height: 1, flex: 4, marginTop: 5, }} />
                                            <View style={{flex: 4, flexDirection: 'row', marginTop: 10, }}>
                                                <Image source={require('../../assets/images/visa.png')} 
                                                    style={{height: 40, width: 40, resizeMode: 'contain', }}/>
                                                <Image source={require('../../assets/images/mastercard.png')} 
                                                    style={{height: 40, width: 40, resizeMode: 'contain', marginLeft: 5, }}/>
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            )
                        }
                    </View>
                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({

    safeAreaView: {
        flex: 1,
    },

    containerScroll:
    {
        flex: 1,
        backgroundColor: '#efefef',
    },

    container: {
        flex: 1,
        flexDirection: 'column',
    },

    image: {
        height: 20,
        width: 20,
        resizeMode: 'contain',
    },

    cardNote: {
        backgroundColor: Colors.backgroundColor,
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 5,
        marginLeft: 10,
        marginRight: 10,
        padding: 10,
        elevation: 2,
        borderRadius: 5,
    },

    textNote: {
        fontSize: 12,
        marginLeft: 3,
        color: Colors.text,
    },

    text: {
        fontSize: 15,
        marginLeft: 10,
        color: Colors.text,
    },

    textLeft: {
        flex: 1,
        fontSize: 15,
        marginRight: 10,
        textAlign: 'left',
        color: Colors.text,
    },

    textRight: {
        flex: 1,
        fontSize: 14,
        textAlign: 'right',
        color: Colors.text,
    },

    textRight2: {
        flex: 1,
        fontSize: 14,
        textAlign: 'right',
    },

    textTotalRight: {
        flex: 1,
        fontSize: 15,
        textAlign: 'right',
        color: '#93b843',
    },

    textSession: {
        fontSize: 15,
        marginLeft: 3,
        color: '#edb636',
    },

    textLocation: {
        fontSize: 15,
        marginLeft: 5,
        color: '#93b843',
    },

    cardVertical: {
        backgroundColor: Colors.backgroundColor,
        flex: 1,
        flexDirection: 'column',
        marginTop: 5,
        marginBottom: 5,
        marginLeft: 10,
        marginRight: 10,
        padding: 10,
        elevation: 2,
        borderRadius: 5,
    },

    cardHorizontal: {
        backgroundColor: Colors.backgroundColor,
        flex: 1,
        flexDirection: 'row',
        marginTop: 5,
        marginBottom: 5,
        marginLeft: 10,
        marginRight: 10,
        padding: 10,
        elevation: 2,
        borderRadius: 5,
    },
});


//textDecorationLine: 'underline'