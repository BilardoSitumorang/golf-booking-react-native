import React from 'react';
import {
    Image,
    StyleSheet,
    Text,
    Platform,
    AsyncStorage,
    SafeAreaView,
    View,
    ListView,
    TouchableOpacity,
    ActivityIndicator,
    Alert,
} from 'react-native';
import Colors from '../../constants/Colors';
import styled from 'styled-components';
import Strings from '../../constants/Strings';
import SearchCourseRowScreen from '../ListRow/SearchCourseRowScreen';
import DateTimePicker from 'react-native-modal-datetime-picker';
import moment from 'moment';
import { SinglePickerMaterialDialog } from 'react-native-material-dialog';
import { getCourses, } from '../../API/CourseService';
import Utils from '../Utils';
import clsSetting from '../../API/clsSetting';
import { getStringToDoubleDate,  } from '../../API/SettingService';
import { findOption,  } from '../../API/OptionService';
import { getProfile,  } from '../../API/UserService';
import { OptionIntEnum } from '../../constants/Enums';

export default class SearchScreen extends React.Component {
    static navigationOptions = {
        title: 'Search',
        headerTintColor: '#ffffff', 
        headerStyle: {
            backgroundColor: '#93b843',
        },
    };

    constructor(props){
        super(props);

        this.params = this.props.navigation.state.params;

        this.state = {
            isLoading: true,
            sessionArr: [ 'Morning', 'Afternoon' ],
            playerArr: [ "1", "2", "3", "4" ],
            email: '',
            location: this.params.Location == 'Click here to search' ? '' : this.params.Location,
            calendar: moment(Date.parse(this.params.Calendar)).format('YYYY-MM-DD'),
            isDateTimePickerVisible: false,
            sessionPicker: false,
            playerPicker: false,
            sessionSelectedItem: this.params.Session,
            playerSelectedItem: this.params.Player,
            session: this.params.SessionSelected,
            player: this.params.PlayerSelected,
            allowpublish: true,
            playertype: 0,
            listVisible: true,
        }
    }

    //Date Picker
    _showDateTimePicker = () => {
        this.setState({ isDateTimePickerVisible: true });
    }
    
    _hideDateTimePicker = () => {
        this.setState({ isDateTimePickerVisible: false });
    }

    _handleDatePicked = (date) => {
        console.log('A date has been picked: ', date);
        this._hideDateTimePicker();
        this.setState({calendar: moment(date).format('YYYY-MM-DD')});
        this.setState({ isLoading: true });
        this.reloadCourse(this.state.calendar, this.state.session, this.state.player, this.state.location);
    };

    async GetItem (course) {
        //Selected Row List
        this.setState({ username : await AsyncStorage.getItem('Username')});
        getProfile(this.state.username).then((jsonProfile) => 
        {
            this.setState({ playertype: jsonProfile.MemberProfile.TipeMember });
            this.props.navigation.navigate('DetailCourse', {Location: this.state.location, Calendar: this.state.calendar,
                Session: this.state.session, Player: this.state.player, PlayerType: this.state.playertype, 
                LocationSelected: course.Id, CourseName: course.Nama, CourseAddress: course.Alamat, 
                CourseImage: course.Gambar !== "" ? course.Gambar : clsSetting.HomeUrl + "images/no_image2.jpg", 
                PublishQouta: course.KuotaPublish, QQuota: course.KuotaQCard});
        })
        .catch((error) => {
            console.error(error);
            this.setState({isLoading: false});
        });
    }

    ListViewItemSeparator = () => {
        return (
            <View style={{ height: .5, width: "100%", backgroundColor: "#000", }} />
        );
    }

    componentDidMount() {
        this.reloadCourse(this.state.calendar, this.state.session, this.state.player, this.state.location);
    }

    isEmpty(obj) {
        for(var key in obj) {
            if(obj.hasOwnProperty(key))
                return false;
        }
        return true;
    }

    reloadCourse = (_calendar, _session, _player, _location ) =>
    {
        var sessionInt = _session === undefined ? 1 : _session === "Morning" ? 1 : 2;
        var playerInt = _player === undefined ? 1 : _player;

        findOption(OptionIntEnum.OptionPublishQuota).then((jsonOption) => 
        {
            this.setState({ allowpublish: Platform.OS == 'ios' ? jsonOption.iOS : jsonOption.Android, });
            if((Date.now() > (new Date()).setHours(8, 30)) && (Date.now() === Date.parse(_calendar)) && sessionInt === 1 ){
                Utils.errAlert("Error Message", Strings.Search.errmsg1);
            }
            else if((Date.now() > (new Date()).setHours(13, 30)) && (Date.now() === Date.parse(_calendar)) && sessionInt === 2 ){
                Utils.errAlert("Error Message", Strings.Search.errmsg1);
            }
            else {
                getStringToDoubleDate(_calendar).then((json) =>
                {
                    getCourses(json.result, sessionInt, playerInt, _location).then((jsonCourse) =>
                    {
                        let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
                        this.setState({
                            isLoading: false,
                            dataSource: ds.cloneWithRows(jsonCourse),
                        });

                        if(this.isEmpty(jsonCourse)){
                            this.setState({
                                listVisible: false,
                            })
                        }
                        else{
                            this.setState({
                                listVisible: true,
                            })
                        }
                    })
                    .catch((error) => {
                        console.error(error);
                    });
                })
                
            }
        })
        .catch((error) => {
            console.error(error);
            this.setState({isLoading: false});
        });
        
    }

    render() {
        if (this.state.isLoading) {
            return (
                <View style={{flex: 1, paddingTop: 20}}>
                    <ActivityIndicator />
                </View>
            );
        }

        return (
            <SafeAreaView style={styles.safeAreaView}>
                <HomeCardView style={{flexDirection: 'row', }}>
                    <View style={{flex: 2, flexDirection: 'row', justifyContent:'center', alignItems: 'center', backgroundColor: '#fff', marginRight: 2,}}>
                        <TouchableOpacity onPress={ this._showDateTimePicker }
                            style={{flex: 2, flexDirection: 'row', justifyContent:'center', alignItems: 'center', padding: 10, marginRight: 2, backgroundColor: '#fff', }} >
                            <Image style={styles.image} 
                                source={require('../../assets/images/calendar.png')}/>
                            <Text style={styles.text}>{this.state.calendar}</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{flex: 1.5, flexDirection: 'row', justifyContent:'center', alignItems: 'center', backgroundColor: '#fff', marginRight: 2,}}>
                        <TouchableOpacity onPress={() => this.setState({ sessionPicker: true }) } 
                            style={{flex: 1.5, flexDirection: 'row', justifyContent:'center', alignItems: 'center', padding: 10, marginRight: 2, backgroundColor: '#fff', }} >
                            <Image style={styles.image} 
                                source={require('../../assets/images/session.png')}/>
                            <Text style={styles.text} >{this.state.sessionSelectedItem === undefined
                                ? 'Morning' : `${
                                    this.state.sessionSelectedItem.label
                                }`}</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{flex: 1, flexDirection: 'row', justifyContent:'center', alignItems: 'center', backgroundColor: '#fff', }}>
                        <TouchableOpacity onPress={() => this.setState({ playerPicker: true }) } 
                            style={{flex: 1, flexDirection: 'row', justifyContent:'center', alignItems: 'center', padding: 10, backgroundColor: '#fff', }} >
                            <Image style={styles.image} 
                                source={require('../../assets/images/golfer.png')}/>
                            <Text style={styles.text} >{this.state.playerSelectedItem === undefined
                                ? '1' : `${
                                    this.state.playerSelectedItem.label
                                }`}</Text>
                        </TouchableOpacity>
                    </View>
                </HomeCardView>
                <View style={styles.containerScroll}>
                    {
                        Utils.renderIf(this.state.listVisible == true,
                            <ListView
                                dataSource={this.state.dataSource}
                                renderSeparator= {this.ListViewItemSeparator}
                                renderRow={(rowData) =>
                                    <TouchableOpacity 
                                        onPress={ this.GetItem.bind(this, rowData) } >
                                        <SearchCourseRowScreen imageUri={{uri: rowData.Gambar != null ? rowData.Gambar : clsSetting.HomeUrl + "images/no_image2.jpg"}}
                                            nama={rowData.Nama}
                                            location={rowData.Alamat}
                                            quotaQ={rowData.KuotaQCard !== 0 ? rowData.KuotaQCard : "Quota Full"}
                                            quotaPublish={rowData.KuotaPublish !== 0 ? rowData.KuotaPublish : "Quota Full"}
                                            allowpublish={this.state.allowpublish}
                                            startfrom={"Rp. " + Utils.currencyfunc(Math.round(rowData.QGolfRate*100)/100)}/>
                                    </TouchableOpacity>
                                }
                            />
                        )
                    }
                    {
                        Utils.renderIf(this.state.listVisible == false,
                            <Text style={styles.textNote}>{Strings.Search.text4}</Text>
                        )
                    }
                    
                    <SinglePickerMaterialDialog
                        title={'Session'}
                        items={this.state.sessionArr.map((row, index) => ({ value: index, label: row }))}
                        visible={this.state.sessionPicker}
                        selectedItem={this.state.sessionSelectedItem}
                        onCancel={() => this.setState({ sessionPicker: false })}
                        onOk={result => {
                            this.setState({ sessionPicker: false });
                            this.setState({
                                sessionSelectedItem: result.selectedItem,
                                session: result.selectedItem.label,
                            });
                            this.setState({ isLoading: true });
                            this.reloadCourse(this.state.calendar, result.selectedItem.label, this.state.player, this.state.location);
                        }}
                    />
                    <SinglePickerMaterialDialog
                        title={'Player'}
                        items={this.state.playerArr.map((row, index) => ({ value: index, label: row }))}
                        visible={this.state.playerPicker}
                        selectedItem={this.state.playerSelectedItem}
                        onCancel={() => this.setState({ playerPicker: false })}
                        onOk={result => {
                            this.setState({ playerPicker: false });
                            this.setState({
                                playerSelectedItem: result.selectedItem,
                                player: result.selectedItem.label,
                            });
                        }}
                    />
                    <DateTimePicker
                        isVisible={this.state.isDateTimePickerVisible}
                        onConfirm={this._handleDatePicked}
                        onCancel={this._hideDateTimePicker}
                    />
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({

    safeAreaView: {
        flex: 1,
        backgroundColor: '#f2f2f2',
    },

    containerScroll:
    {
        flex: 1,
        backgroundColor: '#f2f2f2',
    },

    container: {
        backgroundColor: '#f2f2f2',
        height: 200,
        width: '100%'
    },

    image: {
        width: 25,
        height: 25,
        tintColor: '#000000'
    },

    text: {
        fontSize: 15,
        left: 5,
        color: Colors.text,
    },

    textNote: {
        fontSize: 16,
        marginTop: 10,
        flex: 1,
        textAlign: 'center',
        color: '#414141',
    },

    textTitle: {
        fontSize: 18,
        left: 15,
        top: 15,
        color: '#fff',
        fontWeight: 'bold',
    },

    imageList: {
        flex: 1,
        height: 180,
        width: null,
        borderRadius: 5,
        resizeMode: 'cover',
    },

    cardList: {
        backgroundColor: Colors.backgroundColor,
        height: 180,
        margin: 10,
        padding: 2,
    },
});

const HomeCardView = styled.View`
  justify-content: center;
  align-items: center;
  background-color: #f2f2f2;
`;