import React from 'react';
import { ScrollView, 
    StyleSheet, 
    WebView,
    SafeAreaView,
    View,
    Text,
    Image,
} from 'react-native';
import Strings from '../constants/Strings';

export default class CustomerCallScreen extends React.Component {
    
    static navigationOptions = {
        title: 'Customer Call',
        headerTintColor: '#ffffff', 
        headerStyle: {
            backgroundColor: '#93b843'
        },
    };

    render() {
        return (
            <SafeAreaView style={styles.safeAreaView}>
                <View style={styles.container}>
                    <Text style={styles.text}>{Strings.CustomerCall.text1}</Text>
                    <Image style={{width: 80, height: 80,}} source={require('../assets/images/phone.png')}></Image>
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    
    container: {
        flex: 1,
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
    },

    text: {
        marginLeft: 15,
        marginRight: 15,
        marginBottom: 20,
        fontSize: 15,
        color: '#414141',
        textAlign: 'center',
    },

    safeAreaView: {
        flex: 1
    },
});
