import React from 'react';
import Moment from 'moment';
import clsSetting from '../API/clsSetting';

exports.findHolidayByDate = (_date) => {
    return fetch(clsSetting.APIUrl + "pengaturan/FindHolidayByDate?date=" + _date +
    "&ApiSecretKey=" + clsSetting.ApiSecretKey)
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        if (json.status !== 401 && json.status === 200) {
            return json; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error Holiday By Date ', e));  
}

exports.getNotifications = (_date) => {
    return fetch(clsSetting.APIUrl + "pengaturan/getnotifications?date=" + _date +
    "&ApiSecretKey=" + clsSetting.ApiSecretKey)
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        if (json.status === 200) {
            return json.result; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error Notifications ', e));  
}

exports.getUnreadNotifications = (_userid) => {
    return fetch(clsSetting.APIUrl + "pengaturan/countunreadnotifications?date=" + Date.now() + "&iduser=" + _userid +
    "&ApiSecretKey=" + clsSetting.ApiSecretKey)
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        if (json.status !== 401 && json.status === 200) {
            console.log('json Unread Notifications ', json.result);
            return json; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error Unread Notifications ', e));  
}

exports.getReadNotification = (_userid) => {
    return fetch(clsSetting.APIUrl + "pengaturan/readnotifications?date=" + Date.now() + "&iduser=" + _userid +
    "&ApiSecretKey=" + clsSetting.ApiSecretKey)
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        if (json.status !== 401 && json.status === 200) {
            console.log('json Read Notifications ', json.result);
            return json; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error Read Notifications ', e));  
}

exports.getStringToDoubleDate = (_stringDate) => {
    return fetch(clsSetting.APIUrl + "utility/convertdatetimestringtodouble?param=" + _stringDate + "&ApiSecretKey=" + clsSetting.ApiSecretKey)
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        if (json.status !== 400 && json.status === 200) {
            console.log('json Date Double ', json);
            return json; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error Date Double ', e));  
}

exports.getDoubleToStringDate = (_doubleDate) => {
    return fetch(clsSetting.APIUrl + "utility/convertdoubletodatetimestring?param=" + _doubleDate + "&ApiSecretKey=" + clsSetting.ApiSecretKey)
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        if (json.status !== 400 && json.status === 200) {
            console.log('json Date String ', json);
            return json;
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error Date String ', e));  
}