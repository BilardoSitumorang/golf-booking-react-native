export default class TransactionEventDetailModel
{
    constructor(_nomember, _title, _tipemember, _jenismember, _name, _idpricing )
    {    
        this.NoMember = _nomember;
        this.TitlePemain = _title;
        this.TipeMember = _tipemember;
        this.JenisMember = _jenismember;
        this.NamaPemain = _name;
        this.IdPricing = _idpricing;
    }
}