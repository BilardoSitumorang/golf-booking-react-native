import React from 'react';
import { 
    StyleSheet, 
    Image,
    View,
    Text,
    Dimensions,
} from 'react-native';
import Colors from '../../constants/Colors';
import { Card, } from 'react-native-material-cards';

const dimensions = Dimensions.get('window');
const imageWidth = dimensions.width - 30;
const imageHeight = Math.round(imageWidth / 2);

export default class MerchantRowScreen extends React.Component {
    
    static navigationOptions = {
        title: '',
        header: null
    };

    render() {

        return (
            <Card style={styles.cardList} >
                <View style={{flex: 3,}}>
                    <Image style={styles.imageList} 
                            source={this.props.image}/>
                </View>
                <View style={{flex: 1, paddingLeft: 5, justifyContent: 'center', alignItems: 'center', 
                    flexDirection: 'row',}}>
                    <Text style={styles.text} >{this.props.name}</Text>
                </View>
            </Card>
        );
    }
}

const styles = StyleSheet.create({

    imageList: {
        height: imageHeight,
        width: imageWidth,
        resizeMode: 'cover',
    },

    cardList: {
        backgroundColor: Colors.backgroundColor,
        width: imageWidth,
        height: imageHeight + 30,
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 15,
        marginRight: 15,
        elevation: 2,
        borderRadius: 5,
    },

    text: {
        fontSize: 16,
        color: Colors.text,
        marginTop: 10,
        fontWeight: 'bold',
    },
});