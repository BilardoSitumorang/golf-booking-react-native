import React from 'react';
import {
    Image,
    Dimensions,
    ScrollView,
    StyleSheet,
    AsyncStorage,
    SafeAreaView,
    ActivityIndicator,
    TouchableOpacity,
    ListView,
    Text,
    View,
    BackHandler,
} from 'react-native';
import Colors from '../../constants/Colors';
import Strings from '../../constants/Strings';
import { Divider, Icon, } from 'react-native-elements';
import { Card } from 'react-native-material-cards';
import { getEventTicket, } from '../../API/TransactionService';
import Utils from '../Utils';
import { PaymentIntEnum } from '../../constants/Enums';
import { getDoubleToStringDate,  } from '../../API/SettingService';
import clsSetting from '../../API/clsSetting';
import TicketRowScreen from '../ListRow/TicketRowScreen';
import BookModel from '../../Model/BookModel';
import moment from 'moment';
import MapView from 'react-native-maps';

const dimensions = Dimensions.get('window');
const imageWidth = dimensions.width;
const imageHeight = Math.round(dimensions.width / 2);

export default class TicketEventScreen extends React.Component {
    static navigationOptions = ({navigation}) => ({
        title: 'E-Ticket',
        headerTintColor: '#ffffff', 
        headerStyle: {
            backgroundColor: '#93b843'
        },
        headerLeft: (
            <Icon name="chevron-left"
                onPress={() => {
                    navigation.popToTop();
                }}
                size={35} color="white"/>
        ),
    });

    constructor(props){
        super(props);

        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);

        this.state = {
            isLoading: true,
            username: '',
            bookingcode: '',
            bookingdate: '',
            point: 0,
            conveniencefee: 0,
            total: 0,
            txtpayment: '',
            totaltransfer: 0,

            imageevent: '',
            description: '',
            eventcat: '',
            eventdate: '',
            includes: '',
            excludes: '',
            location: '',

            playervisible: false,
            bookmodel: [],

            latitude: 0,
            longitude: 0,
        };
    }

    handleBackButtonClick() {
        this.props.navigation.popToTop();
        return true;
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount()
    {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    async componentDidMount() {
        var Enumerable = require('../linq');
        this.setState({ bookingcode : await AsyncStorage.getItem('BookingCode')});
        this.setState({ username : await AsyncStorage.getItem('Username')});
            
        getEventTicket(this.state.bookingcode).then((jsonTicket) =>
        {
            this.setState({
                description: jsonTicket.Description,
                eventcat: jsonTicket.Category,
                includes: jsonTicket.Includes == '' ? '-' : jsonTicket.Includes,
                excludes: jsonTicket.Excludes == '' ? "-" : jsonTicket.Excludes,
                eventdate: jsonTicket.TimeStart + ' - ' + jsonTicket.TimeEnd,
                imageevent: clsSetting.HomeUrl + jsonTicket.Thumbnail,
                
                point: jsonTicket.Point,
                conveniencefee: jsonTicket.ConvenienceFee,
                totaltransfer: jsonTicket.Total,
                total: jsonTicket.TotalDetail,

                latitude: jsonTicket.Latitude,
                longitude: jsonTicket.Longitude,
            });

            var i;
            for(i = 0; i < jsonTicket.Players.length; i++) {
                this.state.bookmodel.push(new BookModel(jsonTicket.Players[i].NoMember, '', jsonTicket.Players[i].TipeMember, 
                    jsonTicket.Players[i].JenisMember[i], jsonTicket.Players[i].NamaPemain));
            }
            
            if(jsonTicket.Players.length != 0)
            {
                let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
                this.setState({
                    playervisible: true,
                    dataSource: ds.cloneWithRows(this.state.bookmodel),
                });
            }
            
            this.setState({ bookingdate: moment(jsonTicket.Date).format('DD MMM YYYY'), });
            this.setState({isLoading: false, });
        })
        .catch((error) => {
            console.error(error);
            this.setState({isLoading: false, });
        });
    }

    ListViewItemSeparator = () => {
        return (
            <View style={{ height: .5, width: "100%", backgroundColor: "#000", }} />
        );
    }

    render() {
        if (this.state.isLoading) {
            return (
                <View style={{flex: 1, paddingTop: 20}}>
                    <ActivityIndicator />
                </View>
            );
        }

        return (
            <SafeAreaView style={styles.safeAreaView}>
                <ScrollView style={styles.containerScroll}>
                    <View style={styles.container}>
                        <View style={{flex: 1, flexDirection: 'row', backgroundColor: '#efefef', paddingLeft: 15, 
                            paddingRight: 15, paddingTop: 10, paddingRight: 10, alignItems: 'center'}}>
                            <View style={{flex: 1, flexDirection: 'row', marginTop: 5, marginBottom: 5,}}>
                                <Text style={{fontSize: 15, color: Colors.text, fontWeight: 'bold',}} >
                                    {Strings.TicketEvent.text1}</Text>
                                <Text style={{fontSize: 15, color: Colors.text, fontWeight: 'bold', }} >
                                    {this.state.bookingcode}</Text>
                            </View>
                            <Text style={{fontSize: 12, color: Colors.text, flex: 1, textAlign: 'right',}} >
                                {this.state.bookingdate}</Text>
                        </View>
                        <Image style={styles.image} 
                            source={{uri: this.state.imageevent}}/>
                        <Card style={styles.cardVertical}>
                            <View style={{flex: 1, flexDirection: 'row', marginTop: 5, justifyContent: 'center',}}>
                                <Image source={require('../../assets/images/calendar.png')} 
                                    style={{height: 20, width: 20, resizeMode: 'contain', tintColor: '#93b843', marginRight: 5, }}/>
                                <Text style={[styles.text, {color: Colors.text}]} >{this.state.bookingdate}</Text>
                            </View>
                        </Card>
                        <Text style={styles.textKet} >{Strings.TicketEvent.text2}</Text>
                        <Card style={styles.cardVertical}>
                            <View style={{flex: 1, flexDirection: 'row', marginTop: 5, }}>
                                <Text style={[styles.text, {color: Colors.text}]} >{this.state.description}</Text>
                            </View>
                        </Card>
                        <Text style={styles.textKet} >{Strings.TicketEvent.text3}</Text>
                        <Card style={styles.cardVertical}>
                            <View style={{flex: 1, flexDirection: 'row', marginTop: 5, }}>
                                <Image style={{height: 20, width: 20}} 
                                    source={require('../../assets/images/ic_tag.png')}/>
                                <Text style={[styles.text, {color: Colors.text, marginLeft: 5, }]} >{this.state.eventcat}</Text>
                            </View>
                            <View style={{flex: 1, flexDirection: 'row', marginTop: 5, }}>
                                <Image style={{height: 20, width: 20}} 
                                    source={require('../../assets/images/ic_timer.png')}/>
                                <Text style={[styles.text, {color: Colors.text, marginLeft: 5, }]} >{this.state.eventdate}</Text>
                            </View>
                            <Text style={styles.textKet2} >{Strings.TicketEvent.text4}</Text>
                            <View style={{flex: 1, flexDirection: 'row', marginTop: 5, }}>
                                <Text style={[styles.text, {color: Colors.text}]} >{this.state.includes}</Text>
                            </View>
                            <Text style={styles.textKet2} >{Strings.TicketEvent.text5}</Text>
                            <View style={{flex: 1, flexDirection: 'row', marginTop: 5, }}>
                                <Text style={[styles.text, {color: Colors.text}]} >{this.state.excludes}</Text>
                            </View>
                        </Card>
                        {
                            Utils.renderIf(this.state.playervisible,
                                <Text style={styles.textKet} >{Strings.TicketEvent.text6}</Text>
                            )
                        }
                        {
                            Utils.renderIf(this.state.playervisible,
                                <View style={{backgroundColor: '#ffffff', margin: 10, padding: 10, }}>
                                    {/* List Player */}
                                    <ListView
                                        dataSource={this.state.dataSource}
                                        renderSeparator= {this.ListViewItemSeparator}
                                        renderRow={(rowData) =>
                                            <TicketRowScreen 
                                                name={rowData.Nama}
                                                playername={rowData.PlayerName}
                                                type={rowData.Tipe}/>
                                        }
                                    />
                                </View>
                            )
                        }
                        <Text style={styles.textKet} >{Strings.TicketEvent.text7}</Text>
                        <View style={styles.cardVertical}>
                            {
                                Utils.renderIf(this.state.point != 0 || this.state.conveniencefee != 0,
                                    <View style={{flex: 1, flexDirection: 'row', }}>
                                        <Text style={styles.textLeft2} >{Strings.TicketPulsa.text8}</Text>
                                        <Text style={styles.textRight} >{'Rp. ' + Utils.currencyfunc(Math.round(this.state.total*100)/100)}</Text>
                                    </View>

                                )
                            }
                            {
                                Utils.renderIf(this.state.point != 0,
                                    <View style={{flex: 1, flexDirection: 'row', marginTop: 5, }}>
                                        <Text style={styles.textLeft2} >{Strings.TicketPulsa.text9}</Text>
                                        <Text style={styles.textRight} >{'Rp. ' + Utils.currencyfunc(Math.round(this.state.point*100)/100)}</Text>
                                    </View>
                                )
                            }
                            {
                                Utils.renderIf(this.state.conveniencefee != 0,
                                    <View style={{flex: 1, flexDirection: 'row', marginTop: 5, }}>
                                        <Text style={styles.textLeft2} >{Strings.TicketPulsa.text10}</Text>
                                        <Text style={styles.textRight} >{'Rp. ' + Utils.currencyfunc(Math.round(this.state.conveniencefee*100)/100)}</Text>
                                    </View>
                                )
                            }
                            {
                                 Utils.renderIf(this.state.point != 0 || this.state.conveniencefee != 0,
                                    <Divider style={{ backgroundColor: '#414141', height: 1, width: '100%', marginTop: 5, }} />
                                )
                            }
                            <View style={{flex: 1, flexDirection: 'row', marginTop: 5, }}>
                                <Text style={styles.textLeft2} >{Strings.TicketPulsa.text11}</Text>
                                <Text style={styles.textTotalRight} >{'Rp. ' + Utils.currencyfunc(Math.round(this.state.totaltransfer*100)/100)}</Text>
                            </View>
                        </View>
                        <Text style={styles.textKet} >{Strings.TicketEvent.text12}</Text>
                        {/* Google Maps */}
                        <View style={styles.containerMaps}>
                            <MapView // remove if not using Google Maps
                                style={styles.map}
                                region={{
                                    latitude: this.state.latitude,
                                    longitude: this.state.longitude,
                                    latitudeDelta: 0.115,
                                    longitudeDelta: 0.1121,
                                }}
                            ></MapView>
                        </View>
                    </View>
                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({

    safeAreaView: {
        flex: 1,
    },

    containerScroll:
    {
        flex: 1,
        backgroundColor: '#efefef',
    },

    container: {
        flex: 1,
        flexDirection: 'column',
    },

    image: {
        width: imageWidth,
        height: imageHeight,
        resizeMode: 'contain',
    },

    textKet: {
        fontSize: 18,
        marginTop: 10,
        marginLeft: 15,
        marginBottom: 5,
        color: Colors.text,
        fontWeight: 'bold',
    },

    textKet2: {
        fontSize: 15,
        color: Colors.text,
        fontWeight: 'bold',
    },

    text: {
        fontSize: 15,
    },

    textLeft: {
        flex: 1,
        fontSize: 15,
        marginRight: 10,
        textAlign: 'left',
        color: Colors.text,
    },

    textRight: {
        flex: 1,
        fontSize: 15,
        textAlign: 'right',
        color: Colors.text,
    },

    textTotalRight: {
        flex: 1,
        fontSize: 15,
        textAlign: 'right',
        color: '#93b843',
    },

    cardVertical: {
        backgroundColor: Colors.backgroundColor,
        flex: 1,
        flexDirection: 'column',
        marginTop: 5,
        marginBottom: 5,
        marginLeft: 10,
        marginRight: 10,
        padding: 10,
        elevation: 2,
        borderRadius: 5,
    },

    cardHorizontal: {
        backgroundColor: Colors.backgroundColor,
        flex: 1,
        flexDirection: 'row',
        marginTop: 5,
        marginBottom: 5,
        marginLeft: 10,
        marginRight: 10,
        padding: 10,
        elevation: 2,
        borderRadius: 5,
    },

    containerMaps: {
        height: 400,
        width: 400,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },

    map: {
        ...StyleSheet.absoluteFillObject,
    },
});


//textDecorationLine: 'underline'