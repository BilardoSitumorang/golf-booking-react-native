
import React from 'react';
import {
    StyleSheet, 
    AsyncStorage,
    Text, 
    Image,
    KeyboardAvoidingView,
    ActivityIndicator,
    View,
    ScrollView,
    Alert,
    SafeAreaView,
} from 'react-native';
import { Button, } from 'react-native-elements';
import Strings from '../constants/Strings';
import Colors from '../constants/Colors';
import { postForgotPassword } from '../API/UserService';
import BpkTextInput from 'react-native-bpk-component-text-input';
import Utils from './Utils';

export default class ForgotPasswordScreen extends React.Component{
    static navigationOptions = {
        title: '',
        headerTintColor: '#ffffff', 
        headerStyle: {
            backgroundColor: '#93b843'
          },
    };

    constructor(props)
    {
        super(props);
        this.state = {
            isLoading: false,
            email: '',
        };
    }

    render() {
        const gotoSend = () => {
            if(this.state.email.trim() == "")
            {
                Utils.errAlert("Error Message", Strings.ForgotPassword.errmsg1);
            }
            else
            {
                this.setState({isLoading: true});
                postForgotPassword(this.state.email).then((json) =>
                {
                    if(json){
                        Alert.alert(
                            "Confirm Booking",
                            "Reset password is already sent to your email.",
                            [
                                { text: 'YES', onPress: () => {
                                    this.setState({isLoading: false});
                                    this.props.navigation.navigate('SignIn');
                                }},
                            ],
                            { cancelable: false }
                        )
                    }
                });
            }
        }

        if (this.state.isLoading) {
            return (
                <View style={{flex: 1, paddingTop: 20}}>
                    <ActivityIndicator />
                </View>
            );
        }

        return (
            <SafeAreaView style={styles.safeAreaView}>
                <ScrollView style={styles.containerScroll}>
                    <KeyboardAvoidingView style={styles.containerScroll}>
                        <View style={styles.container}>
                            <Image source={require('../assets/images/locked.png')} style={styles.image}/>
                            <Text style={{color: '#999999', fontSize: 18, marginTop: 20, }}>{Strings.ForgotPassword.text1}</Text>
                            <Text style={{color: '#cecece', fontSize: 11, marginTop: 5, }}>{Strings.ForgotPassword.text2}</Text>
                            <View style={{width: '100%', marginTop: 5,}}>
                                <BpkTextInput 
                                    label={Strings.ForgotPassword.emailplaceholder}
                                    style={{marginBottom: 3, marginLeft: 20, marginRight: 20, }}
                                    onChangeText={(text) => this.setState({email: text})}
                                    value={this.state.email} />
                            </View>
                            <View style={{width: '100%', marginTop: 10,}}>
                                <Button 
                                    onPress={ gotoSend }
                                    buttonStyle={{backgroundColor: Colors.button, borderRadius: 5, marginTop: 10, height: 50, }}
                                    title={Strings.ForgotPassword.sendbutton}/>
                            </View>
                        </View>
                    </KeyboardAvoidingView>
                </ScrollView>
            </SafeAreaView>
            
        );
    }
}

const styles = StyleSheet.create({
    safeAreaView: {
        flex: 1
    },

    containerScroll:
    {
        flex: 1,
        backgroundColor: Colors.backgroundColor,
    },
    
    container: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
    },

    image: {
        height: 120,
        width: 120,
        marginTop: 70,
    },

    textcontainer: {
        flexDirection: 'row',
    },

    text: {
        fontSize: 12,
        textAlign: 'right',
        marginTop: 15,
        color: Colors.text,
    },
});
