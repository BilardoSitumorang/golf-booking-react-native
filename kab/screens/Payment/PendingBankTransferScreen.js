import React from 'react';
import {
    Image,
    ScrollView,
    AsyncStorage,
    StyleSheet,
    SafeAreaView,
    TouchableOpacity,
    ActivityIndicator,
    Text,
    View,
    BackHandler,
} from 'react-native';
import Colors from '../../constants/Colors';
import Strings from '../../constants/Strings';
import { Divider, Icon } from 'react-native-elements';
import { Card } from 'react-native-material-cards';
import Utils from '../Utils';
import moment from 'moment';
import { getDoubleToStringDate,  } from '../../API/SettingService';
import { findTrans, getPendingBankInformation,  } from '../../API/TransactionService';
import { TransactionTypeIntEnum, TipePemainStringEnum, TipePemainIntEnum } from '../../constants/Enums';

export default class PendingBankTransferScreen extends React.Component {
    static navigationOptions = ({navigation}) => ({
        title: 'Pending Bank Transfer',
        headerTintColor: '#ffffff', 
        headerStyle: {
            backgroundColor: '#93b843'
        },
        headerLeft: (
            <Icon name="chevron-left"
                onPress={() => {
                    navigation.popToTop();
                }}
                size={35} color="white"/>
        ),
    });

    constructor(props){
        super(props);

        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);

        this.state = {
            isLoading: true,
            username: '',
            bookingcode: '',
            bookingdate: '',
            transtype: '',
            accountname: '',
            accountnumber: '',
            accountnameket: '',
            accountnumberket: '',
            accountbank: '',

            point: 0,
            total: 0,
            totaltransfer: 0,
            conveniencefee: 0,
            convvisible: false,
            chargepayment: false,
            pointvisible: true,

            publishrates: "0",
            visitorrates: '0',
            clubrates: "0",
            qrates: "0",

            publishvisible: true,
            visitorvisible: true,
            clubvisible: true,
            qvisible: true,

            mandiri1visible: false,
            mandiri2visible: false,
            mandiri3visible: false,
            permata1visible: false,
            permata2visible: false,
            bca1visible: false,
            bca2visible: false,
            bca3visible: false,

            mandirilayoutvisible: false,
            permatalayoutvisible: false,
            bcalayoutvisible: false,

            txtmandiri1: '',
            txtmandiri2: '',
            txtmandiri3: '',
            txtpermata1: '',
            txtpermata2: '',
            txtbca1: '',
            txtbca2: '',
            txtbca3: '',
        };
    }

    handleBackButtonClick() {
        this.props.navigation.popToTop();
        return true;
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount()
    {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    async componentDidMount() {
        var Enumerable = require('../linq');
        this.setState({ bookingcode : await AsyncStorage.getItem('BookingCode')});
        this.setState({ username : await AsyncStorage.getItem('Username')});
        this.setState({ transtype : await AsyncStorage.getItem('TransType')});

        getPendingBankInformation(this.state.bookingcode).then((jsonPending) => 
        {      
            this.setState({
                txtmandiri1: 'Enter the Payment Code ' + jsonPending.bill_key,
                txtmandiri2: 'Enter the Payment Code '+ jsonPending.bill_key + ' and proceed by clicking Continue.',
                txtmandiri3: 'Input 013 (Permata Bank Code) and 16 digits virtual account ID: ' + jsonPending.va_number + ' then proceed.',
                txtpermata1: 'Input 013 (Permata Bank Code) and 16 digits virtual account ID: ' + jsonPending.va_number + ' then proceed.',
                txtpermata2: 'Input 16 digits virtual account ID: ' + jsonPending.va_number + ' then proceed.',
                txtbca1: 'Input BCA virtual account ID: ' + jsonPending.va_number + ' then proceed.',
                txtbca2: 'Input BCA virtual account ID: ' + jsonPending.va_number + ' then proceed.',
                txtbca3: 'Input BCA virtual account ID: ' + jsonPending.va_number + ' then proceed.',
            });

            this.setState({
                accountbank: jsonPending.bank,
                accountname: jsonPending.bank == "mandiri" ? jsonPending.biller_Code : jsonPending.va_name,
                accountnumber: jsonPending.bank == "mandiri" ? jsonPending.bill_key : jsonPending.va_number,
                accountnameket: jsonPending.bank == "mandiri" ? 'Company Name' : 'Account Name',
                accountnumberket: jsonPending.bank == "mandiri" ? 'Payment Code' : 'Account Number',
            });

            if(jsonPending.bank == "mandiri"){
                this.setState({ 
                    mandirilayoutvisible: true,
                    permatalayoutvisible: false,
                    bcalayoutvisible: false,
                });
            }
            else if(jsonPending.bank == "bca"){
                this.setState({ 
                    mandirilayoutvisible: false,
                    permatalayoutvisible: false,
                    bcalayoutvisible: true,
                });
            }
            else if(jsonPending.bank == "permata"){
                this.setState({ 
                    mandirilayoutvisible: false,
                    permatalayoutvisible: true,
                    bcalayoutvisible: false,
                });
            }

            findTrans(this.state.bookingcode).then((jsonTrans) =>
            {
                console.log(jsonTrans);
                var publish = Enumerable.from(jsonTrans.PemainViewModel).count(x => x.TipeMember == TipePemainIntEnum.Publish);
                var visitor = Enumerable.from(jsonTrans.PemainViewModel).count(x => x.TipeMember == TipePemainIntEnum.Visitor);
                var club = Enumerable.from(jsonTrans.PemainViewModel).count(x => x.TipeMember == TipePemainIntEnum.Club);
                var qrate = Enumerable.from(jsonTrans.PemainViewModel).count(x => x.TipeMember == TipePemainIntEnum.QMember);

                if(publish > 0)
                {
                    var publishdata= Enumerable.from(jsonTrans.PemainViewModel).where(x => x.TipeMember == TipePemainIntEnum.Publish).firstOrDefault();
                    var publishrate= publishdata.Harga;
                    this.setState({
                        publishrates: publish + " x " + Utils.currencyfunc(Math.round(publishrate*100)/100),
                        total: this.state.total + (publish * publishrate),
                        publishvisible: true, 
                    });

                }
                else
                {
                    this.setState({ publishvisible: false });
                }

                if(visitor > 0)
                {
                    var visitordata= Enumerable.from(jsonTrans.PemainViewModel).where(x => x.TipeMember == TipePemainIntEnum.Visitor).firstOrDefault();
                    var visitorrate= visitordata.Harga;
                    this.setState({
                        visitorrates: visitor + " x " + Utils.currencyfunc(Math.round(visitorrate*100)/100),
                        total: this.state.total + (visitor * visitorrate),
                        visitorvisible: true, 
                    });

                }
                else
                {
                    this.setState({ visitorvisible: false });
                }

                if(club > 0)
                {
                    var clubdata= Enumerable.from(jsonTrans.PemainViewModel).where(x => x.TipeMember == TipePemainIntEnum.Club).firstOrDefault();
                    var clubrate= clubdata.Harga;
                    this.setState({
                        clubrates: club + " x " + Utils.currencyfunc(Math.round(clubrate*100)/100),
                        total: this.state.total + (club * clubrate),
                        clubvisible: true, 
                    });

                }
                else
                {
                    this.setState({ clubvisible: false });
                }

                if(qrate > 0)
                {
                    var qmemberdata= Enumerable.from(jsonTrans.PemainViewModel).where(x => x.TipeMember == TipePemainIntEnum.QMember).firstOrDefault();
                    var qmemberrate= qmemberdata.Harga;
                    this.setState({
                        qrates: qrate + " x " + Utils.currencyfunc(Math.round(qmemberrate*100)/100),
                        total: this.state.total + (qrate * qmemberrate),
                        qvisible: true, 
                    });
                }
                else
                {
                    this.setState({ qvisible: false });
                }
                
                getDoubleToStringDate(jsonTrans.TanggalBuat).then((jsonDate) =>
                {
                    this.setState({ bookingdate: moment(Date.parse(jsonDate.result)).format('DD MMM YYYY'), });
                    if(this.state.transtype == TransactionTypeIntEnum.Reservation)
                    {
                        var Enumerable = require('../linq');

                        this.setState({ 
                            convvisible: false,
                            point: jsonPending.point,
                            pointvisible: jsonPending.point > 0 ? true : false,
                            totaltransfer: jsonPending.amount - jsonPending.point + this.state.conveniencefee,
                        });
                    }
                    else if(this.state.transtype == TransactionTypeIntEnum.RegistrationQ || this.state.transtype == TransactionTypeIntEnum.Event
                        || this.state.transtype == TransactionTypeIntEnum.MobilePulsaPhonePrepaid)
                    {
                        this.setState({
                            conveniencefee: jsonPending.is_charged ? 4000 : 0,
                        })
                        this.setState({ 
                            convvisible: jsonPending.is_charged,
                            total: jsonPending.amount,
                            chargepayment: jsonPending.is_charged,
                            point: jsonPending.point,
                            pointvisible: jsonPending.point > 0 ? true : false,
                            totaltransfer: jsonPending.amount - jsonPending.point +  this.state.conveniencefee,
                        });
                    }

                    this.setState({isLoading: false, });
                }) 
            })
        })
        .catch((error) => {
            console.error(error);
            this.setState({isLoading: false, });
        });
    }

    render() {
        if (this.state.isLoading) {
            return (
                <View style={{flex: 1, paddingTop: 20}}>
                    <ActivityIndicator />
                </View>
            );
        }

        const close = () => {
            this.props.navigation.navigate('App')
        }

        const mandiri1 = () =>
        {
            if(this.state.mandiri1visible){
                this.setState({ mandiri1visible: false, });
            }
            else{
                this.setState({ mandiri1visible: true, });
            }
        }

        const mandiri2 = () =>
        {
            if(this.state.mandiri2visible){
                this.setState({ mandiri2visible: false, });
            }
            else{
                this.setState({ mandiri2visible: true, });
            }
        }

        const mandiri3 = () =>
        {
            if(this.state.mandiri3visible){
                this.setState({ mandiri3visible: false, });
            }
            else{
                this.setState({ mandiri3visible: true, });
            }
        }

        const permata1 = () =>
        {
            if(this.state.permata1visible){
                this.setState({ permata1visible: false, });
            }
            else{
                this.setState({ permata1visible: true, });
            }
        }

        const permata2 = () =>
        {
            if(this.state.permata2visible){
                this.setState({ permata2visible: false, });
            }
            else{
                this.setState({ permata2visible: true, });
            }
        }

        const bca1 = () =>
        {
            if(this.state.bca1visible){
                this.setState({ bca1visible: false, });
            }
            else{
                this.setState({ bca1visible: true, });
            }
        }

        const bca2 = () =>
        {
            if(this.state.bca2visible){
                this.setState({ bca2visible: false, });
            }
            else{
                this.setState({ bca2visible: true, });
            }
        }

        const bca3 = () =>
        {
            if(this.state.bca3visible){
                this.setState({ bca3visible: false, });
            }
            else{
                this.setState({ bca3visible: true, });
            }
        }

        return (
            <SafeAreaView style={styles.safeAreaView}>
                <ScrollView style={styles.containerScroll}>
                    <View style={styles.container}>
                        <View style={{flex: 1, flexDirection: 'row', backgroundColor: '#efefef', paddingLeft: 15, 
                            paddingRight: 15, paddingTop: 10, paddingRight: 10, alignItems: 'center'}}>
                            <View style={{flex: 1, flexDirection: 'row', marginTop: 5, marginBottom: 5,}}>
                                <Text style={{fontSize: 15, color: Colors.text, fontWeight: 'bold',}} >
                                    {Strings.PendingBankTransfer.text1}</Text>
                                <Text style={{fontSize: 15, color: Colors.text, fontWeight: 'bold', }} >
                                    {this.state.bookingcode}</Text>
                            </View>
                            <Text style={{fontSize: 12, color: Colors.text, flex: 1, textAlign: 'right',}} >
                                {this.state.bookingdate}</Text>
                        </View>
                        <View style={styles.cardVertical}>
                            {
                                Utils.renderIf(this.state.convvisible || this.state.pointvisible,
                                    <View style={{flex: 1, flexDirection: 'row', }}>
                                        <Text style={styles.textLeft2} >{Strings.PendingBankTransfer.text2}</Text>
                                        <Text style={styles.textRight} >{'Rp. ' + Utils.currencyfunc(Math.round(this.state.total*100)/100)}</Text>
                                    </View>
                                )
                            }
                            {
                                Utils.renderIf(this.state.pointvisible,
                                    <View style={{flex: 1, flexDirection: 'row', marginTop: 5, }}>
                                        <Text style={styles.textLeft2} >{Strings.PendingBankTransfer.text3}</Text>
                                        <Text style={styles.textRight} >{'Rp. ' + Utils.currencyfunc(Math.round(this.state.point*100)/100)}</Text>
                                    </View>
                                )
                            }
                            {
                                Utils.renderIf(this.state.convvisible,
                                    <View style={{flex: 1, flexDirection: 'row', marginTop: 5, }}>
                                        <Text style={styles.textLeft2} >{Strings.PendingBankTransfer.text4}</Text>
                                        <Text style={styles.textRight} >{'Rp. ' + Utils.currencyfunc(Math.round(this.state.conveniencefee*100)/100)}</Text>
                                    </View>
                                )
                            }
                            {
                                Utils.renderIf(this.state.convvisible || this.state.pointvisible,
                                    <Divider style={{ backgroundColor: '#414141', height: 1, width: '100%', marginTop: 5, }} />
                                )
                            }
                            <View style={{flex: 1, flexDirection: 'row', marginTop: 5, }}>
                                <Text style={styles.textLeft2} >{Strings.PendingBankTransfer.text5}</Text>
                                <Text style={styles.textTotalRight} >{'Rp. ' + Utils.currencyfunc(Math.round(this.state.totaltransfer*100)/100)}</Text>
                            </View>
                        </View>
                        <Text style={styles.textKet} >{Strings.PendingBankTransfer.text6}</Text>
                        <Card style={styles.cardVertical}>
                            <View style={{flex: 1, flexDirection: 'row', }}>
                                <Text style={styles.textLeft2} >{Strings.PendingBankTransfer.text7}</Text>
                                <Text style={styles.textRight} >{this.state.accountbank}</Text>
                            </View>
                            <View style={{flex: 1, flexDirection: 'row', marginTop: 5, }}>
                                <Text style={styles.textLeft2} >{this.state.accountnameket}</Text>
                                <Text style={styles.textRight} >{this.state.accountname}</Text>
                            </View>
                            <View style={{flex: 1, flexDirection: 'row', marginTop: 5, }}>
                                <Text style={styles.textLeft2} >{this.state.accountnumberket}</Text>
                                <Text style={styles.textRight} >{this.state.accountnumber}</Text>
                            </View>
                        </Card>
                        {
                            Utils.renderIf(this.state.transtype == TransactionTypeIntEnum.Reservation,
                                <Text style={styles.textKet} >{Strings.PendingBankTransfer.text63}</Text>
                            )
                        }
                        {
                            Utils.renderIf(this.state.transtype == TransactionTypeIntEnum.Reservation,
                                <View style={styles.cardVertical}>
                                    {
                                        Utils.renderIf(this.state.publishvisible,
                                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', }}>
                                                <Text style={styles.textLeft2}>{Strings.PendingBankTransfer.text64}</Text>
                                                <Text style={styles.textRight}>{this.state.publishrates}</Text>
                                            </View>
                                        )
                                    }
                                    {
                                        Utils.renderIf(this.state.visitorvisible,
                                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', marginTop: 5, }}>
                                                <Text style={styles.textLeft2}>{Strings.PendingBankTransfer.text65}</Text>
                                                <Text style={styles.textRight}>{this.state.visitorrates}</Text>
                                            </View>
                                        )
                                    }
                                    {
                                        Utils.renderIf(this.state.clubvisible,
                                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', marginTop: 5, }}>
                                                <Text style={styles.textLeft2}>{Strings.PendingBankTransfer.text66}</Text>
                                                <Text style={styles.textRight}>{this.state.clubrates}</Text>
                                            </View>
                                        )
                                    }
                                    {
                                        Utils.renderIf(this.state.qvisible,
                                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', marginTop: 5, }}>
                                                <Text style={styles.textLeft2}>{Strings.PendingBankTransfer.text67}</Text>
                                                <Text style={styles.textRight}>{this.state.qrates}</Text>
                                            </View>
                                        )
                                    }
                                </View>
                            )
                        }
                        <Text style={styles.textKet} >{Strings.PendingBankTransfer.text10}</Text>
                        {
                            Utils.renderIf(this.state.mandirilayoutvisible,
                                <View style={styles.cardHorizontal}>
                                    <View style={{flex: 1, flexDirection: 'row', }}>
                                        <TouchableOpacity style={{flex: 1, flexDirection: 'row', }}
                                            onPress={ mandiri1 }>
                                            <Text style={{flex: 2, fontSize: 15, color: Colors.text, }} >{Strings.PendingBankTransfer.text11}</Text>
                                            <Image source={require('../../assets/images/next2.png')} 
                                                style={{height: 15, width: 15, resizeMode: 'contain', alignItems: 'flex-end',}}/>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            )
                        }
                        {
                            Utils.renderIf(this.state.mandiri1visible,
                                <Card style={styles.cardVertical}>
                                    <View style={{flex: 1, flexDirection: 'row', marginRight: 10, }}>
                                        <Text style={styles.textLeft}>{'1. '}</Text>
                                        <Text style={styles.textRight2}>{Strings.PendingBankTransfer.text12}</Text>
                                    </View>
                                    <View style={{flex: 1, flexDirection: 'row', marginRight: 10, }}>
                                        <Text style={styles.textLeft}>{'2. '}</Text>
                                        <Text style={styles.textRight2}>{Strings.PendingBankTransfer.text13}</Text>
                                    </View>
                                    <View style={{flex: 1, flexDirection: 'row', marginRight: 10, }}>
                                        <Text style={styles.textLeft}>{'3. '}</Text>
                                        <Text style={styles.textRight2}>{Strings.PendingBankTransfer.text14}</Text>
                                    </View>
                                    <View style={{flex: 1, flexDirection: 'row', marginRight: 10, }}>
                                        <Text style={styles.textLeft}>{'4. '}</Text>
                                        <Text style={styles.textRight2}>{this.state.txtmandiri1}</Text>
                                    </View>
                                    <View style={{flex: 1, flexDirection: 'row', marginRight: 10, }}>
                                        <Text style={styles.textLeft}>{'5. '}</Text>
                                        <Text style={styles.textRight2}>{Strings.PendingBankTransfer.text15}</Text>
                                    </View>
                                </Card>
                            )
                        }
                        {
                            Utils.renderIf(this.state.mandirilayoutvisible,
                                <View style={styles.cardHorizontal}>
                                    <View style={{flex: 1, flexDirection: 'row', }}>
                                        <TouchableOpacity style={{flex: 1, flexDirection: 'row', }}
                                            onPress={ mandiri2 }>
                                            <Text style={{flex: 2, fontSize: 15, color: Colors.text, }} >{Strings.PendingBankTransfer.text16}</Text>
                                            <Image source={require('../../assets/images/next2.png')} 
                                                style={{height: 15, width: 15, resizeMode: 'contain', alignItems: 'flex-end',}}/>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            )
                        }
                        {
                            Utils.renderIf(this.state.mandiri2visible,
                                <Card style={styles.cardVertical}>
                                    <View style={{flex: 1, flexDirection: 'row', marginRight: 10, }}>
                                        <Text style={styles.textLeft}>{'1. '}</Text>
                                        <Text style={styles.textRight2}>{Strings.PendingBankTransfer.text17}</Text>
                                    </View>
                                    <View style={{flex: 1, flexDirection: 'row', marginRight: 10,  }}>
                                        <Text style={styles.textLeft}>{'2. '}</Text>
                                        <Text style={styles.textRight2}>{Strings.PendingBankTransfer.text18}</Text>
                                    </View>
                                    <View style={{flex: 1, flexDirection: 'row', marginRight: 10, }}>
                                        <Text style={styles.textLeft}>{'3. '}</Text>
                                        <Text style={styles.textRight2}>{Strings.PendingBankTransfer.text19}</Text>
                                    </View>
                                    <View style={{flex: 1, flexDirection: 'row', marginRight: 10, }}>
                                        <Text style={styles.textLeft}>{'4. '}</Text>
                                        <Text style={styles.textRight2}>{Strings.PendingBankTransfer.text20}</Text>
                                    </View>
                                    <View style={{flex: 1, flexDirection: 'row', marginRight: 10, }}>
                                        <Text style={styles.textLeft}>{'5. '}</Text>
                                        <Text style={styles.textRight2}>{this.state.txtmandiri2}</Text>
                                    </View>
                                    <View style={{flex: 1, flexDirection: 'row', marginRight: 10, }}>
                                        <Text style={styles.textLeft}>{'6. '}</Text>
                                        <Text style={styles.textRight2}>{Strings.PendingBankTransfer.text21}</Text>
                                    </View>
                                </Card>
                            )
                        }
                        {
                            Utils.renderIf(this.state.permatalayoutvisible,
                                <View style={styles.cardHorizontal}>
                                    <View style={{flex: 1, flexDirection: 'row', }}>
                                        <TouchableOpacity style={{flex: 1, flexDirection: 'row', }}
                                            onPress={ mandiri3 }>
                                            <Text style={{flex: 2, fontSize: 15, color: Colors.text, }} >{Strings.PendingBankTransfer.text22}</Text>
                                            <Image source={require('../../assets/images/next2.png')} 
                                                style={{height: 15, width: 15, resizeMode: 'contain', alignItems: 'flex-end',}}/>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            )
                        }
                        {
                            Utils.renderIf(this.state.mandiri3visible,
                                <Card style={styles.cardVertical}>
                                    <View style={{flex: 1, flexDirection: 'row', marginRight: 10, }}>
                                        <Text style={styles.textLeft}>{'1. '}</Text>
                                        <Text style={styles.textRight2}>{Strings.PendingBankTransfer.text23}</Text>
                                    </View>
                                    <View style={{flex: 1, flexDirection: 'row', marginRight: 10, }}>
                                        <Text style={styles.textLeft}>{'2. '}</Text>
                                        <Text style={styles.textRight2}>{Strings.PendingBankTransfer.text24}</Text>
                                    </View>
                                    <View style={{flex: 1, flexDirection: 'row', marginRight: 10, }}>
                                        <Text style={styles.textLeft}>{'3. '}</Text>
                                        <Text style={styles.textRight2}>{Strings.PendingBankTransfer.text25}</Text>
                                    </View>
                                    <View style={{flex: 1, flexDirection: 'row', marginRight: 10, }}>
                                        <Text style={styles.textLeft}>{'4. '}</Text>
                                        <Text style={styles.textRight2}>{this.state.txtmandiri3}</Text>
                                    </View>
                                    <View style={{flex: 1, flexDirection: 'row', marginRight: 10, }}>
                                        <Text style={styles.textLeft}>{'5. '}</Text>
                                        <Text style={styles.textRight2}>{Strings.PendingBankTransfer.text26}</Text>
                                    </View>
                                    <View style={{flex: 1, flexDirection: 'row', marginRight: 10, }}>
                                        <Text style={styles.textLeft}>{'6. '}</Text>
                                        <Text style={styles.textRight2}>{Strings.PendingBankTransfer.text27}</Text>
                                    </View>
                                    <View style={{flex: 1, flexDirection: 'row', marginRight: 10, }}>
                                        <Text style={styles.textLeft}>{'7. '}</Text>
                                        <Text style={styles.textRight3}>{Strings.PendingBankTransfer.text28}</Text>
                                    </View>
                                </Card>
                            )
                        }
                        {
                            Utils.renderIf(this.state.permatalayoutvisible,
                                <View style={styles.cardHorizontal}>
                                    <View style={{flex: 1, flexDirection: 'row', }}>
                                        <TouchableOpacity style={{flex: 1, flexDirection: 'row', }}
                                            onPress={ permata1 }>
                                            <Text style={{flex: 2, fontSize: 15, color: Colors.text, }} >{Strings.PendingBankTransfer.text29}</Text>
                                            <Image source={require('../../assets/images/next2.png')} 
                                                style={{height: 15, width: 15, resizeMode: 'contain', alignItems: 'flex-end',}}/>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            )
                        }
                        {
                            Utils.renderIf(this.state.permata1visible, 
                                <Card style={styles.cardVertical}>
                                    <View style={{flex: 1, flexDirection: 'row', marginRight: 10, }}>
                                        <Text style={styles.textLeft}>{'1. '}</Text>
                                        <Text style={styles.textRight2}>{Strings.PendingBankTransfer.text30}</Text>
                                    </View>
                                    <View style={{flex: 1, flexDirection: 'row', marginRight: 10, }}>
                                        <Text style={styles.textLeft}>{'2. '}</Text>
                                        <Text style={styles.textRight2}>{Strings.PendingBankTransfer.text31}</Text>
                                    </View>
                                    <View style={{flex: 1, flexDirection: 'row', marginRight: 10, }}>
                                        <Text style={styles.textLeft}>{'3. '}</Text>
                                        <Text style={styles.textRight2}>{Strings.PendingBankTransfer.text32}</Text>
                                    </View>
                                    <View style={{flex: 1, flexDirection: 'row', marginRight: 10, }}>
                                        <Text style={styles.textLeft}>{'4. '}</Text>
                                        <Text style={styles.textRight3}>{Strings.PendingBankTransfer.text33}</Text>
                                    </View>
                                    <View style={{flex: 1, flexDirection: 'row', marginRight: 10, }}>
                                        <Text style={styles.textLeft}>{'5. '}</Text>
                                        <Text style={styles.textRight2}>{Strings.PendingBankTransfer.text34}</Text>
                                    </View>
                                    <View style={{flex: 1, flexDirection: 'row', marginRight: 10, }}>
                                        <Text style={styles.textLeft}>{'6. '}</Text>
                                        <Text style={styles.textRight2}>{this.state.txtpermata1}</Text>
                                    </View>
                                    <View style={{flex: 1, flexDirection: 'row', marginRight: 10, }}>
                                        <Text style={styles.textLeft}>{'7. '}</Text>
                                        <Text style={styles.textRight3}>{Strings.PendingBankTransfer.text68}</Text>
                                    </View>
                                </Card>
                            )
                        }
                        {
                            Utils.renderIf(this.state.permatalayoutvisible,
                                <View style={styles.cardHorizontal}>
                                    <View style={{flex: 1, flexDirection: 'row', }}>
                                        <TouchableOpacity style={{flex: 1, flexDirection: 'row', }}
                                            onPress={ permata2 }>
                                            <Text style={{flex: 2, fontSize: 15, color: Colors.text, }} >{Strings.PendingBankTransfer.text35}</Text>
                                            <Image source={require('../../assets/images/next2.png')} 
                                                style={{height: 15, width: 15, resizeMode: 'contain', alignItems: 'flex-end',}}/>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            )
                        }
                        {
                            Utils.renderIf(this.state.permata2visible,
                                <Card style={styles.cardVertical}>
                                    <View style={{flex: 1, flexDirection: 'row', marginRight: 10, }}>
                                        <Text style={styles.textLeft}>{'1. '}</Text>
                                        <Text style={styles.textRight2}>{Strings.PendingBankTransfer.text36}</Text>
                                    </View>
                                    <View style={{flex: 1, flexDirection: 'row', marginRight: 10, }}>
                                        <Text style={styles.textLeft}>{'2. '}</Text>
                                        <Text style={styles.textRight2}>{Strings.PendingBankTransfer.text37}</Text>
                                    </View>
                                    <View style={{flex: 1, flexDirection: 'row', marginRight: 10, }}>
                                        <Text style={styles.textLeft}>{'3. '}</Text>
                                        <Text style={styles.textRight2}>{Strings.PendingBankTransfer.text38}</Text>
                                    </View>
                                    <View style={{flex: 1, flexDirection: 'row', marginRight: 10, }}>
                                        <Text style={styles.textLeft}>{'4. '}</Text>
                                        <Text style={styles.textRight2}>{Strings.PendingBankTransfer.text39}</Text>
                                    </View>
                                    <View style={{flex: 1, flexDirection: 'row', marginRight: 10, }}>
                                        <Text style={styles.textLeft}>{'5. '}</Text>
                                        <Text style={styles.textRight2}>{this.state.txtpermata2}</Text>
                                    </View>
                                    <View style={{flex: 1, flexDirection: 'row', marginRight: 10, }}>
                                        <Text style={styles.textLeft}>{'6. '}</Text>
                                        <Text style={styles.textRight2}>{Strings.PendingBankTransfer.text40}</Text>
                                    </View>
                                    <View style={{flex: 1, flexDirection: 'row', marginRight: 10, }}>
                                        <Text style={styles.textLeft}>{'7. '}</Text>
                                        <Text style={styles.textRight3}>{Strings.PendingBankTransfer.text41}</Text>
                                    </View>
                                </Card>
                            )
                        }
                        {
                            Utils.renderIf(this.state.bcalayoutvisible,
                                <View style={styles.cardHorizontal}>
                                    <View style={{flex: 1, flexDirection: 'row', }}>
                                        <TouchableOpacity style={{flex: 1, flexDirection: 'row', }}
                                            onPress={ bca1 }>
                                            <Text style={{flex: 2, fontSize: 15, color: Colors.text, }} >{Strings.PendingBankTransfer.text42}</Text>
                                            <Image source={require('../../assets/images/next2.png')} 
                                                style={{height: 15, width: 15, resizeMode: 'contain', alignItems: 'flex-end',}}/>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            )
                        }
                        {
                            Utils.renderIf(this.state.bca1visible,
                                <Card style={styles.cardVertical}>
                                    <View style={{flex: 1, flexDirection: 'row', marginRight: 10, }}>
                                        <Text style={styles.textLeft}>{'1. '}</Text>
                                        <Text style={styles.textRight2}>{Strings.PendingBankTransfer.text43}</Text>
                                    </View>
                                    <View style={{flex: 1, flexDirection: 'row', marginRight: 10, }}>
                                        <Text style={styles.textLeft}>{'2. '}</Text>
                                        <Text style={styles.textRight2}>{Strings.PendingBankTransfer.text44}</Text>
                                    </View>
                                    <View style={{flex: 1, flexDirection: 'row', marginRight: 10, }}>
                                        <Text style={styles.textLeft}>{'3. '}</Text>
                                        <Text style={styles.textRight2}>{Strings.PendingBankTransfer.text45}</Text>
                                    </View>
                                    <View style={{flex: 1, flexDirection: 'row', marginRight: 10, }}>
                                        <Text style={styles.textLeft}>{'4. '}</Text>
                                        <Text style={styles.textRight2}>{this.state.txtbca1}</Text>
                                    </View>
                                    <View style={{flex: 1, flexDirection: 'row', marginRight: 10, }}>
                                        <Text style={styles.textLeft}>{'5. '}</Text>
                                        <Text style={styles.textRight2}>{Strings.PendingBankTransfer.text46}</Text>
                                    </View>
                                </Card>
                            )
                        }
                        {
                            Utils.renderIf(this.state.bcalayoutvisible,
                                <View style={styles.cardHorizontal}>
                                    <View style={{flex: 1, flexDirection: 'row', }}>
                                        <TouchableOpacity style={{flex: 1, flexDirection: 'row', }}
                                            onPress={ bca2 }>
                                            <Text style={{flex: 2, fontSize: 15, color: Colors.text, }} >{Strings.PendingBankTransfer.text47}</Text>
                                            <Image source={require('../../assets/images/next2.png')} 
                                                style={{height: 15, width: 15, resizeMode: 'contain', alignItems: 'flex-end',}}/>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            )
                        }
                        {
                            Utils.renderIf(this.state.bca2visible,
                                <Card style={styles.cardVertical}>
                                    <View style={{flex: 1, flexDirection: 'row', marginRight: 10, }}>
                                        <Text style={styles.textLeft}>{'1. '}</Text>
                                        <Text style={styles.textRight2}>{Strings.PendingBankTransfer.text48}</Text>
                                    </View>
                                    <View style={{flex: 1, flexDirection: 'row', marginRight: 10, }}>
                                        <Text style={styles.textLeft}>{'2. '}</Text>
                                        <Text style={styles.textRight2}>{Strings.PendingBankTransfer.text49}</Text>
                                    </View>
                                    <View style={{flex: 1, flexDirection: 'row', marginRight: 10, }}>
                                        <Text style={styles.textLeft}>{'3. '}</Text>
                                        <Text style={styles.textRight2}>{Strings.PendingBankTransfer.text50}</Text>
                                    </View>
                                    <View style={{flex: 1, flexDirection: 'row', marginRight: 10, }}>
                                        <Text style={styles.textLeft}>{'4. '}</Text>
                                        <Text style={styles.textRight2}>{this.state.txtbca2}</Text>
                                    </View>
                                    <View style={{flex: 1, flexDirection: 'row', marginRight: 10, }}>
                                        <Text style={styles.textLeft}>{'5. '}</Text>
                                        <Text style={styles.textRight2}>{Strings.PendingBankTransfer.text51}</Text>
                                    </View>
                                    <View style={{flex: 1, flexDirection: 'row', marginRight: 10, }}>
                                        <Text style={styles.textLeft}>{'6. '}</Text>
                                        <Text style={styles.textRight3}>{Strings.PendingBankTransfer.text52}</Text>
                                    </View>
                                    <View style={{flex: 1, flexDirection: 'row', marginRight: 10, }}>
                                        <Text style={styles.textLeft}>{'7. '}</Text>
                                        <Text style={styles.textRight2}>{Strings.PendingBankTransfer.text53}</Text>
                                    </View>
                                </Card>
                            )
                        }
                        {
                            Utils.renderIf(this.state.bcalayoutvisible,
                                <View style={styles.cardHorizontal}>
                                    <View style={{flex: 1, flexDirection: 'row', }}>
                                        <TouchableOpacity style={{flex: 1, flexDirection: 'row', }}
                                            onPress={ bca3 }>
                                            <Text style={{flex: 2, fontSize: 15, color: Colors.text, }} >{Strings.PendingBankTransfer.text54}</Text>
                                            <Image source={require('../../assets/images/next2.png')} 
                                                style={{height: 15, width: 15, resizeMode: 'contain', alignItems: 'flex-end',}}/>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            )
                        }
                        {
                            Utils.renderIf(this.state.bca3visible,
                                <Card style={styles.cardVertical}>
                                    <View style={{flex: 1, flexDirection: 'row', marginRight: 10, }}>
                                        <Text style={styles.textLeft}>{'1. '}</Text>
                                        <Text style={styles.textRight2}>{Strings.PendingBankTransfer.text55}</Text>
                                    </View>
                                    <View style={{flex: 1, flexDirection: 'row', marginRight: 10, }}>
                                        <Text style={styles.textLeft}>{'2. '}</Text>
                                        <Text style={styles.textRight2}>{Strings.PendingBankTransfer.text56}</Text>
                                    </View>
                                    <View style={{flex: 1, flexDirection: 'row', marginRight: 10, }}>
                                        <Text style={styles.textLeft}>{'3. '}</Text>
                                        <Text style={styles.textRight2}>{Strings.PendingBankTransfer.text57}</Text>
                                    </View>
                                    <View style={{flex: 1, flexDirection: 'row', marginRight: 10, }}>
                                        <Text style={styles.textLeft}>{'4. '}</Text>
                                        <Text style={styles.textRight2}>{Strings.PendingBankTransfer.text58}</Text>
                                    </View>
                                    <View style={{flex: 1, flexDirection: 'row', marginRight: 10, }}>
                                        <Text style={styles.textLeft}>{'5. '}</Text>
                                        <Text style={styles.textRight2}>{this.state.txtbca3}</Text>
                                    </View>
                                    <View style={{flex: 1, flexDirection: 'row', marginRight: 10, }}>
                                        <Text style={styles.textLeft}>{'6. '}</Text>
                                        <Text style={styles.textRight2}>{Strings.PendingBankTransfer.text59}</Text>
                                    </View>
                                    <View style={{flex: 1, flexDirection: 'row', marginRight: 10, }}>
                                        <Text style={styles.textLeft}>{'7. '}</Text>
                                        <Text style={styles.textRight2}>{Strings.PendingBankTransfer.text60}</Text>
                                    </View>
                                    <View style={{flex: 1, flexDirection: 'row', marginRight: 10, }}>
                                        <Text style={styles.textLeft}>{'8. '}</Text>
                                        <Text style={styles.textRight2}>{Strings.PendingBankTransfer.text61}</Text>
                                    </View>
                                    <View style={{flex: 1, flexDirection: 'row', marginRight: 10, }}>
                                        <Text style={styles.textLeft}>{'9. '}</Text>
                                        <Text style={styles.textRight2}>{Strings.PendingBankTransfer.text62}</Text>
                                    </View>
                                </Card>
                            )
                        }
                    </View>
                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({

    safeAreaView: {
        flex: 1,
    },

    containerScroll:
    {
        flex: 1,
        backgroundColor: '#efefef',
    },

    container: {
        flex: 1,
        flexDirection: 'column',
    },

    image: {
        height: 20,
        width: 20,
        resizeMode: 'contain',
    },

    textKet: {
        fontSize: 18,
        marginTop: 10,
        marginLeft: 15,
        marginBottom: 5,
        color: Colors.text,
        fontWeight: 'bold',
    },

    cardNote: {
        backgroundColor: Colors.backgroundColor,
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 5,
        marginLeft: 10,
        marginRight: 10,
        padding: 10,
        elevation: 2,
        borderRadius: 5,
    },

    textNote: {
        fontSize: 12,
        marginLeft: 3,
        color: Colors.text,
    },

    text: {
        fontSize: 15,
        marginLeft: 10,
        color: Colors.text,
    },

    textLeft2: {
        flex: 1,
        fontSize: 15,
        marginRight: 10,
        textAlign: 'left',
        color: Colors.text,
    },

    textLeft: {
        fontSize: 15,
        marginRight: 10,
        textAlign: 'left',
        color: Colors.text,
    },

    textRight: {
        flex: 1,
        fontSize: 14,
        textAlign: 'right',
        color: Colors.text,
    },

    textRight2: {
        fontSize: 15,
        textAlign: 'left',
        color: Colors.text,
    },

    textRight3: {
        fontSize: 15,
        textAlign: 'left',
        paddingRight: 10,
        color: Colors.text,
    },

    textTotalRight: {
        flex: 1,
        fontSize: 15,
        textAlign: 'right',
        color: '#93b843',
    },

    cardVertical: {
        backgroundColor: Colors.backgroundColor,
        flex: 1,
        flexDirection: 'column',
        marginTop: 5,
        marginBottom: 5,
        marginLeft: 10,
        marginRight: 10,
        padding: 10,
        elevation: 2,
        borderRadius: 5,
    },

    cardHorizontal: {
        backgroundColor: Colors.backgroundColor,
        flex: 1,
        flexDirection: 'row',
        marginTop: 5,
        marginBottom: 5,
        marginLeft: 10,
        marginRight: 10,
        padding: 10,
        elevation: 2,
        borderRadius: 5,
    },
});