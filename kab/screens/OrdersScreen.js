import React from 'react';
import { 
    View,
    Dimensions, 
    StyleSheet,
    SafeAreaView,
    TouchableOpacity,
    Animated,
} from 'react-native';
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import OrdersActiveScreen from './OrdersActiveScreen';
import { TransactionTypeIntEnum } from '../constants/Enums';
import { Constants } from 'expo';
import ScrollableTabView, {DefaultTabBar, } from 'react-native-scrollable-tab-view';

const dimensions = Dimensions.get('window');
const width = dimensions.width;

export default class OrdersScreen extends React.Component {
    static navigationOptions = {
        title: '',
        header: null,
    };

    render() {
        const tabUnderlineStyle = {
            position: 'absolute',
            width: width / 4,
            height: 4,
            backgroundColor: '#93b843',
            bottom: 0,
        };

        return (
            <ScrollableTabView style={{marginTop: 20, }} 
                tabBarActiveTextColor={"#93b843"} 
                tabBarInactiveTextColor={"#414141"}
                tabBarUnderlineStyle={tabUnderlineStyle} >
                <OrdersActiveScreen tabLabel="Reservation" type={TransactionTypeIntEnum.Reservation} nav={this.props.navigation}/>
                <OrdersActiveScreen tabLabel="Registration" type={TransactionTypeIntEnum.RegistrationQ} nav={this.props.navigation}/>
                <OrdersActiveScreen tabLabel="Event" type={TransactionTypeIntEnum.Event} nav={this.props.navigation}/>
                <OrdersActiveScreen tabLabel="Pulsa" type={TransactionTypeIntEnum.MobilePulsaPhonePrepaid} nav={this.props.navigation}/>
            </ScrollableTabView>
        );
    }
}

const styles = StyleSheet.create({
    safeAreaView: {
        flex: 1
    },

    container: {
        flex: 1,
        paddingTop: 15,
        backgroundColor: '#f2f2f2',
    },

    tabBar: {
        flexDirection: 'row',
        paddingTop: Constants.statusBarHeight,
        backgroundColor: '#fff',
    },

    tabItem: {
        flex: 1,
        alignItems: 'center',
        padding: 16,
    },
});
