import React from 'react';
import Moment from 'moment';
import clsSetting from '../API/clsSetting';

exports.getMerchantCategories = () => {
    return fetch(clsSetting.APIUrl + "partnership/getmerchantcategories?ApiSecretKey=" + clsSetting.ApiSecretKey)
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        if (json.status === 200) {
            return json.result; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error Merchant Categories ', e));  
}

exports.findMerchantCategory = (_id) => {
    return fetch(clsSetting.APIUrl + "partnership/findmerchantcategory?id=" + _id +
    "&ApiSecretKey=" + clsSetting.ApiSecretKey)
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        if (json.status === 200) {
            return json.result; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error Find Merchant Category ', e));  
}

exports.getPartnerships = () => {
    return fetch(clsSetting.APIUrl + "partnership/getpartnerships?ApiSecretKey=" + clsSetting.ApiSecretKey)
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        if (json.status === 200) {
            return json.result; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error Partnerships ', e));  
}

exports.getPartnershipByMerchant = (_merchantid) => {
    return fetch(clsSetting.APIUrl + "partnership/getpartnershipsbymerchant?idMerchant=" + _merchantid +
    "&ApiSecretKey=" + clsSetting.ApiSecretKey)
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        if (json.status === 200) {
            return json.result; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error Partnership By Merchant ', e));  
}

exports.findPartnership = (_id) => {
    return fetch(clsSetting.APIUrl + "partnership/findpartnership?id=" + _id +
    "&ApiSecretKey=" + clsSetting.ApiSecretKey)
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        if (json.status === 200) {
            return json.result; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error Find Partnership ', e));  
}

exports.getMerchants = () => {
    return fetch(clsSetting.APIUrl + "partnership/getmerchants?ApiSecretKey=" + clsSetting.ApiSecretKey)
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        if (json.status === 200) {
            return json.result; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error Merchants ', e));  
}

exports.getMerchantByCategory = (_categoryid) => {
    return fetch(clsSetting.APIUrl + "partnership/getmerchantsbycategory?idCategory=" + _categoryid +
    "&ApiSecretKey=" + clsSetting.ApiSecretKey)
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        if (json.status === 200) {
            return json.result; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error Merchant By Category ', e));  
}

exports.findMerchant = (_id) => {
    return fetch(clsSetting.APIUrl + "partnership/findmerchant?id=" + _id +
    "&ApiSecretKey=" + clsSetting.ApiSecretKey)
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        if (json.status === 200) {
            return json.result; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error Find Merchant ', e));  
}

exports.getAdvertisements = () => {
    return fetch(clsSetting.APIUrl + "partnership/getadvertisements?date=" + Date.now + 
    "&ApiSecretKey=" + clsSetting.ApiSecretKey)
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        if (json.status === 200) {
            return json.result; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error Advertisement ', e));  
}

exports.findAdvertisement = (_id) => {
    return fetch(clsSetting.APIUrl + "partnership/findadvertisement?id=" + _id +
    "&ApiSecretKey=" + clsSetting.ApiSecretKey)
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        if (json.status === 200) {
            return json.result; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error Find Advertisement ', e));  
}