import React from 'react';
import {
    StyleSheet,
    SafeAreaView,
    TouchableOpacity,
    ListView,
    ActivityIndicator,
    AsyncStorage,
    View,
} from 'react-native';
import { getOptions, } from '../../API/OptionService';
import { getEventPrices, } from '../../API/EventService';
import ListEventPriceRowScreen from '../ListRow/ListEventPriceRowScreen';
import { KartuStringEnum, OptionIntEnum, TipePemainIntEnum, TransactionTypeIntEnum } from '../../constants/Enums';
import { Dialog, } from "react-native-simple-dialogs";
import Utils from '../Utils';
import BpkTextInput from 'react-native-bpk-component-text-input';

export default class ListEventPriceScreen extends React.Component {
    static navigationOptions = {
        title: 'Event Price',
        headerTintColor: '#ffffff', 
        headerStyle: {
            backgroundColor: '#93b843'
        },
    };

    constructor(props){
        super(props);

        this.params = this.props.navigation.state.params;

        this.state = {
            isLoading: true,
            inputvisible: false,
            username: '',
            usertype: '',
            inputname: '',

            idevent: this.params.IdEvent,
            location: this.params.Location,
            calendar: this.params.Calendar,
            total: 0,

            publishrate: 0,
            idpublish: 0,
            clubrate: 0,
            idclub: 0,
            qrate: 0,
            idqrate: 0,
        };
    }

    async componentDidMount() {
        this.setState({ usertype : await AsyncStorage.getItem('TipeUser')});
        this.setState({ username : await AsyncStorage.getItem('Username')});
        var Enumerable = require('../linq');

        getOptions().then((jsonOption) =>
        {
            this.setState({
                eventOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionEvent).firstOrDefault(),
                maintenanceOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionMaintenancePeriod).firstOrDefault()
            });
            if (this.state.usertype == TipePemainIntEnum.Agent)
            {
                getEventPrices(this.state.idevent, "").then((jsonEvent) =>
                {
                    let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
                    this.setState({
                        isLoading: false,
                        dataSource: ds.cloneWithRows(jsonEvent),
                    });
                });
            }
            else
            {
                getEventPrices(this.state.idevent, this.state.username).then((jsonEvent) =>
                {
                    let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
                    this.setState({
                        isLoading: false,
                        dataSource: ds.cloneWithRows(jsonEvent),
                    });
                });
            }

            
        });
    }

    GetItem (row) {
        //Selected Row List
        
    }

    ListViewItemSeparator = () => {
        return (
            <View style={{ height: 4, width: "100%", }} />
        );
    }

    render() {
        if (this.state.isLoading) {
            return (
                <View style={{flex: 1, paddingTop: 20}}>
                    <ActivityIndicator />
                </View>
            );
        }

        return (
            <SafeAreaView style={styles.safeAreaView}>
                <View style={styles.container}>
                    <ListView
                        dataSource={this.state.dataSource}
                        renderSeparator= {this.ListViewItemSeparator}
                        renderRow={(rowData) =>
                            <TouchableOpacity 
                                onPress={ this.GetItem.bind(this, rowData) } >
                                <ListEventPriceRowScreen
                                    title={rowData.PricingName}
                                    session={rowData.Session}
                                    agent={this.state.usertype == TipePemainIntEnum.Agent ? true : false}
                                    username={this.state.username}
                                    maintenanceOption={this.state.maintenanceOption}
                                    eventOption={this.state.eventOption}
                                    usertype={this.state.usertype}
                                    idpricing={rowData.Id}
                                    idevent={this.state.idevent}
                                    property={this.props}
                                    total={Utils.currencyfunc(Math.round(rowData.Price*100)/100)}
                                    ket2={"1 Adult (Rp " + Utils.currencyfunc(Math.round(rowData.Price*100)/100) + "/pax)"}/>
                            </TouchableOpacity>
                        }
                    />
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({

    safeAreaView: {
        flex: 1,
    },

    containerScroll:
    {
        flex: 1,
        backgroundColor: '#efefef',
    },

    container: {
        flex: 1,
        flexDirection: 'column',
        margin: 10,
    },


});