import React from 'react';
import {
    StyleSheet,
    View,
    WebView,
    SafeAreaView,
    ActivityIndicator,
} from 'react-native';
import Colors from '../../constants/Colors';
import { getPartnershipByMerchant } from '../../API/PartnershipService';
import Utils from '../Utils';
import clsSetting from '../../API/clsSetting';
import DetailMerchantRowScreen from '../ListRow/DetailMerchantRowScreen';

export default class DetailPartnershipScreen extends React.Component{
    static navigationOptions = {
        title: 'Website Partnership',
        headerTintColor: '#ffffff', 
        headerStyle: {
            backgroundColor: '#93b843',
        },
    };

    constructor(props)
    {
        super(props);
        this.params = this.props.navigation.state.params;
        this.state = {
            url: this.params.Url,
        };
    }

    render() {
        return (
            <SafeAreaView style={styles.safeAreaView}>
                <View style={styles.containerView}>
                    <WebView
                        source={{uri: this.state.url}}
                    />
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.backgroundColor,
    },

    safeAreaView: {
        flex: 1
    },

    containerView: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
    },

    image: {
        height: 70,
        width: 70,
    },

    textTitle: {
        fontSize: 15,
        color: Colors.text,
        marginLeft: 15,
    },

    text: {
        fontSize: 13,
        marginTop: 5,
        marginLeft: 15,
        marginRight: 15,
        color: Colors.text,
    },
});

