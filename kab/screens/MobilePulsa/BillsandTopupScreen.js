import React from 'react';
import {
    Image,
    ScrollView,
    StyleSheet,
    SafeAreaView,
    TouchableOpacity,
    Text,
    View,
} from 'react-native';
import Colors from '../../constants/Colors';
import Strings from '../../constants/Strings';
import { Divider, } from 'react-native-elements';
import { Card } from 'react-native-material-cards';

export default class BillsandTopupScreen extends React.Component {
    static navigationOptions = {
        title: 'Bills & Topup',
        headerTintColor: '#ffffff', 
        headerStyle: {
            backgroundColor: '#93b843'
        },
    };

    render() {
        const gotoPulsa = () => {
            this.props.navigation.navigate('Pulsa')
        }

        return (
            <SafeAreaView style={styles.safeAreaView}>
                <ScrollView style={styles.containerScroll}>
                    <View style={styles.container}>
                        <View style={{flex: 1, flexDirection: 'row', paddingLeft: 10, 
                            paddingRight: 10, paddingTop: 10, paddingRight: 10, alignItems: 'center', }}>
                            <Card style={styles.cardVertical}>
                                <TouchableOpacity style={{flex: 1, flexDirection: 'column', justifyContent: 'center', 
                                    alignItems: 'center',}}
                                    onPress={ gotoPulsa }>
                                    <Image source={require('../../assets/images/pulsa.png')} style={styles.image}/>
                                    <Text style={styles.text} >{Strings.BillsAndToup.text1}</Text>
                                </TouchableOpacity>
                            </Card>
                            <Card style={styles.cardVertical}>
                                <Image source={require('../../assets/images/pulsa.png')} style={styles.imageGray}/>
                                <Text style={styles.text} >{Strings.BillsAndToup.text2}</Text>
                            </Card>
                            <Card style={styles.cardVertical}>
                                <Image source={require('../../assets/images/drop.png')} style={styles.imageGray}/>
                                <Text style={styles.text} >{Strings.BillsAndToup.text3}</Text>
                            </Card>
                        </View>
                        <View style={{flex: 1, flexDirection: 'row', paddingLeft: 10, 
                            paddingRight: 10, paddingTop: 10, paddingRight: 10, alignItems: 'center'}}>
                            <Card style={styles.cardVertical}>
                                <Image source={require('../../assets/images/thunder.png')} style={styles.imageGray}/>
                                <Text style={styles.text} >{Strings.BillsAndToup.text4}</Text>
                            </Card>
                            <Card style={styles.cardVertical}>
                                <Image source={require('../../assets/images/bpjs.png')} style={styles.imageGray}/>
                                <Text style={styles.text} >{Strings.BillsAndToup.text5}</Text>
                            </Card>
                            <Card style={styles.cardVertical}>
                                <Image source={require('../../assets/images/game.png')} style={styles.imageGray}/>
                                <Text style={styles.text} >{Strings.BillsAndToup.text6}</Text>
                            </Card>
                        </View>
                        <View style={{flex: 1, flexDirection: 'row', paddingLeft: 10, 
                            paddingRight: 10, paddingTop: 10, paddingRight: 10, alignItems: 'center'}}>
                            <Card style={styles.cardVertical}>
                                <Image source={require('../../assets/images/satellitedish.png')} style={styles.imageGray}/>
                                <Text style={styles.text} >{Strings.BillsAndToup.text7}</Text>
                            </Card>
                            <Card style={styles.cardVertical}>
                                <Image source={require('../../assets/images/calculator.png')} style={styles.imageGray}/>
                                <Text style={styles.text} >{Strings.BillsAndToup.text8}</Text>
                            </Card>
                            <Card style={styles.cardVertical}>
                                <Image source={require('../../assets/images/telkom.png')} style={styles.imageGray}/>
                                <Text style={styles.text} >{Strings.BillsAndToup.text9}</Text>
                            </Card>
                        </View>
                    </View>
                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({

    safeAreaView: {
        flex: 1,
    },

    containerScroll:
    {
        flex: 1,
        backgroundColor: '#efefef',
    },

    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#f2f2f2',
    },

    image: {
        height: 35, 
        width: 35, 
        resizeMode: 'contain', 
        tintColor: '#000',
    },

    imageGray: {
        height: 35, 
        width: 35, 
        resizeMode: 'contain', 
        tintColor: '#efefef',
    },

    text: {
        fontSize: 13,
        marginTop: 5,
        color: Colors.text,
        textAlign: 'center',
    },

    cardVertical: {
        backgroundColor: Colors.backgroundColor,
        height: 100,
        flexDirection: 'column',
        justifyContent: 'center', 
        alignItems: 'center',
        marginTop: 5,
        marginBottom: 5,
        marginLeft: 5,
        marginRight: 5,
        padding: 10,
        elevation: 2,
        borderRadius: 5,
    },
});