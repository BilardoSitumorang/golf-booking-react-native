import React from 'react';
import { 
    ScrollView, 
    StyleSheet,
    ListView,
    ActivityIndicator,
    SafeAreaView,
    TouchableOpacity,
    Text,
    View,
} from 'react-native';
import { getNotifications,  } from '../API/SettingService';
import moment from 'moment';

export default class InboxScreen extends React.Component {
    static navigationOptions = {
        title: '',
        header: null,
    };

    constructor(props){
        super(props);

        this.state = {
            isLoading: true,
        }
    }

    GetItem (course) {
        //Selected Row List
    }

    ListViewItemSeparator = () => {
        return (
            <View style={{ height: .5, width: "100%", backgroundColor: "#f2f2f2", }} />
        );
    }

    componentDidMount() {
        var datenow = moment(Date.now()).format('YYYY-MM-DD');
        getNotifications(datenow).then((json) =>
        {
            console.log('notif', json);
            let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
            this.setState({
                isLoading: false,
                dataSource: ds.cloneWithRows(json[0].Notification),
            });
        })
        .catch((error) => {
            console.error(error);
            this.setState({ isLoading: false});
        });
    }

    render() {
        if (this.state.isLoading) {
            return (
                <View style={{flex: 1, paddingTop: 20}}>
                    <ActivityIndicator />
                </View>
            );
        }

        return (
            <SafeAreaView style={styles.safeAreaView}>
                <View style={styles.container}>
                    <ListView
                        dataSource={this.state.dataSource}
                        renderSeparator= {this.ListViewItemSeparator}
                        renderRow={(rowData) =>
                            <TouchableOpacity 
                                onPress={ this.GetItem.bind(this, rowData) } >
                                <View style={{backgroundColor: '#fff'}}>
                                    <Text style={{marginTop: 10, marginLeft: 15, marginRight: 15,
                                        fontSize: 17, fontWeight:'bold' }} >{rowData.Title}</Text>
                                    <Text style={{marginTop: 5, marginBottom: 10, marginLeft: 20, 
                                        marginRight: 20, fontSize: 15, }} >{rowData.Description}</Text>
                                </View>
                            </TouchableOpacity>
                        }
                    />
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    safeAreaView: {
        flex: 1
    },

    containerScroll: {
        flex: 1,
        backgroundColor: '#ffffff',
    },

    container: {
        flex: 1,
        paddingTop: 15,
        backgroundColor: '#f2f2f2',
    },
});
