import React from 'react';
import {
    Image,
    Platform,
    ScrollView,
    StyleSheet,
    SafeAreaView,
    TouchableOpacity,
    ActivityIndicator,
    Dimensions,
    Text,
    View,
} from 'react-native';
import Colors from '../../constants/Colors';
import Strings from '../../constants/Strings';
import { Button, } from 'react-native-elements';
import { getOptions,  } from '../../API/OptionService';
import { OptionStringEnum, OptionIntEnum } from '../../constants/Enums';
import clsSetting from '../../API/clsSetting';
import Utils from '../Utils';

export default class DetailCardScreen extends React.Component {
    static navigationOptions = {
        title: 'Member Card',
        headerTintColor: '#ffffff', 
        headerStyle: {
            backgroundColor: '#93b843'
        },
    };

    constructor(props){
        super(props);

        this.params = this.props.navigation.state.params;

        this.state = {
            isLoading: true,
            username: '',

            id: this.params.Id,
            type: this.params.Type,
            name: this.params.Name,
            description: this.params.Description,
            price: this.params.Price,
            rate: this.params.Rate,
            cardid: this.params.ClassId,
            cardimage: this.params.ClassImage !== "" ? clsSetting.HomeUrl + this.params.ClassImage : clsSetting.HomeUrl + "images/no_image2.jpg",
            absorbcreditcardfee: this.params.AbsorbCreditCardFee,
            publishQuota: this.params.PublishQouta,
            qQuota: this.params.QQuota,
        }
    }

    componentDidMount() {
        var Enumerable = require('../linq');
        getOptions().then((jsonOption) => 
        {
            this.setState({
                registrationOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionRegistration).firstOrDefault(),
                maintenanceOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionMaintenancePeriod).firstOrDefault()
            });

            this.setState({ isLoading: false, });
        })
        .catch((error) => {
            console.error(error);
            this.setState({isLoading: false, });
        });
    }

    render() {
        if (this.state.isLoading) {
            return (
                <View style={{flex: 1, paddingTop: 20}}>
                    <ActivityIndicator />
                </View>
            );
        }

        const gotoForm1 = () => {
            var platform = Platform.OS === 'ios' ? this.state.registrationOption.iOS : this.state.registrationOption.Android;
            if(Utils.getMaintenanceOption(this.state.maintenanceOption)){
                Utils.alertBackToMain(this.state.maintenanceOption.Note, this.props);
            }
            else if(!platform) {
                Utils.errAlert("QAccess Alert", this.state.registrationOption.Note);
            }
            else 
            {
                this.props.navigation.navigate('Form1', {Id: this.state.id});
            }
        }

        const dimensions = Dimensions.get('window');
        const imageWidth = dimensions.width - 30;
        const btnHeight = Math.round(dimensions.width * 9 / 16);
        const btnWidth = dimensions.width - 30;

        return (
            <SafeAreaView style={styles.safeAreaView}>
                <ScrollView style={styles.containerScroll}>
                    <View style={styles.container}>
                        <Image 
                            style={{width: imageWidth, height: 220, resizeMode: 'stretch', borderRadius: 2, marginTop: 20, marginLeft: 15, marginRight: 15, }} 
                            source={{uri: this.state.cardimage}}/>
                        <View style={{flex:1, flexDirection: 'column', margin: 15, justifyContent: 'center', alignItems: 'center',}}>
                            <Text style={{fontSize: 16, color: Colors.text, fontWeight: 'bold', flex: 1,}}>{this.state.name}</Text>
                            <Text style={{fontSize: 13, color: Colors.text, flex: 1, }}>{this.state.description}</Text>
                        </View>
                        <Button 
                            onPress={ gotoForm1 }
                            buttonStyle={{backgroundColor: Colors.button, borderRadius: 5, marginTop: 10, marginLeft: 5, marginRight: 5, marginBottom: 20, height: 50, }}
                            title={Strings.DetailCard.joinbutton}/>
                    </View>
                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({

    safeAreaView: {
        flex: 1,
    },

    containerScroll:
    {
        flex: 1,
        backgroundColor: '#ffffff',
    },

    container: {
        flex: 1,
        flexDirection: 'column',
    },
});
