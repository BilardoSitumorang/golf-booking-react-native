import React from 'react';
import {
    Image,
    Platform,
    ScrollView,
    StyleSheet,
    SafeAreaView,
    TouchableOpacity,
    ActivityIndicator,
    AsyncStorage,
    Text,
    Dimensions,
    View,
} from 'react-native';
import Colors from '../../constants/Colors';
import Strings from '../../constants/Strings';
import { Button, } from 'react-native-elements';
import { Card, } from 'react-native-material-cards';
import BpkTextInput from 'react-native-bpk-component-text-input';
import { getOptions,  } from '../../API/OptionService';
import { getVirtualAccount, postAddMember } from '../../API/UserService';
import Enums, { TipePemainIntEnum, TipePemainEnum, PaymentIntEnum, OptionIntEnum, PlatformIntEnum, TransactionTypeIntEnum } from '../../constants/Enums';
import Utils from '../Utils';
import { getDoubleToStringDate,  } from '../../API/SettingService';
import RegisterQViewModel from '../../Model/RegisterQViewModel';
import moment from 'moment';
import clsSetting from '../../API/clsSetting';

const dimensions = Dimensions.get('window');
const imgWidth = dimensions.width - 20;
const btnWidth = dimensions.width - 60;

export default class RenewalScreen extends React.Component {
    static navigationOptions = {
        title: 'Renewal QGolf',
        headerTintColor: '#ffffff', 
        headerStyle: {
            backgroundColor: '#93b843'
        },
    };

    constructor(props){
        super(props);

        this.state = {
            isLoading: true,
            username: '',
            cardinput: '',
            mycardnumber: '',
            virtualcardvisible: false,
            imgcardvisible: false,

            nomembervirtualcard: '',
            tanggaljoinvirtualcard: '',
            namevirtualcard: '',
            imgcard: '',
            imgprofile: '',
        }
    }

    fn_loadcard = (_json) =>
    {
        getDoubleToStringDate(_json.ValidEnd).then((jsonDate) =>
        {
            this.setState({
                nomembervirtualcard: _json.NoMember,
                tanggaljoinvirtualcard: moment(Date.parse(jsonDate.result)).format('DD MMM YYYY'),
            });

            var type = _json.TypeMember;
            var typeMember = (type == TipePemainIntEnum.Agent) ? "Visitor - Agent" : Enums.TipePemainEnum(type);
            var kartu = _json.JenisMember;
            if(_json.Name !== '')
            {
                if(_json.Name.includes("@"))
                {
                    var name = _json.name.split('@')[0];
                    this.setState({ namevirtualcard: name });
                }
                else
                {
                    this.setState({ namevirtualcard: _json.Name });
                }
            }

            if(_json.Foto !== '')
            {
                this.setState({ imgprofile: clsSetting.HomeUrl + _json.Foto });
            }

            if(_json.Class.Image !== '')
            {
                this.setState({ imgcard: clsSetting.HomeUrl + _json.Class.Image });
            }

            if(_json.Class.Name === "Red")
            {
                //Set Red Card
            }

            if(_json.Class.Name === "Elder")
            {
                //Set Elder Card
            }

            this.setState({ virtualcardvisible: true, });
        })
        
    }

    async componentDidMount() {
        var Enumerable = require('../linq');
        this.setState({ mycardnumber : await AsyncStorage.getItem('MyCardNumber')});
        this.setState({ username : await AsyncStorage.getItem('Username')});

        getOptions().then((jsonOption) => 
        {
            this.setState({
                registrationOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionRegistration).firstOrDefault(),
                maintenanceOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionMaintenancePeriod).firstOrDefault()
            });

            getVirtualAccount(this.state.username).then(async (jsonvirtual) => 
            {
                if(jsonvirtual.TypeMember == TipePemainIntEnum.Publish || jsonvirtual.TypeMember == TipePemainIntEnum.Visitor || 
                    jsonvirtual.TypeMember == TipePemainIntEnum.Agent)
                {
                    Utils.alertBackToMain(Strings.Renewal.errmsg2, this.props);
                }
                else
                {
                    this.setState({ mycardnumber: jsonvirtual.NoMember });
                    await AsyncStorage.setItem("MyCardNumber", this.state.mycardnumber);
                    this.fn_loadcard(jsonvirtual);

                }
                this.setState({
                    cardinput: this.state.mycardnumber,
                    isLoading: false
                });

            })
            .catch((error) => {
                console.error(error);
            });
        })
        .catch((error) => {
            console.error(error);
            this.setState({isLoading: false, });
        });
    }

    render() {
        if (this.state.isLoading) {
            return (
                <View style={{flex: 1, paddingTop: 20}}>
                    <ActivityIndicator />
                </View>
            );
        }

        const pay = () =>
        {
            var platform = Platform.OS === 'ios' ? this.state.registrationOption.iOS : this.state.registrationOption.Android;
            
            if(Utils.getMaintenanceOption(this.state.maintenanceOption)){
                Utils.alertBackToMain(this.state.maintenanceOption.Note, this.props);
            }
            else if(!platform) {
                Utils.errAlert("QAccess Alert", this.state.registrationOption.Note);
            }
            else{
                if(this.state.cardinput == "")
                {
                    Utils.errAlert("Error Message", Strings.Renewal.errmsg1);
                }
                else
                {
                    var platformMobile = Platform.OS === 'ios' ? PlatformIntEnum.IOs : PlatformIntEnum.Android;
                    var registerModel = new RegisterQViewModel('', '', '', '', '', '', '', '', '', '', 
                        '', '', '', '', this.state.username, '', '', '', '', 
                        '', '', '', '', '', PaymentIntEnum.NotSpecified, '', 0, moment(Date.now()).format('YYYY-MM-DD'), 
                        0, 1, this.state.cardinput, this.state.username, null, platformMobile);
                    
                    postAddMember(registerModel).then(async (jsonAddMember) => 
                    {
                        await AsyncStorage.setItem("BookingCode", jsonAddMember);
                        this.props.navigation.navigate('Payment', {TransType: TransactionTypeIntEnum.RegistrationQ});
                    })
                    .catch((error) => {
                        console.error(error);
                    });
                }
            }

        }

        return (
            <SafeAreaView style={styles.safeAreaView}>
                <ScrollView style={styles.containerScroll}>
                    <View style={styles.container}>
                        {
                            Utils.renderIf(this.state.virtualcardvisible, 
                                <View style={{backgroundColor: '#efefef', flex: 1, 
                                    marginLeft: 10, marginRight: 10, marginTop: 10, marginBottom: 5, borderRadius: 5}}>
                                    <Image source={{uri: this.state.imgcard }} 
                                        style={{height: 220, width: imgWidth, resizeMode: 'stretch', }}/>
                                    <View style={styles.containerHover}>
                                        <View style={{flex: 1, flexDirection:'row', marginTop: 5, marginRight: 10, }}>
                                            <Image style={{width: 70, height: 70, borderRadius: 35, borderWidth: 1, 
                                                borderColor: '#414141'}} source={{uri: this.state.imgprofile }}/>
                                        </View>
                                        <View style={{flex: 1, marginTop: 5, marginRight: 10, }}>
                                            <Text style={styles.text}>{this.state.namevirtualcard}</Text>
                                        </View>
                                        <View style={{flex: 1, flexDirection: 'row', marginLeft: 10, marginRight: 10, marginTop: 5, }}>
                                            <Text style={styles.textLeftCard}>{'Expired Date' + "" + this.state.tanggaljoinvirtualcard}</Text>
                                            <Text style={styles.textRightCard}>{this.state.nomembervirtualcard}</Text>
                                        </View>
                                    </View>
                                </View>
                            )
                        }
                        <Card style={styles.card}>
                            <View style={{width: '100%', flexDirection: 'row', }}>
                                <BpkTextInput 
                                    label={''}
                                    onChangeText={(text) => this.setState({cardinput: text})}
                                    value={this.state.cardinput}
                                    style={{flex: 1, marginBottom: 3, }}/>
                            </View>
                        </Card>
                        <Text style={styles.textKet} >{Strings.Renewal.text2}</Text>
                        <View style={{backgroundColor: '#ffffff', paddingLeft: 20, paddingRight: 20, paddingTop: 10, paddingBottom: 10, }}>
                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', }}>
                                <Text style={styles.textLeft2}>{'• '}</Text>
                                <Text style={styles.textRight2}>{Strings.Renewal.text3}</Text>
                            </View>
                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', }}>
                                <Text style={styles.textLeft2}>{'• '}</Text>
                                <Text style={styles.textRight2}>{Strings.Renewal.text4}</Text>
                            </View>
                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', }}>
                                <Text style={styles.textLeft2}>{'• '}</Text>
                                <Text style={styles.textRight2}>{Strings.Renewal.text5}</Text>
                            </View>
                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', }}>
                                <Text style={styles.textLeft2}>{'• '}</Text>
                                <Text style={styles.textRight2}>{Strings.Renewal.text6}</Text>
                            </View>
                        </View>
                        <Button 
                            onPress={ pay }
                            buttonStyle={{backgroundColor: Colors.button, borderRadius: 5, marginTop: 10, marginLeft: 5, marginRight: 5, marginBottom: 20, height: 50, }}
                            title={Strings.Renewal.continuebutton}/>
                    </View>
                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({

    safeAreaView: {
        flex: 1,
    },

    containerScroll:
    {
        flex: 1,
        backgroundColor: '#efefef',
    },

    container: {
        flex: 1,
        flexDirection: 'column',
    },

    containerHover: {
        position: 'absolute',
        top: 80,
        left: 0,
        right: 0, 
        flexDirection: 'column',
        alignItems: 'flex-end',
    },

    text: {
        fontSize: 14,
        color: '#fff',
    },

    textLeftCard: {
        flex: 1,
        fontSize: 14,
        color: '#fff',
    },

    textRightCard: {
        flex: 1,
        fontSize: 14,
        color: '#fff',
        textAlign: 'right',
    },

    textKet: {
        fontSize: 18,
        marginTop: 10,
        marginLeft: 15,
        marginBottom: 10,
        marginRight: 15,
        color: Colors.text,
        fontWeight: 'bold',
    },

    textLeft2: {
        fontSize: 14,
        marginTop: 5,
        color: Colors.text,
        width: 10,
    },

    textRight2: {
        flex: 1,
        fontSize: 12,
        marginTop: 5,
        color: Colors.text,
    },

    card: {
        backgroundColor: Colors.backgroundColor,
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 10,
        marginRight: 10,
        marginTop: 10,
        marginBottom: 5,
        padding: 10,
        elevation: 2,
        borderRadius: 5,
    },
});
