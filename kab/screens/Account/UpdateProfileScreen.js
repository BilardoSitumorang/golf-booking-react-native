import React from 'react';
import {
    Image,
    StyleSheet,
    Text,
    View,
    ScrollView,
    TouchableOpacity,
    AsyncStorage,
    SafeAreaView,
} from 'react-native';
import Colors from '../../constants/Colors';
import Strings from '../../constants/Strings';
import { Button, Divider, } from 'react-native-elements';
import DateTimePicker from 'react-native-modal-datetime-picker';
import RadioGroup from 'react-native-radio-buttons-group';
import BpkTextInput from 'react-native-bpk-component-text-input';
import moment from 'moment';
import { getProfile, postUpdateProfile } from '../../API/UserService';
import { getDoubleToStringDate, getStringToDoubleDate} from '../../API/SettingService';
import clsSetting from '../../API/clsSetting';
import ProfileModel from '../../Model/ProfileModel';
import Utils from '../Utils';

var radio_props = [
    {label: Strings.UpdateProfile.gendermale , value: 0 },
    {label: Strings.UpdateProfile.genderfemale, value: 1 }
];

export default class UpdateProfileScreen extends React.Component {
    static navigationOptions = {
        title: 'Update Profile',
        headerTintColor: '#ffffff', 
        headerStyle: {
            backgroundColor: '#93b843'
        },
    };

    constructor(props)
    {
        super(props);
        this.state = {
            username: '',
            name: '',
            dateofbirth: moment(Date.now()).format('YYYY-MM-DD'),
            gender: '',
            address: '',
            city: '',
            postalcode: '',
            isDateTimePickerVisible: false,
            data: [
                {
                    label: 'Male',
                    color: 'green',
                },
                {
                    label: 'Female',
                    color: 'green',
                },
            ],
        };
    }

    onPress = data => this.setState({ data });

    async componentDidMount() {
        this.setState({ username : await AsyncStorage.getItem('Username')});
        getProfile(this.state.username).then((json) =>
        {
            this.setState({ 
                name : json.MemberProfile.Nama,
                type : json.MemberProfile.TipeMember,
                gender : json.MemberProfile.JenisKelamin,
                address : json.MemberProfile.Alamat == null ? '' : json.MemberProfile.Alamat,
                city: json.MemberProfile.Kota == null ? '' : json.MemberProfile.Kota,
                postalcode: json.MemberProfile.KodePos == null ? '' : json.MemberProfile.KodePos,
                foto: json.MemberProfile.Foto != "" ? clsSetting.HomeUrl + "" + json.MemberProfile.Foto : '',
            });

            getDoubleToStringDate(json.MemberProfile.TanggalLahir).then((jsonDate) =>
            {
                this.setState({ 
                    dateofbirth : moment(Date.parse(jsonDate.result)).format('YYYY-MM-DD'),
                });
    
            })
        })
        .catch((error) => {
            console.error(error);
        });
    }

    //Date Picker
    _showDateTimePicker = () => {
        this.setState({ isDateTimePickerVisible: true });
    }
    
    _hideDateTimePicker = () => {
        this.setState({ isDateTimePickerVisible: false });
    }

    _handleDatePicked = (date) => {
        console.log('A date has been picked: ', date);
        this._hideDateTimePicker();
        this.setState({dateofbirth: moment(date).format('YYYY-MM-DD')});
    };

    render() {
        let selectedButton = this.state.data.find(e => e.selected == true);
        selectedButton = selectedButton ? selectedButton.value : this.state.data[0].label;

        const gotoUpdate = () => {
            getStringToDoubleDate(this.state.dateofbirth).then(async (jsonDate) =>
            {
                var idGender = selectedButton == "Female" ? 2 : 1;
                var profile = new ProfileModel(this.state.username, this.state.address, idGender, this.state.postalcode,
                    this.state.name,  this.state.city, jsonDate.result);
                postUpdateProfile(profile).then(async (json) =>
                {
                    if(json.status == 200){
                        this.props.navigation.goBack();
                    }
                    else{
                        Utils.errAlert("QAccess Alert", json.message);
                    }
                })
            })
        }
        return (
            <SafeAreaView style={styles.safeAreaView}>
                <ScrollView style={styles.containerScroll}>
                    <View style={styles.container}>
                        <Image style={{width: 100, height: 100, borderRadius: 50, borderWidth: 1, marginTop: 30,
                            backgroundColor: '#fff', borderColor: '#d1d1d1'}} source={require('../../assets/images/account.png')}/>
                        <View style={{marginTop: 10, }}>
                            <View style={{width: '100%', flexDirection: 'row', marginTop: 5,}}>
                                <BpkTextInput 
                                    label={Strings.UpdateProfile.nameplaceholder}
                                    onChangeText={(text) => this.setState({name: text})}
                                    value={this.state.name}
                                    style={{flex: 1, marginBottom: 3, marginLeft: 20, marginRight: 20, }}/>
                            </View>
                            <View style={{width: '100%', flexDirection: 'row', marginTop: 15,}}>
                                {/* date of birth */}
                                <TouchableOpacity onPress={ this._showDateTimePicker } style={{flexDirection: 'column', marginLeft: 20, marginRight: 20,
                                    flex: 1, backgroundColor: '#ffffff', }}>
                                    <Text style={styles.input} >{this.state.dateofbirth}</Text>
                                    <Divider style={{ height: 1, backgroundColor: '#efefef', width: '100%', marginTop: 10, }} />
                                </TouchableOpacity>
                            </View>
                            <View style={{width: '100%', marginTop: 5,}}>
                                <View style={{marginBottom: 3, marginLeft: 20, marginRight: 20,}}>
                                    <Text style={styles.text} >{Strings.UpdateProfile.genderlabel}</Text>
                                    <RadioGroup 
                                        radioButtons={this.state.data} 
                                        onPress={this.onPress}
                                        flexDirection='row' />
                                </View>
                            </View>
                            <View style={{width: '100%', flexDirection: 'row', marginTop: 5,}}>
                                <BpkTextInput 
                                    label={Strings.UpdateProfile.addressplaceholder}
                                    onChangeText={(text) => this.setState({address: text})}
                                    value={this.state.address}
                                    style={{flex:1, marginBottom: 3, marginLeft: 20, marginRight: 20, }}
                                    multiline />
                            </View>
                            <View style={{width: '100%', flexDirection: 'row', marginTop: 5,}} >
                                <View style={{flex: 1, }}>
                                    <BpkTextInput 
                                        label={Strings.UpdateProfile.cityplaceholder}
                                        onChangeText={(text) => this.setState({city: text})}
                                        value={this.state.city}
                                        style={{marginBottom: 3, marginLeft: 20, }}/>
                                </View>
                                <View style={{flex: 1, }}>
                                    <BpkTextInput 
                                        label={Strings.UpdateProfile.postalcodeplaceholder}
                                        onChangeText={(text) => this.setState({postalcode: text})}
                                        value={this.state.postalcode}
                                        style={{marginBottom: 3, marginLeft: 10, marginRight: 20, }}/>
                                </View>
                            </View>
                        </View>
                        <View style={{width: '95%', marginLeft: 20, marginRight: 20, }}>
                            <Button 
                                buttonStyle={{backgroundColor: Colors.button, borderRadius: 5, marginTop: 10, elevation: 2, height: 50, }}
                                title={Strings.UpdateProfile.updatebutton}
                                onPress={ gotoUpdate }/>
                        </View>
                        <DateTimePicker
                            isVisible={this.state.isDateTimePickerVisible}
                            onConfirm={this._handleDatePicked}
                            onCancel={this._hideDateTimePicker}
                        />
                    </View>
                </ScrollView>
            </SafeAreaView>
        );
    }   
}

const styles = StyleSheet.create({
    safeAreaView: {
        flex: 1
    },

    containerScroll: {
        flex: 1,
        backgroundColor: '#ffffff',
    },

    container: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
    },

    image: {
        height: 100,
        width: 100,
        marginTop: 5,
        borderRadius: 50,
    },

    text: {
        fontSize: 13,
        textAlign: 'left',
        color: '#d1d1d1',
    },

    input: {
        fontSize: 14,
        textAlign: 'left',
        color: '#414141',
    },
});
