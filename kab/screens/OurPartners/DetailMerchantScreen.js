import React from 'react';
import {
    StyleSheet, 
    AsyncStorage,
    Dimensions,
    Text, 
    Image,
    TouchableOpacity,
    View,
    ListView,
    ScrollView,
    SafeAreaView,
    ActivityIndicator,
} from 'react-native';
import Strings from '../../constants/Strings';
import Colors from '../../constants/Colors';
import { getPartnershipByMerchant } from '../../API/PartnershipService';
import Utils from '../Utils';
import clsSetting from '../../API/clsSetting';
import DetailMerchantRowScreen from '../ListRow/DetailMerchantRowScreen';

const dimensions = Dimensions.get('window');
const imageWidth = dimensions.width - 50;
const imageHeight = dimensions.width - 100;

export default class DetailMerchantScreen extends React.Component{
    static navigationOptions = {
        title: 'Detail Merchant',
        headerTintColor: '#ffffff', 
        headerStyle: {
            backgroundColor: '#93b843',
        },
    };

    constructor(props)
    {
        super(props);

        this.params = this.props.navigation.state.params;

        this.state = {
            isLoading: true,
            title: this.params.Row.Name,
            location: this.params.Row.Address,
            website: this.params.Row.Website,
            description: this.params.Row.Description,
            image: (this.params.Row.Thumbnail != undefined || this.params.Row.Thumbnail != "") ? clsSetting.HomeUrl + this.params.Row.Thumbnail : clsSetting.HomeUrl + "images/no_image2.jpg",
            listvisible: false,
        };
    }

    componentDidMount() {
        getPartnershipByMerchant(this.params.Row.Id).then((json) =>
        {
            let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
            this.setState({
                isLoading: false,
                dataSource: ds.cloneWithRows(json),
            });

            if(this.state.dataSource != undefined){
                this.setState({ listvisible : true});
            }
        })
        .catch((error) => {
            console.error(error);
            this.setState({ isLoading: false});
        });
    }

    GetItem (row) {
        this.props.navigation.navigate('DetailPartnership', {Url: clsSetting.PartnershipUrl + "?id=" + row.Id});
    }

    ListViewItemSeparator = () => {
        return (
            <View style={{ height: .5, width: "100%", backgroundColor: "#000", }} />
        );
    }

    render() {
        if (this.state.isLoading) {
            return (
                <View style={{flex: 1, paddingTop: 20}}>
                    <ActivityIndicator />
                </View>
            );
        }


        return (
            <SafeAreaView style={styles.safeAreaView}>
                <ScrollView style={styles.container}>
                <View style={styles.containerView}>
                    <View style={{justifyContent: 'center', alignItems: 'center', }}>
                        <Image source={{uri: this.state.image}} style={styles.image2}/>
                    </View>
                    <View style={{flexDirection: 'column', marginLeft: 15, marginRight: 15, marginTop: 5, marginBottom: 5, }}>
                        <Text style={styles.textTitle}>{this.state.title}</Text>
                        <View style={{flexDirection: 'row', marginTop: 5, }}>
                            <Image style={{height: 20, width: 20, }} 
                                    source={require('../../assets/images/location.png')}/>
                            <Text style={{marginLeft: 5, fontSize: 13, color: '#414141'}}>{this.state.location}</Text>
                        </View>
                        {
                            Utils.renderIf(this.state.website != undefined || this.state.website != "", 
                                <View style={{flexDirection: 'row', marginTop: 5, }}>
                                    <Text style={{fontSize: 13, color: '#414141'}}>{Strings.DetailMerchant.text1}</Text>
                                    <Text style={{marginLeft: 5, fontSize: 13, color: '#414141'}}>{this.state.website}</Text>
                                </View>
                            )
                        }
                        <Text style={{fontSize: 13, color: '#414141', marginTop: 5,}}>{this.state.description}</Text>
                    </View>
                    {
                        Utils.renderIf(this.state.listvisible,
                            <ListView
                                dataSource={this.state.dataSource}
                                renderSeparator= {this.ListViewItemSeparator}
                                renderRow={(rowData) =>
                                    <TouchableOpacity 
                                        onPress={ this.GetItem.bind(this, rowData) } >
                                        <DetailMerchantRowScreen
                                            image={{uri: rowData.Thumbnail !== "" ? clsSetting.HomeUrl + rowData.Thumbnail : clsSetting.HomeUrl + "images/no_image2.jpg"}}
                                            name={rowData.Name}
                                            />
                                    </TouchableOpacity>
                                }
                            />
                        )
                    }
                </View>
                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.backgroundColor,
    },

    safeAreaView: {
        flex: 1
    },

    containerView: {
        flex: 1,
        flexDirection: 'column',
    },

    image2: {
        height: imageHeight,
        width: imageWidth,
        resizeMode: 'cover',
        marginTop: 20,
    },

    image: {
        height: 70,
        width: 70,
    },

    textTitle: {
        fontSize: 16,
        color: Colors.text,
        fontWeight: 'bold',
    },

    text: {
        fontSize: 13,
        marginTop: 5,
        marginLeft: 15,
        marginRight: 15,
        color: Colors.text,
    },
});

