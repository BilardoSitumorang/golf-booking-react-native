import React from 'react';
import {
    StyleSheet,
    Text,
    SafeAreaView,
    View,
    ListView,
    TouchableOpacity,
    ActivityIndicator,
} from 'react-native';
import Colors from '../../constants/Colors';
import Strings from '../../constants/Strings';
import { getCoursesByName, } from '../../API/CourseService';
import BpkTextInput from 'react-native-bpk-component-text-input';
import Utils from '../Utils';

export default class ListCourseScreen extends React.Component {
    static navigationOptions = {
        title: 'Location',
        headerTintColor: '#ffffff', 
        headerStyle: {
            backgroundColor: '#93b843',
        },
    };

    constructor(props){
        super(props);

        this.params = this.props.navigation.state.params;

        this.state = {
            isLoading: true,
            listvisible: true,
            username: '',

            session: this.params.Session,
            player: this.params.Player,
            calendar: this.params.Calendar,
            location: this.params.Location == 'Click here to search' ? '' : this.params.Location,
            sessionselected: this.params.SessionSelected,
            playerselected: this.params.PlayerSelected,

            inputlocation: '',
        }
    }

    GetItem (course) {
        //Selected Row List
        const { navigation } = this.props;
        navigation.goBack();
        this.params.onSelect({location: course.Nama, calendar: this.state.calendar, 
            sessionSelectedItem: this.state.session, playerSelectedItem: this.state.player,
            session: this.state.sessionselected, player: this.state.playerselected});
    }

    ListViewItemSeparator = () => {
        return (
            <View style={{ height: .5, width: "100%", backgroundColor: "#000", }} />
        );
    }

    fn_getLocation = () =>
    {
        getCoursesByName(this.state.inputlocation).then((jsonCourse) => 
        {
            if(jsonCourse != null){
                let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
                this.setState({
                    isLoading: false,
                    dataSource: ds.cloneWithRows(jsonCourse),
                    listvisible: true,
                });
            }
            else{
                this.setState({
                    isLoading: false,
                    listvisible: false,
                });
            }
        })
        .catch((error) => {
            console.error(error);
            this.setState({isLoading: false});
        });
    }

    componentDidMount() {
        this.fn_getLocation();
    }

    render() {
        if (this.state.isLoading) {
            return (
                <View style={{flex: 1, paddingTop: 20}}>
                    <ActivityIndicator />
                </View>
            );
        }

        return (
            <SafeAreaView style={styles.safeAreaView}>
                <View style={styles.containerScroll}>
                    <BpkTextInput 
                        label={'Location'}
                        onChangeText={(text) => {
                            this.setState({inputlocation: text});
                            this.fn_getLocation();
                        } }
                        value={this.state.inputlocation}
                        style={{marginTop: 5, marginBottom: 5, marginLeft: 15, marginRight: 15, }}/>
                    {
                        Utils.renderIf(this.state.listvisible,
                            <ListView
                                dataSource={this.state.dataSource}
                                renderSeparator= {this.ListViewItemSeparator}
                                renderRow={(rowData) =>
                                    <TouchableOpacity 
                                        onPress={ this.GetItem.bind(this, rowData) } >
                                        <Text style={[styles.text, {marginTop: 10, marginBottom: 10, marginLeft: 15, 
                                            marginRight: 15,}]} >{rowData.Nama}</Text>
                                    </TouchableOpacity>
                                }
                            />
                        )
                    }
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({

    safeAreaView: {
        flex: 1,
        backgroundColor: '#f2f2f2',
    },

    containerScroll:
    {
        flex: 1,
        backgroundColor: '#f2f2f2',
    },

    container: {
        backgroundColor: '#f2f2f2',
        height: 200,
        width: '100%'
    },

    text: {
        fontSize: 15,
        left: 5,
        color: Colors.text,
    },

    cardList: {
        backgroundColor: Colors.backgroundColor,
        height: 180,
        margin: 10,
        padding: 2,
    },
});