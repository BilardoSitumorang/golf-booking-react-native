import clsSetting from "../API/clsSetting";

export default class PendingBankTransferModel
{
    constructor(_bank, _vanumber, _vaname, _billercode, _billerkey, _amount, _point)
    {
        this.Bank = _bank;
        this.Va_Number = _vanumber;
        this.Va_Name = _vaname;
        this.Biller_Code = _billercode;
        this.Biller_Key = _billerkey;
        this.Amount = _amount;
        this.Point = _point;
    }
}