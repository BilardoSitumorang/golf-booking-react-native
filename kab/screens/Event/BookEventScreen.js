import React from 'react';
import {
    Image,
    Platform,
    ScrollView,
    StyleSheet,
    Text,
    SafeAreaView,
    TouchableOpacity,
    ListView,
    ActivityIndicator,
    AsyncStorage,
    Alert,
    View,
} from 'react-native';
import Colors from '../../constants/Colors';
import Strings from '../../constants/Strings';
import {Button, Card} from 'react-native-elements';
import EventBookModel from '../../Model/EventBookModel';
import TransactionEventModel from '../../Model/TransactionEventModel';
import TransactionEventDetailModel from '../../Model/TransactionEventDetailModel';
import Utils from '../Utils';
import { getOptions,  } from '../../API/OptionService';
import { postCreateEvent,  } from '../../API/TransactionService';
import Enums, { KartuStringEnum, OptionIntEnum, PlatformIntEnum, TransactionTypeIntEnum, TipePemainStringEnum, PaymentIntEnum } from '../../constants/Enums';
import BookReservationRowScreen from '../ListRow/BookReservationRowScreen';

export default class BookEventScreen extends React.Component {
    static navigationOptions = {
        title: 'Event',
        headerTintColor: '#ffffff', 
        headerStyle: {
            backgroundColor: '#93b843'
        },
    };

    constructor(props){
        super(props);

        this.params = this.props.navigation.state.params;

        this.state = {
            isLoading: true,
            username: '',

            idevent: this.params.IdEvent,
            location: this.params.Location,
            calendar: this.params.Calendar,
            total: 0,

            publishrate: this.params.PublishRate,
            idpublish: this.params.IdPublish,
            clubrate: this.params.ClubRate,
            idclub: this.params.IdClub,
            qrate: this.params.QRate,
            idqrate: this.params.IdQRate,

            arrname: [],
            arrplayername: [],
            arrtype: [],
            arrkartu: [],
            arrpricing: [],
            arridpricing: [],
            payarrtype: [],
            bookmodel: [],
        };
    }

    checkArrNameEmpty = () => {
        var returnTemp = false;
        var i;
        for(i = 0; i < this.state.arrname.length; i++)
        {
            if(this.state.arrname[i] != ''){
                returnTemp = true;
            }
        }

        return returnTemp;
    }

    componentDidMount() {
        var Enumerable = require('../linq');

        getOptions().then((jsonOption) => 
        {
            this.setState({
                eventOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionEvent).firstOrDefault(),
                maintenanceOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionMaintenancePeriod).firstOrDefault()
            });

            if(this.params.ArrName === undefined){
                var i;
                for(i = 0; i < 4; i++) {
                    this.state.arrname.push('');
                    this.state.arrplayername.push('');
                    this.state.arrtype.push('');
                    this.state.arrkartu.push('');
                    this.state.arridpricing.push(0);
                    this.state.arrpricing.push(0);
                }
            }

            var j;
            for(j = 0; j < 4; j++) {
                this.state.bookmodel.push(new EventBookModel(this.state.arrname[j], '', this.state.arrtype[j], 
                    this.state.arrkartu[j], this.state.arrplayername[j], this.state.arridpricing[j], this.state.arrpricing[j]));
            }

            let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
            this.setState({
                isLoading: false,
                dataSource: ds.cloneWithRows(this.state.bookmodel),
            });
        })
        .catch((error) => {
            console.error(error);
            this.setState({isLoading: false, });
        });
    }

    ListViewItemSeparator = () => {
        return (
            <View style={{ height: .5, width: "100%", backgroundColor: "#414141", }} />
        );
    }

    GetItem (row, sectionID, rowID) {
        //Selected Row List
    }

    updatePlayer (rowID) {
        this.props.navigation.navigate('PlayerEvent', {IdEvent: this.state.idevent, Location: this.state.title, Calendar: this.state.eventdate, PublishRate: this.state.publishrate, IdPublish: this.state.idpublish,
            ClubRate: this.state.clubrate, IdClub: this.state.idclub, QRate: this.state.qrate, IdQRate: this.state.idqrate,
            ArrName: JSON.stringify(this.state.arrname), ArrPlayerName: JSON.stringify(this.state.arrplayername), 
            ArrKartu: JSON.stringify(this.state.arrkartu), ArrType: JSON.stringify(this.state.arrtype), ArrIdPricing: JSON.stringify(this.state.arridpricing), 
            ArrPricing: JSON.stringify(this.state.arrpricing), BookModel: JSON.stringify(this.state.bookmodel),
            EditedPosition: rowID, onSelect: this.onSelect, Total: this.state.total, });
    };

    deletePlayer (rowID) {
        var totalCount = this.state.total - this.state.bookmodel[rowID].Pricing;

        this.setState({
            total: totalCount,
        });

        this.state.arrname[rowID] = ('');
        this.state.arrplayername[rowID] = ('');
        this.state.arrtype[rowID] = ('');
        this.state.arrkartu[rowID] = ('');
        this.state.arridpricing[rowID] = ('');
        this.state.arrpricing[rowID] = ('');
        this.state.bookmodel[rowID] = new EventBookModel(this.state.arrname[rowID], '', this.state.arrtype[rowID], 
            this.state.arrkartu[rowID], this.state.arrplayername[rowID], this.state.arridpricing[rowID], this.state.arrpricing[rowID]);
        let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.setState({
            dataSource: ds.cloneWithRows(this.state.bookmodel),
        });
    };

    onSelect = data => {
        this.setState(data);
        let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.setState({
            dataSource: ds.cloneWithRows(this.state.bookmodel),
        });
    };

    createTrans = async () => {
        this.setState({isLoading: true, })
        var platform = Platform.OS === 'ios' ? this.state.eventOption.iOS : this.state.eventOption.Android;
        if(Utils.getMaintenanceOption(this.state.maintenanceOption)){
            Utils.alertBackToMain(this.state.maintenanceOption.Note, this.props);
        }
        else if(!platform) {
            Utils.errAlert("QAccess Alert", this.state.eventOption.Note);
        }
        else 
        {
            if(this.state.total != 0)
            {
                this.setState({ username : await AsyncStorage.getItem('Username')});
                var platformMobile = Platform.OS === 'ios' ? PlatformIntEnum.IOs : PlatformIntEnum.Android;
                
                var arrTransDetailModel = [];
                var i;
                for(i = 0; i < 4; i++) {

                    var _titlePemain = '';
                    var _noMember = '';
                    var _namaPemain = '';
                    var _jenisMember = 0;
                    var _tipeMember = 0;
                    var _idpricing = 0;

                    if(this.state.bookmodel[i].IdPricing != 0){

                        if (this.state.arrtype[i] === TipePemainStringEnum.Publish)
                        {
                            _noMember = '';
                            _namaPemain = this.state.arrplayername[i];
                        }
                        else if (this.state.arrtype[i] == TipePemainStringEnum.QMember || this.state.arrtype[i] == TipePemainStringEnum.Club || this.state.arrtype[i] == KartuStringEnum.Mandiri)
                        {
                            _noMember = this.state.arrname[i];
                            _jenisMember = Number.parseInt(this.state.arrkartu[i]);
                            _namaPemain = this.state.arrplayername[i];
                        }
                        else
                        {
                            _jenisMember = 0;
                        }

                        if (this.state.arrtype[i] == KartuStringEnum.Mandiri)
                        {
                            //arrType[i] = KartuEnum.Mandiri.GetDescription();
                            this.state.payarrtype.push(TipePemainStringEnum.Club);
                        }
                        else
                        {
                            this.state.payarrtype.push(this.state.arrtype[i]);
                        }
                        _tipeMember = Enums.TipePemainEnumConvert(this.state.payarrtype[i]);
                        _idpricing = this.state.arridpricing[i];
                        arrTransDetailModel.push(new TransactionEventDetailModel(_noMember, _titlePemain, _tipeMember, _jenisMember, _namaPemain, _idpricing));
                    }
                }

                var transactionModel = new TransactionEventModel(
                    this.state.idevent, this.state.username, PaymentIntEnum.NotSpecified, null, platformMobile, arrTransDetailModel);

                postCreateEvent(transactionModel).then(async (jsonTrans) =>
                {
                    await AsyncStorage.setItem("BookingCode", jsonTrans);
                    this.props.navigation.navigate('Payment', {TransType: TransactionTypeIntEnum.Event});
                });          
            }
        }
    }
    
    render() {

        if (this.state.isLoading) {
            return (
                <View style={{flex: 1, paddingTop: 20}}>
                    <ActivityIndicator />
                </View>
            );
        }

        const gotoPay = () => {
            var platform = Platform.OS === 'ios' ? this.state.eventOption.iOS : this.state.eventOption.Android;
            if(Utils.getMaintenanceOption(this.state.maintenanceOption)){
                Utils.alertBackToMain(this.state.maintenanceOption.Note, this.props);
            }
            else if(!platform) {
                Utils.errAlert("QAccess Alert", this.state.eventOption.Note);
            }
            else 
            {
                if(!this.checkArrNameEmpty()){
                    Utils.errAlert("Error Message", Strings.BookReservation.errmsg1);
                }
                else if(this.state.kerjasamalapangan === false && this.fn_SearchMandiriPlayer())
                {
                    Utils.errAlert("Error Message", Strings.BookReservation.errmsg2);
                }
                else
                {
                    Alert.alert(
                        "Confirm Booking",
                        Strings.BookReservation.msgdialog,
                        [
                            { text: 'Yes', onPress: () => {
                                this.createTrans();
                            }},
                            { text: 'No', onPress: () => console.log('onpress')}
                        ],
                        { cancelable: false }
                    )
                }
                
            }
        }

        return (
            <SafeAreaView style={styles.safeAreaView}>
                <ScrollView style={styles.containerScroll}>
                    <View style={styles.container}>
                        <Text style={styles.textKet} >{Strings.BookEvent.text1}</Text>
                        <Card style={styles.card}>
                            <View style={{flex: 1, flexDirection: 'row',}}>
                                <Image style={styles.image} 
                                    source={require('../../assets/images/location.png')}/>
                                <Text style={styles.textLocation}>{this.state.location}</Text>
                            </View>
                            <View style={{flex: 1, flexDirection: 'row', marginTop: 5, }}>
                                <Image style={styles.image} 
                                    source={require('../../assets/images/calendar.png')}/>
                                <Text style={styles.text}>{this.state.calendar}</Text>
                            </View>
                        </Card>
                        <Text style={styles.textKet} >{Strings.BookEvent.text2}</Text>
                        <ListView
                            dataSource={this.state.dataSource}
                            renderSeparator= {this.ListViewItemSeparator}
                            renderRow={(rowData, sectionID, rowID) =>
                                // <TouchableOpacity 
                                //     onPress={ this.GetItem.bind(this, rowData, sectionID, rowID) } >
                                //     <BookReservationRowScreen 
                                //         name={rowData.Nama == '' ? (Number.parseInt(rowID) + 1) + ". Fill in player details"  : rowData.Nama + "(" + rowData.PlayerName + ")"}
                                //         type={rowData.Tipe}/>
                                // </TouchableOpacity>
                                <View style={styles.containerList} >
                                    <View style={{flex: 1, flexDirection: 'row', marginLeft: 20, marginRight: 20, marginTop: 5, marginBottom: 5,}}>
                                        <View style={{flex: 3, flexDirection: 'column'}}>
                                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', }}>
                                                <Text style={styles.textLeft2}>{rowData.Nama == '' ? (Number.parseInt(rowID) + 1) + ". Fill in player details"  : rowData.Nama + "(" + rowData.PlayerName + ")"}</Text>
                                            </View>
                                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', }}>
                                                <Text style={[styles.textLeft2, {color: '#edb636'}]}>{rowData.Tipe}</Text>
                                            </View>
                                        </View>
                                        <View style={{flex: 1, flexDirection: 'row',}}>
                                            <View style={{justifyContent: 'center', alignItems: 'center', marginTop: 5, marginBottom: 5, marginLeft: 5, padding: 5, }}>
                                                <TouchableOpacity 
                                                    onPress={ this.updatePlayer.bind(this, rowID) }
                                                    style={{ justifyContent: 'center', alignItems: 'center',}}
                                                    >
                                                    <Image style={{height: 30, width: 30, tintColor: '#93b843'}} 
                                                        source={require('../../assets/images/ic_edit.png')}/>
                                                </TouchableOpacity>
                                            </View>
                                            <View style={{justifyContent: 'center', alignItems: 'center', marginTop: 5, marginBottom: 5, marginRight: 20, padding: 5, }}>
                                                <TouchableOpacity 
                                                    onPress={ this.deletePlayer.bind(this, rowID) }
                                                    style={{justifyContent: 'center', alignItems: 'center',}}
                                                    >
                                                    <Image style={{height: 30, width: 30, tintColor: '#b84242', marginLeft: 10, marginTop: 5, marginBottom: 5,}} 
                                                        source={require('../../assets/images/ic_delete.png')}/>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                    </View>
                                </View>
                            }
                        />
                        <View style={{flex: 1, flexDirection: 'row',  backgroundColor: '#fff', paddingLeft: 15, paddingRight: 15, paddingTop: 5,
                            paddingBottom: 5, }}>
                            <Text style={styles.textLeft}>{Strings.BookEvent.text3}</Text>
                            <Text style={styles.textRight}>{"Rp. " + Utils.currencyfunc(Math.round(this.state.total*100)/100)}</Text>
                        </View>
                        <Button 
                            onPress={ gotoPay }
                            buttonStyle={{backgroundColor: Colors.button, borderRadius: 5, marginTop: 20, marginBottom: 20, height: 50, }}
                            title={Strings.BookEvent.continuebutton}/>
                    </View>
                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({

    safeAreaView: {
        flex: 1,
    },

    containerScroll:
    {
        flex: 1,
        backgroundColor: '#f2f2f2',
    },

    container: {
        backgroundColor: '#f2f2f2',
        flex: 1,
        flexDirection: 'column',
    },

    image: {
        width: 25,
        height: 25,
    },

    textKet: {
        fontSize: 18,
        marginTop: 5,
        marginLeft: 15,
        marginBottom: 5,
        marginRight: 15,
        color: Colors.text,
        fontWeight: 'bold',
    },

    card: {
        backgroundColor: Colors.backgroundColor,
        flex: 1,
        flexDirection: 'column',
        margin: 10,
        padding: 10,
        elevation: 2,
        borderRadius: 5,
    },

    text: {
        fontSize: 15,
        marginLeft: 5,
        color: Colors.text,
    },

    textSession: {
        fontSize: 15,
        marginLeft: 5,
        color: '#edb636',
    },

    textLocation: {
        fontSize: 15,
        marginLeft: 5,
        color: '#93b843',
    },

    textLeft: {
        flex: 1,
        fontSize: 16,
        marginRight: 10,
        textAlign: 'left',
        fontWeight: 'bold',
        color: Colors.text,
    },

    textRight: {
        flex: 1,
        fontSize: 16,
        marginRight: 10,
        textAlign: 'right',
        fontWeight: 'bold',
        color: Colors.text,
    },

    containerList: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#fff',
    },

    textLeft2: {
        fontSize: 14,
        marginTop: 5,
        color: Colors.text,
    },

    textRight2: {
        flex: 1,
        fontSize: 12,
        marginTop: 5,
        color: Colors.text,
    },
});
