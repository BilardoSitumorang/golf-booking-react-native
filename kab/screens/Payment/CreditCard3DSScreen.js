import React from 'react';
import {  
    StyleSheet, 
    WebView,
    AsyncStorage,
    SafeAreaView 
} from 'react-native';
import clsSetting from '../../API/clsSetting';
import { OptionIntEnum, PaymentIntEnum, TransactionTypeIntEnum, } from '../../constants/Enums';

export default class CreditCardDSScreen extends React.Component {
    static navigationOptions = {
        title: '',
        header: null,
    };

    constructor(props){
        super(props);

        this.params = this.props.navigation.state.params;

        this.state = {
            transtype: this.params.TransType,
            point: this.params.Point,
            paymenttype: this.params.PaymentType,
            chargepayment: this.params.ChargePayment,
            bookingcode: this.params.BookingCode,
            bookingdate: this.params.BookingDate,
            total: this.params.Total,
            containmandiri: this.params.ContainMandiri,
            mandiricard: this.params.MandiriCard,

            cardname: this.params.NameOnCard,
            cardnumber: this.params.CardNumber,
            cardmm: this.params.CardMonth,
            cardyy: this.params.CardYear,
            cardcvv: this.params.CardCVV,
            url: '',
            properties: this.props,
        };
    }

    async _onNavigationChange(state) {
        if (state.url.includes('Success')) {

            //Call Ticket
            if(this.state.transtype == TransactionTypeIntEnum.Reservation){
                await AsyncStorage.setItem("BookingCode", this.state.bookingcode);
                this.props.navigation.navigate('TicketReservation');
            }
            else if(this.state.transtype == TransactionTypeIntEnum.RegistrationQ){
                console.log('berhasil');
                await AsyncStorage.setItem("BookingCode", this.state.bookingcode);
                this.props.navigation.navigate('TicketRegistration');
            }
            else if(this.state.transtype == TransactionTypeIntEnum.Event){
                await AsyncStorage.setItem("BookingCode", this.state.bookingcode);
                this.props.navigation.navigate('TicketEvent');
            }
            else if(this.state.transtype == TransactionTypeIntEnum.MobilePulsaPhonePrepaid){
                await AsyncStorage.setItem("BookingCode", this.state.bookingcode);
                this.props.navigation.navigate('TicketPulsa');
            }
        }
        else if (state.url.includes('Close')) {
            this.props.navigation.navigate('App');
            
        }
        else if (state.url.includes('Failed')) {
            //this.props.navigation.navigate('Payment', {TransType: this.state.transtype});
            this.props.navigation.goBack();
        }
        else
        {
            //Call Ticket
        }
    }

    componentDidMount() {
        if(this.state.transtype == TransactionTypeIntEnum.Reservation){
            this.setState({
                url: clsSetting.CreditCardPaymentUrl
                    + "?ApiSecretKey=" + clsSetting.ApiSecretKey
                    + "&OrderId=" + this.state.bookingcode
                    + "&Point=" + this.state.point
                    + "&CardNumber=" + this.state.cardnumber
                    + "&CardExpiryMonth=" + this.state.cardmm
                    + "&CardExpiryYear=" + this.state.cardyy
                    + "&CardCVV=" + this.state.cardcvv
                    + "&NameOnCard=" + this.state.cardname,
            });
        }
        else if(this.state.transtype == TransactionTypeIntEnum.Event || this.state.transtype == TransactionTypeIntEnum.RegistrationQ
                || this.state.transtype == TransactionTypeIntEnum.MobilePulsaPhonePrepaid){
            this.setState({
                url: clsSetting.CreditCardPaymentUrl
                    + "?ApiSecretKey=" + clsSetting.ApiSecretKey
                    + "&OrderId=" + this.state.bookingcode
                    + "&Point=" + this.state.point
                    + "&CardNumber=" + this.state.cardnumber
                    + "&CardExpiryMonth=" + this.state.cardmm
                    + "&CardExpiryYear=" + this.state.cardyy
                    + "&CardCVV=" + this.state.cardcvv
                    + "&NameOnCard=" + this.state.cardname
                    + "&ChargePaymentFee=" + this.state.chargepayment,
            });
        }
    }

    render() {
        return (
            <SafeAreaView style={styles.safeAreaView}>
                <WebView onNavigationStateChange={this._onNavigationChange.bind(this)}
                    style={{flex: 1, }}
                    source={{uri: this.state.url}} />
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },

    safeAreaView: {
        flex: 1
    },
});
