import clsSetting from "../API/clsSetting";

export default class ProfileModel
{
    constructor(_email, _alamat, _gender, _postalcode, _nama, _kota, _bod)
    {
        this.Email = _email;
        this.Alamat = _alamat;
        this.JenisKelamin = _gender;
        this.KodePos = _postalcode;
        this.Nama = _nama;
        this.Kota = _kota;
        this.TanggalLahir = _bod;
        this.ApiSecretKey = clsSetting.ApiSecretKey;
    }
}