import React from 'react';
import {
    StyleSheet, 
    Text, 
    View,
    Image,
    TouchableOpacity,
    ScrollView,
    AsyncStorage,
    SafeAreaView,
} from 'react-native';
import { Button, } from 'react-native-elements';
import Strings from '../constants/Strings';
import Colors from '../constants/Colors';
import { postRegisterPhoneVerification, postRegisterPhone } from '../API/UserService';
import BpkTextInput from 'react-native-bpk-component-text-input';
import Utils from './Utils';

export default class RegisterOTPScreen extends React.Component {
    static navigationOptions = {
        title: '',
        header: null,
    };

    constructor(props)
    {
        super(props);
        this.params = this.props.navigation.state.params;

        this.state = {
            otp1: '',
            otp2: '',
            otp3: '',
            otp4: '',
            otp5: '',
            otp6: '',
            otp: '',
            email: '',
        };
    }

    render()
    {
        const gotoContinue = async () => {
            // if(this.state.otp1 === "" || this.state.otp2 === "" || this.state.otp3 === "" || this.state.otp4 === ""
            //     || this.state.otp5 === "" || this.state.otp6 === "")
            // {
            if(this.state.otp === "")
            {
                Utils.errAlert("Error Message", Strings.RegisterOTP.errmsg1);
            }
            else{
                // this.setState({otp: this.state.otp1 + "" + this.state.otp2 + "" + this.state.otp3 + "" + this.state.otp4 +
                // "" + this.state.otp5 + "" + this.state.otp6});
                this.setState({ email : await AsyncStorage.getItem('Username')});
                postRegisterPhoneVerification(this.state.otp, this.state.email).then(async (json) =>
                {
                    if (json.status === 200) {
                        await AsyncStorage.setItem("Username", this.state.email);
                        await AsyncStorage.setItem("IsLogin", "true");
                        this.props.navigation.navigate('App');
                    } else {
                        Utils.errAlert("Error Message", json.message);
                    }
                });
            }
        }

        const gotoRegisterPhone = () => {
            this.props.navigation.navigate('RegisterPhone')
        }

        const gotoSendOTP = async () => {
            console.log("Test", this.params.Idd + " - " + this.params.Phone);
            this.setState({ email : await AsyncStorage.getItem('Username')});
            postRegisterPhone(this.params.Idd, this.params.Phone, this.state.email).then(async (json) =>
            {
                if (json.status != 200) {
                    Utils.errAlert("Error Message", Strings.RegisterOTP.errmsg3);
                }
            });
        }

        return (
            <SafeAreaView style={styles.safeAreaView}>
                <ScrollView style={styles.containerScroll}>
                    <View style={styles.container}>
                        <Image source={require('../assets/images/logo_qaccess.png')} style={styles.image}/>
                        <View style={{flex: 1, flexDirection: 'row', marginTop: 10, marginLeft: 20, marginRight: 20, }}>
                            <View style={{alignItems:'center', justifyContent: 'center', flexDirection: 'row'}}>
                                <View style={{width: '100%', }} >
                                    <BpkTextInput 
                                        label={'OTP'}
                                        keyboardType="phone-pad"
                                        maxLength={6}
                                        style={{marginBottom: 3, marginLeft: 5, marginRight: 10, }}
                                        onChangeText={(text) => this.setState({otp: text})}
                                        value={this.state.otp} />
                                </View>
                                {/* <View style={{width: 50, }} >
                                    <BpkTextInput 
                                        label={''}
                                        keyboardType="phone-pad"
                                        maxLength={1}
                                        style={{marginBottom: 3, marginLeft: 5, }}
                                        onChangeText={(text) => this.setState({otp2: text})}
                                        value={this.state.otp2}
                                        />
                                </View>
                                <View style={{width: 50, }} >
                                    <BpkTextInput 
                                        label={''}
                                        keyboardType="phone-pad"
                                        maxLength={1}
                                        style={{marginBottom: 3, marginLeft: 5,}}
                                        onChangeText={(text) => this.setState({otp3: text})}
                                        value={this.state.otp3} />
                                </View>
                                <View style={{width: 50, }} >
                                    <BpkTextInput 
                                        label={''}
                                        keyboardType="phone-pad"
                                        maxLength={1}
                                        style={{marginBottom: 3, marginLeft: 5, }}
                                        onChangeText={(text) => this.setState({otp4: text})}
                                        value={this.state.otp4} />
                                </View>
                                <View style={{width: 50, }} >
                                    <BpkTextInput 
                                        label={''}
                                        keyboardType="phone-pad"
                                        maxLength={1}
                                        style={{marginBottom: 3, marginLeft: 5, }}
                                        onChangeText={(text) => this.setState({otp5: text})}
                                        value={this.state.otp5} />
                                </View>
                                <View style={{width: 50, }} >
                                    <BpkTextInput 
                                        label={''}
                                        keyboardType="phone-pad"
                                        maxLength={1}
                                        style={{marginBottom: 3, marginLeft: 5, }}
                                        onChangeText={(text) => this.setState({otp6: text})}
                                        value={this.state.otp6} />
                                </View> */}
                            </View>
                        </View>
                        <View style={{width: '95%', marginTop: 5,}}>
                            <Button 
                                onPress={ gotoContinue }
                                buttonStyle={{backgroundColor: Colors.button, borderRadius: 5, marginTop: 20, height: 50, }}
                                title={Strings.RegisterOTP.continuebutton}/>
                        </View>
                        <View style={{width: '95%', marginTop: 1,}}>
                            <Button 
                                onPress={ gotoRegisterPhone }
                                buttonStyle={{backgroundColor: Colors.button, borderRadius: 5, marginTop: 20, height: 50, }}
                                title={Strings.RegisterOTP.changenumberbutton}/>
                        </View>
                        <View style={styles.textcontainer}>
                            <Text style={styles.text}>{Strings.RegisterOTP.text1}</Text>
                            <TouchableOpacity onPress={ gotoSendOTP }>
                                <Text style={styles.textsignup} >{Strings.RegisterOTP.text2}</Text>
                            </TouchableOpacity>
                        </View>
                        <Text style={styles.text}>{Strings.RegisterOTP.text3}</Text>
                    </View>
                </ScrollView>
            </SafeAreaView>
                
            
        );
    }
}
        
const styles = StyleSheet.create({
    safeAreaView: {
        flex: 1
    },

    containerScroll:
    {
        flex: 1,
        backgroundColor: Colors.backgroundColor,
    },

    container: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.backgroundColor,
    },

    image: {
        height: 120,
        width: 120,
        marginTop: 120,
    },

    textcontainer: {
        flexDirection: 'row',
    },

    text: {
        fontSize: 13,
        textAlign: 'center',
        marginTop: 15,
        color: Colors.text,
    },

    textsignup: {
        fontSize: 13,
        textAlign: 'left',
        marginTop: 15,
        marginLeft: 5,
        color: Colors.button,
    },

    textforgot: {
        width: '90%',
        fontSize: 15,
        textAlign: 'right',
        marginTop: 10,
        marginRight: 10,
        color: Colors.button,
    },

    button: {
        width: '100%'
    },

    inputContainer: {
        borderLeftWidth: 1,
        borderRightWidth: 1,
        borderTopWidth: 1,
        borderBottomWidth: 1,
        height: 35,
        marginLeft: 10,
        marginRight: 10,
        marginTop: 60,
        borderRadius: 2,
        width: '90%',
    },

    inputContainer2: {
        borderLeftWidth: 1,
        borderRightWidth: 1,
        borderTopWidth: 1,
        borderBottomWidth: 1,
        height: 35,
        marginLeft: 10,
        marginRight: 10,
        marginTop: 20,
        borderRadius: 2,
        width: '90%',
    },

    input: {
        height: 35,
        backgroundColor: Colors.backgroundColor,
        padding: 10,
        fontSize: 16,
    },
});