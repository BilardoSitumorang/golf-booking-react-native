import React from 'react';
import {
    StyleSheet, 
    AsyncStorage,
    Text, 
    View,
    Image,
    TouchableOpacity,
    ScrollView,
    SafeAreaView,
} from 'react-native';
import { Button, Divider, } from 'react-native-elements';
import Strings from '../../constants/Strings';
import Colors from '../../constants/Colors';
import BpkTextInput from 'react-native-bpk-component-text-input';
import DateTimePicker from 'react-native-modal-datetime-picker';
import moment from 'moment';
import { postRegisterLinkQ } from '../../API/UserService';
import Utils from '../Utils';

export default class LinkQMemberScreen extends React.Component {
    static navigationOptions = {
        title: '',
        header: null,
    };

    constructor(props)
    {
        super(props);
        this.state = {
            birthday: moment(Date.now()).format('YYYY-MM-DD'),
            qgolfnumber: '',
            email: '',
            isDateTimePickerVisible: false,
        };
    }

    //Date Picker
    _showDateTimePicker = () => {
        this.setState({ isDateTimePickerVisible: true });
    }
    
    _hideDateTimePicker = () => {
        this.setState({ isDateTimePickerVisible: false });
    }

    _handleDatePicked = (date) => {
        console.log('A date has been picked: ', date);
        this._hideDateTimePicker();
        this.setState({birthday: moment(date).format('YYYY-MM-DD')});
    };

    render()
    {
        const gotoRegisterPhone = async () => {
            this.setState({email: await AsyncStorage.getItem('Username')});
            if(this.state.qgolfnumber.trim() == "")
            {
                Utils.errAlert("Error Message", Strings.RegisterLinkQMember.errmsg1);
            }
            else if(this.state.qgolfnumber.includes("-") ||  this.state.qgolfnumber.includes(" "))
            {
                Utils.errAlert("Error Message", Strings.RegisterLinkQMember.errmsg2);
            }
            else if(this.state.birthday.trim() == "")
            {
                Utils.errAlert("Error Message", Strings.RegisterLinkQMember.errmsg3);
            }
            else
            {
                postRegisterLinkQ(this.state.email, this.state.birthday, this.state.email).then(async (json) =>
                {
                    if(json)
                    {
                        this.props.navigation.navigate('Account');
                    }
                    
                });
            }
            
        }

        const gotoContinue = () => {
            this.props.navigation.navigate('Account')
        }

        return (
            <SafeAreaView style={styles.safeAreaView}>
                <ScrollView style={styles.containerScroll}>
                    <View style={styles.container}>
                        <Image source={require('../../assets/images/logo_qaccess.png')} style={styles.image}/>
                        <View style={{width: '100%', marginTop: 5,}}>
                            <BpkTextInput 
                                label={Strings.RegisterLinkQMember.numberplaceholder}
                                style={{marginBottom: 3, marginLeft: 20, marginRight: 20, }}
                                onChangeText={(text) => this.setState({qgolfnumber: text})}
                                value={this.state.qgolfnumber} />
                        </View>
                        <View style={{width: '100%', marginTop: 15,}}>
                            <TouchableOpacity onPress={ this._showDateTimePicker } style={{flexDirection: 'column', marginLeft: 20, marginRight: 20,
                                flex: 1, backgroundColor: '#ffffff', }}>
                                <Text style={styles.input} >{this.state.birthday}</Text>
                                <Divider style={{ height: 1, backgroundColor: '#efefef', width: '100%', marginTop: 10, }} />
                            </TouchableOpacity>
                        </View>
                        <View style={{width: '100%', marginTop: 10,}}>
                            <Button 
                                onPress={ gotoRegisterPhone }
                                buttonStyle={{backgroundColor: '#93b843', borderRadius: 5, marginTop: 20, marginLeft: 10, marginRight: 10, height: 50, }}
                                title={Strings.RegisterLinkQMember.continuebutton}/>
                        </View>
                        <View style={styles.textcontainer}>
                            <Text style={styles.text}>{Strings.RegisterLinkQMember.text1}</Text>
                            <TouchableOpacity onPress={ gotoRegisterPhone } underlayColor="white">
                                <Text style={styles.textsignup} >{Strings.RegisterLinkQMember.text2}</Text>
                            </TouchableOpacity>
                        </View>
                        <DateTimePicker
                            isVisible={this.state.isDateTimePickerVisible}
                            onConfirm={this._handleDatePicked}
                            onCancel={this._hideDateTimePicker}
                        />
                    </View>
                </ScrollView>
            </SafeAreaView>
        );
    }
}
        
const styles = StyleSheet.create({
    safeAreaView: {
        flex: 1
    },

    containerScroll:
    {
        flex: 1,
        backgroundColor: Colors.backgroundColor,
    },

    container: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
    },

    image: {
        height: 120,
        width: 120,
        marginTop: 120,
    },

    textcontainer: {
        flexDirection: 'row',
    },

    text: {
        fontSize: 12,
        textAlign: 'right',
        marginTop: 15,
        color: '#d1d1d1',
    },

    textsignup: {
        fontSize: 12,
        textAlign: 'left',
        marginTop: 15,
        marginLeft: 5,
        color: '#93b843',
    },

    textforgot: {
        width: '90%',
        fontSize: 14,
        textAlign: 'right',
        marginTop: 10,
        marginRight: 10,
        color: '#93b843',
    },

    button: {
        width: '100%'
    },

    inputContainer: {
        borderLeftWidth: 1,
        borderRightWidth: 1,
        borderTopWidth: 1,
        borderBottomWidth: 1,
        height: 35,
        marginLeft: 10,
        marginRight: 10,
        marginTop: 60,
        borderRadius: 2,
        width: '90%',
    },

    inputContainer2: {
        borderLeftWidth: 1,
        borderRightWidth: 1,
        borderTopWidth: 1,
        borderBottomWidth: 1,
        height: 35,
        marginLeft: 10,
        marginRight: 10,
        marginTop: 20,
        borderRadius: 2,
        width: '90%',
    },

    input: {
        flex: 1,
        backgroundColor: '#ffffff',
        color: '#414141',
        fontSize: 16,
    },
});