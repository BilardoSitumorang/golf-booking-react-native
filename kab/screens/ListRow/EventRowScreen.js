import React from 'react';
import { 
    ScrollView, 
    StyleSheet, 
    SafeAreaView,
    Image,
    View,
    Text,
    Platform,
} from 'react-native';
import Colors from '../../constants/Colors';
import { Card, CardTitle, CardContent, CardAction, CardButton, CardImage } from 'react-native-material-cards';

export default class EventRowScreen extends React.Component {
    
    static navigationOptions = {
        title: '',
        header: null
    };

    render() {
        return (
            <Card style={styles.cardList} >
                <View style={{flex: 4, paddingTop: 4,}}>
                    <Image style={styles.imageList} 
                            source={this.props.imageUri}/>
                </View>
                <View style={{flex: 1, paddingLeft: 10, marginTop: 5,}}>
                    <Text style={styles.textCard} >{this.props.eventName}</Text>
                </View>
            </Card>
        );
    }
}

const styles = StyleSheet.create({
    
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },

    safeAreaView: {
        flex: 1
    },

    imageList: {
        flex: 1,
        height: null,
        width: 150,
        resizeMode: 'cover',
    },

    cardList: {
        backgroundColor: Colors.backgroundColor,
        width: 150,
        height: 180,
        marginRight: 10,
        elevation: 2,
        borderRadius: 5,
    },
});