import clsSetting from "../API/clsSetting";

export default class TransactionDetailModel
{
    constructor(_nomember, _title, _tipemember, _jenismember, _name)
    {
        this.NoMember = _nomember;
        this.TitlePemain = _title;
        this.TipeMember = _tipemember;
        this.JenisMember = _jenismember;
        this.NamaPemain = _name;
    }
}