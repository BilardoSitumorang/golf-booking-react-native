import React from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    FlatList,
    View,
    ListView,
    ScrollView,
    AsyncStorage,
    ActivityIndicator,
    SafeAreaView,
} from 'react-native';
import { getActiveBalance,  } from '../../API/TransactionService';
import AccountBalanceDetailRowScreen from '../ListRow/AccountBalanceDetailRowScreen';

export default class AccountBalanceDetailListScreen extends React.Component {
    static navigationOptions = {
        title: '',
        header: null
    };

    constructor(props){
        super(props);

        this.state = {
            isLoading: true,
            username: '',
            status: this.props.status,
        }
    }

    GetItem (row) {
        //Selected Row List
    }

    ListViewItemSeparator = () => {
        return (
            <View style={{ height: 2, width: "100%", backgroundColor: "#eee", }} />
        );
    }

    render() {
        return (
            <SafeAreaView style={styles.safeAreaView}>
                <ScrollView style={styles.container}>
                    <View style={styles.container}>
                        <ListView
                            dataSource={this.props.dataSource}
                            renderSeparator= {this.ListViewItemSeparator}
                            renderRow={(rowData) =>
                                <TouchableOpacity 
                                    onPress={ this.GetItem.bind(this, rowData) } >
                                    <AccountBalanceDetailRowScreen 
                                        row={rowData}/>
                                </TouchableOpacity>
                            }
                        />
                    </View>
                </ScrollView>
            </SafeAreaView>
        );
    }   
}

const styles = StyleSheet.create({
    safeAreaView: {
        flex: 1
    },

    containerScroll: {
        flex: 1,
        backgroundColor: '#fff',
    },

    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#eee',
        marginTop: 5,
    },
});
