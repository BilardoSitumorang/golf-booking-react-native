import React from 'react';
import { 
    StyleSheet, 
    Image,
    View,
    Text,
} from 'react-native';
import Colors from '../../constants/Colors';
import Strings from '../../constants/Strings';
import Enums, { TipePemainStringEnum } from '../../constants/Enums';
import Utils from '../Utils';

export default class TicketRowScreen extends React.Component {
    static navigationOptions = {
        title: '',
        header: null,
    };

    constructor(props){
        super(props);

        this.state = {
            visible: this.props.type == TipePemainStringEnum.Visitor || this.props.type == TipePemainStringEnum.Publish ? true : false,
        }
    }

    render() {
        return (
            <View style={styles.container} >
                <View style={{flex: 1, flexDirection: 'row', marginLeft: 5, marginRight: 5, marginTop: 5, marginBottom: 5,}}>
                    {
                        Utils.renderIf(this.state.visible,
                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', }}>
                                <Text style={styles.textLeft}>{this.props.playername}</Text>
                                <Text style={styles.textRight}>{TipePemainStringEnum.Publish}</Text>
                            </View>
                        )
                    }
                    {
                        Utils.renderIf(!this.state.visible,
                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', }}>
                                <Text style={styles.textLeft}>{this.props.name + "(" + this.props.playername + ")"}</Text>
                                <Text style={styles.textRight}>{Enums.TipePemainEnum(this.props.type)}</Text>
                            </View>
                        )
                    }
                </View>
            </View>       
        );
    }
}

const styles = StyleSheet.create({
    
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#fff',
    },

    textLeft: {
        flex: 2,
        fontSize: 15,
        marginTop: 5,
        color: Colors.text,
    },

    textRight: {
        flex: 1,
        fontSize: 15,
        marginTop: 5,
        textAlign: 'right',
        color: '#edb636',
    },
});