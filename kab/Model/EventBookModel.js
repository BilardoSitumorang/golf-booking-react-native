import clsSetting from "../API/clsSetting";

export default class EventBookModel
{
    constructor(_name, _code, _type, _card, _playername, _idpricing, _pricing)
    {
        this.Nama = _name;
        this.Kode = _code;
        this.Tipe = _type;
        this.Kartu = _card;
        this.PlayerName = _playername;
        this.IdPricing = _idpricing;
        this.Pricing = _pricing;
    }
}