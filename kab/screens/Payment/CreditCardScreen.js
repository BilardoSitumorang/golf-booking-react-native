import React from 'react';
import {
    Image,
    Platform,
    ScrollView,
    StyleSheet,
    SafeAreaView,
    TouchableOpacity,
    Text,
    View,
} from 'react-native';
import Colors from '../../constants/Colors';
import Strings from '../../constants/Strings';
import { Divider, } from 'react-native-elements';
import { Card } from 'react-native-material-cards';
import { Button, } from 'react-native-elements';
import BpkTextInput from 'react-native-bpk-component-text-input';
import { OptionIntEnum, PaymentIntEnum, TransactionTypeIntEnum, } from '../../constants/Enums';
import { getOptions,  } from '../../API/OptionService';
import Utils from '../Utils';
import clsSetting from '../../API/clsSetting';

export default class CreditCardScreen extends React.Component {
    static navigationOptions = {
        title: 'Credit Card',
        headerTintColor: '#ffffff', 
        headerStyle: {
            backgroundColor: '#93b843'
        },
    };

    constructor(props){
        super(props);

        this.params = this.props.navigation.state.params;

        this.state = {
            isLoading: true,
            transtype: this.params.TransType,
            point: this.params.Point,
            paymenttype: this.params.PaymentType,
            chargepayment: this.params.ChargePayment,
            bookingcode: this.params.BookingCode,
            bookingdate: this.params.BookingDate,
            total: this.params.Total,
            containmandiri: this.params.ContainMandiri,
            mandiricard: this.params.MandiriCard,

            txtTotalTransfer: '',
            txtPoint: '',
            txtTotalPayment: '',
            txtConvenienceFee: '',
            conveniencevisible: false,
            pointvisible: true,

            platform: '',
            noteerr: '',

            inputcardname: '',
            inputcardnumber: '',
            inputcardmm: '',
            inputcardyy: '',
            inputcardcvv: '',
        };
    }
    onSelect = data => {
        this.setState(data);
    };

    componentDidMount() {
        var Enumerable = require('../linq');

        if(this.state.containmandiri)
        {
            this.setState({ inputcardnumber: this.state.mandiricard.substring(0, 6), })
        }

        getOptions().then((jsonOption) => 
        {
            if(this.state.transtype == TransactionTypeIntEnum.Reservation)
            {
                this.setState({
                    maintenanceOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionMaintenancePeriod).firstOrDefault(),
                    reservationOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionReservation).firstOrDefault(),
                    ccOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionCreditCardReservation).firstOrDefault(),
                });

                this.setState({
                    platform: Platform.OS === 'ios' ? this.state.reservationOption.iOS : this.state.reservationOption.Android,
                    noteerr: this.state.reservationOption.Note,
                });
            }
            else if(this.state.transtype == TransactionTypeIntEnum.RegistrationQ)
            {
                this.setState({
                    maintenanceOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionMaintenancePeriod).firstOrDefault(),
                    registrationOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionRegistration).firstOrDefault(),
                    ccOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionCreditCardRegistration).firstOrDefault(),
                });

                this.setState({ conveniencevisible: true, });
                this.setState({
                    platform: Platform.OS === 'ios' ? this.state.registrationOption.iOS : this.state.registrationOption.Android,
                    noteerr: this.state.registrationOption.Note,
                });
            }
            else if(this.state.transtype == TransactionTypeIntEnum.Event)
            {
                this.setState({
                    maintenanceOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionMaintenancePeriod).firstOrDefault(),
                    eventOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionEvent).firstOrDefault(),
                    ccOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionCreditCardEvent).firstOrDefault(),
                });
                this.setState({ conveniencevisible: true, });
                this.setState({
                    platform: Platform.OS === 'ios' ? this.state.eventOption.iOS : this.state.eventOption.Android,
                    noteerr: this.state.eventOption.Note,
                });
            }
            else if(this.state.transtype == TransactionTypeIntEnum.MobilePulsaPhonePrepaid)
            {
                this.setState({
                    maintenanceOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionMaintenancePeriod).firstOrDefault(),
                    pulsaOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionMobilePulsa).firstOrDefault(),
                    ccOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionCreditCardMobilePulsa).firstOrDefault(),
                });
                this.setState({ conveniencevisible: true, });
                this.setState({
                    platform: Platform.OS === 'ios' ? this.state.pulsaOption.iOS : this.state.pulsaOption.Android,
                    noteerr: this.state.pulsaOption.Note,
                });
            }

            var amountminpoint = this.state.total - this.state.point;
            if(this.state.chargepayment)
            {
                this.setState({
                    txtConvenienceFee: Math.round(2.2 / 100 * amountminpoint) + 2500,
                });
            }
            if (this.state.point > 0)
            {
                this.setState({ 
                    txtPoint: this.state.point / clsSetting.AccountBalance,
                    txtConvenienceFee: this.state.txtConvenienceFee,
                    txtTotalPayment: this.state.total,
                    txtTotalTransfer: this.state.total - this.state.point + this.state.txtConvenienceFee,
                    pointvisible: true,
                });
            }
            else
            {
                this.setState({ 
                    txtPoint: 0,
                    pointvisible: false,
                    txtTotalPayment: this.state.total,
                    txtTotalTransfer: this.state.total + this.state.txtConvenienceFee,
                }); 
            }

            this.setState({isLoading: false, });
        })
        .catch((error) => {
            console.error(error);
            this.setState({isLoading: false, });
        });
    }

    render() {
        const goto3DS = () => {
            var ccPlatform = Platform.OS === 'ios' ? this.state.ccOption.iOS : this.state.ccOption.Android;
            if(Utils.getMaintenanceOption(this.state.maintenanceOption)){
                Utils.alertBackToMain(this.state.maintenanceOption.Note, this.props);
            }
            else if(!this.state.platform) {
                Utils.errAlert("QAccess Alert", this.state.noteerr);
            }
            else if(this.state.paymenttype != PaymentIntEnum.FullPoint && !ccPlatform)
            {
                Utils.errAlert("QAccess Alert", this.state.ccOption.Note);
            }
            else
            {
                if (this.state.inputcardname === '')
                {
                    Utils.errAlert("Error Message", Strings.CreditCard.errmsg1);
                }
                else if (this.state.inputcardnumber === '')
                {
                    Utils.errAlert("Error Message", Strings.CreditCard.errmsg2);
                }
                else if (this.state.inputcardmm === '')
                {
                    Utils.errAlert("Error Message", Strings.CreditCard.errmsg3);
                }
                else if (this.state.inputcardyy === '')
                {
                    Utils.errAlert("Error Message", Strings.CreditCard.errmsg4);
                }
                else if (this.state.inputcardyy.length != 2)
                {
                    Utils.errAlert("Error Message", Strings.CreditCard.errmsg5);
                }
                else if (this.state.inputcardcvv === '')
                {
                    Utils.errAlert("Error Message", Strings.CreditCard.errmsg6);
                }
                else if (this.state.total != 0)
                {
                    this.props.navigation.navigate('CreditCard3DS', {Point: this.state.point, BookingCode: this.state.bookingcode,
                        BookingDate: this.state.bookingdate, ChargePayment: this.state.chargepayment, 
                        TransType: this.state.transtype, PaymentType: PaymentIntEnum.CreditCard, Total: this.state.total,
                        NameOnCard: this.state.inputcardname, CardNumber: this.state.inputcardnumber, 
                        CardMonth: this.state.inputcardmm, CardYear: this.state.inputcardyy, CardCVV: this.state.inputcardcvv,
                        ContainMandiri: this.state.containmandiri, MandiriCard: this.state.mandiricard, onSelect: this.onSelect});

                }
                else
                {
                    Utils.errAlert("Error Message", Strings.Payment.errmsg7);
                }
            }
        }

        return (
            <SafeAreaView style={styles.safeAreaView}>
                <ScrollView style={styles.containerScroll}>
                    <View style={styles.container}>
                        <View style={{flex: 1, flexDirection: 'row', backgroundColor: '#efefef', paddingLeft: 15, 
                            paddingRight: 15, paddingTop: 10, paddingRight: 10, alignItems: 'center'}}>
                            <View style={{flex: 1, flexDirection: 'row', marginTop: 5, marginBottom: 5,}}>
                                <Text style={{fontSize: 15, color: Colors.text, fontWeight: 'bold',}} >
                                    {Strings.CreditCard.text1}</Text>
                                <Text style={{fontSize: 15, color: Colors.text, fontWeight: 'bold', }} >
                                    {this.state.bookingcode}</Text>
                            </View>
                            <Text style={{fontSize: 12, color: Colors.text, flex: 1, textAlign: 'right',}} >
                                {this.state.bookingdate}</Text>
                        </View>
                        <Card style={styles.cardForm}>
                            <View style={{width: '100%', }}>
                                <BpkTextInput 
                                    label={Strings.CreditCard.text7}
                                    style={{marginBottom: 3, marginLeft: 5, marginRight: 5, }}
                                    onChangeText={(text) => this.setState({inputcardname: text})}
                                    value={this.state.inputcardname}/>
                            </View>
                            <View style={{width: '100%',}}>
                                <BpkTextInput 
                                    label={Strings.CreditCard.text8}
                                    maxLength={16}
                                    style={{marginBottom: 3, marginLeft: 5, marginRight: 5, }}
                                    onChangeText={(text) => this.setState({inputcardnumber: text})}
                                    value={this.state.inputcardnumber} />
                            </View>
                            <View style={{width: '100%', flexDirection: 'row', }}>
                                <View style={{flex: 1, }}>
                                    <BpkTextInput 
                                        label={Strings.CreditCard.text9}
                                        maxLength={2}
                                        style={{marginBottom: 3, marginLeft: 5, }}
                                        onChangeText={(text) => this.setState({inputcardmm: text})}
                                        value={this.state.inputcardmm} />
                                </View>
                                <View style={{flex: 2, }}>
                                    <BpkTextInput 
                                        label={Strings.CreditCard.text10}
                                        maxLength={2}
                                        style={{marginBottom: 3, marginLeft: 10, marginRight: 5, }}
                                        onChangeText={(text) => this.setState({inputcardyy: text})}
                                        value={this.state.inputcardyy} />
                                </View>
                            </View>
                            <View style={{width: '100%', }}>
                                <BpkTextInput 
                                    label={Strings.CreditCard.text11}
                                    maxLength={3}
                                    style={{marginBottom: 3, marginLeft: 5, marginRight: 5, }}
                                    secureTextEntry
                                    onChangeText={(text) => this.setState({inputcardcvv: text})}
                                    value={this.state.inputcardcvv} />
                            </View>
                        </Card>
                        <View style={styles.cardVertical}>
                            {
                                Utils.renderIf(this.state.txtConvenienceFee != 0 || this.state.pointvisible,
                                    <View style={{flex: 1, flexDirection: 'row', }}>
                                        <Text style={styles.textLeft} >{Strings.CreditCard.text3}</Text>
                                        <Text style={styles.textRight} >{'Rp. ' + Utils.currencyfunc(Math.round(this.state.txtTotalPayment*100)/100)}</Text>
                                    </View>
                                )
                            }
                            {
                                Utils.renderIf(this.state.pointvisible,
                                    <View style={{flex: 1, flexDirection: 'row', marginTop: 5, }}>
                                        <Text style={styles.textLeft} >{Strings.CreditCard.text4}</Text>
                                        <Text style={styles.textRight} >{'Rp. ' + Utils.currencyfunc(Math.round(this.state.txtPoint*clsSetting.AccountBalance*100)/100)}</Text>
                                    </View>
                                )
                            }
                            {
                                Utils.renderIf(this.state.txtConvenienceFee != 0,
                                    <View style={{flex: 1, flexDirection: 'row', marginTop: 5, }}>
                                        <Text style={styles.textLeft} >{Strings.CreditCard.text5}</Text>
                                        <Text style={styles.textRight} >{'Rp. ' + Utils.currencyfunc(Math.round(this.state.txtConvenienceFee*100)/100)}</Text>
                                    </View>
                                )
                            }
                            {
                                Utils.renderIf(this.state.txtConvenienceFee != 0 || this.state.pointvisible,
                                    <Divider style={{ backgroundColor: '#414141', height: 1, width: '100%', marginTop: 5, }} />
                                )
                            }
                            <View style={{flex: 1, flexDirection: 'row', marginTop: 5, }}>
                                <Text style={styles.textLeft} >{Strings.CreditCard.text6}</Text>
                                <Text style={styles.textTotalRight} >{'Rp. ' + Utils.currencyfunc(Math.round(this.state.txtTotalTransfer*100)/100)}</Text>
                            </View>
                        </View>
                        <Button 
                            onPress={ goto3DS }
                            buttonStyle={{backgroundColor: Colors.button, borderRadius: 5, marginTop: 20, marginLeft: 15, marginRight: 15, marginBottom: 20, height: 50, }}
                            title={Strings.CreditCard.paybutton}/>
                    </View>
                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({

    safeAreaView: {
        flex: 1,
    },

    containerScroll:
    {
        flex: 1,
        backgroundColor: '#efefef',
    },

    container: {
        flex: 1,
        flexDirection: 'column',
    },

    image: {
        height: 20,
        width: 20,
        resizeMode: 'contain',
    },

    cardNote: {
        backgroundColor: Colors.backgroundColor,
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 5,
        marginBottom: 5,
        marginLeft: 10,
        marginRight: 10,
        padding: 10,
        elevation: 2,
        borderRadius: 5,
    },

    textNote: {
        fontSize: 12,
        marginLeft: 3,
        color: Colors.text,
    },

    text: {
        fontSize: 15,
        marginLeft: 10,
        color: Colors.text,
    },

    textLeft: {
        flex: 1,
        fontSize: 15,
        marginRight: 10,
        textAlign: 'left',
        color: Colors.text,
    },

    textRight: {
        flex: 1,
        fontSize: 15,
        textAlign: 'right',
        color: Colors.text,
    },

    textTotalRight: {
        flex: 1,
        fontSize: 15,
        textAlign: 'right',
        color: '#93b843',
    },

    textSession: {
        fontSize: 15,
        marginLeft: 3,
        color: '#edb636',
    },

    cardVertical: {
        backgroundColor: Colors.backgroundColor,
        flex: 1,
        flexDirection: 'column',
        marginTop: 5,
        marginBottom: 5,
        marginLeft: 10,
        marginRight: 10,
        padding: 10,
        elevation: 2,
        borderRadius: 5,
    },

    cardForm: {
        backgroundColor: Colors.backgroundColor,
        flex: 1,
        flexDirection: 'column',
        marginTop: 5,
        marginBottom: 5,
        marginLeft: 10,
        marginRight: 10,
        paddingLeft: 10,
        paddingRight: 10,
        paddingBottom: 10,
        elevation: 2,
        borderRadius: 5,
    },
});


//textDecorationLine: 'underline'