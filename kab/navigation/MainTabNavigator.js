import React from 'react';
import { Platform, StyleSheet, Image, Text,} from 'react-native';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';

import { StackNavigator, TabNavigator } from 'react-navigation';

import TabBarIcon from '../components/TabBarIcon';
import HomeScreen from '../screens/HomeScreen';
import HelpScreen from '../screens/HelpScreen';
import AccountScreen from '../screens/AccountScreen';
import OrdersScreen from '../screens/OrdersScreen';
import InboxScreen from '../screens/InboxScreen';
import Colors from '../constants/Colors';
import TestScreen from '../screens/TestScreen';

const HomeStack = createStackNavigator({
  Home: HomeScreen,
});

HomeStack.navigationOptions = {
  tabBarLabel: ({ focused }) => (
    <Text style={{textAlign: 'center', fontSize: 11, marginBottom: 2, color: focused ? Colors.tabIconSelected : Colors.tabIconDefault}}>
      {'Home'}
    </Text>
  ),
  tintColor: Colors.tabIconSelected,
  tabBarIcon: ({ focused }) => (
    <Image
        source={require('../assets/images/home.png')}
        style={[styles.icon, {tintColor: focused ? Colors.tabIconSelected : Colors.tabIconDefault}]}
    />
  ),
};

const OrdersStack = createStackNavigator({
  Order: OrdersScreen,
});

OrdersStack.navigationOptions = {
  tabBarLabel: ({ focused }) => (
    <Text style={{textAlign: 'center', fontSize: 11, marginBottom: 2, color: focused ? Colors.tabIconSelected : Colors.tabIconDefault}}>
      {'Orders'}
    </Text>
  ),
  tintColor: Colors.tabIconSelected,
  tabBarIcon: ({ focused }) => (
    <Image
        source={require('../assets/images/booking.png')}
        style={[styles.icon, {tintColor: focused ? Colors.tabIconSelected : Colors.tabIconDefault}]}
    />
  ),
};

const InboxStack = createStackNavigator({
  Inbox: InboxScreen,
});

InboxStack.navigationOptions = {
  tabBarLabel: ({ focused }) => (
    <Text style={{textAlign: 'center', fontSize: 11, marginBottom: 2, color: focused ? Colors.tabIconSelected : Colors.tabIconDefault}}>
      {'Inbox'}
    </Text>
  ),
  tintColor: Colors.tabIconSelected,
  tabBarIcon: ({ focused }) => (
    <Image
        source={require('../assets/images/envelope.png')}
        style={[styles.icon, {tintColor: focused ? Colors.tabIconSelected : Colors.tabIconDefault}]}
    />
  ),
};

const HelpStack = createStackNavigator({
  Help: {
      screen: HelpScreen, 
      navigationOptions: {
            header: null
      }
    }
});

HelpStack.navigationOptions = {
  header: null,
  tabBarLabel: ({ focused }) => (
    <Text style={{textAlign: 'center', fontSize: 11, marginBottom: 2, color: focused ? Colors.tabIconSelected : Colors.tabIconDefault}}>
      {'Help'}
    </Text>
  ),
  tabBarIcon: ({ focused }) => (
    <Image
        source={require('../assets/images/help.png')}
        style={[styles.icon, {tintColor: focused ? Colors.tabIconSelected : Colors.tabIconDefault}]}
    />
  ),
};

const AccountStack = createStackNavigator({
  Account: AccountScreen,
});

AccountStack.navigationOptions = {
  tabBarLabel: ({ focused }) => (
    <Text style={{textAlign: 'center', fontSize: 11, marginBottom: 2, color: focused ? Colors.tabIconSelected : Colors.tabIconDefault}}>
      {'Account'}
    </Text>
  ),
  tabBarIcon: ({ focused }) => (
    <Image
        source={require('../assets/images/account.png')}
        style={[styles.icon, {tintColor: focused ? Colors.tabIconSelected : Colors.tabIconDefault}]}
    />
  ),
};

const styles = StyleSheet.create({
  icon: {
    width: 22,
    height: 22,
    marginTop: 2,
  },
});

export default createBottomTabNavigator({
  HomeStack,
  OrdersStack,
  InboxStack,
  HelpStack,
  AccountStack,
});
