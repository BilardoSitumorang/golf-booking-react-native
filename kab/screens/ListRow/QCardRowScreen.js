import React from 'react';
import { 
    ScrollView, 
    StyleSheet, 
    SafeAreaView,
    Image,
    View,
    Text,
    Platform,
} from 'react-native';
import Colors from '../../constants/Colors';
import { Card, CardTitle, CardContent, CardAction, CardButton, CardImage } from 'react-native-material-cards';
import { Divider } from 'react-native-elements';

export default class QCardRowScreen extends React.Component {
    
    static navigationOptions = {
        title: '',
        header: null
    };

    render() {
        return (
            <Card style={styles.cardList} >
                <View style={{flexDirection: 'row', flex: 1, }}>
                    <View style={{width: 150, height: 100}}>
                        <Image 
                            style={{width: 150, height: 100, resizeMode: 'stretch', borderRadius: 2, }} 
                            source={this.props.cardimg}/>
                    </View>
                    <View style={{flex: 1, flexDirection: 'column', marginLeft: 7, marginRight: 7, justifyContent: 'center', alignItems: 'flex-start',}}>
                        <Text style={{fontSize: 14, color: '#414141', flex: 1,}}>{this.props.cardname}</Text>
                        <Divider style={{ height: 1, backgroundColor: '#333', width: '100%',}} />
                       <Text style={{fontSize: 13, color: '#999', flex: 1, }}>{this.props.carddesc}</Text>
                    </View>
                </View>
            </Card>
        );
    }
}

const styles = StyleSheet.create({
    
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },

    safeAreaView: {
        flex: 1
    },

    cardList: {
        backgroundColor: '#fff',
        height: 100,
        marginLeft: 15,
        marginRight: 15,
        elevation: 2,
        borderRadius: 5,
    },
});