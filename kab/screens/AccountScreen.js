import React from 'react';
import {
    Image,
    Platform,
    StyleSheet,
    Text,
    TouchableHighlight,
    AsyncStorage,
    View,
    ScrollView,
    SafeAreaView,
} from 'react-native';
import clsSetting from '../API/clsSetting';
import Colors from '../constants/Colors';
import Strings from '../constants/Strings';
import { Button, Divider, } from 'react-native-elements';
import { Card, } from 'react-native-material-cards';
import { getProfile, getReferralData  } from '../API/UserService';
import { getDoubleToStringDate,  } from '../API/SettingService';
import moment from 'moment';
import Enums, { TipePemainEnum, TipePemainIntEnum, TipePemainStringEnum } from '../constants/Enums';
import Utils from './Utils';

export default class SettingsScreen extends React.Component {
    static navigationOptions = {
        title: '',
        header: null
    };

    constructor(props){
        super(props);

        this.state = {
            isLoading: true,
            username: '',
            nama: '',
            email: '',
            phone: '',
            type: 0,
            gender: '',
            dob: '',
            address: '',
            foto: '',
            totalreferral: '',
            totalreferralplay: '',
            referralmonthly: '',
        }
    }

    async componentDidMount() {
        this.setState({ username : await AsyncStorage.getItem('Username')});
        getProfile(this.state.username).then((json) =>
        {
            console.log('account ', json);
            this.setState({ 
                nama : json.MemberProfile.Nama,
                email : json.MemberProfile.Email,
                phone : json.MemberProfile.Telp,
                type : json.MemberProfile.TipeMember,
                gender : json.MemberProfile.JenisKelamin == 1 ? 'Male' : json.MemberProfile.JenisKelamin == 2 ? "Female" : '-',
                dob : json.MemberProfile.TanggalLahir,
                address : json.MemberProfile.Alamat,
                foto: json.MemberProfile.Foto != null ? clsSetting.HomeUrl + "" + json.MemberProfile.Foto : '',
            });

            var firstchar = json.MemberProfile.Telp.slice(0, 1);
            if(firstchar != '0')
            {
                this.setState({
                    phone : '+62' + json.MemberProfile.Telp,
                })
            }

            getDoubleToStringDate(json.MemberProfile.TanggalLahir).then((jsonDate) =>
            {
                console.log('account ', jsonDate);
                this.setState({ 
                    dob : jsonDate.result,
                });
    
            })
            .catch((error) => {
                console.error(error);
            });

            getReferralData(this.state.username).then((jsonReferral) => 
            {
                console.log('account ', jsonReferral);
                this.setState({ 
                    totalreferral: jsonReferral.ReferralCount,
                    totalreferralplay: jsonReferral.ReferralPlayCount,
                    referralmonthly: jsonReferral.ReferralMonthlyPlayCount,
                });
            })
            .catch((error) => {
                console.error(error);
            });
        })
        .catch((error) => {
            console.error(error);
        });
    }

    render() {
        const gotoUpdate = () => {
            this.props.navigation.navigate('UpdateProfile')
        }

        const gotolinkQ = () => {
            this.props.navigation.navigate('linkqmember')
        }

        gotoLogout = async () => {
            await AsyncStorage.clear();
            this.props.navigation.navigate('Auth');
        };

        return (
            <SafeAreaView style={styles.safeAreaView}>
                <ScrollView style={styles.container}>
                    <View style={styles.container2}>
                        <Card style={styles.card} >
                            <TouchableHighlight onPress={ gotoUpdate } style={{flex: 1, flexDirection: 'row', }}>
                                <Text style={styles.textbutton} >{Strings.Account.updatebutton}</Text>
                            </TouchableHighlight>
                            <View style={{width:'100%', justifyContent: 'center', alignItems: 'center', }}>
                                <View>
                                    {
                                        Utils.renderIf(this.state.foto == '', 
                                            <Image style={{width: 100, height: 100, borderRadius: 50, borderWidth: 1, backgroundColor: '#fff', borderColor: '#d1d1d1'}} 
                                                source={require('../assets/images/register.png')}/>
                                        )
                                    }
                                    {
                                        Utils.renderIf(this.state.foto != '', 
                                            <Image style={{width: 100, height: 100, borderRadius: 50, borderWidth: 1, backgroundColor: '#fff', borderColor: '#d1d1d1'}} 
                                                source={{uri: this.state.foto}}/>
                                        )
                                    }
                                </View>
                                <Text style={styles.textName} >{this.state.nama}</Text>
                            </View>
                            <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginTop: 5,}}>
                                <View>
                                    <Text style={styles.textType} >{ Enums.TipePemainEnum(this.state.type) }</Text>
                                </View>
                                {
                                    Utils.renderIf(this.state.type !== TipePemainIntEnum.QMember,
                                        <View style={{flex: 1, alignItems: 'flex-end'}}>
                                            <TouchableHighlight onPress={ gotolinkQ  }>
                                                <Text style={styles.textbutton} >{Strings.Account.linkmemberbutton}</Text>
                                            </TouchableHighlight>
                                        </View>
                                    )
                                }
                            </View>
                            
                            <Text style={styles.textLabel} >{this.state.email}</Text>
                            <Text style={styles.textLabel} >{this.state.phone}</Text>
                        </Card>
                        <Card 
                            style={styles.card} >
                            <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center', marginTop: 5,}}>
                                <View style={{width: '25%'}}>
                                    <Text style={styles.textLabel} >{Strings.Account.text4}</Text>
                                </View>
                                <View style={{flex: 1, marginLeft: 5,}}>
                                    <Text style={styles.textField} >{this.state.gender === 1 ? "Male" : this.state.gender === 2 ? "Female" : '-'}</Text>
                                </View>
                            </View>
                            <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center', marginTop: 5,}}>
                                <View style={{width: '25%'}}>
                                    <Text style={styles.textLabel} >{Strings.Account.text5}</Text>
                                </View>
                                <View style={{flex: 1, marginLeft: 5,}}>
                                    <Text style={styles.textField} >{moment(new Date(this.state.dob)).format('DD - MMM - YYYY')}</Text>
                                </View>
                            </View>
                            <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center', marginTop: 5,}}>
                                <View style={{width: '25%'}}>
                                    <Text style={styles.textLabel} >{Strings.Account.text6}</Text>
                                </View>
                                <View style={{flex: 1, marginLeft: 5,}}>
                                    <Text style={styles.textField} >{this.state.address}</Text>
                                </View>
                            </View>
                        </Card>
                        <Card 
                            style={styles.cardHorizontal} >
                            <View style={{flex: 1, flexDirection: 'column', alignItems: 'center', width:'50%', }}>
                                <View style={{height: 30,}} >
                                    <Text style={styles.textTotal} >{this.state.totalreferral}</Text>
                                </View>
                                <View>
                                    <Text style={styles.textLabel2} >{Strings.Account.text1}</Text>
                                </View>
                            </View>
                            <Divider style={{ height: '100%', backgroundColor: '#93b843', width: 1,}} />
                            <View style={{flex: 1, flexDirection: 'column', alignItems: 'center', width:'50%', }}>
                                <View style={{height: 30,}} >
                                    <Text style={styles.textTotal} >{this.state.totalreferralplay}</Text>
                                </View>
                                <View>
                                    <Text style={styles.textLabel2} >{Strings.Account.text2}</Text>
                                </View>
                            </View>
                            <Divider style={{ height: '100%', backgroundColor: '#93b843', width: 1,}} />
                            <View style={{flex: 1, flexDirection: 'column', alignItems: 'center', width:'50%', }}>
                                <View style={{height: 30,}} >
                                    <Text style={styles.textTotal} >{this.state.referralmonthly}</Text>
                                </View>
                                <View>
                                    <Text style={styles.textLabel2} >{Strings.Account.text3}</Text>
                                </View>
                            </View>
                        </Card>
                        <View>
                            <Button 
                                onPress={ gotoLogout }
                                buttonStyle={{backgroundColor: Colors.button, borderRadius: 5, marginTop: 10, elevation: 2, height: 50, }}
                                title={Strings.Account.logoutbutton}/>
                        </View>
                    </View>
                </ScrollView>
            </SafeAreaView>
        );
    }   
}

const styles = StyleSheet.create({
    safeAreaView: {
        flex: 1
    },

    containerScroll: {
        flex: 1,
        backgroundColor: '#f2f2f2',
    },

    container: {
        flex: 1,
        flexDirection: 'column',
    },

    container2: {
        flex: 1,
        flexDirection: 'column',
        marginTop: 15,
    },

    image: {
        height: 100,
        width: 100,
        marginTop: 5,
        borderRadius: 50,
    },

    textbutton: {
        flex: 1,
        flexDirection: 'row',
        fontSize: 15,
        textAlign: 'right',
        color: Colors.button,
    },

    textName: {
        fontSize: 20,
        textAlign: 'center',
        marginTop: 5,
        color: Colors.text,
    },

    textLabel: {
        fontSize: 14,
        textAlign: 'left',
        color: '#939393',
        marginTop: 5,
    },

    textLabel2: {
        fontSize: 12,
        textAlign: 'center',
        color: '#939393',
    },

    textField: {
        fontSize: 15,
        textAlign: 'left',
        color: Colors.text,
        marginTop: 5,
    },

    textType: {
        fontSize: 17,
        textAlign: 'left',
        color: '#edb636',
    },

    textTotal: {
        fontSize: 20,
        textAlign: 'center',
        color: Colors.text,
    },

    card: {
        backgroundColor: Colors.backgroundColor,
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        margin: 10,
        padding: 10,
        elevation: 2,
        borderRadius: 5,
    },

    cardHorizontal: {
        backgroundColor: Colors.backgroundColor,
        flex: 1,
        flexDirection: 'row',
        margin: 10,
        padding: 10,
        elevation: 2,
        borderRadius: 5,
    },
});
