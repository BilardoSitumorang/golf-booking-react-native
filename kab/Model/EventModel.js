import clsSetting from "../API/clsSetting";

export default class EventModel
{
    constructor(_id, _title, _desc, _fromdate, _todate, _image, _thumbnail, _createddate, _modifieddate)
    {
        this.Id = _id;
        this.Title = _title;
        this.Description = _desc;
        this.FromDate = _fromdate;
        this.ToDate = _todate;
        this.IdPricing = _idpricing;
        this.Image = _image;
        this.Thumbnail = _thumbnail;
        this.CreatedDate = _createddate;
        this.ModifiedDate = _modifieddate;
    }
}