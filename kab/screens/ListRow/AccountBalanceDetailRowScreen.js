import React from 'react';
import { 
    StyleSheet, 
    Image,
    View,
    Text,
} from 'react-native';
import Colors from '../../constants/Colors';
import Strings from '../../constants/Strings';
import { TransactionTypeIntEnum, TipeActivityIntEnum } from '../../constants/Enums';
import { getDoubleToStringDate,  } from '../../API/SettingService';
import moment from 'moment';
import clsSetting from '../../API/clsSetting';
import Utils from '../Utils';

export default class AccountBalanceDetailRowScreen extends React.Component {
    
    static navigationOptions = {
        title: '',
        header: null,
    };

    constructor(props){
        super(props);

        this.state = {
            row: this.props.row,
            bookingcode: '',
            date: '',
            value: '',
            tipeactivity: '',
        }
    }

    componentDidMount() {
        this.setState({ 
            bookingcode: this.state.row.KodeTransaksi,
            value:  Number.parseInt(this.state.row.TotalPoint) / clsSetting.AccountBalance,
            tipeactivity: this.state.row.TipeActivity,
        });

        if(this.state.row.Tanggal != undefined){
            getDoubleToStringDate(this.state.row.Tanggal).then((jsonDate) =>
            {
                this.setState({ 
                    date : moment(Date.parse(jsonDate.result)).format('YYYY - MM - DD h:mm '),
                });
    
            })
            .catch((error) => {
                console.error(error);
            });
        }
        
    }

    render() {
        return (
            <View style={styles.container} >
                <View style={{flex: 1, flexDirection: 'row', marginLeft: 15, marginRight: 15, backgroundColor: '#fff'}}>
                    <View style={{flex: 2, flexDirection: 'column', marginTop: 5, marginBottom: 5, }}>
                        <Text style={styles.textLeft}>{this.state.bookingcode}</Text>
                        <Text style={styles.textRight}>{this.state.date}</Text>
                    </View>
                    <View style={{flex: 1, flexDirection: 'column', alignItems: 'center', justifyContent: 'center', marginTop: 5, marginBottom: 5, }}>
                        {
                            Utils.renderIf(this.state.tipeactivity === TipeActivityIntEnum.Credit,
                                <Text style={{fontSize: 15, color: '#93b843', fontWeight: 'bold', }}>{this.state.value}</Text>
                            )
                        }
                        {
                            Utils.renderIf(this.state.tipeactivity !== TipeActivityIntEnum.Credit,
                                <Text style={{fontSize: 15, color: '#b84242', fontWeight: 'bold', }}>{this.state.value}</Text>
                            )
                        }
                    </View>
                </View>
            </View>       
        );
    }
}

const styles = StyleSheet.create({
    
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#fff',
    },

    textType: {
        fontSize: 14,
        marginTop: 5,
        color: '#edb636',
        width: 10,
    },

    textLeft: {
        fontSize: 15,
        color: Colors.text,
    },

    textLeft2: {
        fontSize: 15,
        color: '#939393',
    },

    textRight: {
        fontSize: 15,
        marginTop: 5,
        color: Colors.text,
    },
});