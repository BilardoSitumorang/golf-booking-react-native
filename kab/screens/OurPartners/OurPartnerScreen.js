import React from 'react';
import {
    StyleSheet, 
    AsyncStorage,
    Text, 
    Image,
    TouchableOpacity,
    View,
    ListView,
    ScrollView,
    SafeAreaView,
    Dimensions,
    ActivityIndicator,
} from 'react-native';
import Colors from '../../constants/Colors';
import { getMerchantCategories } from '../../API/PartnershipService';
import GridView from 'react-native-super-grid';
import Utils from '../Utils';
import clsSetting from '../../API/clsSetting';

const dimensions = Dimensions.get('window');
const imageWidth = dimensions.width;
const imageHeight = Math.round(dimensions.width * 0.5);

export default class OurPartnerScreen extends React.Component{
    static navigationOptions = {
        title: 'Our Partners',
        headerTintColor: '#ffffff', 
        headerStyle: {
            backgroundColor: '#93b843',
        },
    };

    constructor(props)
    {
        super(props);
        this.state = {
            isLoading: true,
            dataSource: '',
        };
    }

    componentDidMount() {
        getMerchantCategories().then((json) =>
        {
            this.setState({
                isLoading: false,
                dataSource: json,
            });
        })
        .catch((error) => {
            console.error(error);
            this.setState({ isLoading: false});
        });
    }

    GetItem (row) {
        //Selected row Grid
        this.props.navigation.navigate('Merchant', {IdCat: row.Id, Name: row.Name});
    }

    render() {
        if (this.state.isLoading) {
            return (
                <View style={{flex: 1, paddingTop: 20}}>
                    <ActivityIndicator />
                </View>
            );
        }


        return (
            <SafeAreaView style={styles.safeAreaView}>
                <View style={styles.containerView}>
                    <Image source={require('../../assets/images/ourpartner.jpg')} style={styles.image}/>
                    <GridView
                        itemDimension={80}
                        items={this.state.dataSource}
                        style={styles.gridView}
                        renderItem={item => (
                        <View style={{backgroundColor: '#ffffff'}}>
                            <TouchableOpacity style={{justifyContent: 'center', alignContent: 'center', }}
                                onPress={ this.GetItem.bind(this, item) }>
                                <Image style={styles.image2} source={{ uri: clsSetting.HomeUrl + item.Thumbnail}}></Image>
                            </TouchableOpacity>
                        </View>
                        )}
                    />
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.backgroundColor,
    },

    safeAreaView: {
        flex: 1
    },

    containerView: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#ffffff'
    },

    image: {
        height: imageHeight,
        width: imageWidth,
        marginBottom: 10,
    },

    image2: {
        height: 100,
        width: 105,
    },

    textTitle: {
        fontSize: 15,
        color: Colors.text,
        marginLeft: 15,
    },

    text: {
        fontSize: 13,
        marginTop: 5,
        marginLeft: 15,
        marginRight: 15,
        color: Colors.text,
    },

    gridView: {
        paddingTop: 25,
        flex: 1,
    },
});

