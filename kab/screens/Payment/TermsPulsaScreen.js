import React from 'react';
import {
    ScrollView,
    StyleSheet,
    Text,
    Image,
    SafeAreaView,
    View,
} from 'react-native';
import Colors from '../../constants/Colors';
import Strings from '../../constants/Strings';
import { Button, } from 'react-native-elements';
import { PaymentIntEnum } from '../../constants/Enums';

export default class TermsPulsaScreen extends React.Component {
    static navigationOptions = {
        title: 'Terms And Condition',
        headerTintColor: '#ffffff', 
        headerStyle: {
            backgroundColor: '#93b843'
        },
    };

    constructor(props){
        super(props);

        this.params = this.props.navigation.state.params;

        this.state = {
            transtype: this.params.TransType,
            point: this.params.Point,
            paymenttype: this.params.PaymentType,
            chargepayment: this.params.ChargePayment,
            bookingcode: this.params.BookingCode,
            bookingdate: this.params.BookingDate,
            total: this.params.Total,
            containmandiri: this.params.ContainMandiri,
            mandiricard: this.params.MandiriCard,
            transdate: this.params.TransDate,
            eventdate: this.params.EventDate,
            timeevent: this.params.TimeEvent,
            status: this.params.Status,
            bookingdateraw: this.params.BookingDateRaw,
        };
    }

    render() {
        const gotoContinue = () => {
            if(this.state.paymenttype == PaymentIntEnum.BankTransfer)
            {
                this.props.navigation.navigate('BankTransfer', {Point: this.state.point, BookingCode: this.state.bookingcode,
                    BookingDate: this.state.bookingdate, ChargePayment: this.state.chargepayment, 
                    TransType: this.state.transtype, PaymentType: PaymentIntEnum.BankTransfer, Total: this.state.total,
                    TransDate: this.state.transdate, EventDate: this.state.eventdate, TimeEvent: this.state.timeevent, 
                    BookingDateRaw: this.state.bookingdateraw, Status: this.state.status });
            }
            else if(this.state.paymenttype == PaymentIntEnum.CreditCard)
            {
                this.props.navigation.navigate('CreditCard', {Point: this.state.point, BookingCode: this.state.bookingcode,
                    BookingDate: this.state.bookingdate, ChargePayment: this.state.chargepayment, 
                    TransType: this.state.transtype, PaymentType: PaymentIntEnum.CreditCard, Total: this.state.total,
                    ContainMandiri: this.state.containmandiri, MandiriCard: this.state.mandiricard,
                    TransDate: this.state.transdate });
            }
            else if(this.state.paymenttype == PaymentIntEnum.FullPoint)
            {
                this.props.navigation.navigate('CreditCard3DS', {Point: this.state.point, BookingCode: this.state.bookingcode,
                    BookingDate: this.state.bookingdate, ChargePayment: this.state.chargepayment, 
                    TransType: this.state.transtype, PaymentType: PaymentIntEnum.FullPoint, Total: this.state.total,
                    ContainMandiri: this.state.containmandiri, MandiriCard: this.state.mandiricard,
                    TransDate: this.state.transdate,
                    NameOnCard: 'Any Name', CardNumber: '4811111111111114', CardMonth: '05', CardYear: '2020', CardCVV: '123'});
            }
        }

        return (
            <SafeAreaView style={styles.safeAreaView}>
                <ScrollView style={styles.containerScroll}>
                    <View style={styles.container}>
                        <Text style={styles.textKet} >{Strings.TermsPulsa.text1}</Text>
                        <View style={{backgroundColor: '#ffffff', paddingLeft: 20, paddingRight: 20, paddingTop: 10, paddingBottom: 10,}}>
                            <View style={{flex: 1, flexDirection: 'row', }}>
                                <Text style={styles.textLeft2}>{'1 '}</Text>
                                <Text style={styles.textRight2}>{Strings.TermsPulsa.text2}</Text>
                            </View>
                            <View style={{flex: 1, flexDirection: 'row', }}>
                                <Text style={styles.textLeft2}>{'2 '}</Text>
                                <Text style={styles.textRight2}>{Strings.TermsPulsa.text3}</Text>
                            </View>
                            <View style={{flex: 1, flexDirection: 'row', }}>
                                <Text style={styles.textLeft2}>{'3 '}</Text>
                                <Text style={styles.textRight2}>{Strings.TermsPulsa.text4}</Text>
                            </View>
                        </View>
                        <View style={{backgroundColor: '#ffffff', paddingLeft: 20, paddingRight: 20, paddingTop: 10, paddingBottom: 10,}}>
                            <View style={{flex: 1, flexDirection: 'column', alignItems: 'center', }}>
                                <Text style={styles.text}>{Strings.TermsPulsa.text5}</Text>
                                <Text style={styles.textLink}>{Strings.TermsPulsa.text6}</Text>
                            </View>
                        </View>
                        <Button 
                            onPress= { gotoContinue }
                            buttonStyle={{backgroundColor: Colors.button, borderRadius: 5, marginTop: 10, marginLeft: 5, marginRight: 5, marginBottom: 20, height: 50, }}
                            title={Strings.TermsPulsa.continuebutton}/>
                    </View>
                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({

    safeAreaView: {
        flex: 1,
    },

    containerScroll:
    {
        flex: 1,
        backgroundColor: '#efefef',
    },

    container: {
        flex: 1,
        flexDirection: 'column',
    },

    text: {
        fontSize: 14,
        marginLeft: 10,
        color: Colors.text,
    },

    textLink: {
        fontSize: 14,
        marginLeft: 10,
        color: Colors.text,
        textDecorationLine: 'underline',
    },

    textLeft2: {
        fontSize: 14,
        marginTop: 5,
        color: Colors.text,
        width: 10,
    },

    textRight2: {
        flex: 1,
        fontSize: 14,
        marginTop: 5,
        color: Colors.text,
    },

    textKet: {
        fontSize: 18,
        marginTop: 5,
        marginLeft: 15,
        marginBottom: 5,
        marginRight: 15,
        color: Colors.text,
        fontWeight: 'bold',
    },
});
