import React from 'react';
import { 
    StyleSheet, 
    Image,
    View,
    Text,
    Dimensions,
    AsyncStorage,
    TouchableOpacity,
    Platform,
} from 'react-native';
import Colors from '../../constants/Colors';
import { Card, } from 'react-native-material-cards';
import { Divider, Button } from 'react-native-elements';
import { SinglePickerMaterialDialog } from 'react-native-material-dialog';
import Utils from '../Utils';
import BpkTextInput from 'react-native-bpk-component-text-input';
import Enums, {PaymentIntEnum, PlatformIntEnum, TransactionTypeIntEnum, } from '../../constants/Enums';
import TransactionEventModel from '../../Model/TransactionEventModel';
import TransactionEventDetailModel from '../../Model/TransactionEventDetailModel';
import { postCreateEvent,  } from '../../API/TransactionService';

const dimensions = Dimensions.get('window');
const imageWidth = dimensions.width - 10;
const imageHeight = Math.round(dimensions.width / 2);

export default class ListEventPriceRowScreen extends React.Component {
    
    static navigationOptions = {
        title: '',
        header: null
    };

    constructor(props){
        super(props);

        this.state = {
            isLoading: true,
            dialogvisible: false,
            playerPicker: false,
            booknowbutton: 'Book Now',
            playerSelectedItem: undefined,
            player: '1',
            playerArr: [ "1", "2", "3", "4", "5", "6", "7", "8" ],
            inputname: '',
            username: '',
        };
    }

    createTrans = async () => {
        this.setState({ username : await AsyncStorage.getItem('Username')});
        var platform = Platform.OS === 'ios' ? this.props.eventOption.iOS : this.props.eventOption.Android;
        if(Utils.getMaintenanceOption(this.props.maintenanceOption)){
            Utils.alertBackToMain(this.props.maintenanceOption.Note);
        }
        else if(!platform) {
            Utils.errAlert("QAccess Alert", this.props.eventOption.Note);
        }
        else 
        {
            var platformMobile = Platform.OS === 'ios' ? PlatformIntEnum.IOs : PlatformIntEnum.Android;
            var contactname = this.state.username;
            var contactnamebool = true;
            if(this.props.agent){
                if(this.state.inputname == ''){
                    Utils.errAlert("Error Message", 'Contact name can not empty.');
                    contactnamebool = false;
                }
                else{
                    contactname = this.state.inputname;
                }
            }
            if(contactnamebool){
                var arrTransDetailModel = [];
                var i;
                for(i = 0; i < Number.parseInt(this.state.player); i++) {
    
                    var _namaPemain = contactname;
                    var _tipeMember = this.props.usertype;
                    var _idpricing = this.props.idpricing;

                    arrTransDetailModel.push(new TransactionEventDetailModel('', '', _tipeMember, '', _namaPemain, _idpricing));
                }
    
                var transactionModel = new TransactionEventModel(this.props.idevent, this.props.username, PaymentIntEnum.NotSpecified, null, platformMobile, arrTransDetailModel);
    
                postCreateEvent(transactionModel).then(async (jsonTrans) =>
                {
                    await AsyncStorage.setItem("BookingCode", jsonTrans);
                    this.props.property.navigation.navigate('Payment', {TransType: TransactionTypeIntEnum.Event});
                }); 
            }   
        }
    }

    render() {

        const booknow = () => {
            if(this.state.booknowbutton === "Book Now")
            {
                this.setState({
                    booknowbutton: "Cancel",
                    dialogvisible: true,
                });
            }
            else{
                this.setState({
                    booknowbutton: "Book Now",
                    dialogvisible: false,
                });
            }
        }

        const gotopay = () => {
            this.createTrans();
        }

        return (
            <View >
                <View style={styles.cardList} >
                    <View>
                        <Text style={styles.textTitle} >{this.props.title}</Text>
                        <Text style={styles.text} >{'Valid only on selected date visit date'}</Text>
                        <View style={{height: 40, borderRadius: 5, marginTop: 5, justifyContent: 'center', alignContent: 'center', flexDirection: 'row' }}>
                            <Image style={{width: 20, height: 20, marginRight: 5, }} 
                                source={require('../../assets/images/ic_timer.png')}/>
                            <Text style={styles.text} >{Enums.EventSessionEnum(this.props.session)}</Text>
                        </View>
                        <Divider style={{ backgroundColor: '#414141', height: 1, width: '97%', marginTop: 5, }} />
                        <View style={{flex: 1, paddingLeft: 5, marginTop: 5, flexDirection: 'row',}}>
                            <View style={{ flexDirection: 'column', }}>
                                <Text style={styles.textTitle} >{'Total Price'}</Text>
                                <Text style={styles.text} >{"Rp. " + this.props.total}</Text>
                                <Text style={styles.text} >{this.props.ket2}</Text>
                            </View>
                            <View style={{flex: 1, height: 50, flexDirection: 'column', backgroundColor: '#93b843', marginRight: 10, marginLeft: 10, }}>
                                <TouchableOpacity style={{height: 50, justifyContent: 'center', alignItems: 'center' }}
                                            onPress={ booknow } >
                                    <Text style={{fontSize: 16, color: '#fff', fontWeight: 'bold',}} >{this.state.booknowbutton}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        {
                            Utils.renderIf(this.state.dialogvisible,
                                <View style={{flex: 1, marginTop: 5, justifyContent: 'center', alignItems: 'center', 
                                    flexDirection: 'column',}}>
                                    <Divider style={{ backgroundColor: '#414141', height: 1, width: '97%', marginTop: 5, }} />
                                    {
                                        Utils.renderIf(this.props.agent,
                                            <View style={{width: '97%', marginTop: 5 }}>
                                                <BpkTextInput 
                                                    label={'Input Name'}
                                                    onChangeText={(text) => this.setState({inputname: text})}
                                                    value={this.state.inputname}
                                                    style={{marginBottom: 3, marginTop: 5, }}/>
                                            </View>
                                        )
                                    }
                                    <View style={{flex: 1, flexDirection: 'row', justifyContent:'center', alignItems: 'center', backgroundColor: '#fff',
                                        marginTop: 5, marginBottom: 10, }}>
                                        <TouchableOpacity onPress={() => this.setState({ playerPicker: true }) } 
                                            style={{flex: 1, flexDirection: 'row', justifyContent:'center', alignItems: 'center', backgroundColor: '#fff', }} >
                                            <Text style={styles.text} >{'Ticket : '}</Text>
                                            <Text style={styles.text} >{this.state.playerSelectedItem === undefined
                                                ? '1' : `${
                                                    this.state.playerSelectedItem.label
                                                }`}</Text>
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{width: 150, height: 50, flexDirection: 'column', backgroundColor: '#93b843', borderRadius: 5, }}>
                                        <TouchableOpacity style={{width: 150, height: 50, justifyContent: 'center', alignItems: 'center' }}
                                                    onPress={ gotopay } >
                                            <Text style={{fontSize: 16, color: '#fff',}} >{'CONTINUE'}</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            )
                        }
                    </View>
                </View>
                <SinglePickerMaterialDialog
                    title={'Player'}
                    scrolled={true}
                    items={this.state.playerArr.map((row, index) => ({ value: index, label: row }))}
                    visible={this.state.playerPicker}
                    selectedItem={this.state.playerSelectedItem}
                    onCancel={() => this.setState({ playerPicker: false })}
                    onOk={result => {
                        this.setState({ playerPicker: false });
                        this.setState({
                            playerSelectedItem: result.selectedItem,
                            player: result.selectedItem.label,
                        });
                    }}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({

    cardList: {
        backgroundColor: Colors.backgroundColor,
        width: imageWidth,
        padding: 10,
        elevation: 2,
        borderRadius: 5,
    },

    text: {
        flex: 1,
        fontSize: 16,
        color: Colors.text,
        textAlign: 'left',
    },

    textTitle: {
        flex: 1,
        fontSize: 17,
        color: Colors.text,
        fontWeight: 'bold',
    },
});