import React from 'react';
import { ScrollView, 
    StyleSheet, 
    WebView,
    AsyncStorage,
    SafeAreaView 
} from 'react-native';
import clsSetting from '../../API/clsSetting';
import { TransactionTypeIntEnum } from '../../constants/Enums';

export default class BankTransfer3DSScreen extends React.Component {
    static navigationOptions = {
        title: '',
        header: null,
    };

    constructor(props){
        super(props);

        this.params = this.props.navigation.state.params;

        this.state = {
            transtype: this.params.TransType,
            point: this.params.Point,
            paymenttype: this.params.PaymentType,
            chargepayment: this.params.ChargePayment,
            bookingcode: this.params.BookingCode,
            bookingdate: this.params.BookingDate,
            total: this.params.Total,
            bank: this.params.Bank,
            url: '',
        };
    }

    async _onNavigationChange(state) {
        if (state.url.includes('Pending')) {
            await AsyncStorage.setItem("TransType", this.state.transtype + "");
            await AsyncStorage.setItem("BookingCode", this.state.bookingcode);
            this.props.navigation.navigate('PendingBank');
        }
        else if (state.url.includes('Failed') || state.url.includes('Error')) {
            this.props.navigation.goBack();
            this.params.onSelect({point: this.state.point, bookingcode: this.state.bookingcode, 
                bookingdate: this.state.bookingdate, chargepayment:  this.state.chargepayment,
                transtype: this.state.transtype, paymenttype: this.state.paymenttype,
                total: this.state.total, bank: this.state.bank});
        }
    }

    componentDidMount() {
        if(this.state.transtype == TransactionTypeIntEnum.Reservation || this.state.transtype == TransactionTypeIntEnum.RegistrationQ){
            this.setState({
                url: clsSetting.BankTransferPaymentUrl
                    + "?ApiSecretKey=" + clsSetting.ApiSecretKey
                    + "&OrderId=" + this.state.bookingcode
                    + "&Point=" + this.state.point
                    + "&Bank=" + this.state.bank,
            });
        }
        else if(this.state.transtype == TransactionTypeIntEnum.Event || this.state.transtype == TransactionTypeIntEnum.MobilePulsaPhonePrepaid){
            this.setState({
                url: clsSetting.BankTransferPaymentUrl
                    + "?ApiSecretKey=" + clsSetting.ApiSecretKey
                    + "&OrderId=" + this.state.bookingcode
                    + "&ChargePaymentFee=" + this.state.chargepayment
                    + "&Point=" + this.state.point
                    + "&Bank=" + this.state.bank,
            });
        }
    }

    render() {

        return (
            <SafeAreaView style={styles.safeAreaView}>
                <WebView onNavigationStateChange={this._onNavigationChange.bind(this)}
                    style={{flex: 1, }}
                    source={{uri: this.state.url}} />
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },

    safeAreaView: {
        flex: 1
    },
});
