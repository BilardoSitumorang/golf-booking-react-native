import clsSetting from "../API/clsSetting";

export default class RegisterQViewModel
{
    constructor(_fullname, _cardname, _idnumber, _birthplace, _birthdate, _address,
        _village, _subdistrict, _city, _zipcode, _phone, _fax, _mobilephone, _bbpin, 
        _email, _companytype, _companyname, _companyaddress, _companyphone, _companyfax,
        _citizenship, _citizenshipthumbnail, _photo, _photothumbnail, 
        _payment, _delivery, _status, _joindate, _idmembercard, _type, _accountnumber,
        _emailsubmitter, _timezone, _platform )
    {
        this.FullName = _fullname;
        this.CardName = _cardname;
        this.IdNumber = _idnumber;
        this.BirthPlace = _birthplace;
        this.BirthDate = _birthdate;
        this.Address = _address;
        this.Village = _village;
        this.SubDistrict = _subdistrict;
        this.City = _city;
        this.ZipCode = _zipcode;
        this.Phone = _phone;
        this.Fax = _fax;
        this.MobilePhone = _mobilephone;
        this.BBPin = _bbpin;
        this.Email = _email;
        this.CompanyType = _companytype;
        this.CompanyName = _companyname;
        this.CompaynAddress = _companyaddress;
        this.CompanyPhone = _companyphone;
        this.CompanyFax = _companyfax;
        this.Citizenship = _citizenship;
        this.CitizenshipThumbnail = _citizenshipthumbnail;
        this.Photo = _photo;
        this.PhotoThumbnail = this._photothumbnail;

        this.Payment = _payment;
        this.Delivery = _delivery;
        this.Status = _status;
        this.JoinDate = _joindate;
        this.IdMemberCard = _idmembercard;
        this.Type = _type;
        this.AccountNumber = _accountnumber;
        this.EmailSubmitter = _emailsubmitter;
        this.Timezone = _timezone;
        this.Platform = _platform;
        this.ApiSecretKey = clsSetting.ApiSecretKey;
    }
}