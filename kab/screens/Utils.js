import React from 'react';
import Moment from 'moment';
import {
    Alert,
    Platform,
} from 'react-native';
import clsSetting from '../API/clsSetting';
import { getDoubleToStringDate,  } from '../API/SettingService';
import { TipePemainIntEnum, TransactionTypeIntEnum, StatusIntEnum, KartuIntEnum } from '../constants/Enums';

export default class Utils extends React.Component
{
    static errAlert = (_title, _msg) => {
        return Alert.alert(
            _title,
            _msg,
            [
              {text: 'OK', onPress: () => console.log('OK Pressed') },
            ],
            { cancelable: false }
        )
    }

    static alertBackToMain = (_msg, _props) => {
        return Alert.alert(
            "QAccess Alert",
            _msg,
            [
              {text: 'OK', onPress: () => _props.navigation.popToTop() },
            ],
            { cancelable: false }
        )
    }

    static dialogAlert = (_title, _msg) => {
        return Alert.alert(
            'Alert Title',
            'My Alert Msg',
            [
              {text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
              {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
              {text: 'OK', onPress: () => console.log('OK Pressed')},
            ],
            { cancelable: false }
        )
    }

    static currencyfunc = (_number) => 
    {
        return _number.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    }

    static getMaintenanceOption = (_option) => 
    {
        getDoubleToStringDate(_option.FromDate).then((jsonFrom) => {
            
            getDoubleToStringDate(_option.ToDate).then((jsonTo) => {
                
                var platform = (Platform.OS === 'ios') ? _option.iOS : _option.Android;
                return (platform && (Date.now() >= jsonFrom) && (Date.now() <= jsonTo)) ? true : false;
            });
        });
    }

    static renderIf = (condition, content) =>
    {
        if(condition){
            return content;
        }
        else{
            return null;
        }
    }

    static getImageProvider = (_phone) =>
    {
        var numberProvider = _phone.substring(0, 4);
        var provider = '';
        if(numberProvider === "0811")
        {
            provider = require("../assets/images/simpati.png");
        }
        else if (numberProvider === "0812" || numberProvider === "0813" || numberProvider === "0821" || numberProvider === "0822")
        {
            provider = require("../assets/images/simpati.png");
        }
        else if(numberProvider === "0852" || numberProvider === "0853" || numberProvider ===  "0823" || numberProvider === "0851")
        {
            provider = require("../assets/images/kartuas.png");
        }
        else if (numberProvider === "0814" )
        {
            provider = require("../assets/images/mentari.png");
        }
        else if (numberProvider === "0815" || numberProvider === "0816" || numberProvider === "0855" || numberProvider === "0858")
        {
            provider = require("../assets/images/mentari.png");
        }
        else if (numberProvider === "0856" || numberProvider === "0857")
        {
            provider = require("../assets/images/im3.png");
        }
        else if (numberProvider === "0817" || numberProvider === "0818" || numberProvider === "0819" || numberProvider === "0859"
                || numberProvider === "0877" || numberProvider === "0878")
        {
            provider = require("../assets/images/xl.png");
        }
        else if (numberProvider === "0838" || numberProvider === "0831" || numberProvider === "0832" || numberProvider === "0833")
        {
            provider = require("../assets/images/axis.png");
        }
        else if (numberProvider === "0894"  || numberProvider === "0895" || numberProvider === "0896" || numberProvider === "0897" || numberProvider === "0898" 
                    || numberProvider === "0899")
        {
            provider = require("../assets/images/tri.png");
        }
        else if (numberProvider === "0881" || numberProvider === "0882" || numberProvider === "0883" || numberProvider === "0884"
                    || numberProvider === "0885" || numberProvider === "0886" || numberProvider === "0887" || numberProvider === "0888"
                || numberProvider === "0889")
        {
            provider = require("../assets/images/smartfren.png");
        }
        else
        {
            provider = require("../assets/images/simpati.png");
        }
        return provider;
    }

    static getImageProvider2 = (_phone) =>
    {
        _tempphone = _phone;
        var firstchar = _phone.slice(0, 1);
        if(firstchar != '0')
        {

            _tempphone = "0" + "" + _phone;
        }
        var numberProvider = _tempphone.substring(0, 4);
        var provider = '';
        if(numberProvider === "0811")
        {
            provider = require("../assets/images/simpati.png");
        }
        else if (numberProvider === "0812" || numberProvider === "0813" || numberProvider === "0821" || numberProvider === "0822")
        {
            provider = require("../assets/images/simpati.png");
        }
        else if(numberProvider === "0852" || numberProvider === "0853" || numberProvider ===  "0823" || numberProvider === "0851")
        {
            provider = require("../assets/images/kartuas.png");
        }
        else if (numberProvider === "0814" )
        {
            provider = require("../assets/images/mentari.png");
        }
        else if (numberProvider === "0815" || numberProvider === "0816" || numberProvider === "0855" || numberProvider === "0858")
        {
            provider = require("../assets/images/mentari.png");
        }
        else if (numberProvider === "0856" || numberProvider === "0857")
        {
            provider = require("../assets/images/im3.png");
        }
        else if (numberProvider === "0817" || numberProvider === "0818" || numberProvider === "0819" || numberProvider === "0859"
                || numberProvider === "0877" || numberProvider === "0878")
        {
            provider = require("../assets/images/xl.png");
        }
        else if (numberProvider === "0838" || numberProvider === "0831" || numberProvider === "0832" || numberProvider === "0833")
        {
            provider = require("../assets/images/axis.png");
        }
        else if (numberProvider === "0895" || numberProvider === "0896" || numberProvider === "0897" || numberProvider === "0898" 
                    || numberProvider === "0899")
        {
            provider = require("../assets/images/tri.png");
        }
        else if (numberProvider === "0881" || numberProvider === "0882" || numberProvider === "0883" || numberProvider === "0884"
                    || numberProvider === "0885" || numberProvider === "0886" || numberProvider === "0887" || numberProvider === "0888"
                || numberProvider === "0889")
        {
            provider = require("../assets/images/smartfren.png");
        }
        else
        {
            provider = require("../assets/images/simpati.png");
        }
        return provider;
    }

    static getTotalBooking = (_json) =>
    {
        var Enumerable = require('./linq');
        var publish = Enumerable.from(_json).count(x => x.TipeMember == TipePemainIntEnum.Publish);
        var visitor = Enumerable.from(_json).count(x => x.TipeMember == TipePemainIntEnum.Visitor);
        var club = Enumerable.from(_json).count(x => x.TipeMember == TipePemainIntEnum.Club);
        var qmember = Enumerable.from(_json).count(x => x.TipeMember == TipePemainIntEnum.QMember);

        var total = 0;

        if(publish > 0)
        {
            var publishdata= Enumerable.from(_json).where(x => x.TipeMember == TipePemainIntEnum.Publish).firstOrDefault();
            var publishrate= publishdata.Harga;
            total = total + (publish * publishrate);
        }

        if(visitor > 0)
        {
            var visitordata= Enumerable.from(_json).where(x => x.TipeMember == TipePemainIntEnum.Visitor).firstOrDefault();
            var visitorrate= visitordata.Harga;
            total = total + (visitor * visitorrate);
        }

        if(club > 0)
        {
            var clubdata= Enumerable.from(_json).where(x => x.TipeMember == TipePemainIntEnum.Club).firstOrDefault();
            var clubrate= clubdata.Harga;
            total = total + (club * clubrate);
        }

        if(qmember > 0)
        {
            var qmemberdata= Enumerable.from(_json).where(x => x.TipeMember == TipePemainIntEnum.QMember).firstOrDefault();
            var qmemberrate= qmemberdata.Harga;
            total = total + (qmember * qmemberrate);
        }

        return total;
    }

    static convertMinuteIntoString = (_minute) =>
    {
        var minutestr = '';
        console.log('convert ',_minute);
        if(_minute != NaN){
            if(_minute / 60 <= 1)
            {
                minutestr = _minute + " minutes";
            }
            else
            {
                var hourRemain = Math.floor(_minute / 60);
                minutestr = hourRemain+ " hours";
            }
        }
        
        return minutestr;
    }

    static getTotalMinuteRemaining = (_totalhours, _transtype, _status) =>
    {
        console.log('total minute', _totalhours);
        var hours = _totalhours; // 60;
        if (_transtype == TransactionTypeIntEnum.RegistrationQ && _status != StatusIntEnum.Pending)
                return 1440;
        else if (_transtype == TransactionTypeIntEnum.Event)
        {
            if (hours > 24)
                return 1440;
            else if (hours > 3 && hours <= 24)
                return 60;
            else
                return 30;
        }
        else
        {
            if (hours > 24)
                return 90;
            else
                return 30;
        }
    }

    static getCountDays = (_tglmain, _tglbuat) =>
    {
        var tgl = _tglmain - _tglbuat; // milliseconds
        var diffDays = Math.floor(tgl / 86400000); // days
        var diffDaysHour = diffDays * 24;
        var diffHrs = Math.floor((tgl % 86400000) / 3600000); // hours
        console.log('day = hour', diffDays + "=" + diffHrs + "=>" + diffDaysHour);
        return diffDaysHour + diffHrs;
    }

    static getTotalMinutes = (_tglmain, _tglbuat) =>
    {
        var diffMs = (_tglmain - _tglbuat); // milliseconds
        var diffDays = Math.floor(diffMs / 86400000); // days
        var diffHrs = Math.floor((diffMs % 86400000) / 3600000); // hours
        var diffHrsMins = diffHrs * 60;
        var diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000); // minutes
        //var minutes = Math.round(_tgl/(1000*60*60*24));
        
        console.log('get total minute', diffHrsMins + diffMins);
        return diffHrsMins + diffMins;
    }

    static getTotalHours = (_tgl) =>
    {
        var diffHrs = Math.floor((_tgl % 86400000) / 3600000); // hours
        return diffHrs;
    }

    static getLocalTimezone = (_tglbuat) =>
    {
        var dt = new Date(_tglbuat);
        dt.setHours(dt.getHours() + (-7));
        var currentOffset = (new Date()).getTimezoneOffset() / 60;
        dt.setHours(dt.getHours() - currentOffset);
        return dt;
    }

    static getImageMyCard = (_type) =>
    {
        var img = '';
        if(_type === TipePemainIntEnum.Agent)
        {
            img = require('../assets/images/Agent2.png');
        }
        else if (_type == TipePemainIntEnum.Publish || _type == TipePemainIntEnum.Visitor)
        {
            img = require('../assets/images/Visitor2.png');
        }
        else{
            img = require('../assets/images/Visitor2.png');
        }
        return img;
    }

    static getImageKartu = (_kartu) =>
    {
        var img = '';
        if(_kartu === KartuIntEnum.Premium)
        {
            img = require('../assets/images/premium.png');
        }
        else if (_kartu == KartuIntEnum.Easy)
        {
            img = require('../assets/images/easy.png');
        }
        else if(_kartu == KartuIntEnum.Corporate)
        {
            img = require('../assets/images/corporate.png');
        }
        else if(_kartu == KartuIntEnum.Elder)
        {
            img = require('../assets/images/elder.png');
        }
        else
        {
            img = require('../assets/images/premium.png');
        }
        return img;
    }
}