import React from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    AsyncStorage,
    Platform,
    FlatList,
    ListView,
    View,
    ScrollView,
    ActivityIndicator,
    SafeAreaView,
    RefreshControl,
} from 'react-native';
import OrdersRowScreen from './ListRow/OrdersRowScreen';
import { getBookingHistory } from '../API/TransactionService';
import { getDoubleToStringDate,  } from '../API/SettingService';
import { TransactionTypeIntEnum, StatusIntEnum, PaymentIntEnum, TipePemainIntEnum, OptionIntEnum, } from '../constants/Enums';
import Utils from './Utils';
import moment from 'moment';
import Colors from '../constants/Colors';
import { Button } from 'react-native-elements';

export default class OrdersHistoryScreen extends React.Component {
    static navigationOptions = {
        title: 'Orders History',
        headerTintColor: '#ffffff', 
        headerStyle: {
            backgroundColor: '#93b843',
        },
    };

    constructor(props){
        super(props);

        this.params = this.props.navigation.state.params;

        this.state = {
            isLoading: true,
            refreshing: false,
            username: '',
            type: this.params.type,
        }
    }

    async componentDidMount() {
        this.setState({ username : await AsyncStorage.getItem('Username')});
        getBookingHistory(this.state.username, this.state.type).then((json) =>
        {
            let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
            this.setState({
                isLoading: false,
                dataSource: ds.cloneWithRows(json),
            });

        })
        .catch((error) => {
            console.error(error);
            this.setState({isLoading: false,});
        });
    }

    async GetItem (row) {
        //Selected Row List
        
        if (row.Status == StatusIntEnum.Paid && row.Type == TransactionTypeIntEnum.MobilePulsaPhonePrepaid)
        { //Pulsa
            await AsyncStorage.setItem("BookingCode", row.Kode);
            this.props.navigation.navigate('TicketPulsa');
        }
        else if (row.Status == StatusIntEnum.Paid && row.Type == TransactionTypeIntEnum.Event)
        {
            await AsyncStorage.setItem("BookingCode", row.Kode);
            this.props.navigation.navigate('TicketEvent');
        }
        else if (row.Status == StatusIntEnum.Paid && row.Type == TransactionTypeIntEnum.RegistrationQ)
        {
            await AsyncStorage.setItem("BookingCode", row.Kode);
            this.props.navigation.navigate('TicketRegistration');
        }
        else if (row.Status == StatusIntEnum.Paid || row.Status == StatusIntEnum.Verifying)
        {
            await AsyncStorage.setItem("BookingCode", row.Kode);
            this.props.navigation.navigate('TicketReservation');
        }
        else if (row.Type == TransactionTypeIntEnum.MobilePulsaPhonePrepaid && row.Status == StatusIntEnum.Pending)
        {
            await AsyncStorage.setItem("BookingCode", row.Kode);
            await AsyncStorage.setItem("TransType", this.state.type + "");
            this.props.navigation.navigate('PendingBank');
        }
        else if (row.Type == TransactionTypeIntEnum.RegistrationQ && row.Status == StatusIntEnum.Pending)
        {
            await AsyncStorage.setItem("BookingCode", row.Kode);
            await AsyncStorage.setItem("TransType", this.state.type + "");
            this.props.navigation.navigate('PendingBank');
        }
        else if (row.Type == TransactionTypeIntEnum.Event && row.Status == StatusIntEnum.Pending)
        {
            await AsyncStorage.setItem("BookingCode", row.Kode);
            await AsyncStorage.setItem("TransType", this.state.type + "");
            this.props.navigation.navigate('PendingBank');
        }
        else if (row.Status == StatusIntEnum.Book || row.Status == StatusIntEnum.TimeOut || row.Payment == PaymentIntEnum.BankTransfer || row.Status == StatusIntEnum.Pending)
        {
            var Enumerable = require('./linq');
            //var total = Utils.getTotalBooking(row.PemainViewModel);
            var containMandiri = false;
            var countClub = Enumerable.from(row.PemainViewModel).count(x => x.TipeMember == TipePemainIntEnum.Club);
            if (countClub > 0)
            {
                containMandiri = true;
            }

            getDoubleToStringDate(row.Tanggal).then((jsonDate) =>
            {
                var date1 = moment(Date.parse(jsonDate.result)).format('YYYY MM DD');
                getDoubleToStringDate(row.TanggalBuat).then(async (jsonDate2) =>
                {
                    getDoubleToStringDate(row.EventDate).then(async (jsonDate3) =>
                    {
                        var date2 = moment(Date.parse(jsonDate2.result)).format('YYYY MM DD');
                        var totalminuteremain = Utils.getTotalMinuteRemaining(Utils.getCountDays(Date.parse(jsonDate.result),
                            Date.parse(jsonDate2.result)), row.Type, row.Status);
                        var timeremain= Utils.getTotalMinutes(Date.now(), Utils.getLocalTimezone(Date.parse(jsonDate2.result)));

                        if (row.Type == TransactionTypeIntEnum.Event){
                            var timeEvent = row.TimeStart.split(' ');
                            var hour = Number.parseInt(timeEvent[0].split(':')[0]);
                            var minute = Number.parseInt(timeEvent[0].split(':')[1]);
                            if(timeEvent[1] == "PM"){
                                hour = Number.parseInt(hour) + 12;
                            }
                            
                            var date3 = moment(Date.parse(jsonDate3.result)).format('YYYY MM DD');
                            var currentOffset = (new Date()).getTimezoneOffset() / 60;
                            var dateevent = date3.split(' ');
                            var newdate = moment(new Date(dateevent[0], dateevent[1] - 1, dateevent[2], hour, minute)).valueOf();
                            totalminuteremain = Utils.getTotalMinuteRemaining(Utils.getCountDays(newdate, 
                                Date.parse(jsonDate2.result)), row.Type, row.Status);
                            
                        }

                        if (row.Status == StatusIntEnum.TimeOut || (timeremain > totalminuteremain && row.Status == StatusIntEnum.Book) || (timeremain > totalminuteremain && row.Status == StatusIntEnum.Pending))
                        {
                            this.props.nav.navigate('CustomerCall');
                        }
                        else if (row.Type == TransactionTypeIntEnum.RegistrationQ && row.Status == StatusIntEnum.Book)
                        {
                            var platformRegister = Platform.OS === 'ios' ? this.state.registrationOption.iOS : this.state.registrationOption.Android;
                            if(Utils.getMaintenanceOption(this.state.maintenanceOption)){
                                Utils.alertBackToMain(this.state.maintenanceOption.Note);
                            }
                            else if(!platformRegister) {
                                Utils.errAlert("QAccess Alert", this.state.registrationOption.Note);
                            }
                            else
                            {
                                await AsyncStorage.setItem("BookingCode", row.Kode);
                                this.props.nav.navigate('Payment', {TransType: this.state.type});
                            }
                        }
                        else if (row.Type == TransactionTypeIntEnum.MobilePulsaPhonePrepaid && row.Status == StatusIntEnum.Book)
                        {
                            var platformPulsa = Platform.OS === 'ios' ? this.state.pulsaOption.iOS : this.state.pulsaOption.Android;
                            if(Utils.getMaintenanceOption(this.state.maintenanceOption)){
                                Utils.alertBackToMain(this.state.maintenanceOption.Note);
                            }
                            else if(!platformPulsa) {
                                Utils.errAlert("QAccess Alert", this.state.pulsaOption.Note);
                            }
                            else
                            {
                                await AsyncStorage.setItem("BookingCode", row.Kode);
                                this.props.navigation.navigate('Payment', {TransType: this.state.type});
                            }
                        }
                        else if (row.Type == TransactionTypeIntEnum.Event && row.Status == StatusIntEnum.Book)
                        {
                            var platformEvent = Platform.OS === 'ios' ? this.state.eventOption.iOS : this.state.eventOption.Android;
                            if(Utils.getMaintenanceOption(this.state.maintenanceOption)){
                                Utils.alertBackToMain(this.state.maintenanceOption.Note);
                            }
                            else if(!platformEvent) {
                                Utils.errAlert("QAccess Alert", this.state.eventOption.Note);
                            }
                            else
                            {
                                await AsyncStorage.setItem("BookingCode", row.Kode);
                                this.props.navigation.navigate('Payment', {TransType: this.state.type});
                            }
                        }
                        else if (row.Status == StatusIntEnum.Pending)
                        {
                            await AsyncStorage.setItem("BookingCode", row.Kode);
                            await AsyncStorage.setItem("TransType", this.state.type + "");
                            this.props.navigation.navigate('PendingBank');
                        }
                        else
                        {
                            var platformReservation = Platform.OS === 'ios' ? this.state.reservationOption.iOS : this.state.reservationOption.Android;
                            if(Utils.getMaintenanceOption(this.state.maintenanceOption)){
                                Utils.alertBackToMain(this.state.maintenanceOption.Note);
                            }
                            else if(!platformReservation) {
                                Utils.errAlert("QAccess Alert", this.state.reservationOption.Note);
                            }
                            else
                            {
                                await AsyncStorage.setItem("BookingCode", row.Kode);
                                this.props.navigation.navigate('Payment', {TransType: this.state.type, ContainMandiri: containMandiri,});
                            }
                        }
                    })
                })
            })
        }
    }

    ListViewItemSeparator = () => {
        return (
            <View style={{ height: 2, width: "100%", backgroundColor: "#333", }} />
        );
    }

    render() {
        if (this.state.isLoading) {
            return (
                <View style={{flex: 1, paddingTop: 20}}>
                    <ActivityIndicator />
                </View>
            );
        }

        return (
            <SafeAreaView style={styles.safeAreaView}>
                <View style={styles.container}>
                    <ListView
                        dataSource={this.state.dataSource}
                        renderSeparator= {this.ListViewItemSeparator}
                        renderRow={(rowData) =>
                            <TouchableOpacity 
                                onPress={ this.GetItem.bind(this, rowData) } >
                                <OrdersRowScreen 
                                    row={rowData}
                                    code={rowData.Kode}
                                    teetime={rowData.TeeTime}
                                    status={rowData.Status}
                                    course={this.state.type === TransactionTypeIntEnum.Reservation ? rowData.NamaLapangan : 
                                        this.state.type === TransactionTypeIntEnum.RegistrationQ ? rowData.RegisteredQName === "" ? "-" : 
                                        rowData.RegisteredQName : rowData.EventName}
                                    pulsa={rowData.MobilePulsaProduct}
                                    type={this.state.type}/>
                            </TouchableOpacity>
                        }
                    />
                </View>
            </SafeAreaView>
        );
    }   
}

const styles = StyleSheet.create({
    safeAreaView: {
        flex: 1
    },

    containerScroll: {
        flex: 1,
        backgroundColor: '#f2f2f2',
    },

    container: {
        flex: 1,
        flexDirection: 'column',
    },
});
