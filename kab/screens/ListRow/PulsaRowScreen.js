import React from 'react';
import {
    Image,
    ScrollView,
    StyleSheet,
    SafeAreaView,
    TouchableOpacity,
    Text,
    View,
} from 'react-native';
import Colors from '../../constants/Colors';

export default class PulsaRowScreen extends React.Component {
    static navigationOptions = {
        title: '',
        header: null
    };
    
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.card}>
                    <View style={{flex: 2, flexDirection: 'column', }}>
                        <Text style={styles.textNominal}>{this.props.Nominal}</Text>
                        <Text style={styles.textNote}>{this.props.Note}</Text>
                    </View>
                    <View style={{flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center', }}>
                        <Text style={styles.textTotal}>{this.props.Total}</Text>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({

    safeAreaView: {
        flex: 1,
    },

    containerScroll:
    {
        flex: 1,
        backgroundColor: '#efefef',
    },

    container: {
        flex: 1,
        flexDirection: 'column',
    },

    card: {
        backgroundColor: Colors.backgroundColor,
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 10,
        paddingBottom: 10, 
        marginTop: 5,
    },

    textNote: {
        fontSize: 13,
        marginTop: 5,
        color: Colors.text,
    },

    textNominal: {
        fontSize: 15,
        color: Colors.text,
    },

    textTotal: {
        flex: 1,
        fontSize: 15,
        color: '#93b843',
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
    },
});