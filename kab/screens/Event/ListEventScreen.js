import React from 'react';
import {
    StyleSheet,
    SafeAreaView,
    TouchableOpacity,
    ListView,
    ActivityIndicator,
    View,
} from 'react-native';
import Colors from '../../constants/Colors';
import ListEventRowScreen from '../ListRow/ListEventRowScreen';
import { getEvents2, } from '../../API/EventService';
import { getStringToDoubleDate,  } from '../../API/SettingService';
import moment from 'moment';
import clsSetting from '../../API/clsSetting';

export default class ListEventScreen extends React.Component {
    static navigationOptions = {
        title: 'Event',
        headerTintColor: '#ffffff', 
        headerStyle: {
            backgroundColor: '#93b843'
        },
    };

    constructor(props){
        super(props);
        this.state = {
            isLoading: true,
        }
    }

    componentDidMount() {
        getEvents2(moment(Date.now()).format('YYYY-MM-DD')).then((json) =>
        {
            let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
            this.setState({
                isLoading: false,
                dataSource: ds.cloneWithRows(json),
            });
        })
        .catch((error) => {
            console.error(error);
            this.setState({isLoading: false});
        });
    }

    GetItem (row) {
        //Selected Row List
        this.props.navigation.navigate('DetailEvent', {IdEvent: row.Id, Type: row.Category.Type, });
    }

    ListViewItemSeparator = () => {
        return (
            <View style={{ height: .5, width: "100%", backgroundColor: "#000", }} />
        );
    }

    render() {
        const gotoPulsa = () => {
            this.props.navigation.navigate('Pulsa')
        }

        if (this.state.isLoading) {
            return (
                <View style={{flex: 1, paddingTop: 20}}>
                    <ActivityIndicator />
                </View>
            );
        }

        return (
            <SafeAreaView style={styles.safeAreaView}>
                <View style={styles.container}>
                    <ListView
                        dataSource={this.state.dataSource}
                        renderSeparator= {this.ListViewItemSeparator}
                        renderRow={(rowData) =>
                            <TouchableOpacity 
                                onPress={ this.GetItem.bind(this, rowData) } >
                                <ListEventRowScreen
                                    imageUri={{uri: clsSetting.HomeUrl + rowData.Thumbnail}}
                                    eventName={rowData.Name}/>
                            </TouchableOpacity>
                        }
                    />
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({

    safeAreaView: {
        flex: 1,
    },

    containerScroll:
    {
        flex: 1,
        backgroundColor: '#efefef',
    },

    container: {
        flex: 1,
        flexDirection: 'column',
    },


});