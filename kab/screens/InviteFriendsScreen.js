import React from 'react';
import {
    StyleSheet, 
    AsyncStorage,
    Text, 
    Image,
    View,
    ScrollView,
    SafeAreaView,
    Dimensions,
    ActivityIndicator,
    Share,
} from 'react-native';
import { Button, } from 'react-native-elements';
import Strings from '../constants/Strings';
import Colors from '../constants/Colors';
import { getReferralCode } from '../API/UserService';
import clsSetting from '../API/clsSetting';

const dimensions = Dimensions.get('window');
const imageWidth = dimensions.width;
const imageHeight = Math.round(dimensions.width * 0.5);


export default class InviteFriendsScreen extends React.Component{
    static navigationOptions = {
        title: 'Invite Friends',
        headerTintColor: '#ffffff', 
        headerStyle: {
            backgroundColor: '#93b843',
        },
    };

    constructor(props)
    {
        super(props);

        this.state = {
            isLoading: true,
            username: '',
            url: '',
            title: '',
            body: '',
        };
    }

    async componentDidMount() {
        this.setState({ username : await AsyncStorage.getItem('Username')});
        getReferralCode(this.state.username).then((json) =>
        {
            this.setState({ 
                url: clsSetting.HomeUrl +"Register?Reff=" + json,
                body: "Hi! You've been invited to QAccess by " + this.state.username + ". Let's play golf, with less pay and easier booking. Please click on the following link : " +
                    this.state.url,
                title: "You're invited to QAccess",
            });
            this.setState({ isLoading: false});
        })
        .catch((error) => {
            console.error(error);
            this.setState({ isLoading: false});
        });
    }

    render() {
        if (this.state.isLoading) {
            return (
                <View style={{flex: 1, paddingTop: 20}}>
                    <ActivityIndicator />
                </View>
            );
        }

        const sendReferral = () => {
            Share.share({
                message: this.state.body,
                url: this.state.url,
                title: this.state.title
            }, {
                // Android only:
                dialogTitle: 'Share',
                // iOS only:
                excludedActivityTypes: [
                  'com.apple.UIKit.activity.PostToTwitter'
                ]
            })
        }

        return (
            <SafeAreaView style={styles.safeAreaView}>
                <ScrollView style={styles.container}>
                    <View style={styles.containerView}>
                        <Image source={require('../assets/images/banner_001.jpg')} style={styles.image}/>
                        <Text style={styles.textTitle}>{Strings.InviteFriends.text1}</Text>
                        <Text style={styles.text}>{Strings.InviteFriends.text2}</Text>
                        <Text style={styles.text}>{Strings.InviteFriends.text3}</Text>
                        <Text style={styles.text}>{Strings.InviteFriends.text4}</Text>
                        <Text style={styles.text}>{Strings.InviteFriends.text5}</Text>
                        <Text style={styles.text}>{Strings.InviteFriends.text6}</Text>
                        <Text style={styles.text}>{Strings.InviteFriends.text7}</Text>
                        <Text style={styles.text}>{Strings.InviteFriends.text8}</Text>
                        <Text style={styles.text}>{Strings.InviteFriends.text9}</Text>
                        <Text style={styles.text}>{Strings.InviteFriends.text10}</Text>
                        <Text style={styles.text}>{Strings.InviteFriends.text11}</Text>
                        <Text style={styles.text}>{Strings.InviteFriends.text12}</Text>
                        <Text style={styles.text}>{Strings.InviteFriends.text13}</Text>
                        <Text style={styles.text}>{Strings.InviteFriends.text14}</Text>
                        <View style={{marginTop: 10,}}>
                            <Button 
                                onPress={ sendReferral }
                                buttonStyle={{backgroundColor: Colors.button, borderRadius: 5, marginTop: 20, marginBottom: 20, height: 50, }}
                                title={Strings.InviteFriends.sendbutton}/>
                        </View>
                    </View>
                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.backgroundColor,
    },

    safeAreaView: {
        flex: 1
    },

    containerView: {
        flex: 1,
        flexDirection: 'column',
    },

    image: {
        height: imageHeight,
        width: imageWidth,
        marginBottom: 10,
    },

    textTitle: {
        fontSize: 15,
        color: Colors.text,
        marginLeft: 15,
        fontWeight: 'bold',
    },

    text: {
        fontSize: 13,
        marginTop: 5,
        marginLeft: 15,
        marginRight: 15,
        color: Colors.text,
    },
});

