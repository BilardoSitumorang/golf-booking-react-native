import clsSetting from "../API/clsSetting";

export default class MemberCardModel
{
    constructor(_id, _type, _name, _description, _price, _rate, _class, _absorbcreditcard)
    {
        this.Id = _id;
        this.Type = _type;
        this.Description = _description;
        this.Price = _price;
        this.Rate = _rate;
        this.Class = _class;
        this.Absorbcreditcard = _absorbcreditcard;
    }
}

exports.ClassCard
{
    constructor(_id, _name, _image)
    {
        this.Id = _id;
        this.Name = _name;
        this.Image = _image;
    }
}