import React from 'react';
import Moment from 'moment';
import clsSetting from '../API/clsSetting';

exports.getOptions = () => {
    return fetch(clsSetting.APIUrl + "option/get?ApiSecretKey=" + clsSetting.ApiSecretKey)
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        if (json.status !== 400 && json.status === 200) {
            return json.result; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error Options ', e));  
}

exports.findOption = (_id) => {
    return fetch(clsSetting.APIUrl + "option/find?id=" + _id + "&ApiSecretKey=" + clsSetting.ApiSecretKey)
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        if (json.status !== 400 && json.status === 200) {
            return json.result; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error Find Option ', e));  
}

exports.findOptionByName = (_name) => {
    return fetch(clsSetting.APIUrl + "option/findbyname?name=" + _name +
    "&ApiSecretKey=" + clsSetting.ApiSecretKey)
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        if (json.status !== 400 && json.status === 200) {
            return json.result; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error Option By Name ', e));  
}