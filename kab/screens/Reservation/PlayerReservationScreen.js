import React from 'react';
import {
    ScrollView,
    Platform,
    StyleSheet,
    Text,
    SafeAreaView,
    View,
    Image,
    TouchableOpacity,
    ActivityIndicator,
    AsyncStorage,
} from 'react-native';
import Colors from '../../constants/Colors';
import Strings from '../../constants/Strings';
import { Button } from 'react-native-elements';
import { Card } from 'react-native-material-cards';
import BpkTextInput from 'react-native-bpk-component-text-input';
import { getVirtualAccount, checkPlayerTypeReservation } from '../../API/UserService';
import { findOption,  } from '../../API/OptionService';
import { getDoubleToStringDate  } from '../../API/SettingService';
import Enums, { TipePemainIntEnum, TipePemainStringEnum, KartuStringEnum, StatusPemainIntEnum, KartuIntEnum, OptionIntEnum } from '../../constants/Enums';
import Utils from '../Utils';
import clsSetting from '../../API/clsSetting';

export default class PlayerReservationScreen extends React.Component {
    static navigationOptions = {
        title: 'Player',
        headerTintColor: '#ffffff', 
        headerStyle: {
            backgroundColor: '#93b843'
        },
    };

    constructor(props){
        super(props);

        this.params = this.props.navigation.state.params;

        this.state = {
            isLoading: true,
            username: '',
            playernameinput: '',
            playernoinput: '',
            usertype: '',
            typeMember: '',
            allowpublish: true,

            location: this.params.Location,
            locationseletected: this.params.LocationSelected,
            calendar: this.params.Calendar,
            session: this.params.Session,
            player: this.params.Player,
            playertype: this.params.PlayerType,
            
            courseimage: this.params.CourseImage,
            coursename: this.params.CourseName,
            courseaddress: this.params.CourseAddress,
            
            publishQuota: this.params.PublishQouta,
            qQuota: this.params.QQuota,

            kerjasamalapangan: this.params.KerjasamaLapangan,
            containmandiri: false,

            arrName: JSON.parse(this.params.ArrName),
            arrPlayerName: JSON.parse(this.params.ArrPlayerName),
            arrType: JSON.parse(this.params.ArrType),
            arrKartu: JSON.parse(this.params.ArrKartu),
            bookModel: JSON.parse(this.params.BookModel),

            editedposition: this.params.EditedPosition,
            txtsebelumnya: '',
            playernameold: '',
        }
    }

    async componentDidMount() {
        this.setState({ usertype : await AsyncStorage.getItem('TipeUser')});
        this.setState({ username : await AsyncStorage.getItem('Username')});
        this.setState({
            playernoinput: this.state.arrName[this.state.editedposition] === '' ? '' : this.state.arrName[this.state.editedposition],
            txtsebelumnya: this.state.playernoinput,
            playernameinput: this.state.arrPlayerName[this.state.editedposition],
            isLoading: false,
        });
    }

    validasiNonPublishPlayer = (_json, _type, _mandiricard) => 
    {
        if(this.state.qQuota <= 0)
        {
            Utils.errAlert("Error Message", Strings.PlayerReservation.errmsg4);
        }
        else if(this.state.playernoinput === '')
        {
            Utils.errAlert("Error Message", Strings.PlayerReservation.errmsg5);
        }
        else if(_json.Status != 0 && _json.Status != StatusPemainIntEnum.Active)
        {
            Utils.errAlert("Error Message", Strings.PlayerReservation.errmsg6);
        }
        else if(_type != TipePemainIntEnum.QMember && _type != TipePemainIntEnum.Club)
        {
            Utils.errAlert("Error Message", Strings.PlayerReservation.errmsg7);
        }
        else if(this.state.playernameinput === '' && _type != TipePemainIntEnum.QMember)
        {
            Utils.errAlert("Error Message", Strings.PlayerReservation.errmsg2);
        }
        else
        {
            if(this.state.playernoinput === this.state.txtsebelumnya && this.state.typeMember != TipePemainIntEnum.Club)
            {
                if(this.state.playernameinput !== '' && this.state.typeMember != TipePemainStringEnum.QMember)
                {
                    this.setPlayerName();
                }
                this.callActivity();
            }
            else if(_mandiricard !== '' && this.state.playernoinput === _mandiricard && this.state.typeMember === KartuIntEnum.Mandiri)
            {
                Utils.errAlert("Error Message", Strings.PlayerReservation.errmsg8);
            }

            var typeName = Enums.TipePemainEnum(_type);

            if(_type == TipePemainIntEnum.Visitor || _type == TipePemainIntEnum.Visitor)
            {
                typeName = TipePemainStringEnum.Publish;
            }

            var kartu = _json.JenisMember;
            this.state.arrType[this.state.editedposition] = typeName;
            this.state.arrKartu[this.state.editedposition] = kartu;

            if(_type != TipePemainIntEnum.Publish && _type != TipePemainIntEnum.Club)
            {
                var existname = false;
                if(_type == TipePemainIntEnum.QMember)
                {
                    this.setState({
                        playernameold: this.state.playernameinput,
                        playernameinput: _json.CardName,
                    });
                }
                if(kartu != KartuIntEnum.Corporate)
                {
                    var i;
                    for(i = 0; i < this.state.arrName.length; i++)
                    {
                        if(this.state.arrName[i] == this.state.playernoinput){
                            existname = true;
                        }
                    }

                    if(existname)
                    {
                        Utils.errAlert("Error Message", Strings.PlayerReservation.errmsg9);
                    }
                    else
                    {
                        this.setState({ qQuota: this.state.qQuota - 1})
                        this.setArrName();
                        this.setPlayerName();
                        this.callActivity();
                    }
                }
                else
                {
                    if(this.state.arrName.length < 4)
                    {
                        this.setState({qQuota: this.state.qQuota - 1});
                        this.setArrName();
                        this.setPlayerName();

                        var n = 0;
                        var j;
                        for(j = 0; j < this.state.arrName.length; j++)
                        {
                            if(this.state.arrName[j] == this.state.playernoinput){
                                n = n + 1;
                            }

                            if(n > 1) {
                                existname = true;
                            }
                        }

                        if(existname)
                        {
                            this.setPlayerName();
                        }
                        this.callActivity();
                    }
                    else
                    {
                        var n = 0;
                        var changename = false;
                        var j;
                        for(j = 0; j < this.state.arrName.length; j++)
                        {
                            if(this.state.arrName[j] == this.state.playernoinput){
                                n = n + 1;
                            }

                            if(n >= 3) {
                                existname = true;
                            }

                            if(n > 0)
                            {
                                changename = true;
                            }
                        }

                        if(existname)
                        {
                            if(kartu != KartuIntEnum.Corporate)
                            {
                                Utils.errAlert("Error Message", Strings.PlayerReservation.errmsg9);
                            }
                            else
                            {
                                Utils.errAlert("Error Message", Strings.PlayerReservation.errmsg10);
                            }
                        }
                        else
                        {
                            this.setState({qQuota: this.state.qQuota - 1});
                            this.setArrName();
                            this.setPlayerName();
                            if(changename)
                            {
                                this.state.arrPlayerName[this.state.editedposition] = this.state.playernameold.slice(0, 1).toUpperCase() + "" +
                                    this.state.playernameold.slice(1, this.state.playernameold.length);
                            }
                            this.callActivity();
                        }
                    } 
                }
            }
            else
            {
                this.setState({qQuota: this.state.qQuota - 1});
                this.setArrName();
                this.setPlayerName();
                this.callActivity();
            }
        }
    }

    setPlayerName = () => 
    {
        if(this.state.playernameinput != ''){
            this.state.arrPlayerName[this.state.editedposition] = this.state.playernameinput.slice(0, 1).toUpperCase() + "" +
            this.state.playernameinput.slice(1, this.state.playernameinput.length);
        }
    }

    setArrName = () =>
    {
        this.state.arrName[this.state.editedposition] = this.state.playernoinput;
    }

    getMandiriInList = () =>
    {
        var mandiricard = "";
        var i;
        for(i = 0; i < this.state.arrType.length; i++)
        {
            if(this.state.arrType[i] === KartuStringEnum.Mandiri){
                mandiricard = this.state.arrName[i];
            }
        }
        
        return mandiricard;
    }

    validasiPublishPlayer = () => {
        if(this.state.publishQuota <= 0)
        {
            Utils.errAlert("Error Message", Strings.PlayerReservation.errmsg4);
        }
        else if(this.state.playernameinput === "")
        {
            Utils.errAlert("Error Message", Strings.PlayerReservation.errmsg2);
        }
        else
        {
            this.setState({publishQuota:  Number.parseInt(this.state.publishQuota) - 1});
            this.state.arrType[this.state.editedposition] = TipePemainStringEnum.Publish;
            this.state.arrName[this.state.editedposition] = ' ';
            this.setPlayerName();

            this.callActivity();
        }

    }

    callActivity = () => {

        this.state.bookModel[this.state.editedposition].Nama =  this.state.arrName[this.state.editedposition];
        this.state.bookModel[this.state.editedposition].PlayerName = this.state.arrPlayerName[this.state.editedposition];
        this.state.bookModel[this.state.editedposition].Tipe = this.state.arrType[this.state.editedposition];
        this.state.bookModel[this.state.editedposition].Kartu = this.state.arrKartu[this.state.editedposition];

        const { navigation } = this.props;
        navigation.goBack();
        this.params.onSelect({arrName: this.state.arrName, arrType: this.state.arrType, 
            arrKartu: this.state.arrKartu, arrPlayerName:  this.state.arrPlayerName,
            publishQuota: this.state.publishQuota, qQuota: this.state.qQuota,
            bookModel: this.state.bookModel });
    }

    validasiPlayer = () => 
    {
        if(this.state.playernameinput === ''){
            Utils.errAlert("Error Message", Strings.PlayerReservation.errmsg2);
        }
        else {
            var sessionInt = this.state.session === undefined ? 1 : this.state.session === "Morning" ? 1 : 2;
            checkPlayerTypeReservation(this.state.playernoinput, this.state.locationseletected, this.state.calendar,
                sessionInt).then((json) =>
            {
                console.log('Json Card', json.result);
                if(json.status != 200){
                    Utils.errAlert("Error Message", json.message);
                }
                else if(json.status == 200){
                    var type = json.result.TipeMember;
                    findOption(OptionIntEnum.OptionPublishQuota).then((jsonOption) => 
                    {
                        var platform = Platform.OS === 'ios' ? jsonOption.iOS : jsonOption.Android;
                        if(platform){
                            if(type == TipePemainIntEnum.Visitor || type == TipePemainIntEnum.Publish)
                            {
                                this.validasiPublishPlayer();
                            }
                            else 
                            {
                                if(Date.parse(json.result.ExpirationDate) < Date.now()){
                                    Utils.errAlert("Error Message", Strings.PlayerReservation.errmsg3);
                                }
                                else{
                                    this.validasiNonPublishPlayer(json.result, type, this.getMandiriInList());
                                }
                            }
                        }
                        else
                        {
                            if(Date.parse(json.result.ExpirationDate) < Date.now()){
                                Utils.errAlert("Error Message", Strings.PlayerReservation.errmsg3);
                            }
                            else{
                                this.validasiNonPublishPlayer(json.result, type, this.getMandiriInList());
                            }
                        }
                    })
                    this.setState({isLoading: false});
                }
                
            })
            .catch((error) => {
                console.error(error);
                this.setState({isLoading: false});
            });
        }
    }

    render() {
        const fn_GetMyCard = () =>
        {
            if(this.state.usertype === TipePemainIntEnum.Visitor || this.state.usertype === TipePemainIntEnum.Publish)
            {
                Utils.errAlert("Error Message", Strings.PlayerReservation.errmsg1);
            }
            else 
            {
                getVirtualAccount(this.state.username).then((jsonCard) =>
                {
                    this.setState({ 
                        typeMember : Enums.TipePemainEnum(jsonCard.TypeMember),
                        playernoinput: jsonCard.NoMember,
                        playernameinput: jsonCard.CardName == null ? '' : jsonCard.CardName,
                    });
                    this.validasiPlayer();
                    this.setState({isLoading: false});
                })
                .catch((error) => {
                    console.error(error);
                    this.setState({isLoading: false});
                });
            }
        }

        const gotoBook = () => {
            this.validasiPlayer();
        }

        if (this.state.isLoading) {
            return (
                <View style={styles.loading}>
                    <ActivityIndicator size='large' />
                </View>
            );
        }

        return (
            <SafeAreaView style={styles.safeAreaView}>
                <ScrollView style={styles.containerScroll}>
                    <View style={styles.container}>
                        <Card style={styles.card}>
                            <View style={{width: '100%', flexDirection: 'row', }}>
                                <BpkTextInput 
                                    label={Strings.PlayerReservation.text1}
                                    onChangeText={(text) => this.setState({playernoinput: text})}
                                    value={this.state.playernoinput}
                                    style={{flex: 1, marginBottom: 3, }}/>
                            </View>
                            <View style={{width: '100%', flexDirection: 'row', marginTop: 5,}}>
                                <BpkTextInput 
                                    label={Strings.PlayerReservation.text2}
                                    onChangeText={(text) => this.setState({playernameinput: text})}
                                    value={this.state.playernameinput}
                                    style={{flex: 1, marginBottom: 3, }}/>
                            </View>
                        </Card>
                        <Text style={styles.textKet} >{Strings.PlayerReservation.text3}</Text>
                        <View style={{backgroundColor: '#ffffff', paddingLeft: 20, paddingRight: 20, paddingTop: 10, paddingBottom: 10, }}>
                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', }}>
                                <Text style={styles.textLeftBold}>{'• '}</Text>
                                <Text style={styles.textRightBold}>{Strings.PlayerReservation.text4}</Text>
                            </View>
                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', }}>
                                <Text style={styles.textLeft2}>{'• '}</Text>
                                <Text style={styles.textRight2}>{Strings.PlayerReservation.text5}</Text>
                            </View>
                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', }}>
                                <Text style={styles.textLeft2}>{'• '}</Text>
                                <Text style={styles.textRight2}>{Strings.PlayerReservation.text6}</Text>
                            </View>
                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', }}>
                                <Text style={styles.textLeft2}>{'• '}</Text>
                                <Text style={styles.textRight2}>{Strings.PlayerReservation.text7}</Text>
                            </View>
                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', }}>
                            <Text style={styles.textLeft2}>{'• '}</Text>
                                <Text style={styles.textRight2}>{Strings.PlayerReservation.text8}</Text>
                            </View>
                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', }}>
                                <Text style={styles.textLeft2}>{'• '}</Text>
                                <Text style={styles.textRight2}>{Strings.PlayerReservation.text9}</Text>
                            </View>
                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', }}>
                                <Text style={styles.textLeft2}>{'• '}</Text>
                                <Text style={styles.textRight2}>{Strings.PlayerReservation.text10}</Text>
                            </View>
                        </View>
                        <Button 
                            onPress= { fn_GetMyCard }
                            buttonStyle={{backgroundColor: '#d6a431', borderRadius: 5, marginTop: 20, marginLeft: 5, marginRight: 5, marginBottom: 10, height: 50, }}
                            title={Strings.PlayerReservation.usemycardbutton}/>
                        <Button 
                            onPress= { gotoBook }
                            buttonStyle={{backgroundColor: Colors.button, borderRadius: 5, marginTop: 10, marginLeft: 5, marginRight: 5, marginBottom: 20, height: 50, }}
                            title={Strings.PlayerReservation.donebutton}/>
                    </View>
                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({

    safeAreaView: {
        flex: 1,
    },

    containerScroll:
    {
        flex: 1,
        backgroundColor: '#efefef',
    },

    container: {
        flex: 1,
        flexDirection: 'column',
    },

    text: {
        fontSize: 13,
        marginTop: 5,
        color: Colors.text,
    },

    textBold: {
        fontSize: 13,
        marginTop: 5,
        color: Colors.text,
        fontWeight: 'bold',
    },

    textLeft2: {
        fontSize: 14,
        marginTop: 5,
        color: Colors.text,
        width: 10,
    },

    textRight2: {
        flex: 1,
        fontSize: 12,
        marginTop: 5,
        color: Colors.text,
    },

    textLeftBold: {
        fontSize: 14,
        marginTop: 5,
        color: Colors.text,
        width: 10,
        fontWeight: 'bold',
    },

    textRightBold: {
        flex: 1,
        fontSize: 12,
        marginTop: 5,
        color: Colors.text,
        fontWeight: 'bold',
    },

    textKet: {
        fontSize: 18,
        marginTop: 10,
        marginLeft: 15,
        marginBottom: 10,
        marginRight: 15,
        color: Colors.text,
        fontWeight: 'bold',
    },

    card: {
        backgroundColor: Colors.backgroundColor,
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 10,
        marginRight: 10,
        marginTop: 10,
        marginBottom: 5,
        padding: 10,
        elevation: 2,
        borderRadius: 5,
    },

    loading: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(255, 255, 255, 0.2)',
    },

});
