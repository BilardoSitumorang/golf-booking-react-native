import React from 'react';
import { ScrollView, 
    StyleSheet, 
    WebView,
    SafeAreaView } from 'react-native';

export default class HelpScreen extends React.Component {
    
    static navigationOptions = {
        title: 'Help',
        headerTintColor: '#ffffff', 
        headerStyle: {
            backgroundColor: '#93b843'
        },
    };

    render() {
        return (
            <SafeAreaView style={styles.safeAreaView}>
                <WebView source={{uri: 'https://www.qaccess.asia/faq/index2'}} />
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },

    safeAreaView: {
        flex: 1
    },
});
