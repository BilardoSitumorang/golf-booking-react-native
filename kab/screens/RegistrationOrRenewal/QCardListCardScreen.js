import React from 'react';
import {
    ScrollView,
    StyleSheet,
    SafeAreaView,
    TouchableOpacity,
    ActivityIndicator,
    Text,
    FlatList,
    ListView,
    View,
} from 'react-native';
import Colors from '../../constants/Colors';
import Strings from '../../constants/Strings';
import QCardRowScreen from '../ListRow/QCardRowScreen';
import { getMemberCards, } from '../../API/UserService';
import Utils from '../Utils';
import clsSetting from '../../API/clsSetting';

export default class QCardListCardScreen extends React.Component {
    static navigationOptions = {
        title: 'QGolf Card',
        headerTintColor: '#ffffff', 
        headerStyle: {
            backgroundColor: '#93b843'
        },
    };

    constructor(props){
        super(props);

        this.state = {
            isLoading: true,
            listWNIVisible: false,
            listWNAVisible: false,
        }
    }

    componentDidMount() {
        var Enumerable = require('../linq');
        getMemberCards().then((jsonCards) => 
        {
            this.setState({
                memberCardWNI: Enumerable.from(jsonCards).where(x => x.Type == 1).orderBy(t => t.Class.Id).toArray(),
                memberCardWNA: Enumerable.from(jsonCards).where(x => x.Type == 2).orderBy(t => t.Class.Id).toArray()
            });

            let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
            this.setState({
                isLoading: false,
                dataSourceWNI: ds.cloneWithRows(this.state.memberCardWNI),
                dataSourceWNA: ds.cloneWithRows(this.state.memberCardWNA)
            });
        })
        .catch((error) => {
            console.error(error);
            this.setState({isLoading: false, });
        });
        
    }

    GetItem (card) {
        //Selected Row List
        this.props.navigation.navigate('DetailCard', {Id: card.Id, Type: card.Type, Name: card.Name, Description: card.Description,
            Price: card.Price, Rate: card.Rate, ClassId: card.Class.Id, ClassImage: card.Class.Image, 
            AbsorbCreditCardFee: card.AbsorbCreditCardFee });
    }

    ListViewItemSeparator = () => {
        return (
            <View style={{ height: .5, width: "100%", backgroundColor: "#fff", }} />
        );
    }

    render() {
        const setVisibleWNI = () => {
            if(!this.state.listWNIVisible) 
            {
                this.setState({
                    listWNIVisible: true,
                    listWNAVisible: false,
                });
            }
            else
            {
                this.setState({
                    listWNIVisible: false,
                });
            }
        }

        const setVisibleWNA = () => {
            if(!this.state.listWNAVisible) 
            {
                this.setState({
                    listWNIVisible: false,
                    listWNAVisible: true,
                });
            }
            else
            {
                this.setState({
                    listWNAVisible: false,
                });
            }
        }

        if (this.state.isLoading) {
            return (
                <View style={{flex: 1, paddingTop: 20}}>
                    <ActivityIndicator />
                </View>
            );
        }

        return (
            <SafeAreaView style={styles.safeAreaView}>
                <ScrollView style={styles.containerScroll}>
                    <View style={styles.container}>
                        <View style={{backgroundColor: '#fff', elevation: 2, padding: 10, marginTop: 10,  }} >
                            <TouchableOpacity style={{justifyContent: 'center', alignContent: 'center',}} onPress={ setVisibleWNI } >
                                <Text style={{color: Colors.text, fontSize: 16, fontWeight: 'bold', }}>{Strings.QCardList.text1}</Text>
                            </TouchableOpacity>
                        </View>
                        {
                            Utils.renderIf(this.state.listWNIVisible,
                                <ListView
                                    style={{backgroundColor: '#fff', flex: 1,}}
                                    dataSource={this.state.dataSourceWNI}
                                    renderSeparator= {this.ListViewItemSeparator}
                                    renderRow={(rowData) =>
                                        <TouchableOpacity 
                                            onPress={ this.GetItem.bind(this, rowData) } >
                                            <QCardRowScreen 
                                                cardimg={{uri: rowData.Class.Image !== "" ? clsSetting.HomeUrl + rowData.Class.Image : clsSetting.HomeUrl + "images/no_image2.jpg"}}
                                                cardname={rowData.Name}
                                                carddesc={rowData.Description}/>
                                        </TouchableOpacity>
                                    }
                                />
                            )
                        }
                        <View style={{backgroundColor: '#fff', elevation: 2, padding: 10, marginTop: 5, justifyContent: 'center', alignContent: 'center', }} >
                            <TouchableOpacity style={{justifyContent: 'center', alignContent: 'center',}} onPress={ setVisibleWNA } >
                                <Text style={{color: Colors.text, fontSize: 16, fontWeight: 'bold', }}>{Strings.QCardList.text2}</Text>
                            </TouchableOpacity>
                        </View>
                        {
                            Utils.renderIf(this.state.listWNAVisible,
                                <ListView
                                    style={{backgroundColor: '#fff', flex: 1,}}
                                    dataSource={this.state.dataSourceWNA}
                                    renderSeparator= {this.ListViewItemSeparator}
                                    renderRow={(rowData) =>
                                        <TouchableOpacity 
                                            onPress={ this.GetItem.bind(this, rowData) } >
                                            <QCardRowScreen 
                                                cardimg={{uri: rowData.Class.Image !== "" ? clsSetting.HomeUrl + rowData.Class.Image : clsSetting.HomeUrl + "images/no_image2.jpg"}}
                                                cardname={rowData.Name}
                                                carddesc={rowData.Description}/>
                                        </TouchableOpacity>
                                    }
                                />
                            )   
                        }
                    </View>
                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({

    safeAreaView: {
        flex: 1,
    },

    containerScroll:
    {
        flex: 1,
        backgroundColor: '#f2f2f2',
    },

    container: {
        flex: 1,
    },

    card:
    {
        flex:1,
        padding: 10,
        margin: 10,
        elevation: 2,
        borderRadius: 3,
    },
});
