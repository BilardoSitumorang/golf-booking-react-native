import clsSetting from "../API/clsSetting";

export default class PulsaTransactionModel
{
    constructor(_email, _productcode, _productdesc, _productnom,
        _productactiveperiod, _customernumber, _timezone, _producttype,
        _price, _platform, _type)
    {
        this.Email = _email;
        this.ProductCode = _productcode;
        this.ProductDescription = _productdesc;
        this.ProductNominal = _productnom;
        this.ProductActivePeriod = _productactiveperiod;
        this.CustomerNumber = _customernumber;
        this.Timezone = _timezone;
        this.ProductType = _producttype;
        this.Price = _price;
        this.Platform = _platform;
        this.Type = _type;
    }
}