import React from 'react';
import {
    Image,
    Platform,
    ScrollView,
    StyleSheet,
    Text,
    AsyncStorage,
    SafeAreaView,
    View,
} from 'react-native';
import Colors from '../constants/Colors';
import Strings from '../constants/Strings';
import { Button, Card, } from 'react-native-elements';
import EventRowScreen from './ListRow/EventRowScreen';
import SearchCourseRowScreen from './ListRow/SearchCourseRowScreen';

export default class TestScreen extends React.Component {
    
    render() {
        const gotoSearch = () => {
            this.props.navigation.navigate('Search')
        }

        return (
            <SafeAreaView style={styles.safeAreaView}>
                <ScrollView style={styles.containerScroll}>
                    <SearchCourseRowScreen imageUri={require('../assets/images/search_bg2.png')}
                            location={'Ds. Tapos. Cimanggis'}
                            quotaQ={'10'}
                            quotaPublish={'10'}
                            startfrom={'Rp. 100.000'}/>
                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({

    safeAreaView: {
        flex: 1,
    },

    containerScroll:
    {
        flex: 1,
        backgroundColor: '#f1f1f1',
    },

    container: {
        backgroundColor: '#d9d9d9',
        height: 200,
        width: '100%'
    },

    image: {
        width: 20,
        height: 20,
    },

    imageCard: {
        width: 40,
        height: 40,
    },

    text: {
        fontSize: 12,
        left: 5,
        color: Colors.text,
    },

    textLabel: {
        fontSize: 11,
        left: 10,
        color: Colors.text,
    },

    textField: {
        fontSize: 14,
        left: 10,
        color: Colors.text,
    },

    textCard: {
        fontSize: 13,
        top: 5,
        justifyContent: 'center',
        alignItems: 'center',
        color: Colors.text,
    },

    textTitle: {
        fontSize: 18,
        left: 15,
        top: 15,
        color: Colors.text,
        fontWeight: 'bold',
    },

    textButton: {
        flex: 1,
        fontSize: 14,
        right: 15,
        top: 15,
        textAlign: 'right',
        color: Colors.button,
    },


    card: {
        backgroundColor: Colors.backgroundColor,
        flex: 1,
        marginLeft: 15,
        marginRight: 15,
        marginTop: 5,
        padding: 10,
    },

    cardLeft: {
        backgroundColor: Colors.backgroundColor,
        flex: 1,
        height: 100,
        marginLeft: 15,
        marginRight: 5,
        marginTop: 5,
        padding: 10,
    },

    cardRight: {
        backgroundColor: Colors.backgroundColor,
        flex: 1,
        height: 100,
        marginLeft: 5,
        marginRight: 15,
        marginTop: 5,
        padding: 10,
    },
});