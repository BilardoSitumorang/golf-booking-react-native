import React from 'react';
import {
    ScrollView,
    StyleSheet,
    Text,
    Image,
    SafeAreaView,
    TouchableOpacity,
    View,
} from 'react-native';
import Colors from '../../constants/Colors';
import Strings from '../../constants/Strings';
import { Button, } from 'react-native-elements';
import { Card } from 'react-native-material-cards';
import Utils from '../Utils';
import { PaymentIntEnum } from '../../constants/Enums';

export default class TermsRegisterQScreen extends React.Component {
    static navigationOptions = {
        title: 'Terms And Condition',
        headerTintColor: '#ffffff', 
        headerStyle: {
            backgroundColor: '#93b843'
        },
    };

    constructor(props){
        super(props);

        this.params = this.props.navigation.state.params;

        this.state = {
            transtype: this.params.TransType,
            point: this.params.Point,
            paymenttype: this.params.PaymentType,
            chargepayment: this.params.ChargePayment,
            bookingcode: this.params.BookingCode,
            bookingdate: this.params.BookingDate,
            total: this.params.Total,
            containmandiri: this.params.ContainMandiri,
            mandiricard: this.params.MandiriCard,
            transdate: this.params.TransDate,
            eventdate: this.params.EventDate,
            status: this.params.Status,
            timeevent: this.params.TimeEvent,
            bookingdateraw: this.params.BookingDateRaw,

            visiblelayout1: false,
            visiblelayout2: false,
            visiblelayout3: false,
            visiblelayout4: false,
        };
    }

    render() {
        const gotoContinue = () => {
            if(this.state.paymenttype == PaymentIntEnum.BankTransfer)
            {
                this.props.navigation.navigate('BankTransfer', {Point: this.state.point, BookingCode: this.state.bookingcode,
                    BookingDate: this.state.bookingdate, ChargePayment: this.state.chargepayment, 
                    TransType: this.state.transtype, PaymentType: PaymentIntEnum.BankTransfer, Total: this.state.total,
                    TransDate: this.state.transdate, EventDate: this.state.eventdate, TimeEvent: this.state.timeevent, 
                    BookingDateRaw: this.state.bookingdateraw, Status: this.state.status });
            }
            else if(this.state.paymenttype == PaymentIntEnum.CreditCard)
            {
                this.props.navigation.navigate('CreditCard', {Point: this.state.point, BookingCode: this.state.bookingcode,
                    BookingDate: this.state.bookingdate, ChargePayment: this.state.chargepayment, 
                    TransType: this.state.transtype, PaymentType: PaymentIntEnum.CreditCard, Total: this.state.total,
                    ContainMandiri: this.state.containmandiri, MandiriCard: this.state.mandiricard,
                    TransDate: this.state.transdate });
            }
            else if(this.state.paymenttype == PaymentIntEnum.FullPoint)
            {
                this.props.navigation.navigate('CreditCard3DS', {Point: this.state.point, BookingCode: this.state.bookingcode,
                    BookingDate: this.state.bookingdate, ChargePayment: this.state.chargepayment,
                    TransDate: this.state.transdate,
                    TransType: this.state.transtype, PaymentType: PaymentIntEnum.FullPoint, Total: this.state.total,
                    ContainMandiri: this.state.containmandiri, MandiriCard: this.state.mandiricard,
                    NameOnCard: 'Any Name', CardNumber: '4811111111111114', CardMonth: '05', CardYear: '2020', CardCVV: '123'});
            }
        }

        const gotoVisibleLayout1 = () => 
        {
            if(this.state.visiblelayout1)
            {
                this.setState({visiblelayout1: false, });
            }
            else
            {
                this.setState({visiblelayout1: true, });
            }
        }

        const gotoVisibleLayout2 = () => 
        {
            if(this.state.visiblelayout2)
            {
                this.setState({visiblelayout2: false, });
            }
            else
            {
                this.setState({visiblelayout2: true, });
            }
        }

        const gotoVisibleLayout3 = () => 
        {
            if(this.state.visiblelayout3)
            {
                this.setState({visiblelayout3: false, });
            }
            else
            {
                this.setState({visiblelayout3: true, });
            }
        }

        const gotoVisibleLayout4 = () => 
        {
            if(this.state.visiblelayout4)
            {
                this.setState({visiblelayout4: false, });
            }
            else
            {
                this.setState({visiblelayout4: true, });
            }
        }

        return (
            <SafeAreaView style={styles.safeAreaView}>
                <ScrollView style={styles.containerScroll}>
                    <View style={styles.container}>
                        <Card style={styles.cardHorizontal}>
                            <View style={{flex: 1, flexDirection: 'row', }}>
                                <TouchableOpacity style={{flex: 1, flexDirection: 'row', }}
                                    onPress={ gotoVisibleLayout1 }>
                                    <Text style={{flex: 2, fontSize: 15, color: Colors.text, }} >{Strings.TermsRegisterQ.text1}</Text>
                                    <Image source={require('../../assets/images/next2.png')} 
                                        style={{height: 15, width: 15, resizeMode: 'contain', alignItems: 'flex-end',}}/>
                                </TouchableOpacity>
                            </View>
                        </Card>
                        {
                            Utils.renderIf(this.state.visiblelayout1, 
                                <Card style={styles.cardHorizontal}>
                                    <View style={{flex: 1, flexDirection: 'row', }}>
                                        <Text style={{flex: 2, fontSize: 15, color: Colors.text, }} >{Strings.TermsRegisterQ.text2}</Text>
                                    </View>
                                </Card>
                            )
                        }
                        <Card style={styles.cardHorizontal}>
                            <View style={{flex: 1, flexDirection: 'row', }}>
                                <TouchableOpacity style={{flex: 1, flexDirection: 'row', }}
                                    onPress={ gotoVisibleLayout2 }>
                                    <Text style={{flex: 2, fontSize: 15, color: Colors.text, }} >{Strings.TermsRegisterQ.text3}</Text>
                                    <Image source={require('../../assets/images/next2.png')} 
                                        style={{height: 15, width: 15, resizeMode: 'contain', alignItems: 'flex-end',}}/>
                                </TouchableOpacity>
                            </View>
                        </Card>
                        {
                            Utils.renderIf(this.state.visiblelayout2,
                                <Card style={styles.cardHorizontal}>
                                    <View style={{flex: 1, flexDirection: 'row', }}>
                                        <Text style={{flex: 2, fontSize: 15, color: Colors.text, }} >{Strings.TermsRegisterQ.text4}</Text>
                                    </View>
                                </Card>
                            )
                        }
                        <Card style={styles.cardHorizontal}>
                            <View style={{flex: 1, flexDirection: 'row', }}>
                                <TouchableOpacity style={{flex: 1, flexDirection: 'row', }}
                                    onPress={ gotoVisibleLayout3 }>
                                    <Text style={{flex: 2, fontSize: 15, color: Colors.text, }} >{Strings.TermsRegisterQ.text5}</Text>
                                    <Image source={require('../../assets/images/next2.png')} 
                                        style={{height: 15, width: 15, resizeMode: 'contain', alignItems: 'flex-end',}}/>
                                </TouchableOpacity>
                            </View>
                        </Card>
                        {
                            Utils.renderIf(this.state.visiblelayout3,
                                <Card style={styles.cardHorizontal}>
                                    <View style={{flex: 1, flexDirection: 'row', }}>
                                        <Text style={{flex: 2, fontSize: 15, color: Colors.text, }} >{Strings.TermsRegisterQ.text6}</Text>
                                    </View>
                                </Card>
                            )
                        }
                        <Card style={styles.cardHorizontal}>
                            <View style={{flex: 1, flexDirection: 'row', }}>
                                <TouchableOpacity style={{flex: 1, flexDirection: 'row', }}
                                    onPress={ gotoVisibleLayout4 }>
                                    <Text style={{flex: 2, fontSize: 15, color: Colors.text, }} >{Strings.TermsRegisterQ.text7}</Text>
                                    <Image source={require('../../assets/images/next2.png')} 
                                        style={{height: 15, width: 15, resizeMode: 'contain', alignItems: 'flex-end',}}/>
                                </TouchableOpacity>
                            </View>
                        </Card>
                        {
                            Utils.renderIf(this.state.visiblelayout4,
                                <Card style={styles.cardHorizontal}>
                                    <View style={{flex: 1, flexDirection: 'row', }}>
                                        <Text style={{flex: 2, fontSize: 15, color: Colors.text, }} >{Strings.TermsRegisterQ.text8}</Text>
                                    </View>
                                </Card>
                            )
                        }
                        <View style={{backgroundColor: '#ffffff', paddingLeft: 20, paddingRight: 20, paddingTop: 10, paddingBottom: 10,}}>
                            <View style={{flex: 1, flexDirection: 'column', alignItems: 'center', }}>
                                <Text style={styles.text}>{Strings.TermsRegisterQ.text9}</Text>
                                <Text style={styles.textLink}>{Strings.TermsRegisterQ.text10}</Text>
                            </View>
                        </View>
                        <Button 
                            onPress= { gotoContinue }
                            buttonStyle={{backgroundColor: Colors.button, borderRadius: 5, marginTop: 10, marginLeft: 5, marginRight: 5, marginBottom: 20, height: 50, }}
                            title={Strings.TermsRegisterQ.continuebutton}/>
                    </View>
                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({

    safeAreaView: {
        flex: 1,
    },

    containerScroll:
    {
        flex: 1,
        backgroundColor: '#efefef',
    },

    container: {
        flex: 1,
        flexDirection: 'column',
    },

    text: {
        fontSize: 14,
        marginLeft: 10,
        color: Colors.text,
    },

    textLink: {
        fontSize: 14,
        marginLeft: 10,
        color: Colors.text,
        textDecorationLine: 'underline',
    },

    textLeft2: {
        fontSize: 14,
        marginTop: 5,
        color: Colors.text,
        width: 10,
    },

    textRight2: {
        flex: 1,
        fontSize: 14,
        marginTop: 5,
        color: Colors.text,
    },

    textKet: {
        fontSize: 18,
        marginTop: 5,
        marginLeft: 15,
        marginBottom: 5,
        marginRight: 15,
        color: Colors.text,
        fontWeight: 'bold',
    },

    cardHorizontal: {
        backgroundColor: Colors.backgroundColor,
        flex: 1,
        flexDirection: 'row',
        marginTop: 5,
        marginBottom: 5,
        marginLeft: 10,
        marginRight: 10,
        padding: 10,
        elevation: 2,
        borderRadius: 5,
    },
});
