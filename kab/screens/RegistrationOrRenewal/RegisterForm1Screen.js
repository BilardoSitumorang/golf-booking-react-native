import React from 'react';
import {
    Image,
    Platform,
    ScrollView,
    StyleSheet,
    SafeAreaView,
    TouchableOpacity,
    Text,
    Dimensions,
    View,
} from 'react-native';
import Colors from '../../constants/Colors';
import Strings from '../../constants/Strings';
import { Button, Icon, Divider, } from 'react-native-elements';
import BpkTextInput from 'react-native-bpk-component-text-input';
import DateTimePicker from 'react-native-modal-datetime-picker';
import Utils from '../Utils';
import moment from 'moment';

export default class RegisterForm1Screen extends React.Component {
    static navigationOptions = ({navigation}) => ({
        title: 'Register Member',
        headerTintColor: '#ffffff', 
        headerStyle: {
            backgroundColor: '#93b843'
        },
    });

    constructor(props){
        super(props);

        this.params = this.props.navigation.state.params;

        this.state = {
            isLoading: true,
            username: '',

            id: this.params.Id,
            inputidcardname: '',
            inputcardname: '',
            inputidcardnumber: '',
            inputphonenumber: '',
            inputemail: '',
            inputplaceofbirth: '',
            inputdob: moment(Date.now()).format('YYYY-MM-DD'),
            inputaddress: '',
            inputcity: '',
            inputpostalcode: '',

            isDateTimePickerVisible: false,
        }
    }

    //Date Picker
    _showDateTimePicker = () => {
        this.setState({ isDateTimePickerVisible: true });
    }
    
    _hideDateTimePicker = () => {
        this.setState({ isDateTimePickerVisible: false });
    }

    _handleDatePicked = (date) => {
        console.log('A date has been picked: ', date);
        this._hideDateTimePicker();
        this.setState({inputdob: moment(date).format('YYYY-MM-DD')});
    };

    render() {
        const gotoForm2 = () => {
            if(this.state.inputidcardname.trim() == "")
            {
                Utils.errAlert("Error Message", Strings.RegisterForm1.errmsg1);
            }
            else if(this.state.inputcardname.trim() == "")
            {
                Utils.errAlert("Error Message", Strings.RegisterForm1.errmsg2);
            }
            else if(this.state.inputidcardnumber.trim() == "")
            {
                Utils.errAlert("Error Message", Strings.RegisterForm1.errmsg3);
            }
            else if(this.state.inputphonenumber.trim() == "")
            {
                Utils.errAlert("Error Message", Strings.RegisterForm1.errmsg4);
            }
            else if(this.state.inputemail.trim() == "")
            {
                Utils.errAlert("Error Message", Strings.RegisterForm1.errmsg5);
            }
            else if(this.state.inputplaceofbirth.trim() == "")
            {
                Utils.errAlert("Error Message", Strings.RegisterForm1.errmsg6);
            }
            else if(this.state.inputdob.trim() == "")
            {
                Utils.errAlert("Error Message", Strings.RegisterForm1.errmsg7);
            }
            else
            {
                this.props.navigation.navigate('Form2', {Id: this.state.id, IdCardName: this.state.inputidcardname,
                    CardName: this.state.inputcardname, IdCardNumber: this.state.inputidcardnumber, 
                    PhoneNumber: this.state.inputphonenumber, Email: this.state.email, PlaceOfBirth: this.state.inputplaceofbirth,
                    DateOfBirth: this.state.inputdob, Address: this.state.inputaddress, City: this.state.inputcity,
                    PostalCode: this.state.inputpostalcode});
            }
        }

        const dimensions = Dimensions.get('window');
        const btnHeight = Math.round(dimensions.width * 9 / 16);
        const btnWidth = dimensions.width - 30;

        return (
            <SafeAreaView style={styles.safeAreaView}>
                <ScrollView style={styles.containerScroll}>
                    <View style={styles.container}>
                        <View style={{flex:1, justifyContent:'center', 
                            alignContent: 'center', alignItems: 'center',}}>
                            <Image style={{width: 120, height: 120, resizeMode: 'contain', }}
                                source={require('../../assets/images/registermember2.png')}/>
                        </View>
                        <View style={{width: '100%', marginTop: 5,}}>
                            <BpkTextInput 
                                label={Strings.RegisterForm1.text1}
                                style={{marginBottom: 3, marginLeft: 20, marginRight: 20, }}
                                onChangeText={(text) => this.setState({inputidcardname: text})}
                                value={this.state.inputidcardname} />
                        </View>
                        <View style={{width: '100%', marginTop: 5,}}>
                            <BpkTextInput 
                                label={Strings.RegisterForm1.text2}
                                style={{marginBottom: 3, marginLeft: 20, marginRight: 20, }}
                                onChangeText={(text) => this.setState({inputcardname: text})}
                                value={this.state.inputcardname} />
                        </View>
                        <View style={{width: '100%', marginTop: 5,}}>
                            <BpkTextInput 
                                label={Strings.RegisterForm1.text3}
                                style={{marginBottom: 3, marginLeft: 20, marginRight: 20, }}
                                onChangeText={(text) => this.setState({inputidcardnumber: text})}
                                value={this.state.inputidcardnumber} />
                        </View>
                        <View style={{width: '100%', marginTop: 5,}}>
                            <BpkTextInput 
                                label={Strings.RegisterForm1.text4}
                                style={{marginBottom: 3, marginLeft: 20, marginRight: 20,  }}
                                onChangeText={(text) => this.setState({inputphonenumber: text})}
                                value={this.state.inputphonenumber} />
                        </View>
                        <View style={{width: '100%', marginTop: 5,}}>
                            <BpkTextInput 
                                label={Strings.RegisterForm1.text5}
                                style={{marginBottom: 3, marginLeft: 20, marginRight: 20,  }}
                                onChangeText={(text) => this.setState({inputemail: text})}
                                value={this.state.inputemail} />
                        </View>
                        <View style={{width: '100%', marginTop: 5,}}>
                            <BpkTextInput 
                                label={Strings.RegisterForm1.text6}
                                style={{marginBottom: 3, marginLeft: 20, marginRight: 20,  }}
                                onChangeText={(text) => this.setState({inputplaceofbirth: text})}
                                value={this.state.inputplaceofbirth} />
                        </View>
                        <View style={{width: '100%', marginTop: 25,}}>
                            {/* date of birth */}
                            <TouchableOpacity onPress={ this._showDateTimePicker } style={{flexDirection: 'column', marginLeft: 20, marginRight: 20,
                                flex: 1, backgroundColor: '#ffffff', }}>
                                <Text style={styles.input} >{this.state.inputdob}</Text>
                                <Divider style={{ height: 1, backgroundColor: '#efefef', width: '100%', marginTop: 10, }} />
                            </TouchableOpacity>
                        </View>
                        <View style={{width: '100%', marginTop: 5,}}>
                            <BpkTextInput 
                                label={Strings.RegisterForm1.text8}
                                style={{marginBottom: 3, marginLeft: 20, marginRight: 20, }}
                                onChangeText={(text) => this.setState({inputaddress: text})}
                                value={this.state.inputaddress} />
                        </View>
                        <View style={{width: '100%', marginTop: 5, }}>
                            <BpkTextInput 
                                label={Strings.RegisterForm1.text9}
                                style={{marginBottom: 3, marginLeft: 20, marginRight: 20, }}
                                onChangeText={(text) => this.setState({inputcity: text})}
                                value={this.state.inputcity} />
                        </View>
                        <View style={{width: '100%', marginTop: 5,}}>
                            <BpkTextInput 
                                label={Strings.RegisterForm1.text10}
                                style={{marginBottom: 3, marginLeft: 20, marginRight: 20, }}
                                onChangeText={(text) => this.setState({inputpostalcode: text})}
                                value={this.state.inputpostalcode} />
                        </View>
                        <Button 
                            onPress={ gotoForm2 }
                            buttonStyle={{backgroundColor: Colors.button, borderRadius: 5, marginTop: 10, marginLeft: 5, marginRight: 5, marginBottom: 20, height: 50, }}
                            title={Strings.RegisterForm1.continuebutton}/>
                        <DateTimePicker
                            isVisible={this.state.isDateTimePickerVisible}
                            onConfirm={this._handleDatePicked}
                            onCancel={this._hideDateTimePicker}
                        />
                    </View>
                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({

    safeAreaView: {
        flex: 1,
    },

    containerScroll:
    {
        flex: 1,
        backgroundColor: '#ffffff',
    },

    container: {
        flex: 1,
        flexDirection: 'column',
    },
});


{/* Circle Image <View style={{width: 120,
            height: 120,
            borderRadius: 120/2,
            borderWidth: 1,
            borderColor: '#999',
            backgroundColor: '#fff',
            justifyContent: 'center',
            alignContent: 'center',
            marginTop: 15, }}>
        <View style={{flex:1, justifyContent:'center', 
            alignContent: 'center', alignItems: 'center',}}>
            <Image style={{width: 80, height: 80, resizeMode: 'contain', }}
                source={require('../../assets/images/golfer.png')}/>
        </View>
    </View> */}