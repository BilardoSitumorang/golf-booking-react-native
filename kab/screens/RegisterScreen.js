import React from 'react';
import {
    StyleSheet, 
    Text, 
    View,
    Image,
    TextInput,
    TouchableOpacity,
    ActivityIndicator,
    ScrollView,
    AsyncStorage,
    SafeAreaView,
} from 'react-native';
import { Button, } from 'react-native-elements';
import Strings from '../constants/Strings';
import Colors from '../constants/Colors';
import { postRegister, getReferralCode } from '../API/UserService';
import BpkTextInput from 'react-native-bpk-component-text-input';
import Utils from './Utils';

export default class RegisterScreen extends React.Component {
    static navigationOptions = {
        title: '',
        header: null,
    };

    constructor(props)
    {
        super(props);
        this.state = {
            isLoading: false,
            email: '',
            pwd: '',
            confirmpwd: '',
            promotioncode: '',
        };
    }

    render()
    {
        if (this.state.isLoading) {
            return (
                <View style={{flex: 1, paddingTop: 20}}>
                    <ActivityIndicator />
                </View>
            );
        }

        const gotoLogin = () => {
            this.props.navigation.navigate('SignIn')
        }

        const gotoContinue = () => {
            if(this.state.email.trim() == "")
            {
                Utils.errAlert("Error Message", Strings.Register.errmsg1);
            }
            else if(this.state.pwd.trim() == "")
            {
                Utils.errAlert("Error Message", Strings.Register.errmsg2);
            }
            else if(this.state.confirmpwd.trim() == "")
            {
                Utils.errAlert("Error Message", Strings.Register.errmsg3);
            }
            else if(this.state.pwd.trim() !== this.state.confirmpwd)
            {
                Utils.errAlert("Error Message", Strings.Register.errmsg4);
            }
            else
            {
                postRegister(this.state.email, this.state.pwd, this.state.confirmpwd, this.state.promotioncode).then(async (json) =>
                {
                    if (json.status === 200) {
                        getReferralCode(this.state.email).then(async (referraljson) =>{
                            await AsyncStorage.setItem("Referral", referraljson);
                            await AsyncStorage.setItem("Username", this.state.email);
                            this.props.navigation.navigate('RegisterLinkQMember');
                        });
                    } else {
                        Utils.errAlert("Error Message", json.message);
                    }
                });
            }
        }

        return (
            <SafeAreaView style={styles.safeAreaView}>
                <ScrollView style={styles.containerScroll}>
                    <View style={styles.container}>
                        <Image source={require('../assets/images/logo_qaccess.png')} style={styles.image}/>
                        <View style={{width: '100%', marginTop: 5,}}>
                            <BpkTextInput 
                                label={Strings.Register.emailplaceholder}
                                style={{marginBottom: 3, marginLeft: 20, marginRight: 20, }}
                                returnKeyLabel = {"next"}
                                onChangeText={(text) => this.setState({email: text})}
                                value={this.state.email} />
                        </View>
                        <View style={{width: '100%', marginTop: 5,}}>
                            <BpkTextInput 
                                label={Strings.Register.passwordplaceholder}
                                secureTextEntry
                                style={{marginBottom: 3, marginLeft: 20, marginRight: 20, }}
                                onChangeText={(text) => this.setState({pwd: text})}
                                value={this.state.pwd} />
                        </View>
                        <View style={{width: '100%', marginTop: 5,}}>
                            <BpkTextInput 
                                label={Strings.Register.confirmpasswordplaceholder}
                                secureTextEntry
                                style={{marginBottom: 3, marginLeft: 20, marginRight: 20, }}
                                onChangeText={(text) => this.setState({confirmpwd: text})}
                                value={this.state.confirmpwd} />
                        </View>
                        <View style={{width: '100%', marginTop: 5,}}>
                            <BpkTextInput 
                                label={Strings.Register.promotioncodeplaceholder}
                                style={{marginBottom: 3, marginLeft: 20, marginRight: 20, }}
                                onChangeText={(text) => this.setState({promotioncode: text})}
                                value={this.state.promotioncode} />
                        </View>
                        <View style={{width: '95%', marginTop: 10,}}>
                            <Button 
                                onPress={ gotoContinue }
                                buttonStyle={{backgroundColor: Colors.button, borderRadius: 5, marginTop: 20, height: 50, }}
                                title={Strings.Register.signupbutton}/>
                        </View>
                        <View style={styles.textcontainer}>
                            <Text style={styles.text}>{Strings.Register.text1}</Text>
                            <TouchableOpacity onPress={ gotoLogin }>
                                <Text style={styles.textsignup} >{Strings.Register.loginbutton}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>
            </SafeAreaView>
            
        );
    }
}
        
const styles = StyleSheet.create({
    safeAreaView: {
        flex: 1
    },

    containerScroll:
    {
        flex: 1,
        backgroundColor: Colors.backgroundColor,
    },

    container: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
    },

    image: {
        height: 120,
        width: 120,
        marginTop: 70,
    },

    textcontainer: {
        flexDirection: 'row',
    },

    text: {
        fontSize: 12,
        textAlign: 'right',
        marginTop: 15,
        color: Colors.text,
    },

    textsignup: {
        fontSize: 12,
        textAlign: 'left',
        marginTop: 15,
        marginLeft: 5,
        color: Colors.button,
    },

    textforgot: {
        width: '90%',
        fontSize: 14,
        textAlign: 'right',
        marginTop: 10,
        marginRight: 10,
        color: Colors.button,
    },

    button: {
        width: '100%'
    },

    inputContainer: {
        borderLeftWidth: 1,
        borderRightWidth: 1,
        borderTopWidth: 1,
        borderBottomWidth: 1,
        height: 35,
        marginLeft: 10,
        marginRight: 10,
        marginTop: 60,
        borderRadius: 2,
        width: '90%',
    },

    inputContainer2: {
        borderLeftWidth: 1,
        borderRightWidth: 1,
        borderTopWidth: 1,
        borderBottomWidth: 1,
        height: 35,
        marginLeft: 10,
        marginRight: 10,
        marginTop: 20,
        borderRadius: 2,
        width: '90%',
    },

    input: {
        height: 35,
        backgroundColor: Colors.backgroundColor,
        padding: 10,
        fontSize: 16,
    },
});