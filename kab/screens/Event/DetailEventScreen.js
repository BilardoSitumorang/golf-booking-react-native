import React from 'react';
import {
    Image,
    Platform,
    ScrollView,
    StyleSheet,
    SafeAreaView,
    TouchableOpacity,
    ActivityIndicator,
    Dimensions,
    ListView,
    Text,
    View,
} from 'react-native';
import Colors from '../../constants/Colors';
import Strings from '../../constants/Strings';
import { Button, } from 'react-native-elements';
import Utils from '../Utils';
import { getOptions,  } from '../../API/OptionService';
import { OptionIntEnum, TipePemainIntEnum } from '../../constants/Enums';
import { findEvent, getEventPrices, } from '../../API/EventService';
import { getStringToDoubleDate,  } from '../../API/SettingService';
import moment from 'moment';
import clsSetting from '../../API/clsSetting';
import MapView from 'react-native-maps';

const dimensions = Dimensions.get('window');
const imageWidth = dimensions.width;
const imageHeight = Math.round(dimensions.width / 2);

export default class DetailEventScreen extends React.Component {
    static navigationOptions = {
        title: 'Detail Event',
        headerTintColor: '#ffffff', 
        headerStyle: {
            backgroundColor: '#93b843'
        },
    };

    constructor(props){
        super(props);

        this.params = this.props.navigation.state.params;

        this.state = {
            isLoading: true,
            username: '',

            idevent: this.params.IdEvent,
            type: this.params.Type,

            publishrate: 0,
            idpublish: 0,
            clubrate: 0,
            idclub: 0,
            qrate: 0,
            idqrate: 0,

            title: '',
            description: '',
            category: '',
            timestamp: '',
            eventdate: '',
            includes: '',
            excludes: '',
            eventimage: '',

            latitude: 0,
            longitude: 0,
        }
    }

    componentDidMount() {
        var Enumerable = require('../linq');
        getOptions().then((jsonOption) => 
        {
            this.setState({
                eventOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionEvent).firstOrDefault(),
                maintenanceOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionMaintenancePeriod).firstOrDefault()
            });
            
            findEvent(this.state.idevent).then((jsonEvent) =>
            {
                this.setState({
                    title: jsonEvent.Name,
                    description: jsonEvent.Description,
                    category: jsonEvent.Category.Name,
                    timestamp: jsonEvent.TimeStart != '' ? jsonEvent.TimeStart + ' - ' + jsonEvent.TimeEnd : '',
                    eventimage: clsSetting.HomeUrl + jsonEvent.Thumbnail,
                    includes: jsonEvent.Includes,
                    excludes: jsonEvent.Excludes,

                    latitude: jsonEvent.Latitude,
                    longitude: jsonEvent.Longitude,
                });
                this.setState({
                    eventdate: moment(Date.parse(jsonEvent.Date)).format('dddd, DD MMM YYYY'),
                });

                if(this.state.type == 1){
                    getEventPrices(this.state.idevent, '').then((jsonEvent) => 
                    {
                        let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
                        this.setState({
                            isLoading: false,
                            eventPriceList: ds.cloneWithRows(jsonEvent),
                        });

                        var i;
                        for(i = 0; i < jsonEvent.length; i++)
                        {
                            var entrans = jsonEvent[i].Entrants.split(',');
                            if(entrans == undefined)
                            {
                                entrans = jsonEvent[i].Entrants;
                                this.fn_pricing(entrans, jsonEvent[i].Id, jsonEvent[i].Price);
                            }
                            else{
                                var j;
                                for(j = 0; j < entrans.length; j++)
                                {
                                    this.fn_pricing(entrans[j], jsonEvent[i].Id, jsonEvent[i].Price);
                                }
                            }
                        }
                    })
                }
                else{
                    this.setState({
                        isLoading: false,
                    });
                }
            })
        })
        .catch((error) => {
            console.error(error);
            this.setState({isLoading: false, });
        });
        
    }

    fn_pricing = (_entrans, _id, _price) =>
    {
        if(Number.parseInt(_entrans) == TipePemainIntEnum.Publish || Number.parseInt(_entrans) == TipePemainIntEnum.Visitor){
            this.setState({
                idpublish: _id,
                publishrate: _price,
            });
        }
        else if(Number.parseInt(_entrans) == TipePemainIntEnum.Club){
            this.setState({
                idclub: _id,
                clubrate: _price,
            });
        }
        else if(Number.parseInt(_entrans) == TipePemainIntEnum.QMember){
            this.setState({
                idqrate: _id,
                qrate: _price,
            });
        }
    }

    render() {
        const continueclick = () =>
        {
            var platform = Platform.OS === 'ios' ? this.state.eventOption.iOS : this.state.eventOption.Android;
            if(Utils.getMaintenanceOption(this.state.maintenanceOption)){
                Utils.alertBackToMain(this.state.maintenanceOption.Note, this.props);
            }
            else if(!platform) {
                Utils.errAlert("QAccess Alert", this.state.eventOption.Note);
            }
            else {
                if(this.state.type == 1){
                    this.props.navigation.navigate('BookEvent', {IdEvent: this.state.idevent, Location: this.state.title, Calendar: this.state.eventdate, PublishRate: this.state.publishrate, IdPublish: this.state.idpublish,
                        ClubRate: this.state.clubrate, IdClub: this.state.idclub, QRate: this.state.qrate, IdQRate: this.state.idqrate});
                }
                else{
                    this.props.navigation.navigate('ListEventPrice', {IdEvent: this.state.idevent, Location: this.state.title, Calendar: this.state.eventdate, PublishRate: this.state.publishrate, IdPublish: this.state.idpublish,
                        ClubRate: this.state.clubrate, IdClub: this.state.idclub, QRate: this.state.qrate, IdQRate: this.state.idqrate});
                }
            }
        }

        if (this.state.isLoading) {
            return (
                <View style={{flex: 1, paddingTop: 20}}>
                    <ActivityIndicator />
                </View>
            );
        }

        return (
            <SafeAreaView style={styles.safeAreaView}>
                <ScrollView style={styles.containerScroll}>
                    <View style={styles.container}>
                        <Image style={styles.image} 
                            source={{ uri: this.state.eventimage }}/>
                        <View style={{backgroundColor: '#fff', paddingTop: 10, paddingBottom: 10, paddingLeft: 15, paddingRight: 15, }}>
                            <Text style={styles.textTitle}>{this.state.title}</Text>
                        </View>
                        <Text style={styles.textKet}>{Strings.DetailEvent.text1}</Text>
                        <View style={{backgroundColor: '#fff', paddingTop: 10, paddingBottom: 10, paddingLeft: 15, paddingRight: 15, }}>
                            <Text style={styles.text}>{this.state.description}</Text>
                        </View>
                        {
                            Utils.renderIf(this.state.type == 1, 
                                <Text style={styles.textKet}>{Strings.DetailEvent.text6}</Text>
                            )
                        }
                        {
                            Utils.renderIf(this.state.type == 1,
                                <View style={{backgroundColor: '#fff', paddingTop: 10, paddingBottom: 10, paddingLeft: 15, paddingRight: 15,}}>
                                    {
                                        Utils.renderIf(this.state.publishrate != 0, 
                                            <View style={{flex: 1, flexDirection: 'row', }}>
                                                <Text style={styles.textLeft} >{Strings.DetailEvent.text7}</Text>
                                                <Text style={styles.textRight} >{'Rp. ' + Utils.currencyfunc(Math.round(this.state.publishrate*100)/100)}</Text>
                                            </View>
                                        )
                                    }
                                    {
                                        Utils.renderIf(this.state.clubrate != 0,
                                            <View style={{flex: 1, flexDirection: 'row', marginTop: 5, }}>
                                                <Text style={styles.textLeft} >{Strings.DetailEvent.text9}</Text>
                                                <Text style={styles.textRight} >{'Rp. ' + Utils.currencyfunc(Math.round(this.state.clubrate*100)/100)}</Text>
                                            </View>
                                        )
                                    }
                                    {
                                        Utils.renderIf(this.state.qrate != 0,
                                            <View style={{flex: 1, flexDirection: 'row', marginTop: 5, }}>
                                                <Text style={styles.textLeft} >{Strings.DetailEvent.text10}</Text>
                                                <Text style={styles.textRight} >{'Rp. ' + Utils.currencyfunc(Math.round(this.state.qrate*100)/100)}</Text>
                                            </View>
                                        )
                                    }
                                </View>
                            )
                        }
                        <Text style={styles.textKet}>{Strings.DetailEvent.text2}</Text>
                        <View style={{backgroundColor: '#fff', paddingTop: 10, paddingBottom: 10, paddingLeft: 15, paddingRight: 15, }}>
                            <View style={{flex: 1, flexDirection: 'row', }}>
                                <Image style={styles.imageicon} 
                                    source={require('../../assets/images/ic_tag.png')}/>
                                <Text style={styles.textLeft}>{this.state.category}</Text>
                            </View>
                            <View style={{flex: 1, flexDirection: 'row', }}>
                                <Image style={styles.imageicon} 
                                    source={require('../../assets/images/ic_timer.png')}/>
                                <Text style={[styles.textLeft, {marginRight: 15,}]}>{this.state.eventdate + ", " + this.state.timestamp}</Text>
                            </View>
                            <Text style={styles.textTitle}>{Strings.DetailEvent.text3}</Text>
                            <Text style={styles.text}>{this.state.includes}</Text>
                            <Text style={styles.textTitle}>{Strings.DetailEvent.text4}</Text>
                            <Text style={styles.text}>{this.state.excludes}</Text>
                        </View>
                        <Text style={styles.textKet}>{Strings.DetailEvent.text5}</Text>
                        <View style={styles.containerMaps}>
                            <MapView // remove if not using Google Maps
                                style={styles.map}
                                region={{
                                    latitude: this.state.latitude,
                                    longitude: this.state.longitude,
                                    latitudeDelta: 0.015,
                                    longitudeDelta: 0.0121,
                                }}
                            ></MapView>
                        </View>
                        <Button 
                            onPress={ continueclick }
                            buttonStyle={{backgroundColor: Colors.button, borderRadius: 5, marginTop: 10, marginLeft: 5, marginRight: 5, marginBottom: 20, height: 50, }}
                            title={Strings.DetailEvent.findbutton}/>
                    </View>
                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({

    safeAreaView: {
        flex: 1,
    },

    containerScroll:
    {
        flex: 1,
        backgroundColor: '#f2f2f2',
    },

    container: {
        flex: 1,
        flexDirection: 'column',
    },

    image: {
        width: imageWidth,
        height: imageHeight,
        resizeMode: 'contain',
    },

    imageicon: {
        width: 20,
        height: 20,
        marginRight: 5,
        resizeMode: 'contain',
    },

    containerMaps: {
        height: 400,
        width: 400,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },

    map: {
        ...StyleSheet.absoluteFillObject,
    },

    textKet: {
        fontSize: 18,
        marginTop: 10,
        marginLeft: 15,
        marginBottom: 5,
        color: Colors.text,
        fontWeight: 'bold',
    },

    textTitle: {
        fontSize: 18,
        color: Colors.text,
        marginTop: 5,
        fontWeight: 'bold',
    },

    text: {
        fontSize: 13,
        marginTop: 5,
        color: Colors.text,
    },

    textLeft: {
        fontSize: 14,
        marginTop: 5,
        color: Colors.text,
    },

    textRight: {
        flex: 1,
        fontSize: 14,
        marginTop: 5,
        color: Colors.button,
        textAlign: 'right',
    },
});
