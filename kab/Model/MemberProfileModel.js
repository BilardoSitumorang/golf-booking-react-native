import clsSetting from "../API/clsSetting";

export default class MemberProfileModel
{
    constructor(_email, _memberprofile)
    {
        this.Email = _email;
        this.MemberProfile = _memberprofile;
    }
}

exports.MemberProfileModelDetail 
{    
    constructor(_id, _nomember, _name, _cardname, _membercategory, _membertype, _gender, _birthdate, _address, 
        _city, _postalcode, _phone, _idd, _fax, _email, _pinbb, _ptname, _picture, _joindate, _startdate, _enddate )
    {
        this.Id = _id;
        this.NoMember = _nomember;
        this.Nama = _name;
        this.NamaKartu = _cardname;
        this.JenisMember = _membercategory;
        this.TipeMember = _membertype;
        this.JenisKelamin = _gender;
        this.TanggalLahir = _birthdate;
        this.Alamat = _address;
        this.Kota = _city;
        this.KodePos = _postalcode;
        this.Telp = _phone;
        this.Idd = _idd;
        this.Fax = _fax;
        this.Email = _email;
        this.PinBB = _pinb;
        this.NamaPT = _ptname;
        this.Foto = _picture;
        this.TanggalJoin = _joindate;
        this.TanggalBerlakuDari = _startdate;
        this.TanggalBerlakuSampai = _enddate;
    }
}