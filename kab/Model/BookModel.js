import clsSetting from "../API/clsSetting";

export default class BookModel
{
    constructor(_name, _code, _type, _card, _playername)
    {
        this.Nama = _name;
        this.Kode = _code;
        this.Tipe = _type;
        this.Kartu = _card;
        this.PlayerName = _playername;
    }
}