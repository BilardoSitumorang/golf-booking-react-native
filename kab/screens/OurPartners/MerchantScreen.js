import React from 'react';
import {
    StyleSheet, 
    Image,
    TouchableOpacity,
    View,
    ListView,
    ScrollView,
    SafeAreaView,
    ActivityIndicator,
} from 'react-native';
import Colors from '../../constants/Colors';
import { getMerchantByCategory } from '../../API/PartnershipService';
import Utils from '../Utils';
import clsSetting from '../../API/clsSetting';
import MerchantRowScreen from '../ListRow/MerchantScreenRow';

export default class MerchantScreen extends React.Component{
    static navigationOptions = {
        title: 'Merchant',
        headerTintColor: '#ffffff', 
        headerStyle: {
            backgroundColor: '#93b843',
        },
    };

    constructor(props)
    {
        super(props);

        this.params = this.props.navigation.state.params;

        this.state = {
            isLoading: true,
            name: this.params.Name,
        };
    }

    componentDidMount() {
        getMerchantByCategory(this.params.IdCat).then((json) =>
        {
            let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
            this.setState({
                isLoading: false,
                dataSource: ds.cloneWithRows(json),
            });
        })
        .catch((error) => {
            console.error(error);
            this.setState({ isLoading: false});
        });
    }

    GetItem (row) {
        //Selected row ListView
        this.props.navigation.navigate('DetailMerchant', {Row: row});
    }

    ListViewItemSeparator = () => {
        return (
            <View style={{ height: .5, width: "100%", backgroundColor: "#000", }} />
        );
    }

    render() {
        if (this.state.isLoading) {
            return (
                <View style={{flex: 1, paddingTop: 20}}>
                    <ActivityIndicator />
                </View>
            );
        }


        return (
            <SafeAreaView style={styles.safeAreaView}>
                <View style={styles.containerView}>
                    <ListView
                        dataSource={this.state.dataSource}
                        renderSeparator= {this.ListViewItemSeparator}
                        renderRow={(rowData) =>
                            <TouchableOpacity 
                                onPress={ this.GetItem.bind(this, rowData) } >
                                <MerchantRowScreen 
                                    image={{uri: rowData.Thumbnail !== "" ? clsSetting.HomeUrl + rowData.Thumbnail : clsSetting.HomeUrl + "images/no_image2.jpg"}}
                                    name={rowData.Name}
                                    />
                            </TouchableOpacity>
                        }
                    />
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.backgroundColor,
    },

    safeAreaView: {
        flex: 1
    },

    containerView: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
    },
});

