import React from 'react';
import {
    Image,
    Alert,
    Platform,
    ScrollView,
    StyleSheet,
    Text,
    FlatList,
    SafeAreaView,
    TouchableOpacity,
    ListView,
    ActivityIndicator,
    View,
} from 'react-native';
import Colors from '../../constants/Colors';
import Strings from '../../constants/Strings';
import {Button, Card} from 'react-native-elements';
import BookModel from '../../Model/BookModel';
import Utils from '../Utils';
import { getOptions,  } from '../../API/OptionService';
import { KartuStringEnum, OptionIntEnum } from '../../constants/Enums';
import BookReservationRowScreen from '../ListRow/BookReservationRowScreen';

export default class BookReservationScreen extends React.Component {
    static navigationOptions = {
        title: 'Golf Reservation',
        headerTintColor: '#ffffff', 
        headerStyle: {
            backgroundColor: '#93b843'
        },
    };

    constructor(props){
        super(props);

        this.params = this.props.navigation.state.params;

        this.state = {
            isLoading: true,
            username: '',

            publishQuota: this.params.PublishQuota,
            qQuota: this.params.QQuota,

            location: this.params.Location,
            locationseletected: this.params.LocationSelected,
            calendar: this.params.Calendar,
            session: this.params.Session,
            player: this.params.Player,
            playertype: this.params.PlayerType,
            
            courseimage: this.params.CourseImage,
            coursename: this.params.CourseName,
            courseaddress: this.params.CourseAddress,
            
            kerjasamalapangan: this.params.KerjasamaLapangan,
            containmandiri: false,

            playerInt: this.params.Player === undefined ? 1 : this.params.Player,
            arrName: [],
            arrPlayerName: [],
            arrType: [],
            arrKartu: [],
            bookModel: [],
            editedposition: -1,
        };
    }

    checkArrNameEmpty = () => {
        var returnTemp = false;
        var i;
        for(i = 0; i < this.state.arrName.length; i++)
        {
            if(this.state.arrName[i] == ''){
                returnTemp = true;
            }
        }

        return returnTemp;
    }

    fn_SearchMandiriPlayer = () =>
    {
        var contain = false;

        var i;
        for (i = 0; i < this.state.arrType.length; i++)
        {
            if(this.state.arrType[i] === KartuStringEnum.Mandiri)
            {
                contain = true;
            }
        }

        this.setState({containmandiri : contain});
    }

    onSelect = data => {
        this.setState(data);
        let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.setState({
            dataSource: ds.cloneWithRows(this.state.bookModel),
        });
    };

    componentDidMount() {
        var sessionInt = this.state.session === undefined ? 1 : this.state.session === "Morning" ? 1 : 2;
        var Enumerable = require('../linq');

        getOptions().then((jsonOption) => 
        {
            this.setState({
                reservationOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionReservation).firstOrDefault(),
                maintenanceOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionMaintenancePeriod).firstOrDefault()
            });

            if(this.params.ArrName === undefined){
                var i;
                for(i = 0; i < this.state.playerInt; i++) {
                    this.state.arrName.push('');
                    this.state.arrPlayerName.push('');
                    this.state.arrType.push('');
                    this.state.arrKartu.push('');
                }
            }

            var j;
            for(j = 0; j < this.state.playerInt; j++) {
                this.state.bookModel.push(new BookModel(this.state.arrName[j], '', this.state.arrType[j], 
                    this.state.arrKartu[j], this.state.arrPlayerName[j]));
            }

            let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
            this.setState({
                isLoading: false,
                dataSource: ds.cloneWithRows(this.state.bookModel),
            });
        })
        .catch((error) => {
            console.error(error);
            this.setState({isLoading: false, });
        });
    }

    ListViewItemSeparator = () => {
        return (
            <View style={{ height: .5, width: "100%", backgroundColor: "#000", }} />
        );
    }

    GetItem (row, sectionID, rowID) {
        //Selected Row List
    }

    updatePlayer (rowID) {
        this.props.navigation.navigate('PlayerReservation', {Location: this.state.location, Calendar: this.state.calendar,
            Session: this.state.session, Player: this.state.player, PlayerType: this.state.playertype, 
            LocationSelected: this.state.locationseletected, KerjasamaLapangan: this.state.kerjasamalapangan,
            ContainMandiri: false, CourseName: this.state.coursename, CourseAddress: this.state.courseaddress,
            CourseImage: this.state.courseimage, PublishQouta: this.state.publishQuota, QQuota: this.state.qQuota,
            ArrName: JSON.stringify(this.state.arrName), ArrPlayerName: JSON.stringify(this.state.arrPlayerName), ArrKartu: JSON.stringify(this.state.arrKartu), 
            ArrType: JSON.stringify(this.state.arrType), BookModel: JSON.stringify(this.state.bookModel), EditedPosition: rowID, onSelect: this.onSelect});
    };

    deletePlayer (rowID) {
        this.state.arrName[rowID] = ('');
        this.state.arrPlayerName[rowID] = ('');
        this.state.arrType[rowID] = ('');
        this.state.arrKartu[rowID] = ('');
        this.state.bookModel[rowID] = (new BookModel(this.state.arrName[rowID], '', this.state.arrType[rowID], 
            this.state.arrKartu[rowID], this.state.arrPlayerName[rowID]));
        let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.setState({
            dataSource: ds.cloneWithRows(this.state.bookModel),
        });
    };
    
    render() {

        if (this.state.isLoading) {
            return (
                <View style={{flex: 1, paddingTop: 20}}>
                    <ActivityIndicator />
                </View>
            );
        }

        const gotoReview = () => {
            var platform = Platform.OS === 'ios' ? this.state.reservationOption.iOS : this.state.reservationOption.Android;
            if(Utils.getMaintenanceOption(this.state.maintenanceOption)){
                Utils.alertBackToMain(this.state.maintenanceOption.Note, this.props);
            }
            else if(!platform) {
                Utils.errAlert("QAccess Alert", this.state.reservationOption.Note);
            }
            else 
            {
                var boolarrnameempty = this.checkArrNameEmpty();
                console.log('boolean', boolarrnameempty);
                if(boolarrnameempty){
                    Utils.errAlert("Error Message", Strings.BookReservation.errmsg1);
                }
                else if(this.state.kerjasamalapangan === false && this.fn_SearchMandiriPlayer())
                {
                    Utils.errAlert("Error Message", Strings.BookReservation.errmsg2);
                }
                else
                {
                    Alert.alert(
                        "Confirm Booking",
                        Strings.BookReservation.msgdialog,
                        [
                            { text: 'YES', onPress: () => {
                                this.props.navigation.navigate('ReviewReservation', {Location: this.state.location, Calendar: this.state.calendar,
                                    Session: this.state.session, Player: this.state.player, PlayerType: this.state.playertype, 
                                    LocationSelected: this.state.locationseletected, KerjasamaLapangan: this.state.kerjasamalapangan,
                                    ContainMandiri: this.state.containmandiri, CourseName: this.state.coursename, CourseAddress: this.state.courseaddress,
                                    CourseImage: this.state.courseimage, PublishQouta: this.state.publishQuota, QQuota: this.state.qQuota,
                                    ArrName: JSON.stringify(this.state.arrName), ArrPlayerName: JSON.stringify(this.state.arrPlayerName), ArrKartu: JSON.stringify(this.state.arrKartu), 
                                    ArrType: JSON.stringify(this.state.arrType), BookModel: JSON.stringify(this.state.bookModel)});
                            }},
                            { text: 'NO', onPress: () => console.log('onpress')}
                        ],
                        { cancelable: false }
                    )
                }
                
            }
        }

        return (
            <SafeAreaView style={styles.safeAreaView}>
                <ScrollView style={styles.containerScroll}>
                    <View style={styles.container}>
                        <Text style={styles.textKet} >{Strings.BookReservation.text1}</Text>
                        <Card style={styles.card}>
                            <View style={{flex: 1, flexDirection: 'row',}}>
                                <Image style={styles.image} 
                                    source={require('../../assets/images/location.png')}/>
                                <Text style={styles.textLocation}>{this.state.coursename}</Text>
                            </View>
                            <View style={{flex: 1, flexDirection: 'row', marginTop: 5, }}>
                                <Image style={styles.image} 
                                    source={require('../../assets/images/calendar.png')}/>
                                <Text style={styles.text}>{this.state.calendar}</Text>
                                <Text style={styles.textSession}>{this.state.session}</Text>
                            </View>
                        </Card>
                        <Text style={styles.textKet} >{Strings.BookReservation.text2}</Text>
                        <ListView
                            dataSource={this.state.dataSource}
                            renderSeparator= {this.ListViewItemSeparator}
                            renderRow={(rowData, sectionID, rowID) =>
                                // <TouchableOpacity 
                                //     onPress={ this.GetItem.bind(this, rowData, sectionID, rowID) } >
                                //     <BookReservationRowScreen 
                                //         name={rowData.Nama == '' ? (Number.parseInt(rowID) + 1) + ". Fill in player details"  : rowData.Nama + "(" + rowData.PlayerName + ")"}
                                //         type={rowData.Tipe}
                                //         funcUpdate={this.updatePlayer(rowID)}/>
                                // </TouchableOpacity>
                                <View style={styles.containerList} >
                                    <View style={{flex: 1, flexDirection: 'row', marginLeft: 20, marginRight: 20, marginTop: 5, marginBottom: 5,}}>
                                        <View style={{flex: 3, flexDirection: 'column'}}>
                                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', }}>
                                                <Text style={styles.textLeft2}>{rowData.Nama == '' ? (Number.parseInt(rowID) + 1) + ". Fill in player details"  : rowData.Nama + "(" + rowData.PlayerName + ")"}</Text>
                                            </View>
                                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', }}>
                                                <Text style={[styles.textLeft2, {color: '#edb636'}]}>{rowData.Tipe}</Text>
                                            </View>
                                        </View>
                                        <View style={{flex: 1, flexDirection: 'row',}}>
                                            <View style={{justifyContent: 'center', alignItems: 'center', marginTop: 5, marginBottom: 5, marginLeft: 5, padding: 5, }}>
                                                <TouchableOpacity 
                                                    onPress={ this.updatePlayer.bind(this, rowID) }
                                                    style={{ justifyContent: 'center', alignItems: 'center',}}
                                                    >
                                                    <Image style={{height: 30, width: 30, tintColor: '#93b843'}} 
                                                        source={require('../../assets/images/ic_edit.png')}/>
                                                </TouchableOpacity>
                                            </View>
                                            <View style={{justifyContent: 'center', alignItems: 'center', marginTop: 5, marginBottom: 5, marginRight: 20, padding: 5, }}>
                                                <TouchableOpacity 
                                                    onPress={ this.deletePlayer.bind(this, rowID) }
                                                    style={{justifyContent: 'center', alignItems: 'center',}}
                                                    >
                                                    <Image style={{height: 30, width: 30, tintColor: '#b84242', marginLeft: 10, marginTop: 5, marginBottom: 5,}} 
                                                        source={require('../../assets/images/ic_delete.png')}/>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                    </View>
                                </View>
                            }
                        />
                        <Button 
                            onPress={ gotoReview }
                            buttonStyle={{backgroundColor: Colors.button, borderRadius: 5, marginTop: 20, marginBottom: 20, height: 50, }}
                            title={Strings.BookReservation.continuebutton}/>
                    </View>
                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({

    safeAreaView: {
        flex: 1,
    },

    containerScroll:
    {
        flex: 1,
        backgroundColor: '#f2f2f2',
    },

    container: {
        backgroundColor: '#f2f2f2',
        flex: 1,
        flexDirection: 'column',
    },

    image: {
        width: 25,
        height: 25,
    },

    textKet: {
        fontSize: 18,
        marginTop: 5,
        marginLeft: 15,
        marginBottom: 5,
        marginRight: 15,
        color: Colors.text,
        fontWeight: 'bold',
    },

    card: {
        backgroundColor: Colors.backgroundColor,
        flex: 1,
        flexDirection: 'column',
        marginLeft: 10,
        marginBottom: 5,
        marginRight: 10,
        padding: 10,
        elevation: 2,
        borderRadius: 5,
    },

    text: {
        fontSize: 15,
        marginLeft: 5,
        color: Colors.text,
    },

    textSession: {
        fontSize: 15,
        marginLeft: 5,
        color: '#edb636',
    },

    textLocation: {
        fontSize: 15,
        marginLeft: 5,
        color: '#93b843',
    },

    containerList: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#fff',
    },

    textLeft2: {
        fontSize: 14,
        marginTop: 5,
        color: Colors.text,
    },

    textRight2: {
        flex: 1,
        fontSize: 12,
        marginTop: 5,
        color: Colors.text,
    },
});
