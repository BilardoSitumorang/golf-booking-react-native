import React from 'react';
import {
    Image,
    Platform,
    ScrollView,
    StyleSheet,
    Text,
    AsyncStorage,
    FlatList,
    Alert,
    ListView,
    SafeAreaView,
    ActivityIndicator,
    TouchableOpacity,
    View,
} from 'react-native';
import Colors from '../../constants/Colors';
import Strings from '../../constants/Strings';
import {Button, Divider} from 'react-native-elements';
import { Card } from 'react-native-material-cards';
import { getOptions,  } from '../../API/OptionService';
import { findCourse,  } from '../../API/CourseService';
import Enums, { TipePemainStringEnum, StatusIntEnum, PaymentIntEnum, PlatformIntEnum, KartuStringEnum, OptionIntEnum, TransactionTypeIntEnum, KartuIntEnum } from '../../constants/Enums';
import Utils from '../Utils';
import ReviewRowPlayerScreen from '../ListRow/ReviewRowPlayerScreen';
import BookModel from '../../Model/BookModel';
import TransactionReservationModel from '../../Model/TransactionReservationModel';
import { getStringToDoubleDate,  } from '../../API/SettingService';
import TransactionDetailModel from '../../Model/TransactionDetailModel';
import { postCreateTrans,  } from '../../API/TransactionService';

export default class ReviewReservationScreen extends React.Component {
    static navigationOptions = {
        title: 'Review',
        headerTintColor: '#ffffff', 
        headerStyle: {
            backgroundColor: '#93b843'
        },
    };

    constructor(props){
        super(props);

        this.params = this.props.navigation.state.params;

        this.state = {
            isLoading: true,
            username: '',
            responsetransaction: '',
            iscourseratenull: '',
            total: 0,

            publishrates: "0",
            visitorrates: '0',
            clubrates: "0",
            qrates: "0",

            publishvisible: true,
            visitorvisible: true,
            clubvisible: true,
            qvisible: true,

            location: this.params.Location,
            locationseletected: this.params.LocationSelected,
            calendar: this.params.Calendar,
            session: this.params.Session,
            player: this.params.Player,
            playertype: this.params.PlayerType,
            
            courseimage: this.params.CourseImage,
            coursename: this.params.CourseName,
            courseaddress: this.params.CourseAddress,
            
            publishQuota: this.params.PublishQuota,
            qQuota: this.params.QQuota,

            kerjasamalapangan: this.params.KerjasamaLapangan,
            containmandiri: this.params.ContainMandiri,
            
            payArrType: [],

            arrName: JSON.parse(this.params.ArrName),
            arrPlayerName: JSON.parse(this.params.ArrPlayerName),
            arrType: JSON.parse(this.params.ArrType),
            arrKartu: JSON.parse(this.params.ArrKartu),
            bookModel: JSON.parse(this.params.BookModel),
        }
    }

    componentDidMount() {
        var sessionInt = this.state.session === undefined ? 1 : this.state.session === "Morning" ? 1 : 2;
        var playerInt = this.state.player === undefined ? 1 : this.state.player;
        var Enumerable = require('../linq');

        getOptions().then((jsonOption) => 
        {
            this.setState({
                reservationOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionReservation).firstOrDefault(),
                maintenanceOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionMaintenancePeriod).firstOrDefault()
            });

            getStringToDoubleDate(this.state.calendar).then((jsonCalendar) =>
            {
                findCourse(jsonCalendar.result, sessionInt, playerInt, this.state.locationseletected).then((jsonCourse) =>
                {
                    var publish = Enumerable.from(this.state.arrType).count(x => x == TipePemainStringEnum.Publish);
                    var visitor = Enumerable.from(this.state.arrType).count(x => x == TipePemainStringEnum.Visitor);
                    var club = Enumerable.from(this.state.arrType).count(x => x == "Mandiri");
                    var qrate = Enumerable.from(this.state.arrType).count(x => x == TipePemainStringEnum.QMember);

                    if(publish > 0)
                    {
                        this.setState({
                            publishrates: publish + " x " + Utils.currencyfunc(Math.round(jsonCourse.PublishRate*100)/100),
                            total: this.state.total + (publish * jsonCourse.PublishRate),
                            publishvisible: true, 
                        });
                        if(jsonCourse.PublishRate == 0)
                        {
                            this.setState({iscourseratenull: Strings.ReviewReservation.errmsg1});
                        }

                    }
                    else
                    {
                        this.setState({ publishvisible: false });
                    }

                    if(visitor > 0)
                    {
                        this.setState({
                            visitorrates: visitor + " x " + Utils.currencyfunc(Math.round(jsonCourse.VisitorRate*100)/100),
                            total: this.state.total + (visitor * jsonCourse.VisitorRate),
                            visitorvisible: true, 
                        });
                        if(jsonCourse.VisitorRate == 0)
                        {
                            this.setState({iscourseratenull: Strings.ReviewReservation.errmsg2});
                        }

                    }
                    else
                    {
                        this.setState({ visitorvisible: false });
                    }

                    if(club > 0)
                    {
                        this.setState({
                            clubrates: club + " x " + Utils.currencyfunc(Math.round(jsonCourse.MandiriRate*100)/100),
                            total: this.state.total + (club * jsonCourse.MandiriRate),
                            clubvisible: true, 
                        });
                        if(jsonCourse.MandiriRate == 0)
                        {
                            this.setState({iscourseratenull: Strings.ReviewReservation.errmsg3});
                        }

                    }
                    else
                    {
                        this.setState({ clubvisible: false });
                    }

                    if(qrate > 0)
                    {
                        this.setState({
                            qrates: qrate + " x " + Utils.currencyfunc(Math.round(jsonCourse.QGolfRate*100)/100),
                            total: this.state.total + (qrate * jsonCourse.QGolfRate),
                            qvisible: true, 
                        });
                        if(jsonCourse.QGolfRate == 0)
                        {
                            this.setState({iscourseratenull: Strings.ReviewReservation.errmsg4});
                        }

                    }
                    else
                    {
                        this.setState({ qvisible: false });
                    }

                    let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
                    this.setState({
                        dataSource: ds.cloneWithRows(this.state.bookModel),
                    });
                    
                    this.setState({isLoading: false, });

                    if(this.state.iscourseratenull !== '')
                    {
                        Alert.alert(
                            "Error Message",
                            this.state.iscourseratenull,
                            [
                              {text: 'OK', onPress: () => this.props.navigation.goBack('BookReservation', 
                                {
                                    Location: this.state.location, Calendar: this.state.calendar,
                                    Session: this.state.session, Player: this.state.player, PlayerType: this.state.playertype, 
                                    LocationSelected: this.state.locationseletected, KerjasamaLapangan: this.state.kerjasamalapangan,
                                    ContainMandiri: false, CourseName: this.state.coursename, CourseAddress: this.state.courseaddress,
                                    CourseImage: this.state.courseimage, PublishQouta: this.state.publishQuota, QQuota: this.state.qQuota,
                                    ArrName: this.state.arrName, ArrPlayerName: this.state.arrPlayerName, ArrKartu: this.state.arrKartu, 
                                    ArrType: this.state.arrType, 
                                }) },
                            ],
                            { cancelable: false }
                        )
                    }
                })
                .catch((error) => {
                    console.error(error);
                });
            })
        })
        .catch((error) => {
            console.error(error);
            this.setState({isLoading: false, });
        });
    }

    GetItem (row) {
        //Selected Row List
        
    }

    ListViewItemSeparator = () => {
        return (
            <View style={{ height: .5, width: "100%", backgroundColor: "#333", }} />
        );
    }

    render() {
        const gotoPay = async () => {
            this.setState({isLoading: true, })
            var platform = Platform.OS === 'ios' ? this.state.reservationOption.iOS : this.state.reservationOption.Android;
            if(Utils.getMaintenanceOption(this.state.maintenanceOption)){
                Utils.alertBackToMain(this.state.maintenanceOption.Note, this.props);
            }
            else if(!platform) {
                Utils.errAlert("QAccess Alert", this.state.reservationOption.Note);
            }
            else 
            {
                if(this.state.total != 0)
                {
                    getStringToDoubleDate(this.state.calendar).then(async (json) =>
                    {
                        var sessionInt = this.state.session === undefined ? 1 : this.state.session === "Morning" ? 1 : 2;
                        this.setState({ username : await AsyncStorage.getItem('Username')});
                        var platformMobile = Platform.OS === 'ios' ? PlatformIntEnum.IOs : PlatformIntEnum.Android;
                        var arrTransDetailModel = [];
                        var i;
                        for(i = 0; i < this.state.arrName.length; i++) {

                            var _titlePemain = '';
                            var _noMember = '';
                            var _namaPemain = '';
                            var _jenisMember = 0;
                            var _tipeMember = 0;
                            
                            if (this.state.arrType[i] === TipePemainStringEnum.Publish)
                            {
                                _noMember = '';
                                _namaPemain = this.state.arrPlayerName[i];
                            }
                            else if (this.state.arrType[i] == TipePemainStringEnum.QMember || this.state.arrType[i] == TipePemainStringEnum.Club || this.state.arrType[i] == KartuStringEnum.Mandiri)
                            {
                                _noMember = this.state.arrName[i];
                                _jenisMember = Number.parseInt(this.state.arrKartu[i]);
                                _namaPemain = this.state.arrPlayerName[i];
                            }
                            else
                            {
                                _jenisMember = 0;
                            }

                            if (this.state.arrType[i] == KartuStringEnum.Mandiri)
                            {
                                //arrType[i] = KartuEnum.Mandiri.GetDescription();
                                this.state.payArrType.push(TipePemainStringEnum.Club);
                            }
                            else
                            {
                                this.state.payArrType.push(this.state.arrType[i]);
                            }

                            _tipeMember = Enums.TipePemainEnumConvert(this.state.payArrType[i]);

                            arrTransDetailModel.push(new TransactionDetailModel(_noMember, _titlePemain, _tipeMember, _jenisMember, _namaPemain));
                        }

                        var transactionModel = new TransactionReservationModel(
                            this.state.locationseletected, json.result, sessionInt, '', StatusIntEnum.Book, PaymentIntEnum.NotSpecified,
                            platformMobile, this.state.username, arrTransDetailModel);

                        postCreateTrans(transactionModel).then(async (jsonTrans) =>
                        {
                            await AsyncStorage.setItem("BookingCode", jsonTrans.result);
                            this.props.navigation.navigate('Payment', {TransType: TransactionTypeIntEnum.Reservation, ContainMandiri: this.state.containmandiri});
                        })
                        .catch((error) => {
                            console.error(error);
                            this.setState({isLoading: false, });
                        });
                    })
                    
                }
            }
        }

        if (this.state.isLoading) {
            return (
                <View style={{flex: 1, paddingTop: 20}}>
                    <ActivityIndicator />
                </View>
            );
        }

        return (
            <SafeAreaView style={styles.safeAreaView}>
                <ScrollView style={styles.containerScroll}>
                    <View style={styles.container}>
                        <Card style={styles.cardNote}>
                            <Image source={require('../../assets/images/information.png')} 
                                style={{height: 20, width: 20, resizeMode: 'contain', }}/>
                            <Text style={styles.textNote} >{Strings.ReviewReservation.text1}</Text>
                        </Card>
                        <Text style={styles.textKet} >{Strings.ReviewReservation.text2}</Text>
                        <Card style={styles.card}>
                            <View style={{flex: 1, flexDirection: 'row',}}>
                                <Image source={require('../../assets/images/location.png')} 
                                    style={{height: 20, width: 20, resizeMode: 'contain', }}/>
                                <Text style={styles.textLocation} >{this.state.coursename}</Text>
                            </View>
                            <View style={{flex: 1, flexDirection: 'row', marginTop: 10, }}>
                                <Image source={require('../../assets/images/calendar.png')} 
                                    style={{height: 20, width: 20, resizeMode: 'contain', }}/>
                                <Text style={styles.text} >{this.state.calendar}</Text>
                                <Text style={styles.textSession} >{this.state.session}</Text>
                            </View>
                        </Card>
                        <Text style={styles.textKet} >{Strings.ReviewReservation.text3}</Text>
                        <View style={{flex: 1, backgroundColor: '#ffffff', marginTop: 10, marginBottom:10, 
                            paddingLeft: 15, paddingRight: 15, paddingBottom: 10}}>
                            {/* List Player */}
                            <ListView
                                dataSource={this.state.dataSource}
                                renderSeparator= {this.ListViewItemSeparator}
                                renderRow={(rowData) =>
                                    <TouchableOpacity 
                                        onPress={ this.GetItem.bind(this, rowData) } >
                                        <ReviewRowPlayerScreen 
                                            name={rowData.Nama}
                                            playername={rowData.PlayerName}
                                            type={rowData.Tipe}/>
                                    </TouchableOpacity>
                                }
                            />
                        </View>
                        <Text style={styles.textKet} >{Strings.ReviewReservation.text4}</Text>
                        <View style={{backgroundColor: '#ffffff', paddingLeft: 20, paddingRight: 20, paddingTop: 10, paddingBottom: 10,}}>
                            <View style={{flex: 1, flexDirection: 'column',}}>
                                {
                                    Utils.renderIf(this.state.publishvisible, 
                                        <View style={{flex: 1, flexDirection: 'row', }}>
                                            <Text style={styles.textLeft} >{Strings.ReviewReservation.text5}</Text>
                                            <Text style={styles.textRight} >{this.state.publishrates}</Text>
                                        </View>
                                    )
                                }
                                {
                                    Utils.renderIf(this.state.visitorvisible,
                                        <View style={{flex: 1, flexDirection: 'row', marginTop: 5, }}>
                                            <Text style={styles.textLeft} >{Strings.ReviewReservation.text6}</Text>
                                            <Text style={styles.textRight} >{this.state.visitorrates}</Text>
                                        </View>
                                    )
                                }
                                {
                                    Utils.renderIf(this.state.clubvisible,
                                        <View style={{flex: 1, flexDirection: 'row', marginTop: 5, }}>
                                            <Text style={styles.textLeft} >{Strings.ReviewReservation.text7}</Text>
                                            <Text style={styles.textRight} >{this.state.clubrates}</Text>
                                        </View>
                                    )
                                }
                                {
                                    Utils.renderIf(this.state.qvisible,
                                        <View style={{flex: 1, flexDirection: 'row', marginTop: 5, }}>
                                            <Text style={styles.textLeft} >{Strings.ReviewReservation.text8}</Text>
                                            <Text style={styles.textRight} >{this.state.qrates}</Text>
                                        </View>
                                    )
                                }
                                <Divider style={{ backgroundColor: '#414141', height: 1, width: '100%', marginTop: 5, }} />
                                <View style={{flex: 1, flexDirection: 'row', marginTop: 5, }}>
                                    <Text style={styles.textLeft} >{Strings.ReviewReservation.text9}</Text>
                                    <Text style={styles.textTotalRight} >{"Rp. " + Utils.currencyfunc(Math.round(this.state.total*100)/100)}</Text>
                                </View>
                            </View>
                        </View>
                        <Text style={styles.textKet} >{Strings.ReviewReservation.text10}</Text>
                        <View style={{backgroundColor: '#ffffff', paddingLeft: 20, paddingRight: 20, paddingTop: 10, paddingBottom: 10,}}>
                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', }}>
                                <Text style={styles.textLeft2}>{'• '}</Text>
                                <Text style={styles.textRight2}>{Strings.ReviewReservation.text11}</Text>
                            </View>
                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', }}>
                                <Text style={styles.textLeft2}>{'• '}</Text>
                                <Text style={styles.textRight2}>{Strings.ReviewReservation.text12}</Text>
                            </View>
                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', }}>
                                <Text style={styles.textLeft2}>{'• '}</Text>
                                <Text style={styles.textRight2}>{Strings.ReviewReservation.text13}</Text>
                            </View>
                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', }}>
                                <Text style={styles.textLeft2}>{'• '}</Text>
                                <Text style={styles.textRight2}>{Strings.ReviewReservation.text14}</Text>
                            </View>
                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', }}>
                            <Text style={styles.textLeft2}>{'• '}</Text>
                                <Text style={styles.textRight2}>{Strings.ReviewReservation.text15}</Text>
                            </View>
                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', }}>
                                <Text style={styles.textLeft2}>{'• '}</Text>
                                <Text style={styles.textRight2}>{Strings.ReviewReservation.text16}</Text>
                            </View>
                        </View>
                        <Button 
                            onPress= { gotoPay }
                            buttonStyle={{backgroundColor: Colors.button, borderRadius: 5, marginTop: 10, marginLeft: 5, marginRight: 5, marginBottom: 20, height: 50, }}
                            title={Strings.ReviewReservation.continuebutton}/>
                    </View>
                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({

    safeAreaView: {
        flex: 1,
        marginTop: 10,
    },

    containerScroll:
    {
        flex: 1,
        backgroundColor: '#efefef',
    },

    container: {
        flex: 1,
        flexDirection: 'column',
    },

    image: {
        height: 20,
        width: 20,
        resizeMode: 'contain',
    },

    text: {
        fontSize: 15,
        marginLeft: 10,
        color: Colors.text,
    },

    text2: {
        fontSize: 15,
        color: Colors.text,
    },

    textLeft2: {
        fontSize: 15,
        marginTop: 5,
        color: Colors.text,
        width: 10,
    },

    textRight2: {
        flex: 1,
        fontSize: 15,
        marginTop: 5,
        color: Colors.text,
    },

    textLeft: {
        flex: 1,
        fontSize: 15,
        marginRight: 10,
        textAlign: 'left',
        color: Colors.text,
    },

    textRight: {
        flex: 1,
        fontSize: 14,
        textAlign: 'right',
        color: Colors.text,
    },

    textTotalRight: {
        flex: 1,
        fontSize: 14,
        textAlign: 'right',
        color: '#93b843',
    },

    textKet: {
        fontSize: 18,
        marginTop: 5,
        marginLeft: 15,
        marginBottom: 5,
        marginRight: 15,
        color: Colors.text,
        fontWeight: 'bold',
    },

    textNote: {
        fontSize: 12,
        marginLeft: 3,
        color: Colors.text,
    },

    textSession: {
        fontSize: 15,
        marginLeft: 3,
        color: '#edb636',
    },

    textLocation: {
        fontSize: 15,
        marginLeft: 5,
        color: '#93b843',
    },

    cardNote: {
        backgroundColor: Colors.backgroundColor,
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 5,
        marginBottom: 5,
        marginLeft: 10,
        marginRight: 10,
        padding: 10,
        elevation: 2,
        borderRadius: 5,
    },

    card: {
        backgroundColor: Colors.backgroundColor,
        flex: 1,
        flexDirection: 'column',
        marginTop: 5,
        marginBottom: 5,
        marginLeft: 10,
        marginRight: 10,
        padding: 10,
        elevation: 2,
        borderRadius: 5,
    },
});
