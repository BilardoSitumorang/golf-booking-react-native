import React from 'react';
import { 
    StyleSheet, 
    Image,
    View,
    Text,
    Dimensions,
} from 'react-native';
import Colors from '../../constants/Colors';
import { Card, } from 'react-native-material-cards';

const dimensions = Dimensions.get('window');
const imageWidth = dimensions.width - 30;
const imageHeight = Math.round(dimensions.width / 2);

export default class DetailMerchantRowScreen extends React.Component {
    
    static navigationOptions = {
        title: '',
        header: null
    };

    render() {

        return (
            <Card style={styles.cardList} >
                <View style={{flex: 3,}}>
                    <Image style={styles.imageList} 
                            source={this.props.image}/>
                </View>
                <View style={{flex: 1, paddingLeft: 5, marginTop: 5, justifyContent: 'center', alignItems: 'center', 
                    flexDirection: 'row',}}>
                    <Text style={styles.text} >{this.props.name}</Text>
                </View>
            </Card>
        );
    }
}

const styles = StyleSheet.create({

    imageList: {
        flex: 1,
        height: imageHeight,
        width: imageWidth,
        resizeMode: 'cover',
    },

    cardList: {
        backgroundColor: Colors.backgroundColor,
        width: imageWidth,
        height: imageHeight + 30,
        marginLeft: 15,
        marginRight: 15,
        elevation: 2,
        borderRadius: 5,
    },

    text: {
        flex: 1,
        fontSize: 15,
        color: Colors.text,
        textAlign: 'left',
    },
});