import React from 'react';
import Moment from 'moment';
import clsSetting from '../API/clsSetting';

exports.getProvidesPulsa = (_number) => {
    return fetch(clsSetting.APIUrl + "mobilepulsa/getdbpricesbyprovider?number=" + _number + 
    "&type=else&key=" + clsSetting.KeyPulsa)
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        if (json.status === 200) {
            //console.log('json Provides Pulsa ', json.result);
            return json.result; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error Provides Pulsa ', e));  
}

exports.getProvidesData = (_number) => {
    return fetch(clsSetting.APIUrl + "mobilepulsa/getdbpricesbyprovider?number=" + _number + 
    "&type=data&key=" + clsSetting.KeyPulsa)
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        if (json.status === 200) {
            //console.log('json Provides Data ', json.result);
            return json.result; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error Provides Data ', e));  
}

exports.findPulsaTrans = (_bookingcode) => {
    return fetch(clsSetting.APIUrl + "transaksi/findmobilepulsatransactionbycode?kode=" + _bookingcode + 
    "&ApiSecretKey=" + clsSetting.ApiSecretKey)
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        if (json.status === 200) {
            return json.result; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error Find Pulsa Trans ', e));  
}

exports.postCreateTrans = (_pulsatransmodel) => {
    return fetch(clsSetting.APIUrl + "mobilepulsa/createtransaction?key=" + clsSetting.KeyPulsa , {  
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json; charset=utf-8',
        },
        body: JSON.stringify(_pulsatransmodel)
    })
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        if (json.status === 200) {
            return json.result; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error Create Pulsa ', e));  
}