import React from 'react';
import {
    StyleSheet, 
    Text, 
    View,
    Image,
    ScrollView,
    TouchableOpacity,
    SafeAreaView,
    AsyncStorage,
    ListView,
} from 'react-native';
import { Button, Divider, } from 'react-native-elements';
import Strings from '../constants/Strings';
import Colors from '../constants/Colors';
import { getIdd,  } from '../API/UserService';
import { SinglePickerMaterialDialog } from 'react-native-material-dialog';
import BpkTextInput from 'react-native-bpk-component-text-input';
import Utils from './Utils';
import { postRegisterPhone, } from '../API/UserService';

const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2});

export default class RegisterPhoneScreen extends React.Component {
    static navigationOptions = {
        title: '',
        header: null,
    };

    constructor(props)
    {
        super(props);
        this.state = {
            phone: '',
            iddArr: ["1", "60", "62", "65", "673"],
            email: '',
            scrolledSinglePickerVisible: false,
            scrolledSinglePickerSelectedItem: undefined,
            idd: '62',
        };
    }

    // componentDidMount() {
    //     getIdd().then((json) =>
    //     {
    //         this.setState({ iddArr: ds.cloneWithRows(json.result) });
    //         console.log('data parsing ', this.state.iddArr);
    //     });
    // }

    render()
    {
        const gotoRegisterOTP = async () => {
            if(this.state.idd === ''){
                Utils.errAlert("Error Message", Strings.RegisterPhone.errmsg1);
            }
            else if(this.state.phone === "")
            {
                Utils.errAlert("Error Message", Strings.RegisterPhone.errmsg2);
            }
            else
            {
                this.setState({ email : await AsyncStorage.getItem('Username')});
                postRegisterPhone(this.state.idd, this.state.phone ,this.state.email).then(async (json) =>
                {
                    if (json.status === 200) {
                        this.props.navigation.navigate('RegisterOTP', { Idd: this.state.idd, Phone: this.state.phone });
                    } else {
                        Utils.errAlert("Error Message", json.message);
                    }
                });
            }
        }

        return (
            <SafeAreaView style={styles.safeAreaView}>
                <ScrollView style={styles.containerScroll}>
                    <View style={styles.container}>
                        <Image source={require('../assets/images/logo_qaccess.png')} style={styles.image}/>
                        <Text style={styles.text}>{Strings.RegisterPhone.text1}</Text>
                        <View style={{width: '95%', marginTop: 5, flexDirection: 'row', alignItems: 'flex-end', justifyContent: 'center',}}>
                            <View style={{width: '25%',}}>
                                <TouchableOpacity onPress={() => this.setState({ scrolledSinglePickerVisible: true })
                                    } style={{flexDirection: 'column', marginLeft: 20, marginRight: 5, backgroundColor: '#ffffff', }}>
                                    <Text style={styles.inputText} >{this.state.scrolledSinglePickerSelectedItem === undefined
                                        ? this.state.idd : `${
                                            this.state.scrolledSinglePickerSelectedItem.label
                                        }`}</Text>
                                    <Divider style={{ height: 1, backgroundColor: '#efefef', width: '100%', marginTop: 10, marginBottom: 3, }} />
                                </TouchableOpacity>
                            </View>
                            <View style={{width: '75%',}}>
                                <BpkTextInput 
                                    label={Strings.RegisterPhone.numberplaceholder}
                                    style={{marginBottom: 3, marginLeft: 5, marginRight: 20, }}
                                    onChangeText={(text) => this.setState({phone: text})}
                                    value={this.state.phone} />
                            </View>
                        </View>
                        <View style={{width: '95%', marginTop: 10,}}>
                            <Button 
                                onPress={ gotoRegisterOTP }
                                buttonStyle={{backgroundColor: Colors.button, borderRadius: 5, marginTop: 20, height: 50, }}
                                title={Strings.RegisterPhone.continuebutton}/>
                        </View>
                        <SinglePickerMaterialDialog
                            title={'Pick Your Idd!'}
                            scrolled
                            items={this.state.iddArr.map((row, index) => ({ value: index, label: row }))}
                            visible={this.state.scrolledSinglePickerVisible}
                            selectedItem={this.state.scrolledSinglePickerSelectedItem}
                            onCancel={() => this.setState({ scrolledSinglePickerVisible: false })}
                            onOk={result => {
                                this.setState({ scrolledSinglePickerVisible: false });
                                this.setState({
                                    scrolledSinglePickerSelectedItem: result.selectedItem,
                                    idd: result.selectedItem.label,
                                });
                            }}
                        />
                    </View>
                </ScrollView>
            </SafeAreaView>  
        );
    }
}
        
const styles = StyleSheet.create({
    safeAreaView: {
        flex: 1
    },

    containerScroll:
    {
        flex: 1,
        backgroundColor: Colors.backgroundColor,
    },

    container: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
    },

    image: {
        height: 120,
        width: 120,
        marginTop: 120,
    },

    textcontainer: {
        flexDirection: 'row',
    },

    text: {
        fontSize: 12,
        textAlign: 'center',
        marginTop: 10,
        color: Colors.text,
    },

    textsignup: {
        fontSize: 12,
        textAlign: 'left',
        marginTop: 15,
        marginLeft: 5,
        color: Colors.button,
    },

    textforgot: {
        width: '90%',
        fontSize: 14,
        textAlign: 'right',
        marginTop: 10,
        marginRight: 10,
        color: Colors.button,
    },

    button: {
        width: '100%'
    },

    inputContainer: {
        borderLeftWidth: 1,
        borderRightWidth: 1,
        borderTopWidth: 1,
        borderBottomWidth: 1,
        height: 35,
        marginLeft: 10,
        marginRight: 10,
        marginTop: 60,
        borderRadius: 2,
        width: '90%',
    },

    inputContainer2: {
        borderLeftWidth: 1,
        borderRightWidth: 1,
        borderTopWidth: 1,
        borderBottomWidth: 1,
        height: 35,
        marginLeft: 10,
        marginRight: 10,
        marginTop: 20,
        borderRadius: 2,
        width: '90%',
    },

    input: {
        height: 35,
        backgroundColor: Colors.backgroundColor,
        padding: 10,
        fontSize: 16,
    },

    inputText: {
        flex: 1,
        backgroundColor: '#ffffff',
        color: '#414141',
        fontSize: 16,
    },
});