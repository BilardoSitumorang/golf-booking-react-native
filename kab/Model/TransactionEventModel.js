import clsSetting from "../API/clsSetting";

export default class TransactionEventModel
{
    constructor(_idevent, _user, _payment, _timezone, _platform, _pemain )
    {
        this.Id = _idevent;
        this.User = _user;
        this.Payment = _payment;
        this.Timezone = _timezone;
        this.Platform = _platform;
        this.Players = _pemain;
        this.ApiSecretKey = clsSetting.ApiSecretKey;
    }
}

