import React from 'react';
import Moment from 'moment';
import clsSetting from '../API/clsSetting';

exports.getEvents = () => {
    return fetch(clsSetting.APIUrl + "event/getbydate?date=" + Date.now + "&ApiSecretKey=" +
        clsSetting.ApiSecretKey)
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        if (json.status === 200) {
            return json.result; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error Events ', e));  
}

exports.findEvent = (_id) => {
    return fetch(clsSetting.APIUrl + "event/findeventquota?id=" + _id + "&ApiSecretKey=" +
        clsSetting.ApiSecretKey)
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        if (json.status === 200) {
            return json.result; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error Find Event ', e));
}

exports.getEventPrices = (_id, _username) => {
    return fetch(clsSetting.APIUrl + "event/GetEventQuotaPricingByEvent?id=" + _id + "&email=" + _username + "&ApiSecretKey=" +
        clsSetting.ApiSecretKey)
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        console.log('api Event Price ', clsSetting.APIUrl + "event/GetEventQuotaPricingByEvent?id=" + _id + "&email=" + _username + "&ApiSecretKey=" +
        clsSetting.ApiSecretKey);
        if (json.status === 200) {
            return json.result; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error Event Price ', e));
}

exports.getEvents2 = (_date) => {
    return fetch(clsSetting.APIUrl + "event/geteventquotabydate?date=" + _date + "&ApiSecretKey=" +
        clsSetting.ApiSecretKey)
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        if (json.status === 200) {
            return json.result; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error Events2 ', e));
}