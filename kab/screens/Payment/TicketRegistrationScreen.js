import React from 'react';
import {
    Image,
    ScrollView,
    StyleSheet,
    AsyncStorage,
    SafeAreaView,
    ActivityIndicator,
    Dimensions,
    Text,
    View,
    BackHandler,
} from 'react-native';
import Colors from '../../constants/Colors';
import Strings from '../../constants/Strings';
import { Divider, Icon, } from 'react-native-elements';
import { Card } from 'react-native-material-cards';
import { getRegisterQTicket, findTrans, } from '../../API/TransactionService';
import Utils from '../Utils';
import clsSetting from '../../API/clsSetting';
import Enums from '../../constants/Enums';

const dimensions = Dimensions.get('window');
const imageWidth = dimensions.width - 20;

export default class TicketRegistrationScreen extends React.Component {
    static navigationOptions = ({navigation}) => ({
        title: 'E-Ticket',
        headerTintColor: '#ffffff', 
        headerStyle: {
            backgroundColor: '#93b843'
        },
        headerLeft: (
            <Icon name="chevron-left"
                onPress={() => {
                    navigation.popToTop();
                }}
                size={35} color="white"/>
        ),
    });

    constructor(props){
        super(props);

        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);

        this.state = {
            isLoading: true,
            username: '',
            bookingcode: '',
            bookingdate: '',
            point: 0,
            conveniencefee: 0,
            total: 0,
            totaltransfer: 0,

            imgcard: '',
            membername: '',
            memberemail: '',
            membernotelp: '',
        };
    }

    handleBackButtonClick() {
        this.props.navigation.popToTop();
        return true;
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount()
    {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    async componentDidMount() {
        var Enumerable = require('../linq');
        this.setState({ bookingcode : await AsyncStorage.getItem('BookingCode')});
        this.setState({ username : await AsyncStorage.getItem('Username')});
            
        getRegisterQTicket(this.state.bookingcode).then((jsonTicket) =>
        {
            this.setState({
                conveniencefee: jsonTicket.ProcessingFee,
                total: jsonTicket.TotalDetail,
                point:  jsonTicket.Point,
                totaltransfer: jsonTicket.Total,

                membername: jsonTicket.CardName,
                memberemail: jsonTicket.Email,
                membernotelp: jsonTicket.MobilePhone,

                imgcard: jsonTicket.Class != null ? "{ uri: " + clsSetting.HomeUrl + jsonTicket.Class.Image + "}" : Utils.getImageKartu(jsonTicket.Card),
            });

            this.setState({isLoading: false, });
        })
        .catch((error) => {
            console.error(error);
            this.setState({isLoading: false, });
        });
    }

    render() {
        if (this.state.isLoading) {
            return (
                <View style={{flex: 1, paddingTop: 20}}>
                    <ActivityIndicator />
                </View>
            );
        }

        return (
            <SafeAreaView style={styles.safeAreaView}>
                <ScrollView style={styles.containerScroll}>
                    <View style={styles.container}>
                        <View style={{flex: 1, flexDirection: 'row', backgroundColor: '#efefef', paddingLeft: 15, 
                            paddingRight: 15, paddingTop: 10, paddingRight: 10, alignItems: 'center'}}>
                            <View style={{flex: 1, flexDirection: 'row', marginTop: 5, marginBottom: 5,}}>
                                <Text style={{fontSize: 15, color: Colors.text, fontWeight: 'bold',}} >
                                    {Strings.TicketRegistration.text7}</Text>
                                <Text style={{fontSize: 15, color: Colors.text, fontWeight: 'bold', }} >
                                    {this.state.bookingcode}</Text>
                            </View>
                            <Text style={{fontSize: 12, color: Colors.text, flex: 1, textAlign: 'right',}} >
                                {this.state.bookingdate}</Text>
                        </View>
                        <View style={{backgroundColor: '#efefef', flex: 1, 
                            marginLeft: 10, marginRight: 10, marginTop: 10, marginBottom: 5, borderRadius: 5}}>
                            <Image source={this.state.imgcard} 
                                style={{height: 220, width: imageWidth, resizeMode: 'stretch', }}/>
                        </View>
                        <Text style={styles.textKet} >{Strings.TicketRegistration.text1}</Text>
                        <Card style={styles.cardVertical}>
                            <View style={{flex: 1, flexDirection: 'row',}}>
                                <Text style={styles.text} >{this.state.membername}</Text>
                            </View>
                            <View style={{flex: 1, flexDirection: 'row', marginTop: 5, }}>
                                <Text style={styles.text} >{this.state.memberemail}</Text>
                            </View>
                            <View style={{flex: 1, flexDirection: 'row', marginTop: 5, }}>
                                <Text style={styles.text} >{this.state.membernotelp}</Text>
                            </View>
                        </Card>
                        <Text style={styles.textKet} >{Strings.TicketRegistration.text2}</Text>
                        <View style={styles.cardVertical}>
                            {
                                Utils.renderIf(this.state.conveniencefee != 0 || this.state.point != 0,
                                    <View style={{flex: 1, flexDirection: 'row', }}>
                                        <Text style={styles.textLeft2} >{Strings.TicketRegistration.text3}</Text>
                                        <Text style={styles.textRight} >{'Rp. ' + Utils.currencyfunc(Math.round(this.state.total*100)/100)}</Text>
                                    </View>
                                )
                            }
                            {
                                Utils.renderIf(this.state.point != 0,
                                    <View style={{flex: 1, flexDirection: 'row', marginTop: 5, }}>
                                        <Text style={styles.textLeft2} >{Strings.TicketRegistration.text4}</Text>
                                        <Text style={styles.textRight} >{'Rp. ' + Utils.currencyfunc(Math.round(this.state.point*100)/100)}</Text>
                                    </View>
                                )
                            }
                            {
                                Utils.renderIf(this.state.conveniencefee != 0,
                                    <View style={{flex: 1, flexDirection: 'row', marginTop: 5, }}>
                                        <Text style={styles.textLeft2} >{Strings.TicketRegistration.text5}</Text>
                                        <Text style={styles.textRight} >{'Rp. ' + Utils.currencyfunc(Math.round(this.state.conveniencefee*100)/100)}</Text>
                                    </View>
                                )
                            }
                            {
                                Utils.renderIf(this.state.conveniencefee != 0 || this.state.point != 0,
                                    <Divider style={{ backgroundColor: '#414141', height: 1, width: '100%', marginTop: 5, }} />
                                )
                            }
                            <View style={{flex: 1, flexDirection: 'row', marginTop: 5, }}>
                                <Text style={styles.textLeft2} >{Strings.TicketRegistration.text6}</Text>
                                <Text style={styles.textTotalRight} >{'Rp. ' + Utils.currencyfunc(Math.round(this.state.totaltransfer*100)/100)}</Text>
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({

    safeAreaView: {
        flex: 1,
    },

    containerScroll:
    {
        flex: 1,
        backgroundColor: '#efefef',
    },

    container: {
        flex: 1,
        flexDirection: 'column',
    },

    textKet: {
        fontSize: 18,
        marginTop: 10,
        marginLeft: 15,
        marginBottom: 5,
        color: Colors.text,
        fontWeight: 'bold',
    },

    text: {
        fontSize: 15,
        marginLeft: 10,
        color: Colors.text,
    },

    textLeft: {
        flex: 1,
        fontSize: 15,
        marginRight: 10,
        textAlign: 'left',
        color: Colors.text,
    },

    textRight: {
        flex: 1,
        fontSize: 15,
        textAlign: 'right',
        color: Colors.text,
    },

    textTotalRight: {
        flex: 1,
        fontSize: 15,
        textAlign: 'right',
        color: '#93b843',
    },

    cardVertical: {
        backgroundColor: Colors.backgroundColor,
        flex: 1,
        flexDirection: 'column',
        marginTop: 5,
        marginBottom: 5,
        marginLeft: 10,
        marginRight: 10,
        padding: 10,
        elevation: 2,
        borderRadius: 5,
    },

    cardHorizontal: {
        backgroundColor: Colors.backgroundColor,
        flex: 1,
        flexDirection: 'row',
        marginTop: 5,
        marginBottom: 5,
        marginLeft: 10,
        marginRight: 10,
        padding: 10,
        elevation: 2,
        borderRadius: 5,
    },
});


//textDecorationLine: 'underline'