import clsSetting from "../API/clsSetting";

export default class TransactionReservationModel
{
    constructor(_idcourse, _date, _session, _teetime, _status, _payment,
        _platform, _user, _pemain )
    {
        this.IdLapangan = _idcourse;
        this.Tanggal = _date;
        this.Session = _session;
        this.TeeTime = _teetime;
        this.Status = _status;
        this.Payment = _payment;
        this.Platform = _platform;
        this.User = _user;
        this.PemainViewModel = _pemain;
        this.ApiSecretKey = clsSetting.ApiSecretKey;
    }
}