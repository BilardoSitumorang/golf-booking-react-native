import React from 'react';
import {
    Image,
    Platform,
    StyleSheet,
    Text,
    TouchableOpacity,
    AsyncStorage,
    View,
    ScrollView,
    SafeAreaView,
} from 'react-native';

import Colors from '../../constants/Colors';
import Strings from '../../constants/Strings';
import { Button, Avatar, Divider, } from 'react-native-elements';
import { Card, CardTitle, CardContent, CardAction, CardButton, CardImage } from 'react-native-material-cards';

export default class PointScreen extends React.Component {
    static navigationOptions = {
        title: 'QPoint',
        headerTintColor: '#ffffff', 
        headerStyle: {
            backgroundColor: '#93b843'
        },
    };

    render() {
        return (
            <SafeAreaView style={styles.safeAreaView}>
                <ScrollView style={styles.container}>
                    <View style={styles.container}>
                        <Image source={require('../../assets/images/PointBannerMobile.jpg')} style={styles.image}/>
                        <View style={{backgroundColor: '#efefef', }}>
                            <Text style={styles.textTitle} >{Strings.Point.text1}</Text>
                        </View>
                        <Card style={styles.card} >
                            <Text style={styles.textMidAlign} >{Strings.Point.text2}</Text>
                        </Card>
                        <View style={{backgroundColor: '#efefef', }}>
                            <Text style={styles.textTitle} >{Strings.Point.text3}</Text>
                        </View>
                        <Card style={styles.card} >
                            <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: "center", marginTop: 5,}}>
                                <View >
                                    <Image style={{width: 30, height: 30,}} source={require('../../assets/images/fivepoint.png')}/>
                                </View>
                                <View style={{flex: 1, marginLeft: 5,}}>
                                    <Text style={styles.text} >{Strings.Point.text4}</Text>
                                </View>
                            </View>
                            <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: "center", marginTop: 5,}}>
                                <View >
                                    <Image style={{width: 30, height: 30,}} source={require('../../assets/images/golfer.png')}/>
                                </View>
                                <View style={{flex: 1, marginLeft: 5,}}>
                                    <Text style={styles.text} >{Strings.Point.text5}</Text>
                                </View>
                            </View>
                            <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: "center", marginTop: 5,}}>
                                <View >
                                    <Image style={{width: 30, height: 30,}} source={require('../../assets/images/twentypoint.png')}/>
                                </View>
                                <View style={{flex: 1, marginLeft: 5,}}>
                                    <Text style={styles.text} >{Strings.Point.text6}</Text>
                                </View>
                            </View>
                        </Card>
                        <View>
                            <Button 
                                buttonStyle={{backgroundColor: Colors.button, borderRadius: 5, marginTop: 10, marginBottom: 10, elevation: 2,}}
                                title={Strings.Point.continuebutton}/>
                        </View>
                    </View>
                </ScrollView>
            </SafeAreaView>
        );
    }   
}

const styles = StyleSheet.create({
    safeAreaView: {
        flex: 1
    },

    containerScroll: {
        flex: 1,
        backgroundColor: '#efefef',
    },

    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#efefef',
    },

    image: {
        height: 250,
        resizeMode: 'stretch',
    },

    textTitle: {
        fontSize: 18,
        textAlign: 'center',
        color: '#000',
    },

    textMidAlign: {
        fontSize: 13,
        textAlign: 'center',
        marginLeft: 5,
        color: Colors.text,
    },

    text: {
        fontSize: 13,
        textAlign: 'left',
        marginLeft: 5,
        color: Colors.text,
    },

    card: {
        backgroundColor: Colors.backgroundColor,
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        margin: 10,
        padding: 10,
        elevation: 2,
        borderRadius: 5,
    },
});
