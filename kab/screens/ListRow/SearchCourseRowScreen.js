import React from 'react';
import { ScrollView, 
    StyleSheet, 
    SafeAreaView,
    Image,
    View,
    Text,
    Dimensions,
} from 'react-native';
import Colors from '../../constants/Colors';
import Strings from '../../constants/Strings';
import { Card, } from 'react-native-material-cards';
import Utils from '../Utils';

const dimensions = Dimensions.get('window');
const imageWidth = dimensions.width - 20;

export default class SearchCourseRowScreen extends React.Component {
    
    static navigationOptions = {
        title: '',
        header: null,
    };

    render() {

        const gotoDetailCourse = () => {
            this.props.navigation.navigate('Detail')
        }

        return (
            <Card style={styles.cardList} >
                <View style={{flex: 1, flexDirection: 'row'}}>
                    <Image style={styles.imageList} 
                            source={this.props.imageUri}/>
                </View>
                <View style={{position: 'absolute', paddingBottom: 10, width: imageWidth, height: 180, borderRadius: 5,}}>
                    <View style={{width: imageWidth, height: 180, backgroundColor: 'rgba(0, 0, 0, 0.5)', borderRadius: 5, }}>
                        <Text style={styles.textTitle}>{this.props.nama}</Text>
                        <View style={{flexDirection: 'row', left: 10, top: 20, marginBottom: 10, alignItems: 'center'}}>
                            <Image style={{height: 20, width: 20, tintColor: '#fff'}} 
                                    source={require('../../assets/images/location.png')}/>
                            <Text style={{fontSize: 14, color: '#fff'}}>{this.props.location}</Text>
                        </View>
                        <View style={{flexDirection: 'row', left: 15, marginTop: 15, alignItems: 'center'}}>
                            <Text style={{fontSize: 14, color: '#fff', width: 100, }}>{Strings.Search.text1}</Text>
                            <Text style={{fontSize: 14, color: '#fff'}}>{this.props.quotaQ}</Text>
                        </View>
                        {
                            Utils.renderIf(this.props.allowpublish,
                                <View style={{flexDirection: 'row', left: 15, marginTop: 5, alignItems: 'center'}}>
                                    <Text style={{fontSize: 14, color: '#fff', width: 100,}}>{Strings.Search.text2}</Text>
                                    <Text style={{fontSize: 14, color: '#fff'}}>{this.props.quotaPublish}</Text>
                                </View>
                            )
                        }
                        <View style={{height: '50%', flexDirection: 'column', marginTop: 30,}}>
                            <View style={{flexDirection: 'row', left: 15, alignItems: 'center'}}>
                                <Text style={{fontSize: 14, color: '#fff', width: 80,}}>{Strings.Search.text3}</Text>
                                <Text style={{fontSize: 18, fontWeight: 'bold', color: '#fff'}}>{this.props.startfrom}</Text>
                            </View>
                        </View>
                    </View>
                </View>
            </Card>       
        );
    }
}

const styles = StyleSheet.create({
    
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },

    safeAreaView: {
        flex: 1
    },

    textTitle: {
        fontSize: 18,
        left: 15,
        top: 15,
        color: '#fff',
        fontWeight: 'bold',
    },

    imageList: {
        flex: 1,
        height: 180,
        borderRadius: 5,
        resizeMode: 'cover',
    },

    cardList: {
        backgroundColor: Colors.backgroundColor,
        height: 180,
        margin: 10,
        elevation: 2,
        borderRadius: 5,
    },
});