import React from 'react';
import { 
    StyleSheet, 
    Image,
    View,
    Text,
    TouchableOpacity,
} from 'react-native';
import Colors from '../../constants/Colors';
import Strings from '../../constants/Strings';

export default class BookReservationRowScreen extends React.Component {
    
    static navigationOptions = {
        title: '',
        header: null,
    };

    render() {
        return (
            <View style={styles.container} >
                <View style={{flex: 1, flexDirection: 'row', marginLeft: 20, marginRight: 20, marginTop: 5, marginBottom: 5,}}>
                    <View style={{flex: 3, flexDirection: 'column'}}>
                        <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', }}>
                            <Text style={styles.textLeft2}>{this.props.name}</Text>
                        </View>
                        <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', }}>
                            <Text style={[styles.textLeft2, {color: '#edb636'}]}>{this.props.type}</Text>
                        </View>
                    </View>
                    <View style={{flex: 1, flexDirection: 'column',}}>
                        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', marginLeft: 10, marginTop: 5, marginBottom: 5,}}>
                            <TouchableOpacity 
                                onPress={ this.updatePlayer }
                                style={{flex: 1, justifyContent: 'center', alignItems: 'center',}}
                                >
                                <Image style={{height: 20, width: 20, tintColor: '#93b843'}} 
                                    source={require('../../assets/images/ic_edit.png')}/>
                            </TouchableOpacity>
                        </View>
                        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center',}}>
                            <TouchableOpacity 
                                style={{flex: 1, justifyContent: 'center', alignItems: 'center',}}
                                >
                                <Image style={{height: 20, width: 20, tintColor: '#b84242', marginLeft: 10, marginTop: 5, marginBottom: 5,}} 
                                    source={require('../../assets/images/ic_delete.png')}/>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </View>       
        );
    }
}

const styles = StyleSheet.create({
    
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#fff',
    },

    textType: {
        fontSize: 13,
        marginTop: 5,
        color: '#edb636',
        width: 10,
    },

    textLeft2: {
        fontSize: 14,
        marginTop: 5,
        color: Colors.text,
    },

    textRight2: {
        flex: 1,
        fontSize: 12,
        marginTop: 5,
        color: Colors.text,
    },
});