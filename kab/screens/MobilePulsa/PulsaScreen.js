import React from 'react';
import {
    Image,
    ScrollView,
    StyleSheet,
    SafeAreaView,
    AsyncStorage,
    TouchableOpacity,
    Dimensions,
    ListView,
    Text,
    View,
    Animated,
} from 'react-native';
import Colors from '../../constants/Colors';
import Strings from '../../constants/Strings';
import BpkTextInput from 'react-native-bpk-component-text-input';
import { TabView, SceneMap } from 'react-native-tab-view';
import Utils from '../Utils';
import PulsaListScreen from './PulsaListScreen';
import { Divider } from 'react-native-elements';
import { getProvidesPulsa, getProvidesData, postCreateTrans, } from '../../API/PulsaService';
import { TransactionTypeIntEnum, OptionIntEnum } from '../../constants/Enums';
import { getOptions,  } from '../../API/OptionService';
import { Constants } from 'expo';

export default class PulsaScreen extends React.Component {
    static navigationOptions = {
        title: 'Pulsa',
        headerTintColor: '#ffffff', 
        headerStyle: {
            backgroundColor: '#93b843'
        },
    };

    constructor(props){
        super(props);

        this.state = {
            isLoading: false,
            username: '',
            phone: '',
            imagecard: require("../../assets/images/simpati.png"),
            cardvisible: false,
            layoutvisible: true,
            tabvisible: false,
            index: 0,
            routes: [
                { key: 'topup', title: 'TOP - UP' },
                { key: 'data', title: 'DATA' },
            ],
        }
    }

    async componentDidMount() {
        var Enumerable = require('../linq');
        this.setState({ username : await AsyncStorage.getItem('Username')});

        getOptions().then((jsonOption) => {
            this.setState({
                pulsaOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionMobilePulsa).firstOrDefault(),
                maintenanceOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionMaintenancePeriod).firstOrDefault()
            });
        })
        .catch((error) => {
            console.error(error);
        });
    }

    fn_getData = () => 
    {
        var firstchar = this.state.phone.slice(0, 1);
        if(firstchar != '0')
        {
            this.setState({ phone: "0" + "" + this.state.phone, });
        }

        getProvidesPulsa(this.state.phone).then((jsonPulsa) =>
        {
            let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
            this.setState({
                dataSourceData: ds.cloneWithRows(jsonPulsa),
            });

            getProvidesData(this.state.phone).then((jsonData) =>
            {
                this.setState({
                    dataSourcePulsa: ds.cloneWithRows(jsonPulsa),

                    tabvisible: true,
                    layoutvisible: false,
                });
            })

        })
        .catch((error) => {
            console.error(error);
        });
    }

    _renderTabBar = props => {
        const inputRange = props.navigationState.routes.map((x, i) => i);
    
        return (
            <View style={styles.tabBar}>
                {props.navigationState.routes.map((route, i) => {
                    const color = props.position.interpolate({
                        inputRange,
                        outputRange: inputRange.map(
                        inputIndex => (inputIndex === i ? '#93b843' : '#414141')
                    ),
                });
                return (
                    <TouchableOpacity
                        style={styles.tabItem}
                        onPress={() => this.setState({ index: i })}>
                            <Animated.Text style={[{color}, {fontSize: 16,}]}>{route.title}</Animated.Text>
                    </TouchableOpacity>
              );
            })}
          </View>
        );
    };

    render() {
        const gotoPulsa = () => {
            this.props.navigation.navigate('Pulsa')
        }

        const TopupRoute = () => (
            <PulsaListScreen type={'topup'} 
                phone={this.state.phone} 
                username={this.state.username} 
                dataSource={this.state.dataSourcePulsa} 
                pulsaOption={this.state.pulsaOption}
                maintenanceOption={this.state.maintenanceOption}
                property={this.props}
                />
        );
        
        const DataRoute = () => (
            <PulsaListScreen type={'data'} 
                phone={this.state.phone} 
                username={this.state.username} 
                dataSource={this.state.dataSourceData} 
                pulsaOption={this.state.pulsaOption}
                property={this.props}
                maintenanceOption={this.state.maintenanceOption}/>
        );

        return (
            <SafeAreaView style={styles.safeAreaView}>
                <View style={styles.container}>
                    <View style={styles.card}>
                        <View style={{marginTop: 5, marginLeft: 15, marginRight: 15, flexDirection: 'row',}}>
                            <View style={{marginBottom: 3, marginRight: 5, marginTop: 25,}}>
                                <TouchableOpacity style={{flexDirection: 'column', backgroundColor: '#ffffff', }}>
                                    <Text style={styles.inputText2} >{'+62'}</Text>
                                    <Divider style={{ height: 1, backgroundColor: '#efefef', marginTop: 10,  }} />
                                </TouchableOpacity>
                            </View>
                            <View style={{marginLeft: 5, flex: 3}}>
                                <BpkTextInput 
                                    label={Strings.RegisterPhone.numberplaceholder}
                                    maxLength={13}
                                    onChangeText={(text) => {
                                        if(text.length == 3){
                                            var firstchar = text.slice(0, 1);
                                            if(firstchar != '0')
                                            {
                                                this.setState({
                                                    imagecard: Utils.getImageProvider("0" + text),
                                                    cardvisible: true,
                                                })

                                                text= "0" + "" + text;
                                            }
                                        }
                                        if(text.length == 4)
                                        {
                                            this.setState({
                                                imagecard: Utils.getImageProvider(text),
                                                cardvisible: true,
                                            })
                                        }
                                        else if(text.length < 3)
                                        {
                                            this.setState({
                                                cardvisible: false,
                                            })
                                        }
                                        
                                        if(text.length == 8)
                                        {
                                            this.fn_getData();
                                        }
                                        else if(text.length < 8) {
                                            this.setState({
                                                tabvisible: false,
                                                layoutvisible: true,
                                            })
                                        }
                                        this.setState({phone: text});
                                    }}
                                    value={this.state.phone}
                                    style={{marginBottom: 3, }} />
                            </View>
                            {
                                Utils.renderIf(this.state.cardvisible, 
                                    <Image style={{height: 50, width: 50}} source={this.state.imagecard}/>
                                )
                            }
                        </View>
                    </View>
                    {
                        Utils.renderIf(this.state.layoutvisible, 
                            <View style={{marginLeft: 15, marginRight: 15, marginTop: 10, flexDirection: 'column'}}>
                                <View style={{flexDirection: 'row', justifyContent: 'center', alignContent: 'center', }}>
                                    <Image style={[styles.image, {tintColor: "#d2d2d2"}]} 
                                        source={require('../../assets/images/simpati.png')}/>
                                    <Image style={[styles.image, {tintColor: "#d2d2d2", marginLeft: 5, }]}
                                        source={require('../../assets/images/tri.png')}/>
                                    <Image style={[styles.image, {tintColor: "#d2d2d2", marginLeft: 5, }]}
                                        source={require('../../assets/images/bolt.png')}/>
                                    <Image style={[styles.image, {tintColor: "#d2d2d2", marginLeft: 5,}]}
                                        source={require('../../assets/images/im3.png')}/>
                                    <Image style={[styles.image, {tintColor: "#d2d2d2", marginLeft: 5, }]}
                                        source={require('../../assets/images/axis.png')}/>
                                    <Image style={[styles.image, {tintColor: "#d2d2d2", marginLeft: 5, }]}
                                        source={require('../../assets/images/smartfren.png')}/>
                                </View>
                                <View style={{flexDirection: 'row', alignContent: 'center', }}>
                                    <Image style={[styles.image, {tintColor: "#d2d2d2", }]}
                                        source={require('../../assets/images/kartuas.png')}/>
                                    <Image style={[styles.image, {tintColor: '#d2d2d2', marginLeft: 5, }]}
                                        source={require('../../assets/images/xl.png')}/>
                                </View>
                            </View>
                        )
                    }  
                    {
                        Utils.renderIf(this.state.tabvisible,
                            <TabView
                                navigationState={this.state}
                                renderScene={SceneMap({
                                    topup: TopupRoute,
                                    data: DataRoute,
                                })}
                                renderTabBar={this._renderTabBar}
                                onIndexChange={index => this.setState({ index })}
                                initialLayout={{ height: 0, width: Dimensions.get('window').width }}
                            />
                        )
                    }
                    
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({

    safeAreaView: {
        flex: 1,
    },

    containerScroll:
    {
        flex: 1,
        backgroundColor: '#efefef',
    },

    container: {
        flex: 1,
        flexDirection: 'column',
    },

    card: {
        backgroundColor: Colors.backgroundColor,
        flexDirection: 'column',
        elevation: 2,
    },

    inputText: {
        flex: 1,
        backgroundColor: '#ffffff',
        color: '#333',
        fontSize: 16,
    },

    inputText2: {
        backgroundColor: '#ffffff',
        color: '#333',
        fontSize: 16,
    },

    image: {
        width: 50,
        height: 50,
    },

    tabBar: {
        flexDirection: 'row',
        paddingTop: Constants.statusBarHeight,
        backgroundColor: '#fff',
    },

    tabItem: {
        flex: 1,
        alignItems: 'center',
        padding: 16,
    },

});