import React from 'react';
import {
    Image,
    Platform,
    ScrollView,
    StyleSheet,
    Text,
    SafeAreaView,
    ActivityIndicator,
    View,
    AsyncStorage,
} from 'react-native';
import Colors from '../../constants/Colors';
import Strings from '../../constants/Strings';
import { Button } from 'react-native-elements';
import { findCourseWithType, postLastSearch, } from '../../API/CourseService';
import Utils from '../Utils';
import { getStringToDoubleDate,  } from '../../API/SettingService';
import { getOptions,  } from '../../API/OptionService';
import { OptionStringEnum, SessionStringEnum, OptionIntEnum } from '../../constants/Enums';

export default class DetailCourseScreen extends React.Component {
    static navigationOptions = {
        title: 'Detail',
        headerTintColor: '#ffffff', 
        headerStyle: {
            backgroundColor: '#93b843'
        },
    };

    constructor(props){
        super(props);

        this.params = this.props.navigation.state.params;

        this.state = {
            isLoading: true,
            username: '',

            location: this.params.Location,
            locationseletected: this.params.LocationSelected,
            calendar: this.params.Calendar,
            session: this.params.Session,
            player: this.params.Player,
            playertype: this.params.PlayerType,
            
            courseimage: this.params.CourseImage,
            coursename: this.params.CourseName,
            courseaddress: this.params.CourseAddress,
            
            publishQuota: this.params.PublishQouta,
            qQuota: this.params.QQuota,

            publishrate: 0,
            visitorrate: 0,
            clubrate: 0,
            qrate: 0,

            kerjasamalapangan: false,
        }
    }

    async  componentDidMount() {
        var sessionInt = this.state.session === undefined ? 1 : this.state.session === "Morning" ? 1 : 2;
        var playerInt = this.state.player === undefined ? 1 : this.state.player;
        var Enumerable = require('../linq');
        this.setState({ username : await AsyncStorage.getItem('Username')});
        getOptions().then((jsonOption) => 
        {
            this.setState({
                reservationOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionReservation).firstOrDefault(),
                maintenanceOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionMaintenancePeriod).firstOrDefault()
            });

            getStringToDoubleDate(this.state.calendar).then((jsonDate) =>
            {
                findCourseWithType(jsonDate.result, sessionInt, playerInt, this.state.locationseletected, this.state.playertype).then((jsonCourse) =>
                {
                    this.setState({
                        publishrate: jsonCourse.PublishRate != 0 ? Utils.currencyfunc(Math.round(jsonCourse.PublishRate*100)/100) : 0,
                    });

                    this.setState({
                        visitorrate: jsonCourse.VisitorRate != 0 ? Utils.currencyfunc(Math.round(jsonCourse.VisitorRate*100)/100) : 0,
                    });

                    this.setState({
                        clubrate: jsonCourse.MandiriRate != 0 ? Utils.currencyfunc(Math.round(jsonCourse.MandiriRate*100)/100) : 0,
                    });

                    this.setState({
                        qrate: jsonCourse.QGolfRate != 0 ? Utils.currencyfunc(Math.round(jsonCourse.QGolfRate*100)/100) : 0,
                    });

                    this.setState({
                        kerjasamalapangan: jsonCourse.MandiriRate != 0 ? true : false,
                        isLoading: false,
                    });

                    postLastSearch(this.state.username, this.state.locationseletected, null, jsonDate.result, 
                        this.state.session, this.state.player).then((jsonLastSearch) => {
                            console.log(jsonLastSearch);
                        })
                })
                .catch((error) => {
                    console.error(error);
                    this.setState({isLoading: false, });
                });
            })
        })
        .catch((error) => {
            console.error(error);
            this.setState({isLoading: false, });
        });
        
    }

    render() {
        const gotoBook = () => {
            var playerInt = this.state.player === undefined ? 1 : this.state.player;
            var platform = Platform.OS === 'ios' ? this.state.reservationOption.iOS : this.state.reservationOption.Android;
            var totalQuota = this.state.publishQuota + this.state.qQuota;
            if(Utils.getMaintenanceOption(this.state.maintenanceOption)){
                Utils.alertBackToMain(this.state.maintenanceOption.Note, this.props);
            }
            else if(!platform) {
                Utils.errAlert("QAccess Alert", this.state.reservationOption.Note);
            }
            else if(playerInt > totalQuota){
                Utils.errAlert("QAccess Alert", "Quota is not enough!");
            }
            else {
                var week = (new Date(this.state.calendar)).getDay();
                //0 == Sunday, 1 == Monday, 6 == Saturday
                if(this.state.coursename === "Royale Jakarta Golf" && playerInt % 2 != 0){
                    Utils.errAlert("Error Message", Strings.DetailCourse.errmsg1);
                }
                else if( (week == 0 || week == 6) && this.state.coursename === "Klub Golf Bogor Raya" && playerInt < 3){
                    Utils.errAlert("Error Message", Strings.DetailCourse.errmsg2);
                }
                else if( (week == 0 || week == 6) && this.state.session === SessionStringEnum.Afternoon && this.state.coursename === "Lotus Lakes Golf Club" && playerInt < 2){
                    Utils.errAlert("Error Message", Strings.DetailCourse.errmsg3);
                }
                else if( (week == 0 || week == 6) && this.state.session === SessionStringEnum.Morning && this.state.coursename === "Lotus Lakes Golf Club" && playerInt < 3){
                    Utils.errAlert("Error Message", Strings.DetailCourse.errmsg4);
                }
                else {
                    this.props.navigation.navigate('BookReservation', {Location: this.state.location, Calendar: this.state.calendar,
                        Session: this.state.session, Player: this.state.player, PlayerType: this.state.playertype, 
                        LocationSelected: this.state.locationseletected, KerjasamaLapangan: this.state.kerjasamalapangan,
                        CourseName: this.state.coursename, CourseAddress: this.state.courseaddress,
                        CourseImage: this.state.courseimage, PublishQuota: this.state.publishQuota, QQuota: this.state.qQuota});
                }
            }
        }

        if (this.state.isLoading) {
            return (
                <View style={{flex: 1, paddingTop: 20}}>
                    <ActivityIndicator />
                </View>
            );
        }

        return (
            <SafeAreaView style={styles.safeAreaView}>
                <ScrollView style={styles.containerScroll}>
                    <View style={styles.container}>
                        <Image style={styles.image} 
                            source={{uri: this.state.courseimage}}/>
                        <View style={{backgroundColor: '#fff', paddingTop: 10, paddingBottom: 10, paddingLeft: 15, paddingRight: 15, }}>
                            <Text style={styles.textTitle}>{this.state.coursename}</Text>
                            <Text style={styles.text}>{this.state.courseaddress}</Text>
                        </View>
                        <Text style={styles.textKet}>{Strings.DetailCourse.text1}</Text>
                        <View style={{flex: 1, flexDirection: 'column', backgroundColor: '#fff', paddingTop: 10, paddingBottom: 10, paddingLeft: 15, paddingRight: 15,}}>
                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', }}>
                                <Text style={styles.textLeft}>{Strings.DetailCourse.text3}</Text>
                                <Text style={styles.textRight}>{'Rp. ' + this.state.publishrate }</Text>
                            </View>
                            {
                                Utils.renderIf(this.state.clubrate !== 0, 
                                    <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', }}>
                                        <Text style={styles.textLeft}>{Strings.DetailCourse.text4}</Text>
                                        <Text style={styles.textRight}>{'Rp. ' + this.state.clubrate}</Text>
                                    </View>
                                )
                            }
                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', }}>
                                <Text style={styles.textLeft}>{Strings.DetailCourse.text5}</Text>
                                <Text style={styles.textRight}>{'Rp. ' + this.state.qrate}</Text>
                            </View>
                        </View>
                        <Text style={styles.textKet}>{Strings.DetailCourse.text2}</Text>
                        <View style={{backgroundColor: '#fff', paddingTop: 10, paddingBottom: 10, paddingLeft: 15, paddingRight: 15,}}>
                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', }}>
                                <Text style={styles.textLeft2}>{'• '}</Text>
                                <Text style={styles.textRight2}>{Strings.DetailCourse.text6}</Text>
                            </View>
                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', }}>
                                <Text style={styles.textLeft2}>{'• '}</Text>
                                <Text style={styles.textRight2}>{Strings.DetailCourse.text7}</Text>
                            </View>
                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', }}>
                                <Text style={styles.textLeft2}>{'• '}</Text>
                                <Text style={styles.textRight2}>{Strings.DetailCourse.text8}</Text>
                            </View>
                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', }}>
                                <Text style={styles.textLeft2}>{'• '}</Text>
                                <Text style={styles.textRight2}>{Strings.DetailCourse.text9}</Text>
                            </View>
                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', }}>
                                <Text style={styles.textLeft2}>{'• '}</Text>
                                <Text style={styles.textRight2}>{Strings.DetailCourse.text10}</Text>
                            </View>
                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', }}>
                                <Text style={styles.textLeft2}>{'• '}</Text>
                                <Text style={styles.textRight2}>{Strings.DetailCourse.text11}</Text>
                            </View>
                            {
                                Utils.renderIf(this.state.coursename === "Damai Indah Golf - BSD Course", 
                                    <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', }}>
                                        <Text style={styles.textLeft2}>{'• '}</Text>
                                        <Text style={styles.textRight2}>{Strings.DetailCourse.text12}</Text>
                                    </View>
                                )
                            }
                            {
                                Utils.renderIf(this.state.coursename === "Rancamaya Golf & Country Club",
                                    <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', }}>
                                        <Text style={styles.textLeft2}>{'• '}</Text>
                                        <Text style={styles.textRight2}>{Strings.DetailCourse.text13}</Text>
                                    </View>
                                )
                            }
                            
                        </View>
                        <Button 
                            onPress={ gotoBook }
                            buttonStyle={{backgroundColor: Colors.button, borderRadius: 5, marginTop: 20, marginLeft: 10, marginRight: 10, marginBottom: 20, height: 50, }}
                            title={Strings.DetailCourse.continuebutton}/>
                    </View>
                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({

    safeAreaView: {
        flex: 1,
    },

    containerScroll:
    {
        flex: 1,
        backgroundColor: '#f2f2f2',
    },

    container: {
        backgroundColor: '#d9d9d9',
        flex: 1,
        width: '100%'
    },

    image: {
        width: '100%',
        height: 180,
        resizeMode: 'cover',
    },

    text: {
        fontSize: 13,
        marginTop: 5,
        color: Colors.text,
    },

    textLeft: {
        fontSize: 14,
        marginTop: 5,
        color: Colors.text,
        width: 120,
    },

    textLeft2: {
        fontSize: 14,
        marginTop: 5,
        color: Colors.text,
        width: 10,
    },

    textRight: {
        flex: 1,
        fontSize: 14,
        marginTop: 5,
        color: Colors.button,
        textAlign: 'right',
    },

    textRight2: {
        flex: 1,
        fontSize: 12,
        marginTop: 5,
        color: Colors.text,
    },

    textTitle: {
        fontSize: 18,
        color: Colors.text,
        fontWeight: 'bold',
    },

    textKet: {
        fontSize: 18,
        marginTop: 10,
        marginLeft: 15,
        marginBottom: 5,
        color: Colors.text,
        fontWeight: 'bold',
    },
});
