import React from 'react';
import Moment from 'moment';
import clsSetting from '../API/clsSetting';

exports.postRefund = (_id, _status) => {
    return fetch(clsSetting.APIUrl + "transaksi/refund", {  
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json; charset=utf-8',
        },
        body: JSON.stringify({
            IdTransaction: _id,
            Status: _status,
            ApiSecretKey: clsSetting.ApiSecretKey,
        })
    })
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        if (json.status === 200) {
            return json; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error Refund ', e));  
}

exports.findRefundRequestByTrans = (_transid) => {
    return fetch(clsSetting.APIUrl + "transaksi/FindRefundRequestByTransaction?idTransaction=" + _transid +
    "&ApiSecretKey=" + clsSetting.ApiSecretKey)
    .then(response => response.json())
    .then(function(json) {
        return json; 
    })
    .catch(e => console.log('Error Find Refund ', e));
}

exports.postCreateTrans = (_transmodel) => {
    return fetch(clsSetting.APIUrl + "transaksi/create", {  
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json; charset=utf-8',
        },
        body: JSON.stringify(_transmodel)
    })
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        console.log(json);
        if (json.status === 200) {
            return json; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error Create Trans ', e));  
}

exports.findTrans = (_bookingcode) => {
    return fetch(clsSetting.APIUrl + "transaksi/findbycode?kode=" + _bookingcode +
    "&ApiSecretKey=" + clsSetting.ApiSecretKey)
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        if (json.status === 200) {
            return json.result; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error Find Trans ', e));  
}

exports.cancelTrans = (_bookingcode) => {
    return fetch(clsSetting.APIUrl + "transaksi/canceltransaction", {  
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json; charset=utf-8',
        },
        body: JSON.stringify({
            Kode: _bookingcode,
            ApiSecretKey: clsSetting.ApiSecretKey,
        })
    })
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        return json;
    })
    .catch(e => console.log('Error Cancel Trans ', e));  
}

exports.findTransTopUp = (_bookingcode) => {
    return fetch(clsSetting.APIUrl + "transaksi/FindTopUpByCode?kode=" + _bookingcode +
    "&ApiSecretKey=" + clsSetting.ApiSecretKey)
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        if (json.status === 200) {
            return json; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error Find Trans TopUp', e));  
}

exports.getBookingActive = (_username, _booktype) => {
    return fetch(clsSetting.APIUrl + "transaksi/getactivetransactions?username=" + _username +
    "&type=" + _booktype + "&ApiSecretKey=" + clsSetting.ApiSecretKey)
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        if (json.status === 200) {
            return json.result; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error Booking Active ', e));  
}

exports.getBookingHistory = (_username, _booktype) => {
    return fetch(clsSetting.APIUrl + "transaksi/getpasttransactions?username=" + _username +
    "&type=" + _booktype + "&ApiSecretKey=" + clsSetting.ApiSecretKey)
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        if (json.status === 200) {
            return json.result; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error Booking History ', e));  
}

exports.getBookingPaidHistory = (_username) => {
    return fetch(clsSetting.APIUrl + "transaksi/getpasttransactions?username=" + _username +
    "&ApiSecretKey=" + clsSetting.ApiSecretKey)
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        if (json.status === 200) {
            return json.result; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error Booking Paid History ', e));  
}

exports.postUpdateStatusPayment = (_code, _payment) => {
    return fetch(clsSetting.APIUrl + "transaksi/UpdatePayment", {  
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json; charset=utf-8',
        },
        body: JSON.stringify({
            Kode: _code,
            Payment: _payment,
            ApiSecretKey: clsSetting.ApiSecretKey,
        })
    })
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        if (json.status === 200) {
            return json.result; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error Update Status Payment ', e));  
}

exports.postConfirmationTransfer = (_banktransfermodel) => {
    return fetch(clsSetting.APIUrl + "transaksi/BankTransfer", {  
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json; charset=utf-8',
        },
        body: JSON.stringify({_banktransfermodel})
    })
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        if (json.status === 200) {
            return json.result; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error Confirmation Transfer ', e));  
}

exports.getActiveBalance = (_username) => {
    return fetch(clsSetting.APIUrl + "player/GetActivePoint?email=" + _username +
    "&ApiSecretKey=" + clsSetting.ApiSecretKey)
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        if (json.status === 200) {
            return json.result; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error Active Balance ', e));  
}

exports.getPendingBalance = (_username) => {
    return fetch(clsSetting.APIUrl + "player/GetPendingPoint?email=" + _username +
    "&ApiSecretKey=" + clsSetting.ApiSecretKey)
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        if (json.status === 200) {
            return json.result; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error Pending Balance ', e));  
}

exports.getPendingBankInformation = (_code) => {
    return fetch(clsSetting.APIUrl + "transaksi/GetBankTransferInformation?kode=" + _code +
    "&ApiSecretKey=" + clsSetting.ApiSecretKey)
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        if (json.status === 200) {
            return json.result; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error Pending Bank ', e));  
}

exports.postCreateTopUp = (_transtopupmodel) => {
    return fetch(clsSetting.APIUrl + "transaksi/TopUp", {  
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json; charset=utf-8',
        },
        body: JSON.stringify({_transtopupmodel})
    })
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        if (json.status === 200) {
            console.log('json Create Top Up ', json);
            return json.result; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error Create Top Up ', e));  
}

exports.postCreateEvent = (_transeventmodel) => {
    return fetch(clsSetting.APIUrl + "transaksi/CreateEvent", {  
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json; charset=utf-8',
        },
        body: JSON.stringify(_transeventmodel)
    })
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        console.log('api Create Event ', JSON.stringify(_transeventmodel));
        if (json.status === 200) {
            return json.result; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error Create Event ', e));  
}

exports.getEventTicket = (_code) => {
    return fetch(clsSetting.APIUrl + "transaksi/findeventbycode?kode=" + _code +
    "&ApiSecretKey=" + clsSetting.ApiSecretKey)
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        if (json.status === 200) {
            return json.result; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error Event Ticket ', e));   
}

exports.getRegisterQTicket = (_code) => {
    return fetch(clsSetting.APIUrl + "transaksi/findregistrationbycode?kode=" + _code +
    "&ApiSecretKey=" + clsSetting.ApiSecretKey)
    .then(function(response){
        return response.json();
    })
    .then(function(json) {
        if (json.status === 200) {
            return json.result; 
        } else {
            let error = new Error(json.message);
            throw error;
        }
    })
    .catch(e => console.log('Error RegisterQ Ticket ', e));   
}