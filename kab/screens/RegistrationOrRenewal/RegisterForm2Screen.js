import React from 'react';
import {
    Image,
    Platform,
    ScrollView,
    StyleSheet,
    SafeAreaView,
    AsyncStorage,
    TouchableOpacity,
    ActivityIndicator,
    Text,
    Dimensions,
    View,
} from 'react-native';
import Colors from '../../constants/Colors';
import Strings from '../../constants/Strings';
import { Button, } from 'react-native-elements';
import { postAddMember } from '../../API/UserService';
import { getOptions,  } from '../../API/OptionService';
import BpkTextInput from 'react-native-bpk-component-text-input';
import RegisterQViewModel from '../../Model/RegisterQViewModel';
import { getStringToDoubleDate,  } from '../../API/SettingService';
import { OptionIntEnum, TransactionTypeIntEnum, PlatformIntEnum, PaymentIntEnum, } from '../../constants/Enums';
import Utils from '../Utils';
import moment from 'moment';

export default class RegisterForm2Screen extends React.Component {
    static navigationOptions = {
        title: 'Register Member',
        headerTintColor: '#ffffff', 
        headerStyle: {
            backgroundColor: '#93b843'
        },
    };

    constructor(props){
        super(props);

        this.params = this.props.navigation.state.params;

        this.state = {
            isLoading: true,
            username: '',

            id: this.params.Id,
            inputidcardname: this.params.IdCardName,
            inputcardname: this.params.CardName,
            inputidcardnumber: this.params.IdCardNumber,
            inputphonenumber: this.params.PhoneNumber,
            inputemail: this.params.Email,
            inputplaceofbirth: this.params.PlaceOfBirth,
            inputdob: this.params.DateOfBirth,
            inputaddress: this.params.Address,
            inputcity: this.params.City,
            inputpostalcode: this.params.PostalCode,

            inputtypesofcorporation: '',
            inputcorporationname: '',
            inputcorporationphone: '',
            inputcorporationaddress: '',
            inputdeliveryaddress: '',

            imgKTP : '',
            imgProfile: '',

            isDateTimePickerVisible: false,
        }
    }

    componentDidMount() {
        var Enumerable = require('../linq');

        getOptions().then((jsonOption) => 
        {
            this.setState({
                registrationOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionRegistration).firstOrDefault(),
                maintenanceOption: Enumerable.from(jsonOption).where(x => x.Id == OptionIntEnum.OptionMaintenancePeriod).firstOrDefault()
            });

            this.setState({ isLoading: false, });
        })
        .catch((error) => {
            console.error(error);
            this.setState({isLoading: false, });
        });
    }

    render() {
        const gotoPay = async () => {
            var platform = Platform.OS === 'ios' ? this.state.registrationOption.iOS : this.state.registrationOption.Android;
            if(Utils.getMaintenanceOption(this.state.maintenanceOption)){
                Utils.alertBackToMain(this.state.maintenanceOption.Note, this.props);
            }
            else if(!platform) {
                Utils.errAlert("QAccess Alert", this.state.registrationOption.Note);
            }
            else if(this.state.inputdeliveryaddress.trim() == "")
            {
                Utils.errAlert("Error Message", Strings.RegisterForm2.errmsg1);
            }
            else
            {
                this.setState({ username : await AsyncStorage.getItem('Username')});
                var platformMobile = Platform.OS === 'ios' ? PlatformIntEnum.IOs : PlatformIntEnum.Android;
                var registerModel = new RegisterQViewModel(this.state.inputidcardname, this.state.inputcardname,
                    this.state.inputidcardnumber, this.state.inputplaceofbirth, this.state.inputdob,
                    this.state.inputaddress, '', '', this.state.inputcity, this.state.inputpostalcode, 
                    '', '', this.state.inputphonenumber, '', this.state.username, this.state.inputtypesofcorporation,
                    this.state.inputcorporationname, this.state.inputcorporationaddress, this.state.inputcorporationphone, 
                    '', '', '', '', '', PaymentIntEnum.NotSpecified, this.state.inputdeliveryaddress, 0, moment(Date.now()).format('YYYY-MM-DD'), 
                    Number.parseInt(this.state.id), 0, '', this.state.username, null, platformMobile);
                postAddMember(registerModel).then(async (jsonAddMember) => 
                {
                    this.setState({isLoading: false, });
                    await AsyncStorage.setItem("BookingCode", jsonAddMember);
                    this.props.navigation.navigate('Payment', {TransType: TransactionTypeIntEnum.RegistrationQ});
                    
                })
                .catch((error) => {
                    console.error(error);
                    this.setState({isLoading: false, });
                });
            }
        }

        if (this.state.isLoading) {
            return (
                <View style={{flex: 1, paddingTop: 20}}>
                    <ActivityIndicator />
                </View>
            );
        }

        const dimensions = Dimensions.get('window');
        const btnHeight = Math.round(dimensions.width * 9 / 16);
        const btnWidth = dimensions.width - 30;

        return (
            <SafeAreaView style={styles.safeAreaView}>
                <ScrollView style={styles.containerScroll}>
                    <View style={styles.container}>
                        <View style={{marginTop: 10, justifyContent: 'center', alignItems: 'center', }}>
                            <View style={{flex:1, justifyContent:'center', 
                                alignContent: 'center', alignItems: 'center',}}>
                                <Image style={{width: 120, height: 120, resizeMode: 'contain', }}
                                    source={require('../../assets/images/registermember.png')}/>
                            </View>
                            <Text style={{fontSize: 11, color: Colors.text }}>{Strings.RegisterForm2.text1}</Text>
                        </View>
                        <View style={{width: '100%', marginTop: 5,}}>
                            <BpkTextInput 
                                label={Strings.RegisterForm2.text2}
                                style={{marginBottom: 3, marginLeft: 20, marginRight: 20, }}
                                onChangeText={(text) => this.setState({inputtypesofcorporation: text})}
                                value={this.state.inputtypesofcorporation} />
                        </View>
                        <View style={{width: '100%', marginTop: 5,}}>
                            <BpkTextInput 
                                label={Strings.RegisterForm2.text3}
                                style={{marginBottom: 3, marginLeft: 20, marginRight: 20, }}
                                onChangeText={(text) => this.setState({inputcorporationname: text})}
                                value={this.state.inputcorporationname} />
                        </View>
                        <View style={{width: '100%', marginTop: 5,}}>
                            <BpkTextInput 
                                label={Strings.RegisterForm2.text4}
                                style={{marginBottom: 3, marginLeft: 20, marginRight: 20, }}
                                onChangeText={(text) => this.setState({inputcorporationphone: text})}
                                value={this.state.inputcorporationphone} />
                        </View>
                        <View style={{width: '100%', marginTop: 5,}}>
                            <BpkTextInput 
                                label={Strings.RegisterForm2.text5}
                                style={{marginBottom: 3, marginLeft: 20, marginRight: 20,  }}
                                onChangeText={(text) => this.setState({inputcorporationaddress: text})}
                                value={this.state.inputcorporationaddress} />
                        </View>
                        <View style={{width: '100%', marginTop: 5,}}>
                            <BpkTextInput 
                                label={Strings.RegisterForm2.text6}
                                style={{marginBottom: 3, marginLeft: 20, marginRight: 20,  }}
                                onChangeText={(text) => this.setState({inputdeliveryaddress: text})}
                                value={this.state.inputdeliveryaddress} />
                        </View>
                        <View style={{flex:1, justifyContent:'center', alignContent: 'center', alignItems: 'center', }}>
                            <View style={{width: 100,
                                    height: 100,
                                    borderRadius: 100/2,
                                    borderWidth: 1,
                                    borderColor: '#999',
                                    backgroundColor: '#fff',
                                    justifyContent: 'center',
                                    alignContent: 'center',
                                    marginTop: 15, }}>
                                <View style={{flex:1, justifyContent:'center', 
                                    alignContent: 'center', alignItems: 'center',}}>
                                    <Image style={{width: 60, height: 60, resizeMode: 'contain', }}
                                        source={require('../../assets/images/ic_upload_photo.png')}/>
                                </View>
                            </View>
                            <Text style={{fontSize: 15, color: Colors.text }}>{Strings.RegisterForm2.text7}</Text>
                            <Text style={{fontSize: 11, color: Colors.text }}>{Strings.RegisterForm2.text8}</Text>
                        </View>
                        <View style={{backgroundColor: '#f2f2f2', paddingLeft: 15, paddingRight: 15, paddingTop: 10, paddingBottom: 10, marginTop: 10,}}>
                            <Text style={{fontSize: 15, color: Colors.text, fontWeight: 'bold', }}>{Strings.RegisterForm2.text9}</Text>
                        </View>
                        <Text style={{fontSize: 11, marginLeft: 15, marginRight: 15, marginTop: 5, color: Colors.text }}>{Strings.RegisterForm2.text10}</Text>
                        <Text style={{fontSize: 11, marginLeft: 15, marginRight: 15, marginTop: 5, color: Colors.text }}>{Strings.RegisterForm2.text11}</Text>
                        <Text style={{fontSize: 11, marginLeft: 15, marginRight: 15, marginTop: 5, color: Colors.text }}>{Strings.RegisterForm2.text12}</Text>
                        <Button 
                            onPress={ gotoPay }
                            buttonStyle={{backgroundColor: Colors.button, borderRadius: 5, marginTop: 10, marginLeft: 5, marginRight: 5, marginBottom: 20, height: 50, }}
                            title={Strings.RegisterForm2.continuebutton}/>
                    </View>
                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({

    safeAreaView: {
        flex: 1,
    },

    containerScroll:
    {
        flex: 1,
        backgroundColor: '#ffffff',
    },

    container: {
        flex: 1,
    },
});
