import React from 'react';
import {
    Image,
    Dimensions,
    Platform,
    ScrollView,
    StyleSheet,
    Text,
    AsyncStorage,
    TouchableOpacity,
    TouchableHighlight,
    SafeAreaView,
    ListView,
    Alert,
    ActivityIndicator,
    View,
    Linking,
    BackHandler,
    RefreshControl,
} from 'react-native';
import Carousel from 'react-native-snap-carousel';
import Colors from '../constants/Colors';
import styled from 'styled-components';
import Strings from '../constants/Strings';
import { Button, Avatar, colors, Icon, } from 'react-native-elements';
import EventRowScreen from './ListRow/EventRowScreen';
import { Card, } from 'react-native-material-cards';
import { getMainData, getVirtualAccount, postLastAccess, getVersionAndroid, getVersion } from '../API/UserService';
import DateTimePicker from 'react-native-modal-datetime-picker';
import moment from 'moment';
import { SinglePickerMaterialDialog } from 'react-native-material-dialog';
import clsSetting from '../API/clsSetting';
import { Dialog, } from "react-native-simple-dialogs";
import { getLastSearch } from '../API/CourseService';
import Utils from './Utils';
import Enums, {TipePemainIntEnum, PlatformIntEnum, } from '../constants/Enums';

const dimensions = Dimensions.get('window');
const imgWidth = dimensions.width - 40;
const imgCardWidth = dimensions.width - 70;
const imgHeight = dimensions.height - 100;

export default class HomeScreen extends React.Component {
    static navigationOptions = {
        title: '',
        header: null,
    };

    constructor(props){
        super(props);
        this.params = this.props.navigation.state.params;

        this.state = {
            errors: [],
            images: [],
            isLoading: true,
            refreshing: false,
            sessionArr: [ 'Morning', 'Afternoon' ],
            playerArr: [ "1", "2", "3", "4" ],
            email: '',
            isDateTimePickerVisible: false,
            sessionPicker: false,
            playerPicker: false,
            location: 'Click here to search',
            calendar: moment(Date.now()).format('YYYY-MM-DD'),
            sessionSelectedItem: undefined,
            playerSelectedItem: undefined,
            session: 'Morning',
            player: '1',
            point: 0,
            usertype: 1,
            
            mycardvisible: false,
            mycardagentvisible: false,
            advertisementvisible: false,
            mycardimgcard: '',
            mycardimgprofile: '',
            mycardname: '',
            mycardexpired: '',
            mycardnumber: '',
            mycardimagepublish: '',

            imgadvertisement: '',

            lastsearchvisible: true,
            lastsearchdata: undefined,
            lastsearchgambar: '',
            lastsearchcoursename: '',
            lastsearchdate: '',
            lastsearchsession: 1,
            lastsearchplayer: 0,

            nDataAdvertisement: 0,
        }
        this.props = props;
        this._carousel = {};
    }

    async componentDidMount() {
        this.setState({ email : await AsyncStorage.getItem('Username')});
        getMainData(this.state.email).then((json) =>
        {
            getLastSearch(this.state.email).then((jsonLastSearch) =>
            {
                let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
                this.setState({
                    
                    dataSource: ds.cloneWithRows(json.events),
                    dataAdvertisement: json.advertisements,
                    images: json.promobanners,

                    lastsearchdata: jsonLastSearch,

                    point: json.balance.Balance,
                    isLoading: false,
                });

                if(this.state.dataAdvertisement != undefined){
                    this.setState({
                        nDataAdvertisement: this.state.dataAdvertisement.length,
                    });
                }

                if(this.state.lastsearchdata == undefined){
                    this.setState({
                        lastsearchvisible: false,
                    })
                }
                else{
                    this.setState({
                        lastsearchgambar: jsonLastSearch.Course.Gambar,
                        lastsearchcoursename: jsonLastSearch.Course.Nama,
                        lastsearchdate: jsonLastSearch.Date,
                        lastsearchsession: jsonLastSearch.Session,
                        lastsearchplayer: jsonLastSearch.Players,
                        lastsearchvisible: true,
                    })
                }

                this.fn_GetMyCard();
                this.fn_Advertisement();
                this.fn_getLastVersion();
                this.fn_setLastAccess();
                
                if((Date.now() < (new Date()).setHours(8, 30))){
                    this.setState({
                        calendar: moment(Date.now()).format('YYYY-MM-DD'),
                        session: "Morning",
                        sessionSelectedItem: this.state.sessionArr.map((row, index) => ({ value: index, label: row }))[0],
                    });
                }
                else if((Date.now() > (new Date()).setHours(8, 30)) && (Date.now() < (new Date()).setHours(13, 30))){
                    this.setState({
                        calendar: moment(Date.now()).format('YYYY-MM-DD'),
                        session: "Afternoon",
                        sessionSelectedItem: this.state.sessionArr.map((row, index) => ({ value: index, label: row }))[1],
                    });
                }
                else{
                    var date = new Date();
                    // add a day
                    date.setDate(date.getDate() + 1);

                    this.setState({
                        calendar: moment(date).format('YYYY-MM-DD'),
                        session: "Morning",
                        sessionSelectedItem: this.state.sessionArr.map((row, index) => ({ value: index, label: row }))[0],
                    });
                }
            })
        });
    }

    _refreshControl(){
        return (
            <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={() => this._refreshListView()} />
        )
    }

    _refreshListView(){

        this.setState({ refreshing: true })

        getMainData(this.state.email).then((json) =>
        {
            getLastSearch(this.state.email).then((jsonLastSearch) =>
            {
                let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
                this.setState({
                    dataSource: ds.cloneWithRows(json.events),
                    images: json.promobanners,
                    lastsearchdata: jsonLastSearch,
                    point: json.balance.Balance,
                });

                if(this.state.lastsearchdata == undefined){
                    this.setState({
                        lastsearchvisible: false,
                    })
                }
                else{
                    this.setState({
                        lastsearchgambar: jsonLastSearch.Course.Gambar,
                        lastsearchcoursename: jsonLastSearch.Course.Nama,
                        lastsearchdate: jsonLastSearch.Date,
                        lastsearchsession: jsonLastSearch.Session,
                        lastsearchplayer: jsonLastSearch.Players,
                        lastsearchvisible: true,
                    })
                }

                this.fn_GetMyCard();
                
            })
        });

        this.setState({refreshing:false})
    }

    onSelect = data => {
        this.setState(data);
    };

    //Date Picker
    _showDateTimePicker = () => {
        this.setState({ isDateTimePickerVisible: true });
    }
    
    _hideDateTimePicker = () => {
        this.setState({ isDateTimePickerVisible: false });
    }

    _handleDatePicked = (date) => {
        console.log('A date has been picked: ', date);
        this._hideDateTimePicker();
        this.setState({calendar: moment(date).format('YYYY-MM-DD')});
    };

    GetItem (row) {
        //Selected Row List
        this.props.navigation.navigate('DetailEvent', {IdEvent: row.Id, Type: row.Category.Type, });
    }

    ListViewItemSeparator = () => {
        return (
            <View style={{ height: .5, width: "100%", backgroundColor: "#000", }} />
        );
    }

    handleSnapToItem(index){
        console.log("snapped to ", index)
    }
    
    _renderItem = ( {item, index} ) => {
        
        //console.log("rendering,", index, item)      
        return (
            <View style={{justifyContent: 'center', alignItems: 'center', width: 300, }}>
                <CurrentVideoTO
                    onPress={ () => { 
                        if(item.Activity == 'MandiriWebActivity'){

                        }
                        else if(item.Activity == 'InviteFriendsActivity'){
                            this.gotoInviteFriends
                        }
                        else if(item.Activity == 'OurPartnersActivity'){
                            this.gotoPartners
                        } 
                        else if(item.Activity == 'Bills&TopupActivity'){
                            this.gotoBillsAndTopUp
                        } 
                        else if(item.Activity == 'QCardActivity'){
                            this.gotoQCard
                        } 
                        else if(item.Activity == 'EventActivity'){
                            this.gotoEvent
                        } 
                        this._carousel.snapToItem(index);
                    }} >
                    {
                        (Platform.OS === 'ios') ? 
                        (
                            <CurrentVideoImage style={{resizeMode: 'cover', }} source={{uri: clsSetting.HomeUrl + item.Thumbnail}} />
                        )
                        :
                        (
                            <CurrentVideoImage style={{resizeMode: 'contain', }} source={{uri: clsSetting.HomeUrl + item.Thumbnail}} />
                        )
                    }
                    
                </CurrentVideoTO>
                
                <Text style={{color:'#fff', top: 28, justifyContent:'center'}} >{''}</Text>
            </View>
        );
    }

    fn_GetMyCard = async () =>
    {
        getVirtualAccount(this.state.email).then(async (jsonCard) =>
        {
            await AsyncStorage.setItem("TipeUser", jsonCard.TypeMember + "");
            var checktype = jsonCard.TypeMember != TipePemainIntEnum.Visitor  && jsonCard.TypeMember != TipePemainIntEnum.Publish && jsonCard.TypeMember != TipePemainIntEnum.Agent ? true : false;
            this.setState({
                usertype: jsonCard.TypeMember,
                mycardimgcard: (checktype) ? clsSetting.HomeUrl + jsonCard.Class.Image : "",
                mycardimgprofile: clsSetting.HomeUrl + jsonCard.Foto,
                mycardname: jsonCard.Name,
                mycardnumber: jsonCard.NoMember,
                mycardexpired: jsonCard.ValidEnd + "",
            });
        })
        .catch((error) => {
            console.error(error);
        });
        
    }

    fn_setLastAccess = () =>
    {
        var os = Platform.OS == 'ios' ? PlatformIntEnum.IOs : PlatformIntEnum.Android;
        const devicename = Expo.Constants.deviceName;
        const version = clsSetting.VersionMobile;
        postLastAccess(this.state.email, devicename, '', version);
    }

    fn_getLastVersion = async () =>
    {
        var os = Platform.OS === 'ios' ? 2 : 1;
        getVersion(os).then((jsonversion) =>
        {
            var currentVersionInDB = jsonversion.result.CurrentVersion;
            var lastVersionInDB = jsonversion.result.LastVersion;
            var appStoreLink = jsonversion.result.LinkApps;

            if (clsSetting.VersionMobile !== lastVersionInDB)
            {
                if (currentVersionInDB === lastVersionInDB)
                {
                    Alert.alert(
                        "Warning",
                        "Your apps is out of dated, please update your apps!",
                        [
                            { text: 'YES', onPress: () => {
                                Linking.openURL(appStoreLink);
                                BackHandler.exitApp();
                            }},
                            { text: 'NO', onPress: () => {
                                BackHandler.exitApp();
                            }}
                        ],
                        { cancelable: false }
                    )
                }
                else
                {
                    Alert.alert(
                        "Warning",
                        "Your apps is out of dated, please update your apps!",
                        [
                            { text: 'YES', onPress: () => {
                                Linking.openURL(appStoreLink);
                            }},
                            { text: 'NO', onPress: () => console.log('onpress')}
                        ],
                        { cancelable: false }
                    )
                }
            }
        });
        
    }

    fn_Advertisement = () => {
        if(this.state.nDataAdvertisement > 0){
            var img = this.state.dataAdvertisement[this.state.nDataAdvertisement - 1].Thumbnail;
            this.setState({
                nDataAdvertisement: Number.parseInt(this.state.nDataAdvertisement) - 1,
                imgadvertisement: clsSetting.HomeUrl + "" + img,
            })
            this.openAdvertisementDialog();
        }
    }

    openCardDialog = (show) => {
        this.setState({ mycardvisible: show });
    }

    openCardAgentDialog = (show) => {
        this.setState({ mycardagentvisible: show });
    }

    openAdvertisementDialog = () => {
        this.setState({ advertisementvisible: true });
    }

    closeAdvertisementDialog = () => {
        this.setState({ advertisementvisible: false });
        this.fn_Advertisement();
    }

    render() {
        const gotoSearch = () => {
            this.props.navigation.navigate('Search', {Location: this.state.location, Calendar: this.state.calendar,
                Session: this.state.sessionSelectedItem, Player: this.state.playerSelectedItem, SessionSelected: this.state.session, 
                PlayerSelected: this.state.player});
        }

        const gotoQCard = () => {
            this.props.navigation.navigate('QCard')
        }

        const gotoPoint = () => {
            this.props.navigation.navigate('Point')
        }

        const gotoTopUp = () => {
            //Coming Soon
            Alert.alert(
                "QAccess Alert",
                "Coming Soon.",
                [
                    { text: 'YES', onPress: () => {console.log('press')}},
                ],
                { cancelable: false }
            )
        }

        const gotoTopUpSetting = () => {
            //Coming Soon
            Alert.alert(
                "QAccess Alert",
                "Coming Soon.",
                [
                    { text: 'YES', onPress: () => {console.log('press')}},
                ],
                { cancelable: false }
            )
        }

        const gotoHotel = () => {
            //Coming Soon
            Alert.alert(
                "QAccess Alert",
                "Coming Soon.",
                [
                    { text: 'YES', onPress: () => {console.log('press')}},
                ],
                { cancelable: false }
            )
        }

        const gotoFlight = () => {
            //Coming Soon
            Alert.alert(
                "QAccess Alert",
                "Coming Soon.",
                [
                    { text: 'YES', onPress: () => {console.log('press')}},
                ],
                { cancelable: false }
            )
        }

        const gotoAccountBalance = () => {
            this.props.navigation.navigate('AccountBalanceDetail', {Point: this.state.point})
        }

        const gotoCard = () => {
            if(this.state.usertype == TipePemainIntEnum.Visitor || this.state.usertype == TipePemainIntEnum.Publish || 
                this.state.usertype == TipePemainIntEnum.Agent){
                this.openCardAgentDialog(true);
            }
            else
            {
                this.openCardDialog(true);
            }
        }

        const gotoBillsAndTopUp = () => {
            this.props.navigation.navigate('BillAndTopUp')
        }

        const gotoInviteFriends = () => {
            this.props.navigation.navigate('InviteFriends')
        }

        const gotoPartners = () => {
            this.props.navigation.navigate('OurPartner')
        }

        const gotoCourses = () => {
            this.props.navigation.navigate('Courses', {Location: this.state.location, Calendar: this.state.calendar, 
                Session: this.state.sessionSelectedItem, SessionSelected: this.state.session,
                Player: this.state.playerSelectedItem, PlayerSelected: this.state.player, onSelect: this.onSelect})
        }

        const gotoEvent = () => {
            this.props.navigation.navigate('ListEvent')
        }

        if (this.state.isLoading) {
            return (
                <View style={{flex: 1, paddingTop: 20}}>
                    <ActivityIndicator />
                </View>
            );
        }

        return (
            <SafeAreaView style={styles.safeAreaView}>
                <View style={{height: 60, backgroundColor: '#fff', flexDirection: 'row', padding: 15, marginTop: 10, justifyContent: 'center', alignItems: 'center'}}>
                    <View style={{flex: 1, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', padding: 5,}}>
                        <Image style={{height: 20, resizeMode: 'contain', width: 100, }} source={require('../assets/images/logo2.png')}/>
                    </View>
                    <View style={{width: 100, height: 35, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', borderRadius: 5, borderWidth: 1, borderColor: Colors.button, }}>
                        <TouchableOpacity onPress={ gotoCard } 
                                style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center', padding: 10,}}>
                            <Image style={styles.imageMyCard} 
                                source={require('../assets/images/card.png')}/>
                            <Text style={styles.text}>{Strings.Home.text1}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <ScrollView 
                    style={styles.containerScroll}
                    refreshControl={this._refreshControl()}>
                    <Card
                        style={styles.cardPoint}>
                        <View style={{flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center', }}>
                            <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', }}>
                                <Text style={styles.textPointLeft}>{Strings.Home.text16}</Text>
                                <Text style={styles.textPointRight}>{(this.state.point / clsSetting.AccountBalance)}</Text>
                            </View>
                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginTop: 10,}}>
                                <View style={{flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}>
                                    <TouchableOpacity onPress={ gotoAccountBalance } 
                                        style={{flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center',}}>
                                        <Avatar rounded size="xlarge" activeOpacity={0.7} overlayContainerStyle={{backgroundColor: '#93b843'}} source={require('../assets/images/pointhistory.png')}/>
                                        <Text style={styles.textPoint} >{Strings.Home.text10}</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={{flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}>
                                    <TouchableOpacity onPress={ gotoTopUp } 
                                        style={{flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center',}}>
                                        <Avatar rounded size="xlarge" activeOpacity={0.7} overlayContainerStyle={{backgroundColor: '#93b843'}} source={require('../assets/images/pointtopup.png')}/>
                                        <Text style={styles.textPoint} >{Strings.Home.text11}</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={{flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}>
                                    <TouchableOpacity onPress={ gotoTopUpSetting } 
                                        style={{flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center',}}>
                                        <Avatar rounded size="xlarge" activeOpacity={0.7} overlayContainerStyle={{backgroundColor: '#93b843'}} source={require('../assets/images/pointsetting.png')}/>
                                        <Text style={styles.textPoint} >{Strings.Home.text12}</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={{flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}>
                                    <TouchableOpacity onPress={ gotoPoint } 
                                        style={{flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center',}}>
                                        <Avatar rounded size="xlarge" activeOpacity={0.7} overlayContainerStyle={{backgroundColor: '#93b843'}} source={require('../assets/images/pointhelp.png')}/>
                                        <Text style={styles.textPoint} >{Strings.Home.text9}</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </Card>
                    <Card
                        style={styles.cardAndroid} >
                        <TouchableOpacity onPress={ gotoCourses }
                            style={{flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', }}>
                            <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', }}>
                                <View style={{width: 25, height: 25, justifyContent: 'center', alignItems: 'center'}}>
                                    <Image style={styles.image} 
                                        source={require('../assets/images/location.png')}/>
                                </View>
                                <View style={{flex: 1, flexDirection: 'column', }}>
                                    <Text style={styles.textLabel} >{Strings.Home.text2}</Text>
                                    <Text style={[styles.textField, {right: 10, }]} >{this.state.location}</Text>
                                </View>
                            </View>
                        </TouchableOpacity>
                    </Card>
                    <Card 
                        style={styles.cardAndroid} >
                        <TouchableOpacity onPress={ this._showDateTimePicker } 
                            style={{flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', }}>
                            <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', }}>
                                <View style={{width: 25, height: 25, justifyContent: 'center', alignItems: 'center'}}>
                                    <Image style={styles.image} 
                                        source={require('../assets/images/calendar.png')}/>
                                </View>
                                <View style={{flex: 1, flexDirection: 'column', }}>
                                    <Text style={styles.textLabel} >{Strings.Home.text3}</Text>
                                    <Text style={styles.textField} >{this.state.calendar}</Text>
                                </View>
                            </View>
                        </TouchableOpacity>
                    </Card>
                    <Card 
                        style={styles.cardAndroid} >
                        <TouchableOpacity onPress={() => this.setState({ sessionPicker: true }) } 
                            style={{flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', }}>
                            <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', }}>
                                <View style={{width: 25, height: 25, justifyContent: 'center', alignItems: 'center'}}>
                                    <Image style={styles.image} 
                                        source={require('../assets/images/session.png')}/>
                                </View>
                                <View style={{flex: 1, flexDirection: 'column', }}>
                                    <Text style={styles.textLabel} >{Strings.Home.text4}</Text>
                                    <Text style={styles.textField} >{this.state.sessionSelectedItem === undefined
                                        ? 'Morning' : `${
                                            this.state.sessionSelectedItem.label
                                        }`}</Text>
                                </View>
                            </View>
                        </TouchableOpacity>
                    </Card>
                    <Card 
                        style={styles.cardAndroid} >
                        <TouchableOpacity onPress={() => this.setState({ playerPicker: true }) } 
                            style={{flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', }}>
                            <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', }}>
                                <View style={{width: 25, height: 25, justifyContent: 'center', alignItems: 'center'}}>
                                    <Image style={styles.image} 
                                        source={require('../assets/images/golfer.png')}/>
                                </View>
                                <View style={{flex: 1, flexDirection: 'column', }}>
                                    <Text style={styles.textLabel} >{Strings.Home.text5}</Text>
                                    <Text style={styles.textField} >{this.state.playerSelectedItem === undefined
                                        ? '1' : `${
                                            this.state.playerSelectedItem.label
                                        }`}</Text>
                                </View>
                            </View>
                        </TouchableOpacity>
                    </Card>   
                    <View>
                        <Button 
                            onPress={ gotoSearch }
                            buttonStyle={{backgroundColor: Colors.button, borderRadius: 5, marginTop: 10, elevation: 2, height: 50, }}
                            title={Strings.Home.searchbutton}/>
                    </View>
                    <Card
                        style={styles.cardMenu}>
                        <View style={{flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center', }}>
                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginTop: 5,}}>
                                <View style={{flex: 3, flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}>
                                    <TouchableOpacity style={{justifyContent: 'center', alignItems: 'center'}}
                                        onPress={ gotoQCard }>
                                        <Image style={styles.imageCard} source={require('../assets/images/card.png')}/>
                                        <Text style={styles.textMenu} >{Strings.Home.text6}</Text>
                                    </TouchableOpacity>    
                                </View>
                                <View style={{flex: 3, flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}>
                                    <TouchableOpacity style={{justifyContent: 'center', alignItems: 'center'}} 
                                        onPress={ gotoBillsAndTopUp }>
                                        <Image style={styles.imageCard} source={require('../assets/images/bills.png')}/>
                                        <Text style={styles.textMenu} >{Strings.Home.text7}</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={{flex: 3, flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}>
                                    <TouchableOpacity style={{justifyContent: 'center', alignItems: 'center'}} 
                                        onPress={ gotoInviteFriends }>
                                        <Image style={styles.imageCard} source={require('../assets/images/invite.png')}/>
                                        <Text style={styles.textMenu} >{Strings.Home.text8}</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={{flex: 3, flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}>
                                    <TouchableOpacity style={{justifyContent: 'center', alignItems: 'center'}} 
                                        onPress={ gotoPartners }>
                                        <Image style={styles.imageCard} source={require('../assets/images/partner.png')}/>
                                        <Text style={styles.textMenu} >{Strings.Home.text13}</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </Card>
                    <Card
                        style={styles.cardMenu}>
                        <View style={{flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center', }}>
                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginTop: 5,}}>
                                <View style={{flex: 3, flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}>
                                </View>
                                <View style={{flex: 3, flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}>
                                    <TouchableOpacity style={{justifyContent: 'center', alignItems: 'center'}}
                                        onPress={ gotoFlight }>
                                        <Image style={styles.imageCard} source={require('../assets/images/flight.png')}/>
                                        <Text style={styles.textMenu} >{Strings.Home.text17}</Text>
                                    </TouchableOpacity>    
                                </View>
                                <View style={{flex: 3, flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}>
                                    <TouchableOpacity style={{justifyContent: 'center', alignItems: 'center'}} 
                                        onPress={ gotoHotel }>
                                        <Image style={styles.imageCard} source={require('../assets/images/hotel.png')}/>
                                        <Text style={styles.textMenu} >{Strings.Home.text18}</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={{flex: 3, flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}>
                                </View>
                            </View>
                        </View>
                    </Card>
                    {/* <Card
                        style={styles.cardMenu}>
                        <View style={{flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center', }}>
                            <View style={{flex: 1, flexDirection: 'row', marginTop: 5, justifyContent: 'center', alignItems: 'center',}}>
                                <View style={{flexDirection: 'column', justifyContent: 'center', alignItems: 'center', marginLeft: 20, marginRight: 10}}>
                                    <TouchableOpacity style={{justifyContent: 'center', alignItems: 'center'}}
                                        onPress={ gotoFlight }>
                                        <Image style={styles.imageCard} source={require('../assets/images/flight.png')}/>
                                        <Text style={styles.textMenu} >{Strings.Home.text17}</Text>
                                    </TouchableOpacity>    
                                </View>
                                <View style={{flexDirection: 'column', justifyContent: 'center', alignItems: 'center', marginLeft: 40}}>
                                    <TouchableOpacity style={{justifyContent: 'center', alignItems: 'center'}} 
                                        onPress={ gotoHotel }>
                                        <Image style={styles.imageCard} source={require('../assets/images/hotel.png')}/>
                                        <Text style={styles.textMenu} >{Strings.Home.text18}</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </Card> */}
                    <View style={{marginTop: 10,}}>
                        {
                            Utils.renderIf(this.state.lastsearchvisible == true,
                                <Text style={styles.textTitle} >{Strings.Home.text14}</Text>
                            )
                        }
                        {
                            Utils.renderIf(this.state.lastsearchvisible == true,
                                <View style={styles.cardList} >
                                    <View style={{flexDirection: 'row', flex: 1, }}>
                                        <View style={{width: 140, height: 100}}>
                                            <Image style={{width: 140, height: 100, resizeMode: 'cover', borderRadius: 2, }} source={{uri: this.state.lastsearchgambar,}}/>
                                        </View>
                                        <View style={{flex:1, flexDirection: 'column', marginLeft: 7, marginTop: 5, marginBottom: 5, marginRight: 7, justifyContent: 'center', alignItems: 'flex-start',}}>
                                            <Text style={{fontSize: 15, color: '#000', flex: 1, }}>{this.state.lastsearchcoursename}</Text>
                                            <Text style={{fontSize: 13, color: '#000', flex: 1, }}>{moment(Date.parse(this.state.lastsearchdate)).format('DD MMM YYYY')}</Text>
                                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
                                                <Text style={{fontSize: 13, color: '#000', width: 70, }}>{Enums.EventSessionEnum(this.state.lastsearchsession)}</Text>
                                                <Text style={{fontSize: 13, color: '#000', }}>{this.state.lastsearchplayer + ' Player'}</Text>
                                            </View>
                                        </View>
                                    </View>
                                </View>
                            )
                        }
                    </View>
                    <View style={{marginTop: 30, marginBottom: 30,}}>
                        <View style={{flex: 1, flexDirection: 'row', alignItems: 'center',}}>
                            <Text style={styles.textTitle} >{Strings.Home.text15}</Text>
                            <TouchableOpacity style={{flex: 1, justifyContent: 'flex-end', right: 15, top: 20, }} 
                                onPress={ gotoEvent }>
                                <Text style={styles.textButton} >{Strings.Home.seemorebutton}</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{height: 200, top: 20, bottom: 10, paddingLeft: 15,}}>
                            <ListView
                                horizontal={true}
                                dataSource={this.state.dataSource}
                                renderRow={(rowData) =>
                                    <TouchableOpacity 
                                        onPress={ this.GetItem.bind(this, rowData) } >
                                        <EventRowScreen
                                            imageUri={{uri: clsSetting.HomeUrl + rowData.Thumbnail}}
                                            eventName={rowData.Name}/>
                                    </TouchableOpacity>
                                }
                            />
                        </View>
                    </View>
                    <View style={{marginBottom: 20,}}>
                        <Carousel
                            ref={(c) => { this._carousel = c; }}
                            data={this.state.images}
                            renderItem={this._renderItem.bind(this)}
                            itemWidth={300}
                            sliderWidth={400}
                            layout={'default'}
                            backgroundColor={'#fff'}
                            firstItem={0}
                        />
                    </View>
                    <SinglePickerMaterialDialog
                        title={'Session'}
                        items={this.state.sessionArr.map((row, index) => ({ value: index, label: row }))}
                        visible={this.state.sessionPicker}
                        selectedItem={this.state.sessionSelectedItem}
                        onCancel={() => this.setState({ sessionPicker: false })}
                        onOk={result => {
                            this.setState({ sessionPicker: false });
                            this.setState({
                                sessionSelectedItem: result.selectedItem,
                                session: result.selectedItem.label,
                            });
                        }}
                    />
                    <SinglePickerMaterialDialog
                        title={'Player'}
                        items={this.state.playerArr.map((row, index) => ({ value: index, label: row }))}
                        visible={this.state.playerPicker}
                        selectedItem={this.state.playerSelectedItem}
                        onCancel={() => this.setState({ playerPicker: false })}
                        onOk={result => {
                            this.setState({ playerPicker: false });
                            this.setState({
                                playerSelectedItem: result.selectedItem,
                                player: result.selectedItem.label,
                            });
                        }}
                    />
                    <DateTimePicker
                        isVisible={this.state.isDateTimePickerVisible}
                        onConfirm={this._handleDatePicked}
                        onCancel={this._hideDateTimePicker}
                    />
                    <Dialog
                        title=""
                        animationType="fade"
                        contentStyle={
                            {
                                alignItems: "center",
                                justifyContent: "center",
                            }
                        }
                        dialogStyle={{backgroundColor: 'transparent', borderRadius: 0, elevation: 0,}}
                        onTouchOutside={ () => this.openCardDialog(false) }
                        visible={ this.state.mycardvisible }
                    >
                        <View>
                            <View style={{backgroundColor: '#efefef', marginLeft: 5, marginRight: 5, 
                                marginTop: 10, marginBottom: 5, borderRadius: 5}}>
                                <Image source={{uri: this.state.mycardimgcard }} 
                                    style={{height: 180, width: imgCardWidth, resizeMode: 'stretch', }}/>
                                <View style={styles.containerHover}>
                                    <View style={{flex: 1, flexDirection:'row', marginTop: 5, marginRight: 10, }}>
                                        <Image style={{width: 70, height: 70, borderRadius: 35, borderWidth: 1, 
                                            borderColor: '#414141'}} source={{uri: this.state.mycardimgprofile }}/>
                                    </View>
                                    <View style={{flex: 1, marginTop: 5, marginRight: 10, }}>
                                        <Text style={{fontSize: 14, color: '#fff',}}>{this.state.mycardname}</Text>
                                    </View>
                                    <View style={{flex: 1, flexDirection: 'row', marginLeft: 10, marginRight: 10, marginTop: 5, }}>
                                        <Text style={styles.textLeftCard}>{'Expired Date' + "" + this.state.mycardexpired}</Text>
                                        <Text style={styles.textRightCard}>{this.state.mycardnumber}</Text>
                                    </View>
                                </View>
                            </View>
                            <View style={styles.containerHover2}>
                                <TouchableOpacity
                                    onPress={() => this.openCardDialog(false) }>
                                    <Image
                                        style={{height: 30, width: 30, tintColor: '#000'}}
                                        source={require('../assets/images/icon_close.png')}
                                    />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </Dialog>
                    <Dialog
                        title=""
                        animationType="fade"
                        contentStyle={
                            {
                                alignItems: "center",
                                justifyContent: "center",
                            }
                        }
                        dialogStyle={{backgroundColor: 'transparent', borderRadius: 0, elevation: 0,}}
                        onTouchOutside={ () => this.openCardAgentDialog(false) }
                        visible={ this.state.mycardagentvisible }
                    >
                        <View>
                            <View style={{marginLeft: 5, marginRight: 5, 
                                marginTop: 10, marginBottom: 5, borderRadius: 5}}>
                                <Image source={Utils.getImageMyCard(this.state.usertype)} 
                                    style={{height: 180, width: imgCardWidth, resizeMode: 'stretch', }}/>
                                <View style={styles.containerHover}>
                                    <View style={{flex: 1, flexDirection: 'row', marginLeft: 35, marginRight: 10, marginTop: 25, }}>
                                        <Text style={styles.textLeftCard}>{this.state.mycardname}</Text>
                                    </View>
                                </View>
                            </View>
                            <View style={styles.containerHover2}>
                                <TouchableOpacity
                                    onPress={() => this.openCardAgentDialog(false) }>
                                    <Image
                                        style={{height: 30, width: 30, tintColor: '#000'}}
                                        source={require('../assets/images/icon_close.png')}
                                    />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </Dialog>
                    <Dialog
                        title=""
                        animationType="fade"
                        contentStyle={
                            {
                                alignItems: "center",
                                justifyContent: "center",
                            }
                        }
                        dialogStyle={{backgroundColor: 'transparent', borderRadius: 0, elevation: 0,}}
                        onTouchOutside={ () => this.closeAdvertisementDialog() }
                        visible={ this.state.advertisementvisible }
                    >
                        <View>
                            <Image
                                source={{uri: this.state.imgadvertisement,}}
                                style={ {
                                    height: 400, 
                                    width: 300,
                                    backgroundColor: "transparent",
                                    padding: 1,
                                    resizeMode: "contain",
                                }}
                            />
                            <View style={styles.containerHover2}>
                                <TouchableOpacity
                                    onPress={() => this.closeAdvertisementDialog() }>
                                    <Image
                                        style={{height: 30, width: 30, tintColor: '#000'}}
                                        source={require('../assets/images/icon_close.png')}
                                    />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </Dialog>
                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({

    safeAreaView: {
        flex: 1,
        top: 15,
    },

    containerScroll:
    {
        flex: 1,
        backgroundColor: '#fff',
    },

    image: {
        width: 25,
        height: 25,
        tintColor: '#000000'
    },

    imageMyCard: {
        width: 25,
        height: 25,
    },

    imageCard: {
        width: 30,
        height: 30,
        tintColor: '#93b843'
    },

    imageCardPoint: {
        width: 50,
        height: 50,
        borderRadius: 10,
        backgroundColor: '#fff',
    },

    textLabel: {
        fontSize: 13,
        left: 10,
        color: Colors.text,
    },

    text: {
        fontSize: 13,
        left: 5,
        color: Colors.text,
    },

    textLeftCard: {
        flex: 1,
        fontSize: 14,
        color: '#fff',
    },

    textRightCard: {
        flex: 1,
        fontSize: 14,
        color: '#fff',
        textAlign: 'right',
    },

    textField: {
        fontSize: 16,
        left: 10,
        color: Colors.text,
    },

    textTitle: {
        fontSize: 20,
        left: 15,
        top: 15,
        color: Colors.text,
        fontWeight: 'bold',
    },

    textButton: {
        flex: 1,
        fontSize: 15,
        textAlign: 'right',
        color: Colors.button,
    },

    textMenu: {
        fontSize: 12,
        marginTop: 10,
        color: Colors.button,
    },

    textPoint: {
        fontSize: 15,
        marginTop: 10,
        color: '#fff',
    },

    textPointLeft: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#fff',
        padding: 5,
        textAlign: 'center',
    },

    textPointRight: {
        flex: 1,
        fontSize: 18,
        fontWeight: 'bold',
        color: '#fff',
        textAlign: 'right',
    },

    cardPoint: {
        backgroundColor: '#93b843',
        flex: 1,
        marginLeft: 15,
        marginRight: 15,
        marginTop: 5,
        paddingTop: 7,
        paddingBottom: 7,
        paddingLeft: 7,
        paddingRight: 10,
        elevation: 2,
        borderRadius: 5,
    },

    cardMenu: {
        backgroundColor: '#fff',
        flex: 1,
        marginLeft: 15,
        marginRight: 15,
        marginTop: 15,
        padding: 10,
        elevation: 2,
        borderRadius: 5,
    },

    cardAndroid: {
        backgroundColor: Colors.backgroundColor,
        flex: 1,
        marginLeft: 15,
        marginRight: 15,
        marginTop: 5,
        padding: 10,
        elevation: 2,
        borderRadius: 5,
    },

    cardList: {
        backgroundColor: '#fff',
        height: 100,
        marginTop: 20,
        marginLeft: 15,
        marginRight: 15,
        elevation: 2,
        borderRadius: 5,
    },

    containerHover: {
        position: 'absolute',
        top: 55,
        left: 0,
        right: 0, 
        flexDirection: 'column',
        alignItems: 'flex-end',
    },

    containerHover2: {
        position: 'absolute',
        top: 5,
        left: 0,
        right: 0, 
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'flex-end',
    },
});

const HomeCardView = styled.View`
  justify-content: center;
  align-items: center;
  background-color: #fff;
`;

const CurrentVideoImage = styled.Image`
  top: 5;
  box-shadow: 5px 10px;
  width: 300;
  height: 200;
  border-radius: 10;
`;

const CurrentVideoTO = styled.TouchableOpacity``