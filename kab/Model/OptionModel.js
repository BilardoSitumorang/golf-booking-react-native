import clsSetting from "../API/clsSetting";

export default class OptionModel
{
    constructor(_id, _name, _value, _note, _fromdate, _todate)
    {
        this.Id = _id;
        this.Name = _name;
        this.Value = _value;
        this.Note = _note;
        this.FromDate = _fromdate;
        this.ToDate = _todate;
    }
}